webpackJsonp([21],{

/***/ 101:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DireitosProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_http__ = __webpack_require__(189);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


//import 'rxjs/add/operator/map';
var DireitosProvider = /** @class */ (function () {
    function DireitosProvider(http) {
        this.http = http;
        this.direitos = [
            { nome: 'Política Nacional do Idoso', code: 1, lei: "(Lei nº. 8.842, de 4 de janeiro de 1994)", descricao: '<div style="font-size: 1.5em; font-weight: bold">Presidência da República<br>Casa Civil<br></div>Subchefia para Assuntos Jurídicos<br><br>LEI Nº 8.842, DE 4 DE JANEIRO DE 1994.<br>Dispõe sobre a política nacional do idoso, cria o Conselho Nacional do Idoso e dá outras providências..<br>O PRESIDENTE DA REPÚBLICA Faço saber que o Congresso Nacional decreta e eu sanciono a seguinte lei:<br>CAPÍTULO I<br>Da Finalidade<br><br>Art. 1º A política nacional do idoso tem por objetivo assegurar os direitos sociais do idoso, criando condições para promover sua autonomia, integração e participação efetiva na sociedade.<br><br>Art. 2º Considera-se idoso, para os efeitos desta lei, a pessoa maior de sessenta anos de idade.<br><br>CAPÍTULO II<br>Dos Princípios e das Diretrizes<br><br>SEÇÃO I<br>Dos Princípios<br><br>Art. 3° A política nacional do idoso reger-se-á pelos seguintes princípios:<br><br>I - a família, a sociedade e o estado têm o dever de assegurar ao idoso todos os direitos da cidadania, garantindo sua participação na comunidade, defendendo sua dignidade, bem-estar e o direito à vida;<br>II - o processo de envelhecimento diz respeito à sociedade em geral, devendo ser objeto de conhecimento e informação para todos;<br>III - o idoso não deve sofrer discriminação de qualquer natureza;<br>IV - o idoso deve ser o principal agente e o destinatário das transformações a serem efetivadas através desta política;<br>V - as diferenças econômicas, sociais, regionais e, particularmente, as contradições entre o meio rural e o urbano do Brasil deverão ser observadas pelos poderes públicos e pela sociedade em geral, na aplicação desta lei.<br><br>SEÇÃO II<br>Das Diretrizes<br><br>Art. 4º Constituem diretrizes da política nacional do idoso:<br><br>I - viabilização de formas alternativas de participação, ocupação e convívio do idoso, que proporcionem sua integração às demais gerações;<br>II - participação do idoso, através de suas organizações representativas, na formulação, implementação e avaliação das políticas, planos, programas e projetos a serem desenvolvidos;<br>III - priorização do atendimento ao idoso através de suas próprias famílias, em detrimento do atendimento asilar, à exceção dos idosos que não possuam condições que garantam sua própria sobrevivência;<br>IV - descentralização político-administrativa;<br>V - capacitação e reciclagem dos recursos humanos nas áreas de geriatria e gerontologia e na prestação de serviços;<br>VI - implementação de sistema de informações que permita a divulgação da política, dos serviços oferecidos, dos planos, programas e projetos em cada nível de governo;<br>VII - estabelecimento de mecanismos que favoreçam a divulgação de informações de caráter educativo sobre os aspectos biopsicossociais do envelhecimento;<br>VIII - priorização do atendimento ao idoso em órgãos públicos e privados prestadores de serviços, quando desabrigados e sem família;<br>IX - apoio a estudos e pesquisas sobre as questões relativas ao envelhecimento.<br>Parágrafo único. É vedada a permanência de portadores de doenças que necessitem de assistência médica ou de enfermagem permanente em instituições asilares de caráter social.<br><br>CAPÍTULO III<br>Da Organização e Gestão<br><br>Art. 5º Competirá ao órgão ministerial responsável pela assistência e promoção social a coordenação geral da política nacional do idoso, com a participação dos conselhos nacionais, estaduais, do Distrito Federal e municipais do idoso.<br><br>Art. 6º Os conselhos nacional, estaduais, do Distrito Federal e municipais do idoso serão órgãos permanentes, paritários e deliberativos, compostos por igual número de representantes dos órgãos e entidades públicas e de organizações representativas da sociedade civil ligadas à área.<br><br>Art. 7º Compete aos conselhos de que trata o artigo anterior a formulação, coordenação, supervisão e avaliação da política nacional do idoso, no âmbito das respectivas instâncias político-administrativas.<br><br>Art. 7o Compete aos Conselhos de que trata o art. 6o desta Lei a supervisão, o acompanhamento, a fiscalização e a avaliação da política nacional do idoso, no âmbito das respectivas instâncias político-administrativas. (Redação dada pelo Lei nº 10.741, de 2003) <br><br>Art. 8º À União, por intermédio do ministério responsável pela assistência e promoção social, compete:<br><br>I - coordenar as ações relativas à política nacional do idoso;<br><br>II - participar na formulação, acompanhamento e avaliação da política nacional do idoso;<br><br>III - promover as articulações intraministeriais e interministeriais necessárias à implementação da política nacional do idoso;<br><br>IV - (Vetado;)<br><br>V - elaborar a proposta orçamentária no âmbito da promoção e assistência social e submetê-la ao Conselho Nacional do Idoso.<br><br>Parágrafo único. Os ministérios das áreas de saúde, educação, trabalho, previdência social, cultura, esporte e lazer devem elaborar proposta orçamentária, no âmbito de suas competências, visando ao financiamento de programas nacionais compatíveis com a política nacional do idoso.<br><br>Art. 9º (Vetado.)<br><br>Parágrafo único. (Vetado.)<br><br>CAPÍTULO IV<br>Das Ações Governamentais<br><br>Art. 10. Na implementação da política nacional do idoso, são competências dos órgãos e entidades públicos:<br><br>I - na área de promoção e assistência social:<br><br>a) prestar serviços e desenvolver ações voltadas para o atendimento das necessidades básicas do idoso, mediante a participação das famílias, da sociedade e de entidades governamentais e não-governamentais.<br><br>b) estimular a criação de incentivos e de alternativas de atendimento ao idoso, como centros de convivência, centros de cuidados diurnos, casas-lares, oficinas abrigadas de trabalho, atendimentos domiciliares e outros;<br><br>c) promover simpósios, seminários e encontros específicos;<br><br>d) planejar, coordenar, supervisionar e financiar estudos, levantamentos, pesquisas e publicações sobre a situação social do idoso;<br><br>e) promover a capacitação de recursos para atendimento ao idoso;<br><br>II - na área de saúde:<br><br>a) garantir ao idoso a assistência à saúde, nos diversos níveis de atendimento do Sistema Único de Saúde;<br><br>b) prevenir, promover, proteger e recuperar a saúde do idoso, mediante programas e medidas profiláticas;<br><br>c) adotar e aplicar normas de funcionamento às instituições geriátricas e similares, com fiscalização pelos gestores do Sistema Único de Saúde;<br><br>d) elaborar normas de serviços geriátricos hospitalares;<br><br>e) desenvolver formas de cooperação entre as Secretarias de Saúde dos Estados, do Distrito Federal, e dos Municípios e entre os Centros de Referência em Geriatria e Gerontologia para treinamento de equipes interprofissionais;<br><br>f) incluir a Geriatria como especialidade clínica, para efeito de concursos públicos federais, estaduais, do Distrito Federal e municipais;<br><br>g) realizar estudos para detectar o caráter epidemiológico de determinadas doenças do idoso, com vistas a prevenção, tratamento e reabilitação; e<br>h) criar serviços alternativos de saúde para o idoso;<br><br>III - na área de educação:<br><br>a) adequar currículos, metodologias e material didático aos programas educacionais destinados ao idoso;<br><br>b) inserir nos currículos mínimos, nos diversos níveis do ensino formal, conteúdos voltados para o processo de envelhecimento, de forma a eliminar preconceitos e a produzir conhecimentos sobre o assunto;<br><br>c) incluir a Gerontologia e a Geriatria como disciplinas curriculares nos cursos superiores;<br><br>d) desenvolver programas educativos, especialmente nos meios de comunicação, a fim de informar a população sobre o processo de envelhecimento;<br><br>e) desenvolver programas que adotem modalidades de ensino à distância, adequados às condições do idoso;<br><br>f) apoiar a criação de universidade aberta para a terceira idade, como meio de universalizar o acesso às diferentes formas do saber;<br><br>IV - na área de trabalho e previdência social:<br><br>a) garantir mecanismos que impeçam a discriminação do idoso quanto a sua participação no mercado de trabalho, no setor público e privado;<br><br>b) priorizar o atendimento do idoso nos benefícios previdenciários;<br><br>c) criar e estimular a manutenção de programas de preparação para aposentadoria nos setores público e privado com antecedência mínima de dois anos antes do afastamento;<br><br>V - na área de habitação e urbanismo:<br><br>a) destinar, nos programas habitacionais, unidades em regime de comodato ao idoso, na modalidade de casas-lares;<br><br>b) incluir nos programas de assistência ao idoso formas de melhoria de condições de habitabilidade e adaptação de moradia, considerando seu estado físico e sua independência de locomoção;<br><br>c) elaborar critérios que garantam o acesso da pessoa idosa à habitação popular;<br><br>d) diminuir barreiras arquitetônicas e urbanas;<br><br>VI - na área de justiça:<br><br>a) promover e defender os direitos da pessoa idosa;<br><br>b) zelar pela aplicação das normas sobre o idoso determinando ações para evitar abusos e lesões a seus direitos;<br><br>VII - na área de cultura, esporte e lazer:<br><br>a) garantir ao idoso a participação no processo de produção, reelaboração e fruição dos bens culturais;<br><br>b) propiciar ao idoso o acesso aos locais e eventos culturais, mediante preços reduzidos, em âmbito nacional;<br><br>c) incentivar os movimentos de idosos a desenvolver atividades culturais;<br><br>d) valorizar o registro da memória e a transmissão de informações e habilidades do idoso aos mais jovens, como meio de garantir a continuidade e a identidade cultural;<br><br>e) incentivar e criar programas de lazer, esporte e atividades físicas que proporcionem a melhoria da qualidade de vida do idoso e estimulem sua participação na comunidade.<br><br>§ 1º É assegurado ao idoso o direito de dispor de seus bens, proventos, pensões e benefícios, salvo nos casos de incapacidade judicialmente comprovada.<br><br>§ 2º Nos casos de comprovada incapacidade do idoso para gerir seus bens, ser-lhe-á nomeado Curador especial em juízo.<br><br>§ 3º Todo cidadão tem o dever de denunciar à autoridade competente qualquer forma de negligência ou desrespeito ao idoso.<br><br>CAPÍTULO V<br><br>Do Conselho Nacional<br><br>Art. 11. (Vetado.)<br><br>Art. 12. (Vetado.)<br><br>Art. 13. (Vetado.)<br><br>Art. 14. (Vetado.)<br><br>Art. 15. (Vetado.)<br><br>Art. 16. (Vetado.)<br><br>Art. 17. (Vetado.)<br><br>Art. 18. (Vetado.)<br><br>CAPÍTULO VI<br><br>Das Disposições Gerais<br><br>Art. 19. Os recursos financeiros necessários à implantação das ações afetas às áreas de competência dos governos federal, estaduais, do Distrito Federal e municipais serão consignados em seus respectivos orçamentos.<br><br>Art. 20. O Poder Executivo regulamentará esta lei no prazo de sessenta dias, a partir da data de sua publicação.<br><br>Art. 21. Esta lei entra em vigor na data de sua publicação.<br><br>Art. 22. Revogam-se as disposições em contrário.<br><br>Brasília, 4 de janeiro de 1994, 173º da Independência e 106º da República.<br><br>ITAMAR FRANCO<br><br>Leonor Barreto Franco<br><br>Este texto não substitui o publicado no D.O.U. de 5.1.1994' },
            { nome: 'Estatuto do Idoso', lei: "Lei nº. 10.741, de 1 de outubro de 2003<br>", code: 2, descricao: '<div style="font-size: 1.5em; font-weight: bold">Presidência da República<br>Casa Civil</div><br><br>Subchefia para Assuntos Jurídicos<br><br>LEI No 10.741, DE 1º DE OUTUBRO DE 2003.<br><br>Vigência<br><br>(Vide Decreto nº 6.214, de 2007)<br><br>Dispõe sobre o Estatuto do Idoso e dá outras providências.<br><br>O PRESIDENTE DA REPÚBLICA Faço saber que o Congresso Nacional decreta e eu sanciono a seguinte Lei:<br><br>TÍTULO I<br><br>Disposições Preliminares<br><br>Art. 1o É instituído o Estatuto do Idoso, destinado a regular os direitos assegurados às pessoas com idade igual ou superior a 60 (sessenta) anos.<br><br>Art. 2o O idoso goza de todos os direitos fundamentais inerentes à pessoa humana, sem prejuízo da proteção integral de que trata esta Lei, assegurando-se-lhe, por lei ou por outros meios, todas as oportunidades e facilidades, para preservação de sua saúde física e mental e seu aperfeiçoamento moral, intelectual, espiritual e social, em condições de liberdade e dignidade.<br><br>Art. 3o É obrigação da família, da comunidade, da sociedade e do Poder Público assegurar ao idoso, com absoluta prioridade, a efetivação do direito à vida, à saúde, à alimentação, à educação, à cultura, ao esporte, ao lazer, ao trabalho, à cidadania, à liberdade, à dignidade, ao respeito e à convivência familiar e comunitária.<br><br>§ 1º A garantia de prioridade compreende:(Redação dada pela Lei nº 13.466, de 2017)<br><br>I – atendimento preferencial imediato e individualizado junto aos órgãos públicos e privados prestadores de serviços à população;<br><br>II – preferência na formulação e na execução de políticas sociais públicas específicas;<br><br>III – destinação privilegiada de recursos públicos nas áreas relacionadas com a proteção ao idoso;<br><br>IV – viabilização de formas alternativas de participação, ocupação e convívio do idoso com as demais gerações;<br><br>V – priorização do atendimento do idoso por sua própria família, em detrimento do atendimento asilar, exceto dos que não a possuam ou careçam de condições de manutenção da própria sobrevivência;<br><br>VI – capacitação e reciclagem dos recursos humanos nas áreas de geriatria e gerontologia e na prestação de serviços aos idosos;<br><br>VII – estabelecimento de mecanismos que favoreçam a divulgação de informações de caráter educativo sobre os aspectos biopsicossociais de envelhecimento;<br><br>VIII – garantia de acesso à rede de serviços de saúde e de assistência social locais.<br><br>IX – prioridade no recebimento da restituição do Imposto de Renda. (Incluído pela Lei nº 11.765, de 2008).<br><br>§ 2ºDentre os idosos, é assegurada prioridade especial aos maiores de oitenta anos, atendendo-se suas necessidades sempre preferencialmente em relação aos demais idosos.(Incluído pela Lei nº 13.466, de 2017)<br><br>Art. 4o Nenhum idoso será objeto de qualquer tipo de negligência, discriminação, violência, crueldade ou opressão, e todo atentado aos seus direitos, por ação ou omissão, será punido na forma da lei.<br><br>§ 1o É dever de todos prevenir a ameaça ou violação aos direitos do idoso.<br><br>§ 2o As obrigações previstas nesta Lei não excluem da prevenção outras decorrentes dos princípios por ela adotados.<br><br>Art. 5o A inobservância das normas de prevenção importará em responsabilidade à pessoa física ou jurídica nos termos da lei.<br><br>Art. 6o Todo cidadão tem o dever de comunicar à autoridade competente qualquer forma de violação a esta Lei que tenha testemunhado ou de que tenha conhecimento.<br><br>Art. 7o Os Conselhos Nacional, Estaduais, do Distrito Federal e Municipais do Idoso, previstos na Lei no 8.842, de 4 de janeiro de 1994, zelarão pelo cumprimento dos direitos do idoso, definidos nesta Lei.<br><br>TÍTULO II<br><br>Dos Direitos Fundamentais<br><br>CAPÍTULO I<br><br>Do Direito à Vida<br><br>Art. 8o O envelhecimento é um direito personalíssimo e a sua proteção um direito social, nos termos desta Lei e da legislação vigente.<br><br>Art. 9o É obrigação do Estado, garantir à pessoa idosa a proteção à vida e à saúde, mediante efetivação de políticas sociais públicas que permitam um envelhecimento saudável e em condições de dignidade.<br><br>CAPÍTULO II<br><br>Do Direito à Liberdade, ao Respeito e à Dignidade<br><br>Art. 10. É obrigação do Estado e da sociedade, assegurar à pessoa idosa a liberdade, o respeito e a dignidade, como pessoa humana e sujeito de direitos civis, políticos, individuais e sociais, garantidos na Constituição e nas leis.<br><br>§ 1o O direito à liberdade compreende, entre outros, os seguintes aspectos:<br><br>I – faculdade de ir, vir e estar nos logradouros públicos e espaços comunitários, ressalvadas as restrições legais;<br><br>II – opinião e expressão;<br><br>III – crença e culto religioso;<br><br>IV – prática de esportes e de diversões;<br><br>V – participação na vida familiar e comunitária;<br><br>VI – participação na vida política, na forma da lei;<br><br>VII – faculdade de buscar refúgio, auxílio e orientação.<br><br>§ 2o O direito ao respeito consiste na inviolabilidade da integridade física, psíquica e moral, abrangendo a preservação da imagem, da identidade, da autonomia, de valores, idéias e crenças, dos espaços e dos objetos pessoais.<br><br>§ 3o É dever de todos zelar pela dignidade do idoso, colocando-o a salvo de qualquer tratamento desumano, violento, aterrorizante, vexatório ou constrangedor.<br><br>CAPÍTULO III<br><br>Dos Alimentos<br><br>Art. 11. Os alimentos serão prestados ao idoso na forma da lei civil.<br><br>Art. 12. A obrigação alimentar é solidária, podendo o idoso optar entre os prestadores.<br><br>Art. 13.As transações relativas a alimentos poderão ser celebradas perante o Promotor de Justiça ou Defensor Público, que as referendará, e passarão a ter efeito de título executivo extrajudicial nos termos da lei processual civil. (Redação dada pela Lei nº 11.737, de 2008)<br><br>Art. 14. Se o idoso ou seus familiares não possuírem condições econômicas de prover o seu sustento, impõe-se ao Poder Público esse provimento, no âmbito da assistência social.<br><br>CAPÍTULO IV<br><br>Do Direito à Saúde<br><br>Art. 15. É assegurada a atenção integral à saúde do idoso, por intermédio do Sistema Único de Saúde – SUS, garantindo-lhe o acesso universal e igualitário, em conjunto articulado e contínuo das ações e serviços, para a prevenção, promoção, proteção e recuperação da saúde, incluindo a atenção especial às doenças que afetam preferencialmente os idosos.<br><br>§ 1o A prevenção e a manutenção da saúde do idoso serão efetivadas por meio de:<br><br>I – cadastramento da população idosa em base territorial;<br><br>II – atendimento geriátrico e gerontológico em ambulatórios;<br><br>III – unidades geriátricas de referência, com pessoal especializado nas áreas de geriatria e gerontologia social;<br><br>IV – atendimento domiciliar, incluindo a internação, para a população que dele necessitar e esteja impossibilitada de se locomover, inclusive para idosos abrigados e acolhidos por instituições públicas, filantrópicas ou sem fins lucrativos e eventualmente conveniadas com o Poder Público, nos meios urbano e rural;<br><br>V – reabilitação orientada pela geriatria e gerontologia, para redução das seqüelas decorrentes do agravo da saúde.<br><br>§ 2o Incumbe ao Poder Público fornecer aos idosos, gratuitamente, medicamentos, especialmente os de uso continuado, assim como próteses, órteses e outros recursos relativos ao tratamento, habilitação ou reabilitação.<br><br>§ 3o É vedada a discriminação do idoso nos planos de saúde pela cobrança de valores diferenciados em razão da idade.<br><br>§ 4o Os idosos portadores de deficiência ou com limitação incapacitante terão atendimento especializado, nos termos da lei.<br><br>§ 5o É vedado exigir o comparecimento do idoso enfermo perante os órgãos públicos, hipótese na qual será admitido o seguinte procedimento: (Incluído pela Lei nº 12.896, de 2013)<br><br>I - quando de interesse do poder público, o agente promoverá o contato necessário com o idoso em sua residência; ou(Incluído pela Lei nº 12.896, de 2013)<br><br>II - quando de interesse do próprio idoso, este se fará representar por procurador legalmente constituído.(Incluído pela Lei nº 12.896, de 2013)<br><br>§ 6o É assegurado ao idoso enfermo o atendimento domiciliar pela perícia médica do Instituto Nacional do Seguro Social - INSS, pelo serviço público de saúde ou pelo serviço privado de saúde, contratado ou conveniado, que integre o Sistema Único de Saúde - SUS, para expedição do laudo de saúde necessário ao exercício de seus direitos sociais e de isenção tributária. (Incluído pela Lei nº 12.896, de 2013)<br><br>§ 7ºEm todo atendimento de saúde, os maiores de oitenta anos terão preferência especial sobre os demais idosos, exceto em caso de emergência. (Incluído pela Lei nº 13.466, de 2017).<br><br>Art. 16. Ao idoso internado ou em observação é assegurado o direito a acompanhante, devendo o órgão de saúde proporcionar as condições adequadas para a sua permanência em tempo integral, segundo o critério médico.<br><br>Parágrafo único. Caberá ao profissional de saúde responsável pelo tratamento conceder autorização para o acompanhamento do idoso ou, no caso de impossibilidade, justificá-la por escrito.<br><br>Art. 17. Ao idoso que esteja no domínio de suas faculdades mentais é assegurado o direito de optar pelo tratamento de saúde que lhe for reputado mais favorável.<br><br>Parágrafo único. Não estando o idoso em condições de proceder à opção, esta será feita:<br><br>I – pelo curador, quando o idoso for interditado;<br><br>II – pelos familiares, quando o idoso não tiver curador ou este não puder ser contactado em tempo hábil;<br><br>III – pelo médico, quando ocorrer iminente risco de vida e não houver tempo hábil para consulta a curador ou familiar;<br><br>IV – pelo próprio médico, quando não houver curador ou familiar conhecido, caso em que deverá comunicar o fato ao Ministério Público.<br><br>Art. 18. As instituições de saúde devem atender aos critérios mínimos para o atendimento às necessidades do idoso, promovendo o treinamento e a capacitação dos profissionais, assim como orientação a cuidadores familiares e grupos de auto-ajuda.<br><br>Art. 19.Os casos de suspeita ou confirmação de violência praticada contra idosos serão objeto de notificação compulsória pelos serviços de saúde públicos e privados à autoridade sanitária, bem como serão obrigatoriamente comunicados por eles a quaisquer dos seguintes órgãos: (Redação dada pela Lei nº 12.461, de 2011)<br><br>I – autoridade policial;<br><br>II – Ministério Público;<br><br>III – Conselho Municipal do Idoso;<br><br>IV – Conselho Estadual do Idoso;<br><br>V – Conselho Nacional do Idoso.<br><br>§ 1oPara os efeitos desta Lei, considera-se violência contra o idoso qualquer ação ou omissão praticada em local público ou privado que lhe cause morte, dano ou sofrimento físico ou psicológico. (Incluído pela Lei nº 12.461, de 2011)<br><br>§ 2oAplica-se, no que couber, à notificação compulsória prevista no caput deste artigo, o disposto na Lei no 6.259, de 30 de outubro de 1975. (Incluído pela Lei nº 12.461, de 2011)<br><br>CAPÍTULO V<br><br>Da Educação, Cultura, Esporte e Lazer<br><br>Art. 20. O idoso tem direito a educação, cultura, esporte, lazer, diversões, espetáculos, produtos e serviços que respeitem sua peculiar condição de idade.<br><br>Art. 21. O Poder Público criará oportunidades de acesso do idoso à educação, adequando currículos, metodologias e material didático aos programas educacionais a ele destinados.<br><br>§ 1o Os cursos especiais para idosos incluirão conteúdo relativo às técnicas de comunicação, computação e demais avanços tecnológicos, para sua integração à vida moderna.<br><br>§ 2o Os idosos participarão das comemorações de caráter cívico ou cultural, para transmissão de conhecimentos e vivências às demais gerações, no sentido da preservação da memória e da identidade culturais.<br><br>Art. 22. Nos currículos mínimos dos diversos níveis de ensino formal serão inseridos conteúdos voltados ao processo de envelhecimento, ao respeito e à valorização do idoso, de forma a eliminar o preconceito e a produzir conhecimentos sobre a matéria.<br><br>Art. 23. A participação dos idosos em atividades culturais e de lazer será proporcionada mediante descontos de pelo menos 50% (cinqüenta por cento) nos ingressos para eventos artísticos, culturais, esportivos e de lazer, bem como o acesso preferencial aos respectivos locais.<br><br>Art. 24. Os meios de comunicação manterão espaços ou horários especiais voltados aos idosos, com finalidade informativa, educativa, artística e cultural, e ao público sobre o processo de envelhecimento.<br><br>Art. 25.As instituições de educação superior ofertarão às pessoas idosas, na perspectiva da educação ao longo da vida, cursos e programas de extensão, presenciais ou a distância, constituídos por atividades formais e não formais.(Redação dada pela lei nº 13.535, de 2017)<br><br>Parágrafo único.O poder público apoiará a criação de universidade aberta para as pessoas idosas e incentivará a publicação de livros e periódicos, de conteúdo e padrão editorial adequados ao idoso, que facilitem a leitura, considerada a natural redução da capacidade visual.(Incluído pela lei nº 13.535, de 2017)<br><br>CAPÍTULO VI<br><br>Da Profissionalização e do Trabalho<br><br>Art. 26. O idoso tem direito ao exercício de atividade profissional, respeitadas suas condições físicas, intelectuais e psíquicas.<br><br>Art. 27. Na admissão do idoso em qualquer trabalho ou emprego, é vedada a discriminação e a fixação de limite máximo de idade, inclusive para concursos, ressalvados os casos em que a natureza do cargo o exigir.<br><br>Parágrafo único. O primeiro critério de desempate em concurso público será a idade, dando-se preferência ao de idade mais elevada.<br><br>Art. 28. O Poder Público criará e estimulará programas de:<br><br>I – profissionalização especializada para os idosos, aproveitando seus potenciais e habilidades para atividades regulares e remuneradas;<br><br>II – preparação dos trabalhadores para a aposentadoria, com antecedência mínima de 1 (um) ano, por meio de estímulo a novos projetos sociais, conforme seus interesses, e de esclarecimento sobre os direitos sociais e de cidadania;<br><br>III – estímulo às empresas privadas para admissão de idosos ao trabalho.<br><br>CAPÍTULO VII<br><br>Da Previdência Social<br><br>Art. 29. Os benefícios de aposentadoria e pensão do Regime Geral da Previdência Social observarão, na sua concessão, critérios de cálculo que preservem o valor real dos salários sobre os quais incidiram contribuição, nos termos da legislação vigente.<br><br>Parágrafo único. Os valores dos benefícios em manutenção serão reajustados na mesma data de reajuste do salário-mínimo, pro rata, de acordo com suas respectivas datas de início ou do seu último reajustamento, com base em percentual definido em regulamento, observados os critérios estabelecidos pela Lei no 8.213, de 24 de julho de 1991.<br><br>Art. 30. A perda da condição de segurado não será considerada para a concessão da aposentadoria por idade, desde que a pessoa conte com, no mínimo, o tempo de contribuição correspondente ao exigido para efeito de carência na data de requerimento do benefício.<br><br>Parágrafo único. O cálculo do valor do benefício previsto no caput observará o disposto no caput e § 2o do art. 3o da Lei no 9.876, de 26 de novembro de 1999, ou, não havendo salários-de-contribuição recolhidos a partir da competência de julho de 1994, o disposto no art. 35 da Lei no 8.213, de 1991.<br><br>Art. 31. O pagamento de parcelas relativas a benefícios, efetuado com atraso por responsabilidade da Previdência Social, será atualizado pelo mesmo índice utilizado para os reajustamentos dos benefícios do Regime Geral de Previdência Social, verificado no período compreendido entre o mês que deveria ter sido pago e o mês do efetivo pagamento.<br><br>Art. 32. O Dia Mundial do Trabalho, 1o de Maio, é a data-base dos aposentados e pensionistas.<br><br>CAPÍTULO VIII<br><br>Da Assistência Social<br><br>Art. 33. A assistência social aos idosos será prestada, de forma articulada, conforme os princípios e diretrizes previstos na Lei Orgânica da Assistência Social, na Política Nacional do Idoso, no Sistema Único de Saúde e demais normas pertinentes.<br><br>Art. 34. Aos idosos, a partir de 65 (sessenta e cinco) anos, que não possuam meios para prover sua subsistência, nem de tê-la provida por sua família, é assegurado o benefício mensal de 1 (um) salário-mínimo, nos termos da Lei Orgânica da Assistência Social – Loas. (Vide Decreto nº 6.214, de 2007)<br><br>Parágrafo único. O benefício já concedido a qualquer membro da família nos termos do caput não será computado para os fins do cálculo da renda familiar per capita a que se refere a Loas.<br><br>Art. 35. Todas as entidades de longa permanência, ou casa-lar, são obrigadas a firmar contrato de prestação de serviços com a pessoa idosa abrigada. <br><br>§ 1o No caso de entidades filantrópicas, ou casa-lar, é facultada a cobrança de participação do idoso no custeio da entidade.<br><br>§ 2o O Conselho Municipal do Idoso ou o Conselho Municipal da Assistência Social estabelecerá a forma de participação prevista no § 1o, que não poderá exceder a 70% (setenta por cento) de qualquer benefício previdenciário ou de assistência social percebido pelo idoso.<br><br>§ 3o Se a pessoa idosa for incapaz, caberá a seu representante legal firmar o contrato a que se refere o caput deste artigo.<br><br>Art. 36. O acolhimento de idosos em situação de risco social, por adulto ou núcleo familiar, caracteriza a dependência econômica, para os efeitos legais.(Vigência)<br><br>CAPÍTULO IX<br><br>Da Habitação<br><br>Art. 37. O idoso tem direito a moradia digna, no seio da família natural ou substituta, ou desacompanhado de seus familiares, quando assim o desejar, ou, ainda, em instituição pública ou privada.<br><br>§ 1o A assistência integral na modalidade de entidade de longa permanência será prestada quando verificada inexistência de grupo familiar, casa-lar, abandono ou carência de recursos financeiros próprios ou da família<br><br>§ 2o Toda instituição dedicada ao atendimento ao idoso fica obrigada a manter identificação externa visível, sob pena de interdição, além de atender toda a legislação pertinente.<br><br>§ 3o As instituições que abrigarem idosos são obrigadas a manter padrões de habitação compatíveis com as necessidades deles, bem como provê-los com alimentação regular e higiene indispensáveis às normas sanitárias e com estas condizentes, sob as penas da lei.<br><br>Art. 38. Nos programas habitacionais, públicos ou subsidiados com recursos públicos, o idoso goza de prioridade na aquisição de imóvel para moradia própria, observado o seguinte:<br><br>I - reserva de pelo menos 3% (três por cento) das unidades habitacionais residenciais para atendimento aos idosos;(Redação dada pela Lei nº 12.418, de 2011)<br><br>II – implantação de equipamentos urbanos comunitários voltados ao idoso;<br><br>III – eliminação de barreiras arquitetônicas e urbanísticas, para garantia de acessibilidade ao idoso;<br><br>IV – critérios de financiamento compatíveis com os rendimentos de aposentadoria e pensão.<br><br>Parágrafo único.As unidades residenciais reservadas para atendimento a idosos devem situar-se, preferencialmente, no pavimento térreo. (Incluído pela Lei nº 12.419, de 2011)<br><br>CAPÍTULO X<br><br>Do Transporte<br><br>Art. 39. Aos maiores de 65 (sessenta e cinco) anos fica assegurada a gratuidade dos transportes coletivos públicos urbanos e semi-urbanos, exceto nos serviços seletivos e especiais, quando prestados paralelamente aos serviços regulares.<br><br>§ 1o Para ter acesso à gratuidade, basta que o idoso apresente qualquer documento pessoal que faça prova de sua idade.<br><br>§ 2o Nos veículos de transporte coletivo de que trata este artigo, serão reservados 10% (dez por cento) dos assentos para os idosos, devidamente identificados com a placa de reservado preferencialmente para idosos.<br><br>§ 3o No caso das pessoas compreendidas na faixa etária entre 60 (sessenta) e 65 (sessenta e cinco) anos, ficará a critério da legislação local dispor sobre as condições para exercício da gratuidade nos meios de transporte previstos no caput deste artigo.<br><br>Art. 40. No sistema de transporte coletivo interestadual observar-se-á, nos termos da legislação específica:(Regulamento) (Vide Decreto nº 5.934, de 2006)<br><br>I – a reserva de 2 (duas) vagas gratuitas por veículo para idosos com renda igual ou inferior a 2 (dois) salários-mínimos;<br><br>II – desconto de 50% (cinqüenta por cento), no mínimo, no valor das passagens, para os idosos que excederem as vagas gratuitas, com renda igual ou inferior a 2 (dois) salários-mínimos.<br><br>Parágrafo único. Caberá aos órgãos competentes definir os mecanismos e os critérios para o exercício dos direitos previstos nos incisos I e II.<br><br>Art. 41. É assegurada a reserva, para os idosos, nos termos da lei local, de 5% (cinco por cento) das vagas nos estacionamentos públicos e privados, as quais deverão ser posicionadas de forma a garantir a melhor comodidade ao idoso.<br><br>Art. 42. São asseguradas a prioridade e a segurança do idoso nos procedimentos de embarque e desembarque nos veículos do sistema de transporte coletivo. (Redação dada pela Lei nº 12.899, de 2013)<br><br>TÍTULO III<br><br>Das Medidas de Proteção<br><br>CAPÍTULO I<br><br>Das Disposições Gerais<br><br>Art. 43. As medidas de proteção ao idoso são aplicáveis sempre que os direitos reconhecidos nesta Lei forem ameaçados ou violados:<br><br>I – por ação ou omissão da sociedade ou do Estado;<br><br>II – por falta, omissão ou abuso da família, curador ou entidade de atendimento;<br><br>III – em razão de sua condição pessoal.<br><br>CAPÍTULO II<br><br>Das Medidas Específicas de Proteção<br><br>Art. 44. As medidas de proteção ao idoso previstas nesta Lei poderão ser aplicadas, isolada ou cumulativamente, e levarão em conta os fins sociais a que se destinam e o fortalecimento dos vínculos familiares e comunitários.<br><br>Art. 45. Verificada qualquer das hipóteses previstas no art. 43, o Ministério Público ou o Poder Judiciário, a requerimento daquele, poderá determinar, dentre outras, as seguintes medidas:<br><br>I – encaminhamento à família ou curador, mediante termo de responsabilidade;<br><br>II – orientação, apoio e acompanhamento temporários;<br><br>III – requisição para tratamento de sua saúde, em regime ambulatorial, hospitalar ou domiciliar;<br><br>IV – inclusão em programa oficial ou comunitário de auxílio, orientação e tratamento a usuários dependentes de drogas lícitas ou ilícitas, ao próprio idoso ou à pessoa de sua convivência que lhe cause perturbação;<br><br>V – abrigo em entidade;<br><br>VI – abrigo temporário.<br><br>TÍTULO IV<br><br>Da Política de Atendimento ao Idoso<br><br>CAPÍTULO I<br><br>Disposições Gerais<br><br>Art. 46. A política de atendimento ao idoso far-se-á por meio do conjunto articulado de ações governamentais e não-governamentais da União, dos Estados, do Distrito Federal e dos Municípios.<br><br>Art. 47. São linhas de ação da política de atendimento:<br><br>I – políticas sociais básicas, previstas na Lei no 8.842, de 4 de janeiro de 1994;<br><br>II – políticas e programas de assistência social, em caráter supletivo, para aqueles que necessitarem;<br><br>III – serviços especiais de prevenção e atendimento às vítimas de negligência, maus-tratos, exploração, abuso, crueldade e opressão;<br><br>IV – serviço de identificação e localização de parentes ou responsáveis por idosos abandonados em hospitais e instituições de longa permanência;<br><br>V – proteção jurídico-social por entidades de defesa dos direitos dos idosos;<br><br>VI – mobilização da opinião pública no sentido da participação dos diversos segmentos da sociedade no atendimento do idoso.<br><br>CAPÍTULO II<br><br>Das Entidades de Atendimento ao Idoso<br><br>Art. 48. As entidades de atendimento são responsáveis pela manutenção das próprias unidades, observadas as normas de planejamento e execução emanadas do órgão competente da Política Nacional do Idoso, conforme a Lei no 8.842, de 1994.<br><br>Parágrafo único. As entidades governamentais e não-governamentais de assistência ao idoso ficam sujeitas à inscrição de seus programas, junto ao órgão competente da Vigilância Sanitária e Conselho Municipal da Pessoa Idosa, e em sua falta, junto ao Conselho Estadual ou Nacional da Pessoa Idosa, especificando os regimes de atendimento, observados os seguintes requisitos:<br><br>I – oferecer instalações físicas em condições adequadas de habitabilidade, higiene, salubridade e segurança;<br><br>II – apresentar objetivos estatutários e plano de trabalho compatíveis com os princípios desta Lei;<br><br>III – estar regularmente constituída;<br><br>IV – demonstrar a idoneidade de seus dirigentes.<br><br>Art. 49. As entidades que desenvolvam programas de institucionalização de longa permanência adotarão os seguintes princípios:<br><br>I – preservação dos vínculos familiares;<br><br>II – atendimento personalizado e em pequenos grupos;<br><br>III – manutenção do idoso na mesma instituição, salvo em caso de força maior;<br><br>IV – participação do idoso nas atividades comunitárias, de caráter interno e externo;<br><br>V – observância dos direitos e garantias dos idosos;<br><br>VI – preservação da identidade do idoso e oferecimento de ambiente de respeito e dignidade.<br><br>Parágrafo único. O dirigente de instituição prestadora de atendimento ao idoso responderá civil e criminalmente pelos atos que praticar em detrimento do idoso, sem prejuízo das sanções administrativas.<br><br> Art. 50. Constituem obrigações das entidades de atendimento:<br><br>I – celebrar contrato escrito de prestação de serviço com o idoso, especificando o tipo de atendimento, as obrigações da entidade e prestações decorrentes do contrato, com os respectivos preços, se for o caso;<br><br>II – observar os direitos e as garantias de que são titulares os idosos;<br><br>III – fornecer vestuário adequado, se for pública, e alimentação suficiente;<br><br>IV – oferecer instalações físicas em condições adequadas de habitabilidade;<br><br>V – oferecer atendimento personalizado;<br><br>VI – diligenciar no sentido da preservação dos vínculos familiares;<br><br>VII – oferecer acomodações apropriadas para recebimento de visitas;<br><br>VIII – proporcionar cuidados à saúde, conforme a necessidade do idoso;<br><br>IX – promover atividades educacionais, esportivas, culturais e de lazer;<br><br>X – propiciar assistência religiosa àqueles que desejarem, de acordo com suas crenças;<br><br>XI – proceder a estudo social e pessoal de cada caso;<br><br>XII – comunicar à autoridade competente de saúde toda ocorrência de idoso portador de doenças infecto-contagiosas;<br><br>XIII – providenciar ou solicitar que o Ministério Público requisite os documentos necessários ao exercício da cidadania àqueles que não os tiverem, na forma da lei;<br><br>XIV – fornecer comprovante de depósito dos bens móveis que receberem dos idosos;<br><br>XV – manter arquivo de anotações onde constem data e circunstâncias do atendimento, nome do idoso, responsável, parentes, endereços, cidade, relação de seus pertences, bem como o valor de contribuições, e suas alterações, se houver, e demais dados que possibilitem sua identificação e a individualização do atendimento;<br><br>XVI – comunicar ao Ministério Público, para as providências cabíveis, a situação de abandono moral ou material por parte dos familiares;<br><br>XVII – manter no quadro de pessoal profissionais com formação específica.<br><br>Art. 51. As instituições filantrópicas ou sem fins lucrativos prestadoras de serviço ao idoso terão direito à assistência judiciária gratuita.<br><br>CAPÍTULO III<br><br>Da Fiscalização das Entidades de Atendimento<br><br>Art. 52. As entidades governamentais e não-governamentais de atendimento ao idoso serão fiscalizadas pelos Conselhos do Idoso, Ministério Público, Vigilância Sanitária e outros previstos em lei.<br><br>Art. 53. O art. 7o da Lei no 8.842, de 1994, passa a vigorar com a seguinte redação:<br><br>Art. 7o Compete aos Conselhos de que trata o art. 6o desta Lei a supervisão, o acompanhamento, a fiscalização e a avaliação da política nacional do idoso, no âmbito das respectivas instâncias político-administrativas. (NR)<br><br>Art. 54. Será dada publicidade das prestações de contas dos recursos públicos e privados recebidos pelas entidades de atendimento.<br><br>Art. 55. As entidades de atendimento que descumprirem as determinações desta Lei ficarão sujeitas, sem prejuízo da responsabilidade civil e criminal de seus dirigentes ou prepostos, às seguintes penalidades, observado o devido processo legal:<br><br>I – as entidades governamentais:<br><br>a) advertência;<br><br>b) afastamento provisório de seus dirigentes;<br><br>c) afastamento definitivo de seus dirigentes;<br><br>d) fechamento de unidade ou interdição de programa;<br><br>II – as entidades não-governamentais:<br><br>a) advertência;<br><br>b) multa;<br><br>c) suspensão parcial ou total do repasse de verbas públicas;<br><br>d) interdição de unidade ou suspensão de programa;<br><br>e) proibição de atendimento a idosos a bem do interesse público.<br><br>§ 1o Havendo danos aos idosos abrigados ou qualquer tipo de fraude em relação ao programa, caberá o afastamento provisório dos dirigentes ou a interdição da unidade e a suspensão do programa.<br><br>§ 2o A suspensão parcial ou total do repasse de verbas públicas ocorrerá quando verificada a má aplicação ou desvio de finalidade dos recursos.<br><br>§ 3o Na ocorrência de infração por entidade de atendimento, que coloque em risco os direitos assegurados nesta Lei, será o fato comunicado ao Ministério Público, para as providências cabíveis, inclusive para promover a suspensão das atividades ou dissolução da entidade, com a proibição de atendimento a idosos a bem do interesse público, sem prejuízo das providências a serem tomadas pela Vigilância Sanitária.<br><br>§ 4o Na aplicação das penalidades, serão consideradas a natureza e a gravidade da infração cometida, os danos que dela provierem para o idoso, as circunstâncias agravantes ou atenuantes e os antecedentes da entidade.<br><br>CAPÍTULO IV<br><br>Das Infrações Administrativas<br><br>Art. 56. Deixar a entidade de atendimento de cumprir as determinações do art. 50 desta Lei:<br><br>Pena – multa de R$ 500,00 (quinhentos reais) a R$ 3.000,00 (três mil reais), se o fato não for caracterizado como crime, podendo haver a interdição do estabelecimento até que sejam cumpridas as exigências legais.<br><br>Parágrafo único. No caso de interdição do estabelecimento de longa permanência, os idosos abrigados serão transferidos para outra instituição, a expensas do estabelecimento interditado, enquanto durar a interdição.<br><br>Art. 57. Deixar o profissional de saúde ou o responsável por estabelecimento de saúde ou instituição de longa permanência de comunicar à autoridade competente os casos de crimes contra idoso de que tiver conhecimento:<br><br>Pena – multa de R$ 500,00 (quinhentos reais) a R$ 3.000,00 (três mil reais), aplicada em dobro no caso de reincidência.<br><br>Art. 58. Deixar de cumprir as determinações desta Lei sobre a prioridade no atendimento ao idoso:<br><br>Pena – multa de R$ 500,00 (quinhentos reais) a R$ 1.000,00 (um mil reais) e multa civil a ser estipulada pelo juiz, conforme o dano sofrido pelo idoso.<br><br>CAPÍTULO V<br><br>Da Apuração Administrativa de Infração às <br><br>Normas de Proteção ao Idoso<br><br>Art. 59. Os valores monetários expressos no Capítulo IV serão atualizados anualmente, na forma da lei.<br><br>Art. 60. O procedimento para a imposição de penalidade administrativa por infração às normas de proteção ao idoso terá início com requisição do Ministério Público ou auto de infração elaborado por servidor efetivo e assinado, se possível, por duas testemunhas.<br><br>§ 1o No procedimento iniciado com o auto de infração poderão ser usadas fórmulas impressas, especificando-se a natureza e as circunstâncias da infração.<br><br>§ 2o Sempre que possível, à verificação da infração seguir-se-á a lavratura do auto, ou este será lavrado dentro de 24 (vinte e quatro) horas, por motivo justificado.<br><br>Art. 61. O autuado terá prazo de 10 (dez) dias para a apresentação da defesa, contado da data da intimação, que será feita:<br><br>I – pelo autuante, no instrumento de autuação, quando for lavrado na presença do infrator;<br><br>II – por via postal, com aviso de recebimento.<br><br>Art. 62. Havendo risco para a vida ou à saúde do idoso, a autoridade competente aplicará à entidade de atendimento as sanções regulamentares, sem prejuízo da iniciativa e das providências que vierem a ser adotadas pelo Ministério Público ou pelas demais instituições legitimadas para a fiscalização.<br><br>Art. 63. Nos casos em que não houver risco para a vida ou a saúde da pessoa idosa abrigada, a autoridade competente aplicará à entidade de atendimento as sanções regulamentares, sem prejuízo da iniciativa e das providências que vierem a ser adotadas pelo Ministério Público ou pelas demais instituições legitimadas para a fiscalização.<br><br>CAPÍTULO VI<br><br>Da Apuração Judicial de Irregularidades em Entidade de Atendimento<br><br>Art. 64. Aplicam-se, subsidiariamente, ao procedimento administrativo de que trata este Capítulo as disposições das Leis nos 6.437, de 20 de agosto de 1977, e 9.784, de 29 de janeiro de 1999.<br><br>Art. 65. O procedimento de apuração de irregularidade em entidade governamental e não-governamental de atendimento ao idoso terá início mediante petição fundamentada de pessoa interessada ou iniciativa do Ministério Público.<br><br>Art. 66. Havendo motivo grave, poderá a autoridade judiciária, ouvido o Ministério Público, decretar liminarmente o afastamento provisório do dirigente da entidade ou outras medidas que julgar adequadas, para evitar lesão aos direitos do idoso, mediante decisão fundamentada.<br><br>Art. 67. O dirigente da entidade será citado para, no prazo de 10 (dez) dias, oferecer resposta escrita, podendo juntar documentos e indicar as provas a produzir.<br><br>Art. 68. Apresentada a defesa, o juiz procederá na conformidade do art. 69 ou, se necessário, designará audiência de instrução e julgamento, deliberando sobre a necessidade de produção de outras provas.<br><br>§ 1o Salvo manifestação em audiência, as partes e o Ministério Público terão 5 (cinco) dias para oferecer alegações finais, decidindo a autoridade judiciária em igual prazo.<br><br>§ 2o Em se tratando de afastamento provisório ou definitivo de dirigente de entidade governamental, a autoridade judiciária oficiará a autoridade administrativa imediatamente superior ao afastado, fixando-lhe prazo de 24 (vinte e quatro) horas para proceder à substituição.<br><br>§ 3o Antes de aplicar qualquer das medidas, a autoridade judiciária poderá fixar prazo para a remoção das irregularidades verificadas. Satisfeitas as exigências, o processo será extinto, sem julgamento do mérito.<br><br>§ 4o A multa e a advertência serão impostas ao dirigente da entidade ou ao responsável pelo programa de atendimento.<br><br>TÍTULO V<br><br>Do Acesso à Justiça<br><br>CAPÍTULO I<br><br>Disposições Gerais<br><br>Art. 69. Aplica-se, subsidiariamente, às disposições deste Capítulo, o procedimento sumário previsto no Código de Processo Civil, naquilo que não contrarie os prazos previstos nesta Lei.<br><br>Art. 70. O Poder Público poderá criar varas especializadas e exclusivas do idoso.<br><br>Art. 71. É assegurada prioridade na tramitação dos processos e procedimentos e na execução dos atos e diligências judiciais em que figure como parte ou interveniente pessoa com idade igual ou superior a 60 (sessenta) anos, em qualquer instância.<br><br>§ 1o O interessado na obtenção da prioridade a que alude este artigo, fazendo prova de sua idade, requererá o benefício à autoridade judiciária competente para decidir o feito, que determinará as providências a serem cumpridas, anotando-se essa circunstância em local visível nos autos do processo.<br><br>§ 2o A prioridade não cessará com a morte do beneficiado, estendendo-se em favor do cônjuge supérstite, companheiro ou companheira, com união estável, maior de 60 (sessenta) anos.<br><br>§ 3o A prioridade se estende aos processos e procedimentos na Administração Pública, empresas prestadoras de serviços públicos e instituições financeiras, ao atendimento preferencial junto à Defensoria Publica da União, dos Estados e do Distrito Federal em relação aos Serviços de Assistência Judiciária.<br><br>§ 4o Para o atendimento prioritário será garantido ao idoso o fácil acesso aos assentos e caixas, identificados com a destinação a idosos em local visível e caracteres legíveis.<br><br>§ 5ºDentre os processos de idosos, dar-se-á prioridade especial aos maiores de oitenta anos.(Incluído pela Lei nº 13.466, de 2017).<br><br>CAPÍTULO II<br><br>Do Ministério Público<br><br>Art. 72. (VETADO)<br><br>Art. 73. As funções do Ministério Público, previstas nesta Lei, serão exercidas nos termos da respectiva Lei Orgânica.<br><br>Art. 74. Compete ao Ministério Público:<br><br>I – instaurar o inquérito civil e a ação civil pública para a proteção dos direitos e interesses difusos ou coletivos, individuais indisponíveis e individuais homogêneos do idoso;<br><br>II – promover e acompanhar as ações de alimentos, de interdição total ou parcial, de designação de curador especial, em circunstâncias que justifiquem a medida e oficiar em todos os feitos em que se discutam os direitos de idosos em condições de risco;<br><br>III – atuar como substituto processual do idoso em situação de risco, conforme o disposto no art. 43 desta Lei;<br><br>IV – promover a revogação de instrumento procuratório do idoso, nas hipóteses previstas no art. 43 desta Lei, quando necessário ou o interesse público justificar;<br><br>V – instaurar procedimento administrativo e, para instruí-lo:<br><br>a) expedir notificações, colher depoimentos ou esclarecimentos e, em caso de não comparecimento injustificado da pessoa notificada, requisitar condução coercitiva, inclusive pela Polícia Civil ou Militar;<br><br>b) requisitar informações, exames, perícias e documentos de autoridades municipais, estaduais e federais, da administração direta e indireta, bem como promover inspeções e diligências investigatórias;<br><br>c) requisitar informações e documentos particulares de instituições privadas;<br><br>VI – instaurar sindicâncias, requisitar diligências investigatórias e a instauração de inquérito policial, para a apuração de ilícitos ou infrações às normas de proteção ao idoso;<br><br>VII – zelar pelo efetivo respeito aos direitos e garantias legais assegurados ao idoso, promovendo as medidas judiciais e extrajudiciais cabíveis;<br><br>VIII – inspecionar as entidades públicas e particulares de atendimento e os programas de que trata esta Lei, adotando de pronto as medidas administrativas ou judiciais necessárias à remoção de irregularidades porventura verificadas;<br><br>IX – requisitar força policial, bem como a colaboração dos serviços de saúde, educacionais e de assistência social, públicos, para o desempenho de suas atribuições;<br><br>X – referendar transações envolvendo interesses e direitos dos idosos previstos nesta Lei.<br><br>§ 1o A legitimação do Ministério Público para as ações cíveis previstas neste artigo não impede a de terceiros, nas mesmas hipóteses, segundo dispuser a lei.<br><br>§ 2o As atribuições constantes deste artigo não excluem outras, desde que compatíveis com a finalidade e atribuições do Ministério Público.<br><br>§ 3o O representante do Ministério Público, no exercício de suas funções, terá livre acesso a toda entidade de atendimento ao idoso.<br><br>Art. 75. Nos processos e procedimentos em que não for parte, atuará obrigatoriamente o Ministério Público na defesa dos direitos e interesses de que cuida esta Lei, hipóteses em que terá vista dos autos depois das partes, podendo juntar documentos, requerer diligências e produção de outras provas, usando os recursos cabíveis.<br><br>Art. 76. A intimação do Ministério Público, em qualquer caso, será feita pessoalmente.<br><br>Art. 77. A falta de intervenção do Ministério Público acarreta a nulidade do feito, que será declarada de ofício pelo juiz ou a requerimento de qualquer interessado.<br><br>CAPÍTULO III<br><br>Da Proteção Judicial dos Interesses Difusos, Coletivos e Individuais Indisponíveis ou Homogêneos<br><br>Art. 78. As manifestações processuais do representante do Ministério Público deverão ser fundamentadas.<br><br>Art. 79. Regem-se pelas disposições desta Lei as ações de responsabilidade por ofensa aos direitos assegurados ao idoso, referentes à omissão ou ao oferecimento insatisfatório de:<br><br>I – acesso às ações e serviços de saúde;<br><br>II – atendimento especializado ao idoso portador de deficiência ou com limitação incapacitante;<br><br>III – atendimento especializado ao idoso portador de doença infecto-contagiosa;<br><br>IV – serviço de assistência social visando ao amparo do idoso.<br><br>Parágrafo único. As hipóteses previstas neste artigo não excluem da proteção judicial outros interesses difusos, coletivos, individuais indisponíveis ou homogêneos, próprios do idoso, protegidos em lei.<br><br>Art. 80. As ações previstas neste Capítulo serão propostas no foro do domicílio do idoso, cujo juízo terá competência absoluta para processar a causa, ressalvadas as competências da Justiça Federal e a competência originária dos Tribunais Superiores.<br><br>Art. 81. Para as ações cíveis fundadas em interesses difusos, coletivos, individuais indisponíveis ou homogêneos, consideram-se legitimados, concorrentemente:<br><br>I – o Ministério Público;<br><br>II – a União, os Estados, o Distrito Federal e os Municípios;<br><br>III – a Ordem dos Advogados do Brasil;<br><br>IV – as associações legalmente constituídas há pelo menos 1 (um) ano e que incluam entre os fins institucionais a defesa dos interesses e direitos da pessoa idosa, dispensada a autorização da assembléia, se houver prévia autorização estatutária.<br><br>§ 1o Admitir-se-á litisconsórcio facultativo entre os Ministérios Públicos da União e dos Estados na defesa dos interesses e direitos de que cuida esta Lei.<br><br>§ 2o Em caso de desistência ou abandono da ação por associação legitimada, o Ministério Público ou outro legitimado deverá assumir a titularidade ativa.<br><br>Art. 82. Para defesa dos interesses e direitos protegidos por esta Lei, são admissíveis todas as espécies de ação pertinentes.<br><br>Parágrafo único. Contra atos ilegais ou abusivos de autoridade pública ou agente de pessoa jurídica no exercício de atribuições de Poder Público, que lesem direito líquido e certo previsto nesta Lei, caberá ação mandamental, que se regerá pelas normas da lei do mandado de segurança.<br><br>Art. 83. Na ação que tenha por objeto o cumprimento de obrigação de fazer ou não-fazer, o juiz concederá a tutela específica da obrigação ou determinará providências que assegurem o resultado prático equivalente ao adimplemento.<br><br>§ 1o Sendo relevante o fundamento da demanda e havendo justificado receio de ineficácia do provimento final, é lícito ao juiz conceder a tutela liminarmente ou após justificação prévia, na forma do art. 273 do Código de Processo Civil.<br><br>§ 2o O juiz poderá, na hipótese do § 1o ou na sentença, impor multa diária ao réu, independentemente do pedido do autor, se for suficiente ou compatível com a obrigação, fixando prazo razoável para o cumprimento do preceito.<br><br>§ 3o A multa só será exigível do réu após o trânsito em julgado da sentença favorável ao autor, mas será devida desde o dia em que se houver configurado.<br><br>Art. 84. Os valores das multas previstas nesta Lei reverterão ao Fundo do Idoso, onde houver, ou na falta deste, ao Fundo Municipal de Assistência Social, ficando vinculados ao atendimento ao idoso.<br><br>Parágrafo único. As multas não recolhidas até 30 (trinta) dias após o trânsito em julgado da decisão serão exigidas por meio de execução promovida pelo Ministério Público, nos mesmos autos, facultada igual iniciativa aos demais legitimados em caso de inércia daquele.<br><br>Art. 85. O juiz poderá conferir efeito suspensivo aos recursos, para evitar dano irreparável à parte.<br><br>Art. 86. Transitada em julgado a sentença que impuser condenação ao Poder Público, o juiz determinará a remessa de peças à autoridade competente, para apuração da responsabilidade civil e administrativa do agente a que se atribua a ação ou omissão.<br><br>Art. 87. Decorridos 60 (sessenta) dias do trânsito em julgado da sentença condenatória favorável ao idoso sem que o autor lhe promova a execução, deverá fazê-lo o Ministério Público, facultada, igual iniciativa aos demais legitimados, como assistentes ou assumindo o pólo ativo, em caso de inércia desse órgão.<br><br>Art. 88. Nas ações de que trata este Capítulo, não haverá adiantamento de custas, emolumentos, honorários periciais e quaisquer outras despesas.<br><br>Parágrafo único. Não se imporá sucumbência ao Ministério Público.<br><br>Art. 89. Qualquer pessoa poderá, e o servidor deverá, provocar a iniciativa do Ministério Público, prestando-lhe informações sobre os fatos que constituam objeto de ação civil e indicando-lhe os elementos de convicção.<br><br>Art. 90. Os agentes públicos em geral, os juízes e tribunais, no exercício de suas funções, quando tiverem conhecimento de fatos que possam configurar crime de ação pública contra idoso ou ensejar a propositura de ação para sua defesa, devem encaminhar as peças pertinentes ao Ministério Público, para as providências cabíveis.<br><br>Art. 91. Para instruir a petição inicial, o interessado poderá requerer às autoridades competentes as certidões e informações que julgar necessárias, que serão fornecidas no prazo de 10 (dez) dias.<br><br>Art. 92. O Ministério Público poderá instaurar sob sua presidência, inquérito civil, ou requisitar, de qualquer pessoa, organismo público ou particular, certidões, informações, exames ou perícias, no prazo que assinalar, o qual não poderá ser inferior a 10 (dez) dias.<br><br>§ 1o Se o órgão do Ministério Público, esgotadas todas as diligências, se convencer da inexistência de fundamento para a propositura da ação civil ou de peças informativas, determinará o seu arquivamento, fazendo-o fundamentadamente.<br><br>§ 2o Os autos do inquérito civil ou as peças de informação arquivados serão remetidos, sob pena de se incorrer em falta grave, no prazo de 3 (três) dias, ao Conselho Superior do Ministério Público ou à Câmara de Coordenação e Revisão do Ministério Público.<br><br>§ 3o Até que seja homologado ou rejeitado o arquivamento, pelo Conselho Superior do Ministério Público ou por Câmara de Coordenação e Revisão do Ministério Público, as associações legitimadas poderão apresentar razões escritas ou documentos, que serão juntados ou anexados às peças de informação.<br><br>§ 4o Deixando o Conselho Superior ou a Câmara de Coordenação e Revisão do Ministério Público de homologar a promoção de arquivamento, será designado outro membro do Ministério Público para o ajuizamento da ação.<br><br>TÍTULO VI<br><br>Dos Crimes<br><br>CAPÍTULO I<br><br>Disposições Gerais<br><br>Art. 93. Aplicam-se subsidiariamente, no que couber, as disposições da Lei no 7.347, de 24 de julho de 1985.<br><br>Art. 94. Aos crimes previstos nesta Lei, cuja pena máxima privativa de liberdade não ultrapasse 4 (quatro) anos, aplica-se o procedimento previsto na Lei no 9.099, de 26 de setembro de 1995, e, subsidiariamente, no que couber, as disposições do Código Penal e do Código de Processo Penal. (Vide ADI 3.096-5 - STF)<br><br>CAPÍTULO II<br><br>Dos Crimes em Espécie<br><br>Art. 95. Os crimes definidos nesta Lei são de ação penal pública incondicionada, não se lhes aplicando os arts. 181 e 182 do Código Penal.<br><br>Art. 96. Discriminar pessoa idosa, impedindo ou dificultando seu acesso a operações bancárias, aos meios de transporte, ao direito de contratar ou por qualquer outro meio ou instrumento necessário ao exercício da cidadania, por motivo de idade:<br><br>Pena – reclusão de 6 (seis) meses a 1 (um) ano e multa.<br><br>§ 1o Na mesma pena incorre quem desdenhar, humilhar, menosprezar ou discriminar pessoa idosa, por qualquer motivo.<br><br>§ 2o A pena será aumentada de 1/3 (um terço) se a vítima se encontrar sob os cuidados ou responsabilidade do agente.<br><br>Art. 97. Deixar de prestar assistência ao idoso, quando possível fazê-lo sem risco pessoal, em situação de iminente perigo, ou recusar, retardar ou dificultar sua assistência à saúde, sem justa causa, ou não pedir, nesses casos, o socorro de autoridade pública:<br><br>Pena – detenção de 6 (seis) meses a 1 (um) ano e multa.<br><br>Parágrafo único. A pena é aumentada de metade, se da omissão resulta lesão corporal de natureza grave, e triplicada, se resulta a morte.<br><br>Art. 98. Abandonar o idoso em hospitais, casas de saúde, entidades de longa permanência, ou congêneres, ou não prover suas necessidades básicas, quando obrigado por lei ou mandado:<br><br>Pena – detenção de 6 (seis) meses a 3 (três) anos e multa.<br><br>Art. 99. Expor a perigo a integridade e a saúde, física ou psíquica, do idoso, submetendo-o a condições desumanas ou degradantes ou privando-o de alimentos e cuidados indispensáveis, quando obrigado a fazê-lo, ou sujeitando-o a trabalho excessivo ou inadequado:<br><br>Pena – detenção de 2 (dois) meses a 1 (um) ano e multa.<br><br>§ 1o Se do fato resulta lesão corporal de natureza grave:<br><br>Pena – reclusão de 1 (um) a 4 (quatro) anos.<br><br>§ 2o Se resulta a morte:<br><br>Pena – reclusão de 4 (quatro) a 12 (doze) anos.<br><br>Art. 100. Constitui crime punível com reclusão de 6 (seis) meses a 1 (um) ano e multa:<br><br>I – obstar o acesso de alguém a qualquer cargo público por motivo de idade;<br><br>II – negar a alguém, por motivo de idade, emprego ou trabalho;<br><br>III – recusar, retardar ou dificultar atendimento ou deixar de prestar assistência à saúde, sem justa causa, a pessoa idosa;<br><br>IV – deixar de cumprir, retardar ou frustrar, sem justo motivo, a execução de ordem judicial expedida na ação civil a que alude esta Lei;<br><br>V – recusar, retardar ou omitir dados técnicos indispensáveis à propositura da ação civil objeto desta Lei, quando requisitados pelo Ministério Público.<br><br>Art. 101. Deixar de cumprir, retardar ou frustrar, sem justo motivo, a execução de ordem judicial expedida nas ações em que for parte ou interveniente o idoso:<br><br>Pena – detenção de 6 (seis) meses a 1 (um) ano e multa.<br><br>Art. 102. Apropriar-se de ou desviar bens, proventos, pensão ou qualquer outro rendimento do idoso, dando-lhes aplicação diversa da de sua finalidade:<br><br>Pena – reclusão de 1 (um) a 4 (quatro) anos e multa.<br><br>Art. 103. Negar o acolhimento ou a permanência do idoso, como abrigado, por recusa deste em outorgar procuração à entidade de atendimento:<br><br>Pena – detenção de 6 (seis) meses a 1 (um) ano e multa.<br><br>Art. 104. Reter o cartão magnético de conta bancária relativa a benefícios, proventos ou pensão do idoso, bem como qualquer outro documento com objetivo de assegurar recebimento ou ressarcimento de dívida:<br><br>Pena – detenção de 6 (seis) meses a 2 (dois) anos e multa.<br><br>Art. 105. Exibir ou veicular, por qualquer meio de comunicação, informações ou imagens depreciativas ou injuriosas à pessoa do idoso:<br><br>Pena – detenção de 1 (um) a 3 (três) anos e multa.<br><br>Art. 106. Induzir pessoa idosa sem discernimento de seus atos a outorgar procuração para fins de administração de bens ou deles dispor livremente:<br><br>Pena – reclusão de 2 (dois) a 4 (quatro) anos.<br><br>Art. 107. Coagir, de qualquer modo, o idoso a doar, contratar, testar ou outorgar procuração:<br><br>Pena – reclusão de 2 (dois) a 5 (cinco) anos.<br><br>Art. 108. Lavrar ato notarial que envolva pessoa idosa sem discernimento de seus atos, sem a devida representação legal:<br><br>Pena – reclusão de 2 (dois) a 4 (quatro) anos.<br><br>TÍTULO VII<br><br>Disposições Finais e Transitórias<br><br>Art. 109. Impedir ou embaraçar ato do representante do Ministério Público ou de qualquer outro agente fiscalizador:<br><br>Pena – reclusão de 6 (seis) meses a 1 (um) ano e multa.<br><br>Art. 110. O Decreto-Lei no 2.848, de 7 de dezembro de 1940, Código Penal, passa a vigorar com as seguintes alterações:<br><br>Art. 61. ......................<br><br>......................<br><br>II - ......................<br><br>......................<br><br>h) contra criança, maior de 60 (sessenta) anos, enfermo ou mulher grávida;<br><br>....................... (NR)<br><br>Art. 121. ......................<br><br>......................<br><br>§ 4o No homicídio culposo, a pena é aumentada de 1/3 (um terço), se o crime resulta de inobservância de regra técnica de profissão, arte ou ofício, ou se o agente deixa de prestar imediato socorro à vítima, não procura diminuir as conseqüências do seu ato, ou foge para evitar prisão em flagrante. Sendo doloso o homicídio, a pena é aumentada de 1/3 (um terço) se o crime é praticado contra pessoa menor de 14 (quatorze) ou maior de 60 (sessenta) anos.<br><br>....................... (NR)<br><br>Art. 133. ......................<br><br>......................<br><br>§ 3o ......................<br><br>......................<br><br>III – se a vítima é maior de 60 (sessenta) anos. (NR)<br><br>Art. 140. ......................<br><br>......................<br><br>§ 3o Se a injúria consiste na utilização de elementos referentes a raça, cor, etnia, religião, origem ou a condição de pessoa idosa ou portadora de deficiência:<br><br>...................... (NR)<br><br>Art. 141. ......................<br><br>......................<br><br>IV – contra pessoa maior de 60 (sessenta) anos ou portadora de deficiência, exceto no caso de injúria.<br><br>....................... (NR)<br><br>Art. 148. ......................<br><br>......................<br><br>§ 1o......................<br><br>I – se a vítima é ascendente, descendente, cônjuge do agente ou maior de 60 (sessenta) anos.<br><br>...................... (NR)<br><br>Art. 159......................<br><br>......................<br><br>§ 1o Se o seqüestro dura mais de 24 (vinte e quatro) horas, se o seqüestrado é menor de 18 (dezoito) ou maior de 60 (sessenta) anos, ou se o crime é cometido por bando ou quadrilha.<br><br>...................... (NR)<br><br>Art. 183......................<br><br>......................<br><br>III – se o crime é praticado contra pessoa com idade igual ou superior a 60 (sessenta) anos. (NR)<br><br>Art. 244. Deixar, sem justa causa, de prover a subsistência do cônjuge, ou de filho menor de 18 (dezoito) anos ou inapto para o trabalho, ou de ascendente inválido ou maior de 60 (sessenta) anos, não lhes proporcionando os recursos necessários ou faltando ao pagamento de pensão alimentícia judicialmente acordada, fixada ou majorada; deixar, sem justa causa, de socorrer descendente ou ascendente, gravemente enfermo:<br><br>...................... (NR)<br><br> Art. 111. O O art. 21 do Decreto-Lei no 3.688, de 3 de outubro de 1941, Lei das Contravenções Penais, passa a vigorar acrescido do seguinte parágrafo único:<br><br>Art. 21......................<br><br>......................<br><br>Parágrafo único. Aumenta-se a pena de 1/3 (um terço) até a metade se a vítima é maior de 60 (sessenta) anos. (NR)<br><br> Art. 112. O inciso II do § 4o do art. 1o da Lei no 9.455, de 7 de abril de 1997, passa a vigorar com a seguinte redação:<br><br>Art. 1o ......................<br><br>......................<br><br>§ 4o ......................<br><br>II – se o crime é cometido contra criança, gestante, portador de deficiência, adolescente ou maior de 60 (sessenta) anos;<br><br>...................... (NR)<br><br> Art. 113. O inciso III do art. 18 da Lei no 6.368, de 21 de outubro de 1976, passa a vigorar com a seguinte redação:<br><br>Art. 18......................<br><br>......................<br><br>III – se qualquer deles decorrer de associação ou visar a menores de 21 (vinte e um) anos ou a pessoa com idade igual ou superior a 60 (sessenta) anos ou a quem tenha, por qualquer causa, diminuída ou suprimida a capacidade de discernimento ou de autodeterminação:<br><br>...................... (NR)<br><br>Art. 114. O art 1º da Lei no 10.048, de 8 de novembro de 2000, passa a vigorar com a seguinte redação:<br><br>Art. 1o As pessoas portadoras de deficiência, os idosos com idade igual ou superior a 60 (sessenta) anos, as gestantes, as lactantes e as pessoas acompanhadas por crianças de colo terão atendimento prioritário, nos termos desta Lei. (NR)<br><br>Art. 115. O Orçamento da Seguridade Social destinará ao Fundo Nacional de Assistência Social, até que o Fundo Nacional do Idoso seja criado, os recursos necessários, em cada exercício financeiro, para aplicação em programas e ações relativos ao idoso.<br><br>Art. 116. Serão incluídos nos censos demográficos dados relativos à população idosa do País.<br><br>Art. 117. O Poder Executivo encaminhará ao Congresso Nacional projeto de lei revendo os critérios de concessão do Benefício de Prestação Continuada previsto na Lei Orgânica da Assistência Social, de forma a garantir que o acesso ao direito seja condizente com o estágio de desenvolvimento sócio-econômico alcançado pelo País.<br><br> Art. 118. Esta Lei entra em vigor decorridos 90 (noventa) dias da sua publicação, ressalvado o disposto no caput do art. 36, que vigorará a partir de 1o de janeiro de 2004.<br><br>Brasília, 1o de outubro de 2003; 182o da Independência e 115o da República.<br><br>LUIZ INÁCIO LULA DA SILVA<br><br>Márcio Thomaz Bastos<br><br>Antonio Palocci Filho<br><br>Rubem Fonseca Filho<br><br>Humberto Sérgio Costa LIma<br><br>Guido Mantega<br><br>Ricardo José Ribeiro Berzoini<br><br>Benedita Souza da Silva Sampaio<br><br>Álvaro Augusto Ribeiro Costa<br><br>Este texto não substitui o publicado no DOU de 3.10.2003<br><br><br><br><img src=assets/imgs/brastra.gif alt=logo presidência da Republica><div style=font-size: 1.2em; font-weight: bold>Presidência da República<br>Casa Civil<br></div>LEI Nº 13.466, DE 12 DE JULHO DE 2017<br><br>Altera os arts. 3º, 15 e 71 da Lei nº 10.741, de 1º de outubro de 2003, que dispõe sobre o Estatuto do Idoso e dá outras providências.<br><br>O PRESIDENTE DA REPÚBLICA<br><br>Faço saber que o Congresso Nacional decreta e eu sanciono a seguinte Lei:<br><br>Art. 1º Esta Lei altera os arts. 3º, 15 e 71 da Lei nº 10.741, de 1º de outubro de 2003, que dispõe sobre o Estatuto do Idoso e dá outras providências, a fim de estabelecer a prioridade especial das pessoas maiores de oitenta anos.<br><br>Art. 2º O art. 3º da Lei nº 10.741, de 1º de outubro de 2003, passa a vigorar acrescido do seguinte § 2º, renumerando-se o atual parágrafo único para § 1º:<br><br>"Art. 3º....................................<br><br>§ 1º........................................<br><br>§ 2º Dentre os idosos, é assegurada prioridade especial aos maiores de oitenta anos, atendendo-se suas necessidades sempre preferencialmente em relação aos demais idosos." (NR)<br><br>Art. 3º O art. 15 da Lei nº 10.741, de 1º de outubro de 2003, passa a vigorar acrescido do seguinte § 7º:<br><br>"Art. 15....................................<br><br>§ 7º Em todo atendimento de saúde, os maiores de oitenta anos terão preferência especial sobre os demais idosos, exceto em caso de emergência." (NR)<br><br>Art. 4º O art. 71 da Lei nº 10.741, de 1º de outubro de 2003, passa a vigorar acrescido do seguinte § 5º:<br><br>"Art. 71....................................<br><br>§ 5º Dentre os processos de idosos, dar-se-á prioridade especial aos maiores de oitenta anos." (NR)<br><br>Art. 5º Esta Lei entra em vigor na data de sua publicação.<br><br>Brasília, 12 de julho de 2017; 196º da Independência e 129º da República.<br><br>MICHEL TEMER<br><br>Luislinda Dias de Valois Santos<br><br>'
            },
            { nome: 'Política Nacional de Saúde da Pessoa Idosa', lei: "(Portaria nº. 2.528, de 19 de outubro de 2006)", code: 3, descricao: '<div style="font-size: 1.5em; font-weight: bold">Presidência da República<br>Casa Civil<br></div>PORTARIA Nº 2.528 DE 19 DE OUTUBRO DE 2006<br>Aprova a Política Nacional de Saúde da Pessoa Idosa.<br><br>O MINISTRO DE ESTADO DA SAÚDE, no uso de suas atribuições, e<br><br>Considerando a necessidade de que o setor saúde disponha de uma política atualizada relacionada à saúde do idoso;<br><br>Considerando a conclusão do processo de revisão e atualização do constante da Portaria n° 1.395/GM, de 10 de dezembro de 1999;<br><br>Considerando a publicação da Portaria nº 399/GM, de 22 de fevereiro de 2006, que divulga o Pacto pela Saúde 2006 - Consolidação do SUS e aprova as Diretrizes Operacionais do referido Pacto; e<br><br>Considerando a pactuação da Política na reunião da Comissão Intergestores Tripartite do dia 5 de outubro de 2006 e a aprovação da proposta da Política, pelo Conselho Nacional de Saúde, por meio do Memorando nº 500/SE/CNS/ 2006, resolve:<br><br>Art. 1º  Aprovar a Política Nacional de Saúde da Pessoa Idosa, cujas disposições constam do Anexo a esta Portaria e dela são parte integrante.<br><br>Art. 2º  Determinar que os órgãos e entidades do Ministério da Saúde, cujas ações se relacionem com o tema objeto da Política ora aprovada, promovam a elaboração ou a readequação de seus programas, projetos e atividades em conformidade com as diretrizes e responsabilidades nela estabelecidas.<br><br>Art.3º  Fixar o prazo de 60 (sessenta) dias, a contar da data de publicação desta Portaria, para que o Ministério da Saúde adote as providências necessárias à revisão das Portarias nº 702/GM, de 12 de abril de 2002, e n° 249/SAS/MS, de 16 de abril de 2002, que criam os mecanismos de organização e implantação de Redes Estaduais de Assistência à Saúde do Idoso, compatibilizando-as com as diretrizes estabelecidas na Política Nacional de Saúde da Pessoa Idosa aprovada neste ato.<br><br>Art. 4º  Esta Portaria entra em vigor na data de sua publicação.<br><br>Art. 5º Fica revogada a Portaria nº 1.395/GM, de 10 de dezembro de 1999, publicada no Diário Oficial da União nº 237-E, de 13 de dezembro de 1999, página 20, seção 1.<br><br>JOSÉ AGENOR ÁLVARES DA SILVA<br>ANEXO<br><br>POLÍTICA NACIONAL DE SAÚDE DA PESSOA IDOSA<br><br>INTRODUÇÃO<br><br>No Brasil, o direito universal e integral à saúde foi conquistado pela sociedade na Constituição de 1988 e reafirmado com a criação do Sistema Único de Saúde (SUS), por meio da Lei Orgânica da Saúde nº 8.080/90. Por esse direito, entende-se o acesso universal e equânime a serviços e ações de promoção, proteção e recuperação da saúde, garantindo a integralidade da atenção, indo ao encontro das diferentes realidades e necessidades de saúde da população e dos indivíduos. Esses preceitos constitucionais encontram-se reafirmados pela Lei nº 8.142, de 28 de dezembro de 1990, que dispôs sobre a participação da comunidade na gestão do Sistema Único de Saúde e sobre as transferências intergovernamentais de recursos financeiros na área de saúde e as Normas Operacionais Básicas (NOB), editadas em 1991, 1993 e 1996, que, por sua vez, regulamentam e definem estratégias e movimentos táticos que orientam a operacionalidade do Sistema.<br><br>A regulamentação do SUS estabelece princípios e direciona a implantação de um modelo de atenção à saúde que priorize a descentralização, a universalidade, a integralidade da atenção, a eqüidade e o controle social, ao mesmo tempo em que incorpora, em sua organização, o princípio da territorialidade para facilitar o acesso das demandas populacionais aos serviços de saúde. Com o objetivo de reorganizar a prática assistencial é criado em 1994, pelo Ministério da Saúde, o Programa de Saúde da Família (PSF), tornando-se a estratégia setorial de reordenação do modelo de atenção à saúde, como eixo estruturante para reorganização da prática assistencial, imprimindo nova dinâmica nos serviços de saúde e estabelecendo uma relação de vínculo com a comunidade, humanizando esta prática direcionada à vigilância na saúde, na perspectiva da intersetorialidade (Brasil, 1994), denominando-se não mais programa e sim Estratégia Saúde da Família (ESF).<br><br>Concomitante à regulamentação do SUS, o Brasil organiza-se para responder às crescentes demandas de sua população que envelhece. A Política Nacional do Idoso, promulgada em 1994 e regulamentada em 1996, assegura direitos sociais à pessoa idosa, criando condições para promover sua autonomia, integração e participação efetiva na sociedade e reafirmando o direito à saúde nos diversos níveis de atendimento do SUS (Lei nº 8.842/94 e Decreto nº 1.948/96).<br><br>Em 1999, a Portaria Ministerial nº 1.395 anuncia a Política Nacional de Saúde do Idoso, a qual determina que os órgãos e entidades do Ministério da Saúde relacionados ao tema promovam a elaboração ou a readequação de planos, projetos e atividades na conformidade das diretrizes e responsabilidades nela estabelecidas (Brasil, 1999). Essa política assume que o principal problema que pode afetar o idoso é a perda de sua capacidade funcional, isto é, a perda das habilidades físicas e mentais necessárias para realização de atividades básicas e instrumentais da vida diária.<br><br>Em 2002, é proposta a organização e a implantação de Redes Estaduais de Assistência à Saúde do Idoso (Portaria nº 702/SAS/MS, de 2002), tendo como base as condições de gestão e a divisão de responsabilidades definida pela Norma Operacional de Assistência à Saúde (NOAS). Como parte de operacionalização das redes, são criadas as normas para cadastramento de Centros de Referência em Atenção à Saúde do Idoso (Portaria nº 249/SAS/MS, de 2002).<br><br>Em 2003, o Congresso Nacional aprova e o Presidente da República sanciona o Estatuto do Idoso, elaborado com intensa participação de entidades de defesa dos interesses dos idosos. O Estatuto do Idoso amplia a resposta do Estado e da sociedade às necessidades da população idosa, mas não traz consigo meios para financiar as ações propostas. O Capítulo IV do Estatuto reza especificamente sobre o papel do SUS na garantia da atenção à saúde da pessoa idosa de forma integral, em todos os níveis de atenção.<br><br>Assim, embora a legislação brasileira relativa aos cuidados da população idosa seja bastante avançada, a prática ainda é insatisfatória. A vigência do Estatuto do Idoso e seu uso como instrumento para a conquista de direitos dos idosos, a ampliação da Estratégia Saúde da Família que revela a presença de idosos e famílias frágeis e em situação de grande vulnerabilidade social e a inserção ainda incipiente das Redes Estaduais de Assistência à Saúde do Idoso tornaram imperiosa a readequação da Política Nacional de Saúde da Pessoa Idosa (PNSPI).<br><br>Em fevereiro de 2006, foi publicado, por meio da Portaria nº 399/GM, o documento das Diretrizes do Pacto pela Saúde que contempla o Pacto pela Vida. Neste documento, a saúde do idoso aparece como uma das seis prioridades pactuadas entre as três esferas de governo sendo apresentada uma série de ações que visam, em última instância, à implementação de algumas das diretrizes da Política Nacional de Atenção à Saúde do Idoso.<br><br>A publicação do Pacto pela Vida, particularmente no que diz respeito à saúde da população idosa, representa, sem sombra de dúvida, um avanço importante. Entretanto, muito há que se fazer para que o Sistema Único de Saúde dê respostas efetivas e eficazes às necessidades e demandas de saúde da população idosa brasileira. Dessa maneira, a participação da Comissão Intergestores Tripartite e do Conselho Nacional de Saúde, no âmbito nacional, é de fundamental importância para a discussão e formulação de estratégias de ação capazes de dar conta da heterogeneidade da população idosa e, por conseguinte, da diversidade de questões apresentadas.<br><br>Cabe destacar, por fim, que a organização da rede do SUS é fundamental para que as diretrizes dessa Política sejam plenamente alcançadas. Dessa maneira, torna-se imperiosa a revisão da Portaria nº 702/GM, de 12 de abril de 2002, que cria os mecanismos de organização e implantação de Redes Estaduais de Assistência à Saúde do Idoso e a Portaria nº 249/SAS, de 16 de abril de 2002, com posterior pactuação na Comissão Intergestores Tripartite.<br><br>A meta final deve ser uma atenção à saúde adequada e digna para os idosos e idosas brasileiras, principalmente para aquela parcela da população idosa que teve, por uma série de razões, um processo de envelhecimento marcado por doenças e agravos que impõem sérias limitações ao seu bem-estar.<br><br>1. Finalidade<br><br>A finalidade primordial da Política Nacional de Saúde da Pessoa Idosa é recuperar, manter e promover a autonomia e a independência dos indivíduos idosos, direcionando medidas coletivas e individuais de saúde para esse fim, em consonância com os princípios e diretrizes do Sistema Único de Saúde. É alvo dessa política todo cidadão e cidadã brasileiros com 60 anos ou mais de idade.<br><br>Considerando:<br><br>a) o contínuo e intenso processo de envelhecimento populacional brasileiro;<br><br>b) os inegáveis avanços políticos e técnicos no campo da gestão da saúde;<br><br>c) o conhecimento atual da Ciência;<br><br>d) o conceito de saúde para o indivíduo idoso se traduz mais pela sua condição de autonomia e independência que pela presença ou ausência de doença orgânica;<br><br>e) a necessidade de buscar a qualidade da atenção aos indivíduos idosos por meio de ações fundamentadas no paradigma da promoção da saúde;<br><br>f) o compromisso brasileiro com a Assembléia Mundial para o Envelhecimento de 2002, cujo Plano de Madri fundamenta-se em: (a) participação ativa dos idosos na sociedade, no desenvolvimento e na luta contra a pobreza; (b) fomento à saúde e bem-estar na velhice: promoção do envelhecimento saudável; e (c) criação de um entorno propício e favorável ao envelhecimento; e<br><br>g) escassez de recursos sócio-educativos e de saúde direcionados ao atendimento ao idoso;<br><br>A necessidade de enfrentamento de desafios como:<br><br>a) a escassez de estruturas de cuidado intermediário ao idoso no SUS, ou seja, estruturas de suporte qualificado para idosos e seus familiares destinadas a promover intermediação segura entre a alta hospitalar e a ida para o domicílio;<br><br>b) número insuficiente de serviços de cuidado domiciliar ao idoso frágil previsto no Estatuto do Idoso. Sendo a família, via de regra, a executora do cuidado ao idoso, evidencia-se a necessidade de se estabelecer um suporte qualificado e constante aos responsáveis por esses cuidados, tendo a atenção básica por meio da Estratégia Saúde da Família um papel fundamental;<br><br>c) a escassez de equipes multiprofissionais e interdisciplinares com conhecimento em envelhecimento e saúde da pessoa idosa; e<br><br>d) a implementação insuficiente ou mesmo a falta de implementação das Redes de Assistência à Saúde do Idoso.<br><br>2. Justificativa<br><br>O Brasil envelhece de forma rápida e intensa. No Censo de 2000, contava com mais de 14,5 milhões de idosos (IBGE, 2002), em sua maioria com baixo nível socioeconômico e educacional e com uma alta prevalência de doenças crônicas e causadoras de limitações funcionais e de incapacidades (Lima-Costa et al, 2003; Ramos, 2002). A cada ano, 650 mil novos idosos são incorporados à população brasileira (IBGE, 2000). Essa transição demográfica repercute na área da saúde, em relação à necessidade de (re)organizar os modelos assistenciais (Lima-Costa & Veras, 2003). A maior causa de mortalidade entre idosos brasileiros é o acidente vascular cerebral (Lima-Costa et al., 2000). Na transição epidemiológica brasileira ocorrem incapacidades resultantes do não-controle de fatores de risco preveníveis (Lima-Costa et al., 2003).<br><br>O sistema de saúde brasileiro tradicionalmente está organizado para atender à saúde materno-infantil e não tem considerado o envelhecimento como uma de suas prioridades. Uma importante conseqüência do aumento do número de pessoas idosas em uma população é que esses indivíduos provavelmente apresentarão um maior número de doenças e/ou condições crônicas que requerem mais serviços sociais e médicos e por mais tempo (Firmo et al, 2003). Isso já pode ser notado, uma vez que a população idosa, que hoje representa cerca de 9% da população, consome mais de 26% dos recursos de internação hospitalar no SUS (Lima-Costa et al, 2000). Além disso, é notável a carência de profissionais qualificados para o cuidado ao idoso, em todos os níveis de atenção.<br><br>Outro fato importante a ser considerado é que saúde para a população idosa não se restringe ao controle e à prevenção de agravos de doenças crônicas não-transmissíveis. Saúde da pessoa idosa é a interação entre a saúde física, a saúde mental, a independência financeira, a capacidade funcional e o suporte social (Ramos, 2002).<br><br>As políticas públicas de saúde, objetivando assegurar atenção a toda população, têm dado visibilidade a um segmento populacional até então pouco notado pela saúde pública - os idosos e as idosas com alto grau de dependência funcional -. É possível a criação de ambientes físicos, sociais e atitudinais que possibilitem melhorar a saúde das pessoas com incapacidades tendo como uma das metas ampliar a participação social dessas pessoas na sociedade (Lollar & Crews, 2002). Por isso mesmo, é imprescindível oferecer cuidados sistematizados e adequados a partir dos recursos físicos, financeiros e humanos de que se dispõe hoje.<br><br>2.1. O Grande Desafio: o Envelhecimento Populacional em Condição de Desigualdade Social e de Gênero<br><br>Envelhecimento populacional é definido como a mudança na estrutura etária da população, o que produz um aumento do peso relativo das pessoas acima de determinada idade, considerada como definidora do início da velhice (Carvalho & Garcia, 2003). No Brasil, é definida como idosa a pessoa que tem 60 anos ou mais de idade (BRASIL, 2003).<br><br>Nos últimos 60 anos, o número absoluto de pessoas com 60 anos ou mais de idade aumentou nove vezes (Beltrão, Camarano e Kanso, 2004). Não só a população brasileira está envelhecendo, mas a proporção da população “mais idosa”, ou seja, a de 80 anos ou mais de idade, também está aumentando, alterando a composição etária dentro do próprio grupo. Significa dizer que a população idosa também está envelhecendo (Camarano et al, 1999). Em 2000, esse segmento representou 12,6% do total da população idosa brasileira. Isso leva a uma heterogeneidade do segmento idoso brasileiro, havendo no grupo pessoas em pleno vigor físico e mental e outras em situações de maior vulnerabilidade (Camarano et al, 2004).<br><br>O envelhecimento é também uma questão de gênero. Cinqüenta e cinco por cento da população idosa são formados por mulheres. A proporção do contingente feminino é tanto mais expressiva quanto mais idoso for o segmento. Essa predominância feminina se dá em zonas urbanas. Nas rurais, predominam os homens, o que pode resultar em isolamento e abandono dessas pessoas (Camarano et al, 2004; Camarano et al, 1999; Saad, 1999).<br><br>Quanto ao local de moradia, os idosos podem estar no ambiente familiar ou em instituições de longa permanência para idosos (ILPI). Cuidados institucionais não são prática generalizada nas sociedades latinas. É consenso entre as mais variadas especialidades científicas que a permanência dos idosos em seus núcleos familiares e comunitários contribui para o seu bem-estar (Camarano & Pasinato, 2004). No entanto, os dados referentes à população idosa institucionalizada no Brasil são falhos. Em 2002, a Comissão de Direitos Humanos da Câmara dos Deputados publicou o relatório “V Caravana Nacional de Direitos Humanos: uma amostra da Realidade dos Abrigos e Asilos de Idosos no Brasil”. De acordo com o relatório, havia cerca de 19.000 idosos institucionalizados em todo o País, o que representa 0,14% do total de idosos brasileiros. É de se esperar que esse número seja bem maior levando-se em conta que muitas das instituições asilares não são cadastradas e que grande parte funciona na clandestinidade.<br><br>A heterogeneidade do grupo de idosos, seja em termos etários, de local de moradia ou socioeconômicos, acarreta demandas diferenciadas, o que tem rebatimento na formulação de políticas públicas para o segmento (Camarano et al, 2004).<br><br>O envelhecimento populacional desafia a habilidade de produzir políticas de saúde que respondam às necessidades das pessoas idosas. A proporção de usuários idosos de todos os serviços prestados tende a ser cada vez maior, quer pelo maior acesso às informações do referido grupo etário, quer pelo seu expressivo aumento relativo e absoluto na população brasileira. (Lima-Costa & Veras, 2003).<br><br>Além disso, os idosos diferem de acordo com a sua história de vida, com seu grau de independência funcional e com a demanda por serviços mais ou menos específicos. Todos necessitam, contudo, de uma avaliação pautada no conhecimento do processo de envelhecimento e de suas peculiaridades e adaptada à realidade sócio-cultural em que estão inseridos. Faz-se, portanto, necessário que os serviços que prestam atendimento a idosos respondam a necessidades específicas e distingam-se pela natureza da intensidade dos serviços que ofereçam.<br><br>Cumpre notar que os idosos são potenciais consumidores de Serviços de Saúde e de Assistência. Esse grupo sabidamente apresenta uma grande carga de doenças crônicas e incapacitantes, quando comparado a outros grupos etários (Lima-Costa et al, 2003a; Lima-Costa et al, 2003b; Caldas, 2003). Disso resulta uma demanda crescente por serviços sociais e de saúde (Lima-Costa & Veras, 2003).<br><br>2.2. Contextualização: Responder às Demandas das Pessoas Idosas mais Frágeis dentre a População em Maior Risco de Vulnerabilidade<br><br>O envelhecimento populacional cursa com o aumento de doenças e condições que podem levar a incapacidade funcional. Para Verbrugge & Jette (1994), a incapacidade funcional é a dificuldade experimentada em realizar atividades em qualquer domínio da vida devido a um problema físico ou de saúde. Ela também pode ser entendida como a distância entre a dificuldade apresentada e os recursos pessoais e ambientais de que dispõe para superá-la (Hébert, 2003). Incapacidade é mais um processo do que um estado estático (Iezzoni, 2002). A Organização Mundial de Saúde (OMS) em sua Classificação Internacional de Funções, Incapacidade e Saúde (CIF, 2001) vê a incapacidade e as funções de uma pessoa como a interação dinâmica entre condições de saúde - doenças, lesões, traumas etc - e fatores contextuais, incluindo atributos pessoais e ambientais. A dependência é a expressão da dificuldade ou incapacidade em realizar uma atividade específica por causa de um problema de saúde (Hébert, 2003). No entanto, cabe enfatizar que a existência de uma incapacidade funcional, independentemente de sua origem, é o que determina a necessidade de um cuidador (Néri & Sommerhalder, 2002).<br><br>Incapacidade funcional e limitações físicas, cognitivas e sensoriais não são conseqüências inevitáveis do envelhecimento. A prevalência da incapacidade aumenta com a idade, mas a idade sozinha não prediz incapacidade (Lollar & Crews, 2002). Mulheres, minorias e pessoas de baixo poder socioeconômico são particularmente vulneráveis (Freedman, Martin e Schoeni, 2002). Independentemente de sua etiologia, pessoas com incapacidade estão em maior risco para problemas de saúde e afins (Lollar & Crews, 2002). A presença de incapacidade é ônus para o indivíduo, para a família, para o sistema de saúde e para a sociedade (Giacomin et al., 2004).<br><br>Estudos brasileiros de base populacional em idosos apontam a existência de incapacidade entre idosos em cifras que variam de 2 a 45% dos idosos (Giacomin et al., 2005; Duarte, 2003; Lima-Costa, 2003; Rosa et al; 2003), dependendo da idade e do sexo.<br><br>Assim, torna-se imprescindível incluir a condição funcional ao se formularem políticas para a saúde dos idosos e responder, prioritariamente, às pessoas idosas que já apresentem alta dependência.<br><br>3. Diretrizes<br><br>Não se fica velho aos 60 anos. O envelhecimento é um processo natural que ocorre  ao longo de toda a experiência de vida do ser humano, por meio de escolhas e de circunstâncias. O preconceito contra a velhice e a negação da sociedade quanto a esse fenômeno colaboram para a dificuldade de se pensar políticas específicas para esse grupo. Ainda há os que pensam que se investe na infância e se gasta na velhice. Deve ser um compromisso de todo gestor em saúde compreender que, ainda que os custos de hospitalizações e cuidados prolongados sejam elevados na parcela idosa, também aí  está se investindo na velhice “Quando o envelhecimento é aceito como um êxito, o aproveitamento da competência, experiência e dos recursos humanos dos grupos mais velhos é assumido com naturalidade, como uma vantagem para o crescimento de sociedades humanas maduras e plenamente integradas” (Plano de Madri, Artigo 6º).<br><br>Envelhecer, portanto, deve ser com saúde, de forma ativa, livre de qualquer tipo de dependência funcional, o que exige promoção da saúde em todas as idades. Importante acrescentar que muitos idosos brasileiros envelheceram e envelhecem apesar da falta de recursos e da falta de cuidados específicos de promoção e de prevenção em saúde. Entre esses estão os idosos que vivem abaixo da linha de pobreza, analfabetos, os seqüelados de acidentes de trabalho, os amputados por arteriopatias, os hemiplégicos, os idosos com síndromes demenciais, e para eles também é preciso achar respostas e ter ações específicas.<br><br>São apresentadas abaixo as diretrizes da Política Nacional de Saúde da Pessoa Idosa:<br><br>a) promoção do envelhecimento ativo e saudável;<br><br>b) atenção integral, integrada à saúde da pessoa idosa;<br><br>c) estímulo às ações intersetoriais, visando à integralidade da atenção;<br><br>d) provimento de recursos capazes de assegurar qualidade da atenção à saúde da pessoa idosa;<br><br>e) estímulo à participação e fortalecimento do controle social;<br><br>f) formação e educação permanente dos profissionais de saúde do SUS na área de saúde da pessoa idosa;<br><br>g) divulgação e informação sobre a Política Nacional de Saúde da Pessoa Idosa para profissionais de saúde, gestores e usuários do SUS;<br><br>h) promoção de cooperação nacional e internacional das experiências na atenção à saúde da pessoa idosa; e<br><br>i) apoio ao desenvolvimento de  estudos e pesquisas.<br><br>3.1. Promoção do Envelhecimento Ativo e Saudável<br><br>A promoção do envelhecimento ativo, isto é, envelhecer mantendo a capacidade funcional e a autonomia, é reconhecidamente a meta de toda ação de saúde. Ela permeia todas as ações desde o pré-natal até a fase da velhice. A abordagem do envelhecimento ativo baseia-se no reconhecimento dos direitos das pessoas idosas e nos princípios de independência, participação, dignidade, assistência e auto-realização determinados pela Organização das Nações Unidas (WHO, 2002). Para tanto é importante entender que as pessoas idosas constituem um grupo heterogêneo. Também será necessário vencer preconceitos e discutir mitos arraigados em nossa cultura. Os profissionais de saúde e a comunidade devem perceber que a prevenção e a promoção de saúde não é privilégio apenas dos jovens. A promoção não termina quando se faz 60 anos e as ações de prevenção, sejam elas primárias, secundárias ou terciárias, devem ser incorporadas à atenção à saúde, em todas as idades.<br><br>Envelhecimento bem sucedido pode ser entendido a partir de seus três componentes: (a) menor probabilidade de doença; (b) alta capacidade funcional física e mental; e (c) engajamento social ativo com a vida (Kalache & Kickbush, 1997; Rowe & Kahn, 1997; Healthy People 2000). O Relatório Healthy People 2000 da OMS enfatiza em seus objetivos: aumentar os anos de vida saudável, reduzir disparidades na saúde entre diferentes grupos populacionais e assegurar o acesso a serviços preventivos de saúde. Além disso, é preciso incentivar e equilibrar a responsabilidade pessoal – cuidado consigo mesmo – ambientes amistosos para a faixa etária e solidariedade entre gerações. As famílias e indivíduos devem se preparar para a velhice, esforçando-se para adotar uma postura de práticas saudáveis em todas as fases da vida (OMS, 2002).<br><br>Com a perspectiva de ampliar o conceito de “envelhecimento saudável”, a Organização Mundial da Saúde propõe “Envelhecimento Ativo: Uma Política de Saúde” (2005), ressaltando que o governo, as organizações internacionais e a sociedade civil devam implementar políticas e programas que melhorem a saúde, a participação e a segurança da pessoa idosa. Considerando o cidadão idoso não mais como passivo, mas como agente das ações a eles direcionadas, numa abordagem baseada em direitos, que valorize os aspectos da vida em comunidade, identificando o potencial para o bem-estar físico, social e mental ao longo do curso da vida.<br><br>Aproveitar todas as oportunidades para:<br><br>a) desenvolver e valorizar  o atendimento acolhedor e resolutivo à pessoa idosa, baseado em critérios de risco;<br><br>b) informar sobre seus direitos, como ser acompanhado por pessoas de sua rede social (livre escolha) e quem são os profissionais que cuidam de sua saúde;<br><br>c) valorizar e respeitar a velhice;<br><br>d) estimular a solidariedade para com esse grupo etário;<br><br>e) realizar ações de prevenção de acidentes no domicílio e nas vias públicas, como quedas e atropelamentos;<br><br>f) realizar ações integradas de combate à violência doméstica e institucional contra idosos e idosas;<br><br>g) facilitar a participação das pessoas idosas em equipamentos sociais, grupos de terceira idade, atividade física, conselhos de saúde locais e conselhos comunitários onde o idoso possa ser ouvido e apresentar suas demandas e prioridades;<br><br>h) articular ações e ampliar a integração entre as secretarias municipais e as estaduais de saúde, e os programas locais desenvolvidos para a difusão da atividade física e o combate ao sedentarismo;<br><br>i) promover a participação nos grupos operativos e nos grupos de convivência, com ações de promoção, valorização de experiências positivas e difusão dessas na rede, nortear e captar experiências;<br><br>j) informar e estimular a prática de nutrição balanceada, sexo seguro, imunização e hábitos de vida saudáveis;<br><br>k) realizar ações motivadoras ao abandono do uso de álcool, tabagismo e sedentarismo, em todos os níveis de atenção;<br><br>l) promover ações grupais integradoras com inserção de avaliação, diagnóstico e  tratamento da saúde mental da pessoa idosa;<br><br>m) reconhecer e incorporar as crenças e modelos culturais dos usuários em seus planos de cuidado, como forma de favorecer a adesão e a eficiência dos recursos e tratamentos disponíveis;<br><br>n) promover a saúde por meio de serviços preventivos primários, tais como a vacinação da população idosa, em conformidade com a Política Nacional de Imunização;<br><br>o) estimular programas de prevenção de agravos de doenças crônicas não-transmissíveis em indivíduos idosos;<br><br>p) implementar ações que contraponham atitudes preconceituosas e sejam esclarecedoras de que envelhecimento não é sinônimo de doença;<br><br>q) disseminar informação adequada sobre o envelhecimento para os profissionais de saúde e para toda a população, em especial para a população idosa;<br><br>r) implementar ações para reduzir hospitalizações e aumentar habilidades para o auto-cuidado dos usuários do SUS;<br><br>s) incluir ações de reabilitação para a pessoa idosa na atenção primária de modo a intervir no processo que origina a dependência funcional;<br><br>t) investir na promoção da saúde em todas as idades; e<br><br>u) articular as ações do Sistema Único de Saúde com o Sistema Único de Assistência Social – SUAS.<br><br>3.2. Atenção Integral e Integrada à Saúde da Pessoa Idosa<br><br>A atenção integral e integrada à saúde da pessoa idosa deverá ser estruturada nos moldes de uma linha de cuidados, com foco no usuário, baseado nos seus direitos, necessidades, preferências e habilidades; estabelecimento de fluxos bidirecionais funcionantes, aumentando e facilitando o acesso a todos os níveis de atenção; providos de condições essenciais - infra-estrutura física adequada, insumos e pessoal qualificado para a boa qualidade técnica.<br><br>Instrumentos gerenciais baseados em levantamento de dados sobre a capacidade funcional (inventários funcionais) e sócio-familiares da pessoa idosa deverão ser implementados pelos gestores municipais e estaduais do SUS, para que haja a participação de profissionais de saúde e usuários na construção de planos locais de ações para enfrentamento das dificuldades inerentes à complexidade de saúde da pessoa idosa.<br><br>Incorporação, na atenção básica, de mecanismos que promovam a melhoria da qualidade e aumento da resolutividade da atenção à pessoa idosa, com envolvimento dos profissionais da atenção básica e das equipes do Saúde da Família, incluindo a atenção domiciliar e ambulatorial, com incentivo à utilização de instrumentos técnicos validados, como de avaliação funcional e psicossocial.<br><br>Incorporação, na atenção especializada, de mecanismos que fortaleçam a atenção à pessoa idosa: reestruturação e implementação das Redes Estaduais de Atenção à Saúde da Pessoa Idosa, visando a integração efetiva com a atenção básica e os demais níveis de atenção, garantindo a integralidade da atenção, por meio do estabelecimento de fluxos de referência e contra-referência; e implementando de forma efetiva modalidades de atendimento que correspondam às necessidades da população idosa, com abordagem multiprofissional e interdisciplinar, sempre que possível. Contemplando também fluxos de retaguarda para a rede hospitalar e demais especialidades, disponíveis no Sistema Único de Saúde.<br><br>A prática de cuidados às pessoas idosas exige abordagem global, interdisciplinar e multidimensional, que leve em conta a grande interação entre os fatores físicos, psicológicos e sociais que influenciam a saúde dos idosos e a importância do ambiente no qual está inserido. A abordagem também precisa ser flexível e adaptável às necessidades de uma clientela específica. A identificação e o reconhecimento da rede de suporte social e de suas necessidades também faz parte da avaliação sistemática, objetivando prevenir e detectar precocemente o cansaço das pessoas que cuidam. As intervenções devem ser feitas e orientadas com vistas à promoção da autonomia e independência da pessoa idosa,  estimulando-a para o auto-cuidado. Grupos de auto-ajuda entre as pessoas que cuidam devem ser estimulados.<br><br>Uma abordagem preventiva e uma intervenção precoce são sempre preferíveis às intervenções curativas tardias. Para tanto, é necessária a vigilância de todos os membros da equipe de saúde, a aplicação de instrumentos de avaliação e de testes de triagem, para detecção de distúrbios cognitivos, visuais, de mobilidade, de audição, de depressão e do comprometimento precoce da funcionalidade, dentre outros.<br><br>O modelo de atenção à saúde baseado na assistência médica individual não se mostra eficaz na prevenção, educação e intervenção, em questões sociais, ficando muitas vezes restritas às complicações advindas de afecções crônicas. A cada etapa de intervenção os profissionais deverão considerar os anseios do idoso e de sua família. Pressupondo-se troca de informações e negociação das expectativas de cada um, levando-se em consideração elementos históricos do paciente, seus recursos individuais e sociais e aqueles da rede de suporte social disponível no local.<br><br>Um dos instrumentos gerenciais imprescindíveis é a implementação da avaliação funcional individual e coletiva. A partir da avaliação funcional coletiva determina-se a pirâmide de risco funcional, estabelecida com base nas informações relativas aos critérios de risco da população assistida pelas Unidades Básicas de Saúde (UBS) de cada município. Verifica-se como está distribuída a população adscrita à equipe do Saúde da Família, com base no inventário de risco funcional. Nos municípios que não dispõem da Estratégia Saúde da Família, as equipes das UBS poderão ser responsáveis por esse levantamento e acompanhamento. Assim, é possível conhecer qual a proporção de idosos que vivem em Instituições de Longa Permanência para Idosos, a proporção daqueles com alta dependência funcional – acamados –, a proporção dos que já apresentam alguma incapacidade funcional para atividades básicas da vida diária (AVD) – como tomar banho, vestir-se, usar o banheiro, transferir-se da cama para a cadeira, ser continente e alimentar-se com a própria mão – e qual a proporção de idosos independentes.<br><br>Considera-se o idoso independente aquele que é capaz de realizar sem dificuldades e sem ajuda todas as atividades de vida diária citadas acima. Esses idosos comporão a base da pirâmide.<br><br>Indivíduos idosos, mesmo sendo independentes, mas que apresentem alguma dificuldade nas atividades instrumentais de vida diária (AIVD) – preparar refeições, controlar a própria medicação, fazer compras, controlar o próprio dinheiro, usar o telefone, fazer pequenas tarefas e reparos domésticos e sair de casa sozinho utilizando uma condução coletiva –, são considerados idosos com potencial para desenvolver fragilidade e por isso merecerão atenção específica pelos profissionais de saúde e devem ser acompanhados com maior freqüência.<br><br>Considera-se idoso frágil ou em situação de fragilidade aquele que: vive em ILPI, encontra-se acamado, esteve hospitalizado recentemente por qualquer razão, apresente doenças sabidamente causadoras de incapacidade funcional – acidente vascular encefálico, síndromes demenciais e outras doenças neurodegenerativas, etilismo, neoplasia terminal, amputações de membros –, encontra-se com pelo menos uma incapacidade funcional básica, ou viva situações de violência doméstica. Por critério etário, a literatura estabelece que também é frágil o idoso com 75 anos ou mais de idade. Outros critérios poderão ser acrescidos ou modificados de acordo com as realidades locais.<br><br>Uma vez conhecida a condição de fragilidade, será necessário avaliar os recursos locais para lidar com ela, de modo a facilitar o cuidado domiciliar, incluir a pessoa que cuida no ambiente familiar como um parceiro da equipe de cuidados, fomentar uma rede de solidariedade para com o idoso frágil e sua família, bem como promover a reinserção da parcela idosa frágil na comunidade.<br><br>De acordo com a condição funcional da pessoa idosa serão estabelecidas ações de atenção primária, de prevenção – primária, secundária e terciária –, de reabilitação, para a recuperação da máxima autonomia funcional, prevenção do declínio funcional, e recuperação da saúde. Estarão incluídas nessas ações o controle e a prevenção de agravos de doenças crônicas não-transmissíveis.<br><br>Todo profissional deve procurar promover a qualidade de vida da pessoa idosa, quando chamado a atendê-la. É importante viver muito, mas é fundamental viver bem. Preservar a autonomia e a independência funcional das pessoas idosas deve ser a meta em todos os níveis de atenção.<br><br>Ficam estabelecidos, portanto, os dois grandes eixos norteadores para a integralidade de ações: o enfrentamento de fragilidades, da pessoa idosa, da família e do sistema de saúde; e a promoção da saúde e da integração social, em todos os níveis de atenção.<br><br>3.3. Estímulo às Ações Intersetoriais, visando à Integralidade da Atenção<br><br>A prática da intersetorialidade pressupõe o reconhecimento de parceiros e de órgãos governamentais e não-governamentais que trabalham com a população idosa. A organização do cuidado intersetorial a essa população evita duplicidade de ações, corrige distorções e potencializa a rede de solidariedade.<br><br>As ações intersetoriais visando à integralidade da atenção à saúde da pessoa idosa devem ser promovidas e implementadas, considerando as características e as necessidades locais.<br><br>3.4. Provimento de Recursos Capazes de Assegurar Qualidade da Atenção à Saúde da Pessoa Idosa<br><br>Deverão ser definidas e pactuadas com os estados, o Distrito Federal e os municípios as formas de financiamento que ainda não foram regulamentadas, para aprimoramento da qualidade técnica da atenção à saúde prestada à pessoa idosa. Os mecanismos e os fluxos de financiamento devem ter por base as programações ascendentes de estratégias que possibilitem a valorização do cuidado humanizado ao indivíduo idoso. Abaixo são apresentados os itens prioritários para a pactuação:<br><br>a) provimento de insumos, de suporte em todos os níveis de atenção, prioritariamente na atenção domiciliar inclusive medicamentos;<br><br>b) provimento de recursos para adequação de estrutura física dos serviços próprios do SUS;<br><br>c) provimento de recursos para ações de qualificação e de capacitação de recursos humanos, e incremento da qualidade técnica dos profissionais de saúde do SUS na atenção à pessoa idosa;<br><br>d) produção de material de divulgação e informativos sobre a Política Nacional de Saúde da Pessoa Idosa, normas técnicas e operacionais, protocolos e manuais de atenção, para profissionais de saúde, gestores e usuários do SUS;<br><br>e) implementação de procedimento ambulatorial específico para a avaliação global do idoso; e<br><br>f) determinação de critérios mínimos de estrutura, processo e resultados, com vistas a melhorar o atendimento à população idosa, aplicáveis às unidades de saúde do SUS, de modo que a adequação a esses critérios seja incentivada e mereça reconhecimento.<br><br>3.5. Estímulo à Participação e Fortalecimento do Controle Social<br><br>Deve-se estimular a inclusão nas Conferências Municipais e Estaduais de Saúde de temas relacionados à atenção à população idosa, incluindo o estímulo à participação de cidadãos e cidadãs idosos na formulação e no controle social das ações deliberadas nessas Conferências.<br><br>Devem ser estimulados e implementados os vínculos dos serviços de saúde com os seus usuários, privilegiando os núcleos familiares e comunitários, criando, assim, condições para uma efetiva participação e controle social da parcela idosa da população.<br><br>3.6. Divulgação e Informação sobre a Política Nacional de Saúde da Pessoa Idosa para Profissionais de Saúde, Gestores e Usuários do SUS<br><br>As medidas a serem adotadas buscarão:<br><br>a) incluir a PNSPI na agenda de atividades da comunicação social do SUS;<br><br>b) produzir material de divulgação, tais como cartazes, cartilhas, folhetos e vídeos;<br><br>c) promover ações de informação e divulgação da atenção à saúde da pessoa idosa, respeitando as especificidades regionais e culturais do País e direcionadas aos trabalhadores, aos gestores, aos conselheiros de saúde, bem como aos docentes e discentes da área de saúde e à comunidade em geral;<br><br>d) apoiar e fortalecer ações inovadoras de informação e divulgação sobre a atenção à saúde da pessoa idosa em diferentes linguagens culturais;<br><br>e) identificar, articular e apoiar experiências de educação popular, informação e comunicação em atenção à saúde da pessoa idosa; e<br><br>f) prover apoio técnico e/ou financeiro a projetos de qualificação de profissionais que atuam na Estratégia Saúde da Família e no Programa de Agentes Comunitários de Saúde, para atuação na área de informação, comunicação e educação popular em atenção à saúde da pessoa idosa.<br><br>3.7. Promoção de Cooperação Nacional e Internacional das Experiências na Atenção à Saúde da Pessoa Idosa<br><br>Devem-se fomentar medidas que visem à promoção de cooperação nacional e internacional das experiências bem sucedidas na área do envelhecimento, no que diz respeito à atenção à saúde da pessoa idosa, à formação técnica, à educação em saúde e a pesquisas.<br><br>3.8. Apoio ao Desenvolvimento de Estudos e Pesquisas<br><br>Apoiar o desenvolvimento de estudos e pesquisas que avaliem a qualidade e aprimorem a atenção de saúde à pessoa idosa. Identificar e estabelecer redes de apoio com instituições formadoras, associativas e representativas, universidades, faculdades e órgãos públicos nas três esferas, visando:<br><br>a) fomentar pesquisas em envelhecimento e saúde da pessoa idosa;<br><br>b) identificar e apoiar estudos/pesquisas relativos ao envelhecimento e à saúde da pessoa idosa existentes no Brasil, com o objetivo de socializar, divulgar e embasar novas investigações;<br><br>c) criar banco de dados de pesquisadores e pesquisas em envelhecimento e saúde da pessoa idosa realizadas no Brasil, interligando-o com outros bancos de abrangência internacional;<br><br>d) identificar e divulgar as potenciais linhas de financiamento – Ministério da Ciência e Tecnologia, Fundações Estaduais de Amparo à Pesquisa, terceiro setor e outros – para a pesquisa em envelhecimento e saúde da pessoa idosa;<br><br>e) apoiar a realização de estudo sobre representações sociais, junto a usuários e profissionais de saúde sobre a saúde da pessoa idosa;<br><br>f) priorizar as linhas de pesquisas em envelhecimento e saúde da pessoa idosa a serem implementadas pelo SUS, visando o aprimoramento e a consolidação da atenção à saúde da pessoa idosa no SUS; e<br><br>g) implementar um banco de dados nacional com resultados de avaliação funcional da população idosa brasileira.<br><br>4. Responsabilidades Institucionais<br><br>Caberá aos gestores do SUS, em todos os níveis, de forma articulada e conforme suas competências específicas, prover os meios e atuar para viabilizar o alcance do propósito desta Política Nacional de Saúde da Pessoa Idosa.<br><br>4.1. Gestor Federal<br><br>a) elaborar normas técnicas referentes à atenção à saúde da pessoa idosa no SUS;<br><br>b) definir recursos orçamentários e financeiros para a implementação desta Política, considerando que o financiamento do Sistema Único de Saúde é de competência das três esferas de governo;<br><br>c) estabelecer diretrizes para a qualificação e educação permanente em saúde da pessoa idosa;<br><br>d) manter articulação com os estados e municípios para apoio à implantação e supervisão das ações;<br><br>e) promover articulação intersetorial para a efetivação desta Política Nacional;<br><br>f) estabelecer instrumentos e indicadores para o acompanhamento e avaliação do impacto da implantação/implementação desta Política;<br><br>g) divulgar a Política Nacional de Saúde da Pessoa Idosa; e<br><br>h) estimular pesquisas nas áreas de interesse do envelhecimento e da atenção à saúde da pessoa idosa, nos moldes do propósito e das diretrizes desta Política.<br><br>4.2. Gestor Estadual<br><br>a) elaborar normas técnicas referentes à atenção à saúde da pessoa idosa no SUS;<br><br>b) definir recursos orçamentários e financeiros para a implementação desta Política, considerando que o financiamento do Sistema Único de Saúde é de competência das três esferas de governo;<br><br>c) Discutir e pactuar na Comissão Intergestores Bipartite (CIB) as estratégias e metas a serem alcançadas por essa Política a cada ano;<br><br>d) promover articulação intersetorial para a efetivação da Política;<br><br>e) implementar as diretrizes da educação permanente e qualificação em consonância com a realidade loco regional;<br><br>f) estabelecer instrumentos e indicadores para o acompanhamento e a avaliação do impacto da implantação/implementação desta Política;<br><br>g) manter articulação com municípios para apoio à implantação e supervisão das ações;<br><br>h) divulgar a Política Nacional de Saúde da Pessoa Idosa;<br><br>i) exercer a vigilância sanitária no tocante a Saúde da Pessoa Idosa e a ações decorrentes no seu âmbito; e<br><br>j) apresentar e aprovar proposta de inclusão da Política Nacional de Saúde da Pessoa Idosa no Conselho Estadual de Saúde.<br><br>4.3. Gestor Municipal:<br><br>a) elaborar normas técnicas referentes à atenção à saúde da pessoa idosa no SUS;<br><br>b) definir recursos orçamentários e financeiros para a implementação desta Política, considerando que o financiamento do Sistema Único de Saúde é de competência das três esferas de governo;<br><br>c) discutir e pactuar na Comissão Intergestores Bipartite (CIB) as estratégias e metas a serem alcançadas por essa Política a cada ano;<br><br>d) promover articulação intersetorial para a efetivação da Política;<br><br>e) estabelecer mecanismos para a qualificação dos profissionais do sistema local de saúde;<br><br>f) estabelecer instrumentos de gestão e indicadores para o acompanhamento e a avaliação do impacto da implantação/implementação da Política;<br><br>g) divulgar a Política Nacional de Saúde da Pessoa Idosa; e<br><br>h) apresentar e aprovar proposta de inclusão da Política de Saúde da Pessoa Idosa  no Conselho Municipal de Saúde.<br><br>5. Articulação Intersetorial<br><br>As diretrizes aqui definidas implicam o desenvolvimento de um amplo conjunto de ações, que requerem o compartilhamento de responsabilidades com outros setores. Nesse sentido, os gestores do SUS deverão estabelecer, em suas respectivas áreas de abrangência, processos de articulação permanente, visando ao estabelecimento de parcerias e a integração institucional que viabilizem a consolidação de compromissos multilaterais efetivos. Será buscada, igualmente, a participação de diferentes segmentos da sociedade, que estejam direta ou indiretamente relacionadas com a presente Política. No âmbito federal, o Ministério da Saúde articulará com os diversos setores do Poder Executivo em suas respectivas competências, de modo a alcançar os objetivos a seguir explicitados.<br><br>5.1. Educação<br><br>a) inclusão nos currículos escolares de disciplinas que abordem o processo do envelhecimento, a desmistificação da senescência, como sendo diferente de doença ou de incapacidade, valorizando a pessoa idosa e divulgando as medidas de promoção e prevenção de saúde em todas as faixas etárias;<br><br>b) adequação de currículos, metodologias e material didático de formação de profissionais na área da saúde, visando ao atendimento das diretrizes fixadas nesta Política;<br><br>c) incentivo à criação de Centros Colaboradores de Geriatria e Gerontologia nas instituições de ensino superior, que possam atuar de forma integrada com o SUS, mediante o estabelecimento de referência e contra-referência de ações e serviços para o atendimento integral dos indivíduos idosos e a capacitação de equipes multiprofissionais e interdisciplinares, visando à qualificação contínua do pessoal de saúde nas áreas de gerência, planejamento, pesquisa e assistência à pessoa idosa; e<br><br>d) discussão e readequação de currículos e programas de ensino nas instituições de ensino superior abertas para a terceira idade, consoante às diretrizes fixadas nesta Política.<br><br>5.2. Previdência Social<br><br>a) realização de estudos e pesquisas de cunho epidemiológico junto aos segurados, relativos às doenças e agravos mais prevalentes nesta faixa etária, sobretudo quanto aos seus impactos no indivíduo, na família, na sociedade, na previdência social e no setor saúde; e<br><br>b) elaboração de programa de trabalho conjunto direcionado aos indivíduos idosos segurados, consoante às diretrizes fixadas nesta Política.<br><br>5.3. Sistema Único de Assistência Social:<br><br>a) reconhecimento do risco social da pessoa idosa como fator determinante de sua condição de saúde;<br><br>b) elaboração de inquérito populacional para levantamento e estratificação das condições de risco social da população idosa brasileira;<br><br>c) elaboração de medidas, com o apontamento de soluções, para abordagem da população idosa sob risco social;<br><br>d) criação de mecanismos de monitoramento de risco social individual, de fácil aplicabilidade e utilização por profissionais da atenção básica do SUS e do SUAS;<br><br>e) difusão de informações relativas à preservação da saúde e à prevenção ou recuperação de incapacidades;<br><br>f) inclusão das diretrizes aqui estabelecidas em seus programas de educação continuada;<br><br>g) implantação de política de atenção integral aos idosos residentes em Instituições de Longa Permanência para Idosos;<br><br>h) promoção da formação de grupos sócio-educativos e de auto-ajuda entre os indivíduos idosos,  principalmente para aqueles com doenças e agravos mais prevalentes nesta faixa etária;<br><br>i) implantação e implementação de Centros de Convivência e Centros-Dia, conforme previsto no Decreto nº 1948/96;<br><br>j) apoio à construção de Políticas Públicas de Assistência Social que considerem as pessoas, suas circunstâncias e o suporte social e que atuem como aliadas no processo de desenvolvimento humano e social, e não como tuteladora e assistencialista, tanto na proteção social básica, como na proteção social especial;<br><br>k) compromisso com a universalização do direito, inclusão social, eqüidade, descentralização e municipalização das ações, respeitando a dignidade do cidadão e sua autonomia, favorecendo o acesso à informação, aos benefícios e aos serviços de qualidade, bem como à convivência familiar e comunitária; e<br><br>l) desenvolvimento de ações de enfrentamento à pobreza.<br><br>5.4. Trabalho e Emprego:<br><br>a) elaboração, implantação e implementação de programas de preparação para a aposentadoria nos setores público e privado;<br><br>b) implantação de ações para a eliminação das discriminações no mercado de trabalho e a criação de condições que permitam a inserção da pessoa idosa na vida socioeconômica das comunidades; e<br><br>c) levantamento dos indivíduos idosos já aposentados e que retornaram ao mercado de trabalho, identificando as condições em que atuam no mercado, de forma a coibir abusos e explorações.<br><br>5.5. Desenvolvimento Urbano:<br><br>a) implantação de ações para o cumprimento das leis de acessibilidade (Decreto Lei nº 5296/2004), de modo a auxiliar na manutenção e no apoio à independência funcional da pessoa idosa; e<br><br>b) promoção de ações educativas dirigidas aos agentes executores e beneficiários de programas habitacionais quanto aos riscos ambientais à capacidade funcional dos indivíduos idosos.<br><br>5.6. Transportes:<br><br>a) implantação de ações que permitam e/ou facilitem o deslocamento do cidadão idoso, sobretudo aquele que já apresenta dificuldades de locomoção, tais como elevatórias para acesso aos ônibus na porta de hospitais, rampas nas calçadas, bancos mais altos nas paradas de ônibus. Em conformidade com a Lei da Acessibilidade, Decreto Lei nº 5296, de 2 de dezembro de 2004.<br><br>5.7. Justiça e Direitos Humanos:<br><br>a) promoção e defesa dos direitos da pessoa idosa, no tocante às questões de saúde, mediante o acompanhamento da aplicação das disposições contidas na Lei nº 8.842/94 e seu regulamento (Decreto nº 1.948/96), bem como a Lei nº 10.741/2003, que estabelece o Estatuto do Idoso.<br><br>5.8. Esporte e Lazer<br><br>a) estabelecimento de parceria para a implementação de programas de atividades físicas e recreativas destinados às pessoas idosas.<br><br>5.9.Ciência e Tecnologia:<br><br>fomento à pesquisa na área do envelhecimento, da geriatria e da gerontologia, por intermédio do Conselho Nacional de Desenvolvimento Científico e Tecnológico (CNPq), e demais órgãos de incentivo à pesquisa, contemplando estudos e pesquisas que estejam, prioritariamente, alinhados com as diretrizes propostas nesta Política.<br><br>6. Acompanhamento e Avaliação<br><br>A operacionalização desta Política compreenderá a sistematização de processo contínuo de acompanhamento e avaliação, que permita verificar o alcance de seu propósito – e, conseqüentemente, o seu impacto sobre a saúde dos indivíduos idosos –, bem como proceder a eventuais adequações que se fizerem necessárias.<br><br>Esse processo exigirá a definição de critérios, parâmetros, indicadores e metodologia específicos, capazes de evidenciar, também, a repercussão das medidas levadas a efeito por outros setores, que resultaram da ação articulada preconizada nesta Política, bem como a observância dos compromissos internacionais assumidos pelo País em relação à atenção à saúde dos indivíduos idosos.<br><br>É importante considerar que o processo de acompanhamento e avaliação referido será apoiado, sobretudo para a aferição de resultados no âmbito interno do setor, pelas informações produzidas pelos diferentes planos, programas, projetos, ações e/ou atividades decorrentes desta Política Nacional.<br><br>Além da avaliação nos contextos anteriormente identificados, voltados principalmente para a verificação do impacto das medidas sobre a saúde dos indivíduos idosos, buscar-se-á investigar a repercussão desta Política na qualidade de vida deste segmento populacional.<br><br>Nesse particular, buscar-se-á igualmente conhecer em que medida a Política Nacional de Saúde da Pessoa Idosa tem contribuído para a concretização dos princípios e diretrizes do SUS, na conformidade do Artigo 7º da Lei nº 8.080/90, entre os quais, destacam-se aqueles relativos à integralidade da atenção, à preservação da autonomia das pessoas e ao uso da epidemiologia no estabelecimento de prioridades (respectivamente incisos II, III e VII). Paralelamente, deverá ser observado, ainda, se:<br><br>a) o potencial dos serviços de saúde e as possibilidades de utilização pelo usuário estão sendo devidamente divulgados para a população de forma geral e, principalmente, à população idosa;<br><br>b) as ações, programas, projetos e atividades que operacionalizam esta Política estão sendo desenvolvidos de forma descentralizada, considerando a direção única em cada esfera de gestão; e<br><br>c) a participação dos indivíduos idosos nas diferentes instâncias do SUS está sendo incentivada e facilitada.<br><br>' },
        ];
    }
    DireitosProvider.prototype.loadAll = function () {
        return Promise.resolve(this.direitos);
    };
    ;
    DireitosProvider.prototype.getByID = function (id) {
        for (var i = 0; i < (this.direitos).length; i++) {
            if (this.direitos[i].code == id) {
                return Promise.resolve(this.direitos[i]);
            }
        }
    };
    ;
    DireitosProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_http__["a" /* Http */]])
    ], DireitosProvider);
    return DireitosProvider;
}());

//# sourceMappingURL=direitos.js.map

/***/ }),

/***/ 123:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AddPanturrilhaPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ionic_native_sqlite__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_toast__ = __webpack_require__(44);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__resultado_panturrilha_resultado_panturrilha__ = __webpack_require__(56);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__edit_data_edit_data__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__angular_forms__ = __webpack_require__(15);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




//import { ListPanturrilhaPage } from '../list-panturrilha/list-panturrilha';



//import {Validators, FormBuilder, FormGroup } from '@angular/forms';
//import { PerfilProvider} from '../provider/perfil/perfil'

var AddPanturrilhaPage = /** @class */ (function () {
    function AddPanturrilhaPage(navCtrl, navParams, sqlite, toast, alertCtrl, formbuilder) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.sqlite = sqlite;
        this.toast = toast;
        this.alertCtrl = alertCtrl;
        this.formbuilder = formbuilder;
        this.data = { rowid: 1, nascimento: "", sexo: "", nome: "", imagem: "" };
        this.photo = "assets/imgs/user.png";
        this.panturrilha = { data_exame: "", recomendacao: "", id_perfil: "", circunferencia: "", id_perfil2: "" };
        this.perfils = [];
        this.formgroup = formbuilder.group({
            //nome: ['',Validators.required,Validators.minLength(5)]
            pan: ['', __WEBPACK_IMPORTED_MODULE_6__angular_forms__["f" /* Validators */].compose([__WEBPACK_IMPORTED_MODULE_6__angular_forms__["f" /* Validators */].maxLength(7), __WEBPACK_IMPORTED_MODULE_6__angular_forms__["f" /* Validators */].minLength(2), __WEBPACK_IMPORTED_MODULE_6__angular_forms__["f" /* Validators */].min(15), __WEBPACK_IMPORTED_MODULE_6__angular_forms__["f" /* Validators */].max(99), __WEBPACK_IMPORTED_MODULE_6__angular_forms__["f" /* Validators */].pattern('[^-]+'), __WEBPACK_IMPORTED_MODULE_6__angular_forms__["f" /* Validators */].required])]
        });
        this.pan = this.formgroup.controls['pan'];
    }
    AddPanturrilhaPage.prototype.ionViewDidLoad = function () {
        this.getPerfil();
        this.getUser(1);
    };
    AddPanturrilhaPage.prototype.infoPanturrilha = function () {
        var alert = this.alertCtrl.create({
            title: 'Perímetro da panturrilha',
            message: '<div class="message">Essa é uma medida de triagem indicada para avaliação da massa muscular, cuja redução correlaciona-se com risco de desenvolver a síndrome chamada sarcopenia e desfechos desfavoráveis. sarcopenia é uma síndrome clínica caracterizada pela perda progressiva e generalizada da massa, força e desempenho muscular, com risco de desfechos adversos como incapacidade física, baixa qualidade de vida e óbito. É diagnosticada através da densidade corporal total ou inferida através de medidas indiretas.</div>'
                + '<br><br><div class="referencia"><strong>Fonte:</strong><br>Brasil. Ministério da Saúde. Secretaria de Atenção à Saúde. Departamento de Ações Programáticas Estratégicas.<br><br>Manual para utilização da Caderneta de Saúde da Pessoa Idosa / Ministério da Saúde, Secretaria de Atenção à Saúde, Departamento de Ações Programáticas Estratégicas. – Brasília : Ministério da Saúde, 2018.<br>96 p. : il. <br><br>(ISBN 978-85-334-2613-9)</div>',
            cssClass: 'referencias',
            buttons: ['OK']
        });
        alert.present();
    };
    AddPanturrilhaPage.prototype.savePanturrilha = function () {
        var _this = this;
        this.sqlite.create({
            name: 'idoso.db',
            location: 'default'
        }).then(function (db) {
            db.executeSql('INSERT INTO teste_panturrilha VALUES(NULL,?,?,?,?)', [1, _this.panturrilha.circunferencia, _this.panturrilha.recomendacao, Date.now()])
                .then(function (res) {
                _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__resultado_panturrilha_resultado_panturrilha__["a" /* ResultadoPanturrilhaPage */]);
                console.log(res);
                _this.toast.show('Avaliação salva', '5000', 'center').subscribe(function (toast) {
                    //this.navCtrl.popToRoot();
                });
            })
                .catch(function (e) {
                console.log(e);
                _this.toast.show(e, '0', 'center').subscribe(function (toast) {
                    console.log(toast);
                });
            });
        }).catch(function (e) {
            console.log(e);
            _this.toast.show(e, '0', 'center').subscribe(function (toast) {
                console.log(toast);
            });
        });
    };
    AddPanturrilhaPage.prototype.getPerfil = function () {
        var _this = this;
        this.sqlite.create({
            name: 'idoso.db',
            location: 'default'
        }).then(function (db) {
            db.executeSql('SELECT rowid, nome FROM perfil ORDER BY nome DESC', [])
                .then(function (res) {
                _this.perfils = [];
                for (var i = 0; i < res.rows.length; i++) {
                    _this.perfils.push({ rowid: res.rows.item(i).rowid, nascimento: res.rows.item(i).nascimento, nome: res.rows.item(i).nome, sexo: res.rows.item(i).sexo });
                }
            })
                .catch(function (e) { return console.log(e); });
        }).catch(function (e) { return console.log(e); });
    };
    AddPanturrilhaPage.prototype.addPerfil2 = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__edit_data_edit_data__["a" /* EditDataPage */], {
            rowid: 1
        });
    };
    AddPanturrilhaPage.prototype.getUser = function (rowid) {
        var _this = this;
        this.sqlite.create({
            name: 'idoso.db',
            location: 'default'
        }).then(function (db) {
            db.executeSql('SELECT * FROM perfil WHERE rowid=?', [rowid])
                .then(function (res) {
                if (res.rows.length > 0) {
                    _this.data.rowid = res.rows.item(0).rowid;
                    _this.data.nome = res.rows.item(0).nome;
                    _this.data.sexo = res.rows.item(0).sexo;
                    _this.data.nascimento = res.rows.item(0).nascimento;
                    _this.data.imagem = res.rows.item(0).imagem;
                    _this.photo = res.rows.item(0).imagem;
                }
                if (_this.photo == "") {
                    _this.photo = "assets/imgs/user.png";
                }
            });
        });
    };
    AddPanturrilhaPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-add-panturrilha',template:/*ion-inline-start:"/home/rabaco/Documentos/projetos_ionic/idoso_aplicativo_final/src/pages/add-panturrilha/add-panturrilha.html"*/'<!--\n  Generated template for the AddPanturrilhaPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>Panturrilha</ion-title>\n    <ion-buttons end>\n      <div class="icone-imagem">\n        <img src="{{ photo }}" class="icone-imagem" (click)="addPerfil2()">\n      </div>\n    </ion-buttons>\n  </ion-navbar>\n\n</ion-header>\n<ion-content padding>\n<div class="centralizar-testes"><span class="icone-direita" (click)="infoPanturrilha()"><ion-icon name="md-information-circle"></ion-icon></span>\n<div class="titulo-testes">Instruções</div>\n<!--<img src="assets/imgs/panturrilha.png" class="imagem">-->\n<div class="texto-testes">\nA medida deverá ser realizada na perna esquerda, com uma fita métrica inelástica, na sua parte mais protuberante, com o paciente com a perna dobrada formando um ângulo de 90 graus com o joelho.\n</div>\n<!--<div class="fonte" padding><strong>Fonte:</strong><br>Caderneta de Saúde da Pessoa Idosa<br>Ministério da Saúde<br>2017</div>-->\n</div>\n<div class="quebra">\n\n  <form [formGroup]="formgroup" (ngSubmit)="savePanturrilha()" >\n    <ion-item>\n      <ion-label stacked class="campos-testes">Perímetro:</ion-label>\n      <ion-input type="number" placeholder="|" [(ngModel)]="panturrilha.circunferencia" name="circunferencia" formControlName="pan"></ion-input>\n    </ion-item>\n    <ion-item  *ngIf="pan.hasError(\'required\') && pan.touched" text-warp no-line>\n        <p class="error"> *Perímetro é um campo obrigado</p>\n      </ion-item>\n      <ion-item *ngIf="pan.hasError(\'max\') && pan.touched" text-warp no-line>\n          <p class="error">*Perímetro tem que ser menor que 100 cm</p>\n      </ion-item>\n      <ion-item *ngIf="pan.hasError(\'min\') && pan.touched" text-warp no-line>\n\n          <p class="error">*Perímetro tem que ter o valor maior ou igual a 15</p>\n      </ion-item>\n    <button ion-item round type="submit" block color="botao" class="botao-testes" [disabled]="formgroup.invalid">Concluir</button>\n  </form>\n</div>\n</ion-content>\n'/*ion-inline-end:"/home/rabaco/Documentos/projetos_ionic/idoso_aplicativo_final/src/pages/add-panturrilha/add-panturrilha.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["h" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["i" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1__ionic_native_sqlite__["a" /* SQLite */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_native_toast__["a" /* Toast */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["b" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_6__angular_forms__["a" /* FormBuilder */]])
    ], AddPanturrilhaPage);
    return AddPanturrilhaPage;
}());

//# sourceMappingURL=add-panturrilha.js.map

/***/ }),

/***/ 124:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AddImcPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_sqlite__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_toast__ = __webpack_require__(44);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__resultado_imc_resultado_imc__ = __webpack_require__(57);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_forms__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__edit_data_edit_data__ = __webpack_require__(18);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








/**
 * Generated class for the AddImcPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var AddImcPage = /** @class */ (function () {
    function AddImcPage(navCtrl, navParams, sqlite, toast, alertCtrl, formbuilder) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.sqlite = sqlite;
        this.toast = toast;
        this.alertCtrl = alertCtrl;
        this.formbuilder = formbuilder;
        this.data = { rowid: 1, nascimento: "", sexo: "", nome: "", imagem: "" };
        this.photo = "assets/imgs/user.png";
        this.imc = { data_exame: "", recomendacao: "", id_perfil: "", altura: "", peso: "", id_perfil2: "" };
        this.perfils = [];
        this.formgroup = formbuilder.group({
            //nome: ['',Validators.required,Validators.minLength(5)]
            pes: ['', __WEBPACK_IMPORTED_MODULE_5__angular_forms__["f" /* Validators */].compose([__WEBPACK_IMPORTED_MODULE_5__angular_forms__["f" /* Validators */].maxLength(7), __WEBPACK_IMPORTED_MODULE_5__angular_forms__["f" /* Validators */].minLength(2), __WEBPACK_IMPORTED_MODULE_5__angular_forms__["f" /* Validators */].max(200), __WEBPACK_IMPORTED_MODULE_5__angular_forms__["f" /* Validators */].min(30), __WEBPACK_IMPORTED_MODULE_5__angular_forms__["f" /* Validators */].pattern('[^,-]+'), __WEBPACK_IMPORTED_MODULE_5__angular_forms__["f" /* Validators */].required])],
            alt: ['', __WEBPACK_IMPORTED_MODULE_5__angular_forms__["f" /* Validators */].compose([__WEBPACK_IMPORTED_MODULE_5__angular_forms__["f" /* Validators */].maxLength(5), __WEBPACK_IMPORTED_MODULE_5__angular_forms__["f" /* Validators */].minLength(1), __WEBPACK_IMPORTED_MODULE_5__angular_forms__["f" /* Validators */].max(2.6), __WEBPACK_IMPORTED_MODULE_5__angular_forms__["f" /* Validators */].pattern('[^,-]+'), __WEBPACK_IMPORTED_MODULE_5__angular_forms__["f" /* Validators */].required])]
        });
        this.pes = this.formgroup.controls['pes'];
        this.alt = this.formgroup.controls['alt'];
    }
    AddImcPage.prototype.ionViewDidLoad = function () {
        this.getPerfil();
        this.getUser(1);
    };
    AddImcPage.prototype.infoImc = function () {
        var alert = this.alertCtrl.create({
            title: 'Índice de massa corporal',
            message: '<div class="message">Nos procedimentos de diagnóstico e acompanhamento do estado nutricional de pessoas idosas, o critério prioritário a ser utilizado deve ser a classificação do Índice de Massa Corporal (IMC), recomendado pela Organização Mundial de Saúde (OMS), considerando os pontos de corte diferenciados para a população com 60 anos ou mais de idade. Assim, é importante conhecer algumas alterações fisiológicas características dessa fase do curso da vida.</div>'
                + '<br><br><div class="referencia"><strong>Fonte:</strong><br>Brasil. Ministério da Saúde. Secretaria de Atenção à Saúde. Departamento de Ações Programáticas Estratégicas.<br><br>Manual para utilização da Caderneta de Saúde da Pessoa Idosa / Ministério da Saúde, Secretaria de Atenção à Saúde, Departamento de Ações Programáticas Estratégicas. – Brasília : Ministério da Saúde, 2018.<br>96 p. : il. <br><br>(ISBN 978-85-334-2613-9)</div>',
            cssClass: 'referencias',
            buttons: ['OK']
        });
        alert.present();
    };
    AddImcPage.prototype.saveImc = function () {
        var _this = this;
        this.sqlite.create({
            name: 'idoso.db',
            location: 'default'
        }).then(function (db) {
            db.executeSql('INSERT INTO teste_imc VALUES(NULL,?,?,?,?,?)', [1, _this.imc.peso, _this.imc.altura, _this.imc.recomendacao, Date.now()])
                .then(function (res) {
                _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__resultado_imc_resultado_imc__["a" /* ResultadoImcPage */]);
                console.log(res);
                _this.toast.show('Avaliação salva', '5000', 'center').subscribe(function (toast) {
                    //this.navCtrl.popToRoot();
                });
            })
                .catch(function (e) {
                console.log(e);
                _this.toast.show(e, '5000', 'center').subscribe(function (toast) {
                    console.log(toast);
                });
            });
        }).catch(function (e) {
            console.log(e);
            _this.toast.show(e, '5000', 'center').subscribe(function (toast) {
                console.log(toast);
            });
        });
    };
    AddImcPage.prototype.getPerfil = function () {
        var _this = this;
        this.sqlite.create({
            name: 'idoso.db',
            location: 'default'
        }).then(function (db) {
            db.executeSql('SELECT rowid, nome FROM perfil ORDER BY nome DESC', [])
                .then(function (res) {
                _this.perfils = [];
                for (var i = 0; i < res.rows.length; i++) {
                    _this.perfils.push({ rowid: res.rows.item(i).rowid, nascimento: res.rows.item(i).nascimento, nome: res.rows.item(i).nome, sexo: res.rows.item(i).sexo });
                }
            })
                .catch(function (e) { return console.log(e); });
        }).catch(function (e) { return console.log(e); });
    };
    AddImcPage.prototype.addPerfil2 = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_6__edit_data_edit_data__["a" /* EditDataPage */], {
            rowid: 1
        });
    };
    AddImcPage.prototype.getUser = function (rowid) {
        var _this = this;
        this.sqlite.create({
            name: 'idoso.db',
            location: 'default'
        }).then(function (db) {
            db.executeSql('SELECT * FROM perfil WHERE rowid=?', [rowid])
                .then(function (res) {
                if (res.rows.length > 0) {
                    _this.data.rowid = res.rows.item(0).rowid;
                    _this.data.nome = res.rows.item(0).nome;
                    _this.data.sexo = res.rows.item(0).sexo;
                    _this.data.nascimento = res.rows.item(0).nascimento;
                    _this.data.imagem = res.rows.item(0).imagem;
                    _this.photo = res.rows.item(0).imagem;
                }
                if (_this.photo == "") {
                    _this.photo = "assets/imgs/user.png";
                }
            });
        });
    };
    AddImcPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-add-imc',template:/*ion-inline-start:"/home/rabaco/Documentos/projetos_ionic/idoso_aplicativo_final/src/pages/add-imc/add-imc.html"*/'<!--\n  Generated template for the AddImcPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>Indice de massa corporal </ion-title>\n    <ion-buttons end>\n      <div class="icone-imagem">\n        <img src="{{ photo }}" class="icone-imagem" (click)="addPerfil2()">\n      </div>\n    </ion-buttons>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n\n<div class="centralizar-testes"><span class="icone-direita" (click)="infoImc()"><ion-icon name="md-information-circle"></ion-icon></span>\n    <div class="titulo-testes">Instruções</div>\n<!--<img src="assets/imgs/imc.png" class="imagem">-->\n<div class="texto-testes">\n  Para medir a altura em metros, utilize uma fita métrica ou trena, desde a base, encostando no chão até o topo da cabeça.<br><br>\n  Para medir o peso, verifique se a balança está corretamente calibrada com o marcador no zero antes da pesagem.\n</div>\n<!--<div class="fonte" padding><strong>Fonte:</strong><br>Caderneta de Saúde da Pessoa Idosa<br>Ministério da Saúde<br>2017</div>-->\n\n</div>\n<div class="quebra">\n  <form (ngSubmit)="saveImc()" [formGroup]="formgroup" >  \n    <ion-item>  \n      <ion-label stacked class="campos-testes">Altura:</ion-label>\n      <ion-input type="number" placeholder="|" [(ngModel)]="imc.altura" name="altura"  formControlName="alt" ></ion-input>\n    </ion-item>\n    <ion-item  *ngIf="!imc.altura" text-warp no-lines>\n      <p class="error2">Exemplo: 1,70 ou 1.70</p>\n    </ion-item>\n    <ion-item  *ngIf="alt.hasError(\'required\') && alt.touched" text-warp no-lines>\n        <p class="error"> *Altura é um campo obrigado de ser preenchido</p>\n      </ion-item>\n      <ion-item *ngIf="alt.hasError(\'max\') && alt.touched" text-warp no-lines>\n          <p class="error">* Altura tem que ser menor que 2,6 metros</p>\n      </ion-item>\n    <ion-item>\n      <ion-label stacked class="campos-testes">Peso:</ion-label>\n      <ion-input type="number" placeholder="|" [(ngModel)]="imc.peso" name="peso"  formControlName="pes"></ion-input>\n    </ion-item>\n    <ion-item  *ngIf="!imc.peso" text-warp no-lines>\n      <p class="error2">Exemplo: 60,50 ou 60.50</p>\n    </ion-item>\n    <ion-item  *ngIf="pes.hasError(\'required\') && pes.touched" text-warp no-lines>\n        <p class="error"> *Peso é um campo obrigado de ser preenchido.</p>\n      </ion-item>\n      <ion-item *ngIf="pes.hasError(\'max\') && pes.touched" text-warp no-lines>\n          <p class="error">* peso tem que ser menor que 200 Kg.</p>\n      </ion-item>\n      <ion-item *ngIf="pes.hasError(\'min\') && pes.touched" text-warp no-lines>\n          <p class="error">* peso tem que ser maior que 30 Kg.</p>\n      </ion-item>\n    <button ion-item round type="submit" color="botao" block  class="botao-testes" [disabled]="formgroup.invalid">Concluir</button>\n  </form>\n</div>\n</ion-content>\n'/*ion-inline-end:"/home/rabaco/Documentos/projetos_ionic/idoso_aplicativo_final/src/pages/add-imc/add-imc.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__ionic_native_sqlite__["a" /* SQLite */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_native_toast__["a" /* Toast */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_5__angular_forms__["a" /* FormBuilder */]])
    ], AddImcPage);
    return AddImcPage;
}());

//# sourceMappingURL=add-imc.js.map

/***/ }),

/***/ 125:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AddVes13Page; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_sqlite__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_toast__ = __webpack_require__(44);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__resultado_ves13_resultado_ves13__ = __webpack_require__(58);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_forms__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__edit_data_edit_data__ = __webpack_require__(18);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









var AddVes13Page = /** @class */ (function () {
    function AddVes13Page(navCtrl, navParams, sqlite, toast, alertCtrl, formbuilder) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.sqlite = sqlite;
        this.toast = toast;
        this.alertCtrl = alertCtrl;
        this.formbuilder = formbuilder;
        this.data = { rowid: 1, nascimento: "", sexo: "", nome: "", imagem: "" };
        this.photo = "assets/imgs/user.png";
        this.ves13 = { data_exame: "", recomendacao: "", id_perfil: "", idade: "", percepcao_saude: "", limitacao1: "", limitacao2: "", limitacao3: "", limitacao4: "", limitacao5: "", limitacao6: "", incapacidade1: "", incapacidade2: "", incapacidade3: "", incapacidade4: "", incapacidade5: "", marcelo: "" };
        this.perfils = [];
        this.perfil = { idade: "" };
        this.categories = [];
        this.ida = 0;
        this.slide0Form = formbuilder.group({
            idad: ['', __WEBPACK_IMPORTED_MODULE_5__angular_forms__["f" /* Validators */].compose([__WEBPACK_IMPORTED_MODULE_5__angular_forms__["f" /* Validators */].maxLength(3), __WEBPACK_IMPORTED_MODULE_5__angular_forms__["f" /* Validators */].minLength(2), __WEBPACK_IMPORTED_MODULE_5__angular_forms__["f" /* Validators */].max(140), __WEBPACK_IMPORTED_MODULE_5__angular_forms__["f" /* Validators */].pattern('[^, .-]+'), __WEBPACK_IMPORTED_MODULE_5__angular_forms__["f" /* Validators */].required])]
        });
        this.idad = this.slide0Form.controls['idad'];
        this.slide1Form = formbuilder.group({
            percep: ['', __WEBPACK_IMPORTED_MODULE_5__angular_forms__["f" /* Validators */].required]
        });
        this.percep = this.slide1Form.controls['percep'];
        this.slide2Form = formbuilder.group({
            limita1: ['', __WEBPACK_IMPORTED_MODULE_5__angular_forms__["f" /* Validators */].required]
        });
        this.limita1 = this.slide2Form.controls['limita1'];
        this.slide3Form = formbuilder.group({
            limita2: ['', __WEBPACK_IMPORTED_MODULE_5__angular_forms__["f" /* Validators */].required]
        });
        this.limita2 = this.slide3Form.controls['limita2'];
        this.slide4Form = formbuilder.group({
            limita3: ['', __WEBPACK_IMPORTED_MODULE_5__angular_forms__["f" /* Validators */].required]
        });
        this.limita3 = this.slide4Form.controls['limita3'];
        this.slide5Form = formbuilder.group({
            limita4: ['', __WEBPACK_IMPORTED_MODULE_5__angular_forms__["f" /* Validators */].required]
        });
        this.limita4 = this.slide5Form.controls['limita4'];
        this.slide6Form = formbuilder.group({
            limita5: ['', __WEBPACK_IMPORTED_MODULE_5__angular_forms__["f" /* Validators */].required]
        });
        this.limita5 = this.slide6Form.controls['limita5'];
        this.slide7Form = formbuilder.group({
            limita6: ['', __WEBPACK_IMPORTED_MODULE_5__angular_forms__["f" /* Validators */].required]
        });
        this.limita6 = this.slide7Form.controls['limita6'];
        this.slide8Form = formbuilder.group({
            incapa1: ['', __WEBPACK_IMPORTED_MODULE_5__angular_forms__["f" /* Validators */].required]
        });
        this.incapa1 = this.slide8Form.controls['incapa1'];
        this.slide9Form = formbuilder.group({
            incapa2: ['', __WEBPACK_IMPORTED_MODULE_5__angular_forms__["f" /* Validators */].required]
        });
        this.incapa2 = this.slide9Form.controls['incapa2'];
        this.slide10Form = formbuilder.group({
            incapa3: ['', __WEBPACK_IMPORTED_MODULE_5__angular_forms__["f" /* Validators */].required]
        });
        this.incapa3 = this.slide10Form.controls['incapa3'];
        this.slide11Form = formbuilder.group({
            incapa4: ['', __WEBPACK_IMPORTED_MODULE_5__angular_forms__["f" /* Validators */].required]
        });
        this.incapa4 = this.slide11Form.controls['incapa4'];
        this.slide12Form = formbuilder.group({
            incapa5: ['', __WEBPACK_IMPORTED_MODULE_5__angular_forms__["f" /* Validators */].required]
        });
        this.incapa5 = this.slide12Form.controls['incapa5'];
    }
    AddVes13Page.prototype.ionViewDidLoad = function () {
        this.slides.lockSwipeToNext(true);
        this.getLimitacao();
        this.getIncapacidade1();
        this.getIncapacidade2();
        this.getIncapacidade3();
        this.getIncapacidade4();
        this.getIncapacidade5();
        this.getPercepcao();
        this.getPerfil();
        this.getUser(1);
    };
    AddVes13Page.prototype.infoVes13 = function () {
        var alert = this.alertCtrl.create({
            title: 'Índice de vulnerabilidade',
            message: '<div class="message">O VES-13 é um instrumento simples e eficaz, capaz de identificar o idoso vulnerável residente na comunidade, com base na idade, auto-percepção da saúde, presença de limitações físicas e incapacidades (SALIBA et al., 2001). É um questionário de fácil aplicabilidade, que pode ser respondido pelos próprios profissionais de saúde, pela pessoa idosa ou pelos familiares/cuidadores, dispensando a observação direta do usuário. Baseia-se no registro das habilidades necessárias para a realização das tarefas do cotidiano. Cada item recebe uma determinada pontuação e o somatório final pode variar de 0 a 10 pontos.</div>'
                + '<br><br><div class="referencia"><strong>Fonte:</strong><br>Brasil. Ministério da Saúde. Secretaria de Atenção à Saúde. Departamento de Ações Programáticas Estratégicas.<br><br>Manual para utilização da Caderneta de Saúde da Pessoa Idosa / Ministério da Saúde, Secretaria de Atenção à Saúde, Departamento de Ações Programáticas Estratégicas. – Brasília : Ministério da Saúde, 2018.<br>96 p. : il. <br><br>(ISBN 978-85-334-2613-9)</div>',
            cssClass: 'referencias',
            buttons: ['OK']
        });
        alert.present();
    };
    AddVes13Page.prototype.getPercepcao = function () {
        this.percepcao_saude = [
            { id: '1', lei: '', name: 'Excelente' },
            { id: '2', lei: '', name: 'Muito Bom' },
            { id: '3', lei: '', name: 'Boa' },
            { id: '4', lei: '', name: 'Regular' },
            { id: '5', lei: '', name: 'Ruim ' }
        ];
    };
    AddVes13Page.prototype.getLimitacao = function () {
        this.limitacoes = [
            { id: '1', lei: '', name: 'Nenhuma dificuldade' },
            { id: '2', lei: '', name: 'Pouca dificuldade' },
            { id: '3', lei: '', name: 'Média' },
            { id: '4', lei: '', name: 'Muita dificuldade' },
            { id: '5', lei: '', name: 'Incapaz ' }
        ];
    };
    AddVes13Page.prototype.getIncapacidade1 = function () {
        this.incapacidades1 = [
            { id: '1', lei: '', name: 'Sim' },
            { id: '2', lei: '', name: 'Não ou não faz compras por outros motivos que não a saúde.' }
        ];
    };
    AddVes13Page.prototype.getIncapacidade2 = function () {
        this.incapacidades2 = [
            { id: '1', lei: '', name: 'Sim' },
            { id: '2', lei: '', name: 'Não ou não controla o dinheiro por outros motivos que não a saúde.' }
        ];
    };
    AddVes13Page.prototype.getIncapacidade3 = function () {
        this.incapacidades3 = [
            { id: '1', lei: '', name: 'Sim' },
            { id: '2', lei: '', name: 'Não ou não caminha dentro de casa por outros motivos que não a saúde.' }
        ];
    };
    AddVes13Page.prototype.getIncapacidade4 = function () {
        this.incapacidades4 = [
            { id: '1', lei: '', name: 'Sim' },
            { id: '2', lei: '', name: 'Não ou não realiza tarefas domésticas leves por outros motivos que não a saúde.' }
        ];
    };
    AddVes13Page.prototype.getIncapacidade5 = function () {
        this.incapacidades5 = [
            { id: '1', lei: '', name: 'Sim' },
            { id: '2', lei: '', name: 'Não ou não toma banho sozinho(a) por outros motivos que não a saúde.' }
        ];
    };
    AddVes13Page.prototype.getPerfil = function () {
        var _this = this;
        this.sqlite.create({
            name: 'idoso.db',
            location: 'default'
        }).then(function (db) {
            db.executeSql('SELECT rowid, nome,nascimento FROM perfil where rowid=1', [])
                .then(function (res) {
                _this.perfils = [];
                for (var i = 0; i < res.rows.length; i++) {
                    var myDateString = new Date(res.rows.item(i).nascimento);
                    var milisecondsDiff = Date.now() - (myDateString.getTime());
                    var idade = Math.floor((milisecondsDiff) / (1000 * 3600 * 24) / 365);
                    _this.ida = idade;
                    _this.ves13.idade = "" + idade;
                    if (_this.ves13.idade == "NaN") {
                        _this.ves13.idade = "";
                    }
                    _this.perfils.push({ rowid: res.rows.item(i).rowid, nascimento: res.rows.item(i).nascimento, nome: res.rows.item(i).nome, sexo: res.rows.item(i).sexo, idade: idade });
                }
            })
                .catch(function (e) { return console.log(e); });
        }).catch(function (e) { return console.log(e); });
    };
    AddVes13Page.prototype.saveVes13 = function () {
        var _this = this;
        this.sqlite.create({
            name: 'idoso.db',
            location: 'default'
        }).then(function (db) {
            db.executeSql('INSERT INTO teste_ves13 VALUES(NULL,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)', [1, _this.ves13.idade, _this.ves13.percepcao_saude, _this.ves13.limitacao1, _this.ves13.limitacao2, _this.ves13.limitacao3, _this.ves13.limitacao4, _this.ves13.limitacao5, _this.ves13.limitacao6, _this.ves13.incapacidade1, _this.ves13.incapacidade2, _this.ves13.incapacidade3, _this.ves13.incapacidade4, _this.ves13.incapacidade5, Date.now()])
                .then(function (res) {
                _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__resultado_ves13_resultado_ves13__["a" /* ResultadoVes13Page */]);
                console.log(res);
                _this.toast.show('Avaliação salva', '5000', 'center').subscribe(function (toast) {
                    //this.navCtrl.popToRoot();
                });
            })
                .catch(function (e) {
                console.log(e);
                _this.toast.show(e, '5000', 'center').subscribe(function (toast) {
                    console.log(toast);
                });
            });
        }).catch(function (e) {
            console.log(e);
            _this.toast.show(e, '5000', 'center').subscribe(function (toast) {
                console.log(toast);
            });
        });
    };
    AddVes13Page.prototype.next = function () {
        this.slides.lockSwipeToNext(false);
        this.signupSlider.slideNext();
        this.slides.lockSwipeToNext(true);
    };
    AddVes13Page.prototype.prev = function () {
        this.signupSlider.slidePrev();
    };
    AddVes13Page.prototype.addPerfil2 = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_6__edit_data_edit_data__["a" /* EditDataPage */], {
            rowid: 1
        });
    };
    AddVes13Page.prototype.getUser = function (rowid) {
        var _this = this;
        this.sqlite.create({
            name: 'idoso.db',
            location: 'default'
        }).then(function (db) {
            db.executeSql('SELECT * FROM perfil WHERE rowid=?', [rowid])
                .then(function (res) {
                if (res.rows.length > 0) {
                    _this.data.rowid = res.rows.item(0).rowid;
                    _this.data.nome = res.rows.item(0).nome;
                    _this.data.sexo = res.rows.item(0).sexo;
                    _this.data.nascimento = res.rows.item(0).nascimento;
                    _this.data.imagem = res.rows.item(0).imagem;
                    _this.photo = res.rows.item(0).imagem;
                }
                if (_this.photo == "") {
                    _this.photo = "assets/imgs/user.png";
                }
            });
        });
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* Slides */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* Slides */])
    ], AddVes13Page.prototype, "slides", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('signupSlider'),
        __metadata("design:type", Object)
    ], AddVes13Page.prototype, "signupSlider", void 0);
    AddVes13Page = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-add-ves13',template:/*ion-inline-start:"/home/rabaco/Documentos/projetos_ionic/idoso_aplicativo_final/src/pages/add-ves13/add-ves13.html"*/'<ion-header>\n\n  <ion-navbar>\n    <ion-title>Vulnerabilidade</ion-title>\n    <ion-buttons end>\n        <div class="icone-imagem">\n          <img src="{{ photo }}" class="icone-imagem" (click)="addPerfil2()">\n        </div>\n      </ion-buttons>\n  </ion-navbar>\n</ion-header>\n<ion-content padding>\n    <ion-slides #signupSlider pager>\n          <ion-slide ><span class="icone-direita" (click)="infoVes13()"><ion-icon name="md-information-circle"></ion-icon></span>\n            <div class="titulo-testes" text-left>Instruções</div>\n          <div class="texto-testes" text-left text-wrap>\n          Responda as perguntas nas telas a seguir, clicando em próximo depois de cada resposta.  Ao final, você verá sua pontuação e a recomendação\n          </div>\n          <!--<div class="fonte" padding text-left><strong>Fonte:</strong><br>Caderneta de Saúde da Pessoa Idosa<br>Ministério da Saúde<br>2017</div>-->\n\n          <div class="rodape">\n          <button ion-item round  block color="botao" class="botao-testes" (click)="next()">Próximo</button>\n        </div>\n        </ion-slide>\n          <ion-slide >\n          <form [formGroup]="slide0Form">\n\n          <ion-item >\n          <ion-label stacked class="campos-testes">Idade:</ion-label>\n          <ion-input type="number" placeholder="|" [(ngModel)]="ves13.idade"  name="ves13.idade" formControlName="idad"></ion-input>\n        </ion-item>\n\n            <div class="rodape">\n            <button ion-item round  block color="botao" class="botao-testes" (click)="next()" [disabled]="slide0Form.invalid">Próximo</button>\n</div>\n               </form>\n        </ion-slide>\n        <ion-slide>\n            <form [formGroup]="slide1Form">\n            <div class="titulo-testes" text-left>\n              Auto-percepção de saúde\n            </div>\n              <div class="texto-testes" text-left text-wrap>\n                  Em geral, comparando com outras pessoas de sua idade, você diria que sua saúde é:\n                </div>\n                   <ion-list radio-group name="ves13.percepcao_saude" [(ngModel)]="ves13.percepcao_saude" formControlName="percep">\n                       <ion-item *ngFor="let category of percepcao_saude">\n                           <ion-label class="campos-testes"> {{category.name}}</ion-label>\n                           <ion-radio [value]="category.id" ></ion-radio>\n                       </ion-item>\n                   </ion-list>\n                   <div class="rodape">\n                   <button ion-item round  block color="botao" class="botao-testes" (click)="next()" [disabled]="slide1Form.invalid || slide0Form.invalid">Próximo</button>\n                 </div>\n      </form>\n</ion-slide>\n<ion-slide>\n    <form [formGroup]="slide2Form">\n    <div class="titulo-testes" text-left>Limitações físicas</div>\n    <div class="texto-testes"  text-wrap text-left>\n       Curvar-se, agachar ou ajoelhar-se?\n      </div>\n        <ion-list radio-group name="limitacao1" [(ngModel)]="ves13.limitacao1" formControlName="limita1">\n            <ion-item *ngFor="let limitacao of limitacoes"  >\n                <ion-label class="campos-testes"> {{ limitacao.name }}</ion-label>\n                <ion-radio [value]="limitacao.id"></ion-radio>\n            </ion-item>\n        </ion-list>\n        <div class="rodape">\n      <button ion-item round  block color="botao" class="botao-testes" (click)="next()" [disabled]="slide2Form.invalid || slide1Form.invalid || slide0Form.invalid">Próximo</button>\n    </div>\n      </form>\n</ion-slide>\n<ion-slide>\n    <form [formGroup]="slide3Form">\n      <div class="titulo-testes" text-left>Limitações físicas</div>\n    <div class="texto-testes"  text-wrap text-left>\n        Levantar ou carregar objetos com peso aproximado de 5kg?\n      </div>\n         <ion-list radio-group name="limitacao2" [(ngModel)]="ves13.limitacao2" formControlName="limita2">\n             <ion-item *ngFor="let limitacao of limitacoes">\n                 <ion-label class="campos-testes"> {{ limitacao.name }}</ion-label>\n                 <ion-radio [value]="limitacao.id"></ion-radio>\n             </ion-item>\n              </ion-list>\n              <div class="rodape">\n       <button ion-item round  block color="botao" class="botao-testes" (click)="next()" [disabled]="slide3Form.invalid || slide2Form.invalid || slide1Form.invalid || slide0Form.invalid">Próximo</button>\n</div>\n       </form>\n</ion-slide>\n<ion-slide>\n    <form [formGroup]="slide4Form">\n      <div class="titulo-testes" text-left>Limitações físicas</div>\n    <div class="texto-testes"  text-wrap text-left>Elevar ou estender os braços acima do nível do ombro</div>\n      <ion-list radio-group name="limitacao3" [(ngModel)]="ves13.limitacao3" formControlName="limita3">\n             <ion-item *ngFor="let limitacao of limitacoes">\n                <ion-label class="campos-testes"> {{ limitacao.name }}</ion-label>\n                <ion-radio [value]="limitacao.id"></ion-radio>\n             </ion-item>\n         </ion-list>\n         <div class="rodape">\n       <button ion-item round  block color="botao" class="botao-testes" (click)="next()" [disabled]="slide4Form.invalid || slide3Form.invalid || slide2Form.invalid || slide1Form.invalid || slide0Form.invalid">Próximo</button>\n</div>\n       </form>\n        </ion-slide>\n<ion-slide>\n    <form [formGroup]="slide5Form">\n        <div class="titulo-testes" text-left>Limitações físicas</div>\n    <div class="texto-testes" text-wrap text-left>\n        Escrever ou manusear e segurar pequenos objetos? </div>\n         <ion-list radio-group name="limitacao4" [(ngModel)]="ves13.limitacao4" formControlName="limita4">\n             <ion-item *ngFor="let limitacao of limitacoes">\n                 <ion-label class="campos-testes"> {{ limitacao.name }}</ion-label>\n                 <ion-radio [value]="limitacao.id"></ion-radio>\n             </ion-item>\n         </ion-list>\n         <div class="rodape">\n    <button ion-item round  block color="botao" class="botao-testes" (click)="next()" [disabled]="slide5Form.invalid || slide4Form.invalid || slide3Form.invalid || slide2Form.invalid || slide1Form.invalid || slide0Form.invalid">Próximo</button>\n</div>\n  </form>\n</ion-slide>\n  <ion-slide>\n      <form [formGroup]="slide6Form">\n        <div class="titulo-testes" text-left>Limitações físicas</div>\n      <div class="texto-testes" text-wrap text-left>\n          Andar 400 metros (aproximadamente quatro quarteirões) ?  </div>\n           <ion-list radio-group name="limitacao5" [(ngModel)]="ves13.limitacao5" formControlName="limita5">\n               <ion-item *ngFor="let limitacao of limitacoes">\n                   <ion-label class="campos-testes"> {{ limitacao.name }}</ion-label>\n                   <ion-radio [value]="limitacao.id"></ion-radio>\n               </ion-item>\n           </ion-list>\n           <div class="rodape">\n      <button ion-item round  block color="botao" class="botao-testes" (click)="next()" [disabled]="slide6Form.invalid || slide5Form.invalid || slide4Form.invalid || slide3Form.invalid || slide2Form.invalid || slide1Form.invalid || slide0Form.invalid">Próximo</button>\n</div>\n</form>\n    </ion-slide>\n    <ion-slide>\n        <form [formGroup]="slide7Form">\n          <div class="titulo-testes" text-left>Limitações físicas</div>\n        <div class="texto-testes" text-wrap text-left>\n            Fazer serviço doméstico pesado, como esfregar o chão ou limpar janelas?\n          </div>\n             <ion-list radio-group name="limitacao6" [(ngModel)]="ves13.limitacao6" formControlName="limita6">\n                 <ion-item *ngFor="let limitacao of limitacoes">\n                     <ion-label class="campos-testes"> {{ limitacao.name }}</ion-label>\n                     <ion-radio [value]="limitacao.id"></ion-radio>\n                 </ion-item>\n                </ion-list>\n                <div class="rodape">\n       <button ion-item round  block color="botao" class="botao-testes" (click)="next()" [disabled]="slide7Form.invalid || slide6Form.invalid || slide5Form.invalid || slide4Form.invalid || slide3Form.invalid || slide2Form.invalid || slide1Form.invalid || slide0Form.invalid">Próximo</button>\n</div>\n  </form>\n      </ion-slide>\n      <ion-slide>\n          <form [formGroup]="slide8Form">\n            <div class="titulo-testes" text-left>Incapacidades</div>\n          <div class="texto-testes" text-wrap text-left>\n            Por causa de sua saúde ou condição física, você deixou de fazer compras?\n            </div>\n               <ion-list radio-group name="incapacidade1" [(ngModel)]="ves13.incapacidade1" formControlName="incapa1">\n                   <ion-item *ngFor="let incapacidade1 of incapacidades1">\n                       <ion-label class="campos-testes" text-wrap> {{ incapacidade1.name}}</ion-label>\n                       <ion-radio [value]="incapacidade1.id"></ion-radio>\n                   </ion-item>\n                  </ion-list>\n                  <div class="rodape">\n         <button ion-item round  block color="botao" class="botao-testes" (click)="next()" [disabled]="slide8Form.invalid || slide7Form.invalid || slide6Form.invalid || slide5Form.invalid || slide4Form.invalid || slide3Form.invalid || slide2Form.invalid || slide1Form.invalid || slide0Form.invalid">Próximo</button>\n</div>\n    </form>\n        </ion-slide>\n        <ion-slide>\n            <form [formGroup]="slide9Form">\n              <div class="titulo-testes" text-left>Incapacidades</div>\n            <div class="texto-testes" text-left text-wrap>\n                Por causa de sua saúde ou condição física, você deixou de controlar seu dinheiro, seus gastos ou pagar contas?\n              </div>\n                 <ion-list radio-group name="incapacidade2" [(ngModel)]="ves13.incapacidade2" formControlName="incapa2">\n                     <ion-item *ngFor="let incapacidade2 of incapacidades2">\n                         <ion-label class="campos-testes" text-wrap> {{ incapacidade2.name }}</ion-label>\n                         <ion-radio [value]="incapacidade2.id"></ion-radio>\n                     </ion-item>\n                    </ion-list>\n                    <div class="rodape">\n           <button ion-item round  block color="botao" class="botao-testes" (click)="next()" [disabled]="slide9Form.invalid || slide8Form.invalid || slide7Form.invalid || slide6Form.invalid || slide5Form.invalid || slide4Form.invalid || slide3Form.invalid || slide2Form.invalid || slide1Form.invalid || slide0Form.invalid">Próximo</button>\n</div>\n      </form>\n          </ion-slide>\n          <ion-slide>\n              <form [formGroup]="slide10Form">\n                <div class="titulo-testes" text-left>Incapacidades</div>\n              <div class="texto-testes" text-wrap text-left>\n                Por causa de sua saúde ou condição física, você deixou de caminhar dentro de casa?\n                </div>\n                   <ion-list radio-group name="incapacidade3" [(ngModel)]="ves13.incapacidade3" formControlName="incapa3">\n                       <ion-item *ngFor="let incapacidade3 of incapacidades3">\n                           <ion-label class="campos-testes" text-wrap> {{ incapacidade3.name }}</ion-label>\n                           <ion-radio [value]="incapacidade3.id"></ion-radio>\n                       </ion-item>\n                      </ion-list>\n                      <div class="rodape">\n             <button ion-item round  block color="botao" class="botao-testes" (click)="next()" [disabled]="slide10Form.invalid || slide9Form.invalid || slide8Form.invalid || slide7Form.invalid || slide6Form.invalid || slide5Form.invalid || slide4Form.invalid || slide3Form.invalid || slide2Form.invalid || slide1Form.invalid || slide0Form.invalid">Próximo</button>\n</div>\n        </form>\n            </ion-slide>\n            <ion-slide>\n                <form [formGroup]="slide11Form">\n                  <div class="titulo-testes" text-left>Incapacidades</div>\n                <div class="texto-testes" text-wrap text-left>\n                    Por causa de sua saúde ou condição física, você deixou de realizar tarefas domésticas leves, como lavar louça ou fazer limpeza leve?\n                  </div>\n                     <ion-list radio-group name="incapacidade4" [(ngModel)]="ves13.incapacidade4" formControlName="incapa4">\n                         <ion-item *ngFor="let incapacidade4 of incapacidades4">\n                             <ion-label class="campos-testes" text-wrap> {{ incapacidade4.name }}</ion-label>\n                             <ion-radio [value]="incapacidade4.id"></ion-radio>\n                         </ion-item>\n                        </ion-list>\n                        <div class="rodape">\n               <button ion-item round  block color="botao" class="botao-testes" (click)="next()" [disabled]="slide11Form.invalid || slide10Form.invalid || slide9Form.invalid || slide8Form.invalid || slide7Form.invalid || slide6Form.invalid || slide5Form.invalid || slide4Form.invalid || slide3Form.invalid || slide2Form.invalid || slide1Form.invalid || slide0Form.invalid">Próximo</button>\n</div>\n          </form>\n              </ion-slide>\n              <ion-slide>\n                  <form [formGroup]="slide12Form">\n                    <div class="titulo-testes" text-left>Incapacidades</div>\n                  <div class="texto-testes" text-wrap text-left>\n                    Por causa de sua saúde ou condição física, você deixou de tomar banho sozinho(a)?\n                    </div>\n                       <ion-list radio-group name="incapacidade5" [(ngModel)]="ves13.incapacidade5" formControlName="incapa5">\n                           <ion-item *ngFor="let incapacidade5 of incapacidades5">\n                               <ion-label class="campos-testes" text-wrap> {{ incapacidade5.name }}</ion-label>\n                               <ion-radio [value]="incapacidade5.id"></ion-radio>\n                           </ion-item>\n                          </ion-list>\n<div class="rodape">\n                 <button ion-item round  block color="botao" class="botao-testes" (click)="saveVes13()" [disabled]="slide12Form.invalid || slide11Form.invalid || slide10Form.invalid || slide9Form.invalid || slide8Form.invalid || slide7Form.invalid || slide6Form.invalid || slide5Form.invalid || slide4Form.invalid || slide3Form.invalid || slide2Form.invalid || slide1Form.invalid || slide0Form.invalid">Concluir</button>\n</div>\n            </form>\n                </ion-slide>\n\n</ion-slides>\n</ion-content>\n'/*ion-inline-end:"/home/rabaco/Documentos/projetos_ionic/idoso_aplicativo_final/src/pages/add-ves13/add-ves13.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__ionic_native_sqlite__["a" /* SQLite */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_native_toast__["a" /* Toast */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_5__angular_forms__["a" /* FormBuilder */]])
    ], AddVes13Page);
    return AddVes13Page;
}());

//# sourceMappingURL=add-ves13.js.map

/***/ }),

/***/ 126:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ListPanturrilhaPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_sqlite__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__resultado_panturrilha_resultado_panturrilha__ = __webpack_require__(56);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_common__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__edit_data_edit_data__ = __webpack_require__(18);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



//import { AddPanturrilhaPage } from '../add-panturrilha/add-panturrilha';
//import { EditPanturrilhaPage } from '../edit-panturrilha/edit-panturrilha';



var ListPanturrilhaPage = /** @class */ (function () {
    function ListPanturrilhaPage(navCtrl, sqlite, alertCtrl, actionSheetCtrl, datepipe) {
        this.navCtrl = navCtrl;
        this.sqlite = sqlite;
        this.alertCtrl = alertCtrl;
        this.actionSheetCtrl = actionSheetCtrl;
        this.datepipe = datepipe;
        this.data = { rowid: 1, nascimento: "", sexo: "", nome: "", imagem: "" };
        this.photo = "assets/imgs/user.png";
        this.panturrilhas2 = [];
        this.nome_teste = "";
        this.nome_teste2 = "";
        this.num = 0;
        this.num2 = 0;
        this.linha = 0;
    }
    ListPanturrilhaPage_1 = ListPanturrilhaPage;
    ListPanturrilhaPage.prototype.ionViewDidLoad = function () {
        this.getData();
        this.getData2();
        this.getUser(1);
    };
    /*
     getData() : void
     {
         this.panturrilhas = [
           {id_panturrilha:'1', circunferencia:'33', data_exame:'2018-11-22'},
           {id_panturrilha:'2', circunferencia:'34', data_exame:'2018-11-22'},
           {id_panturrilha:'3', circunferencia:'35', data_exame:'2018-11-22'},
           {id_panturrilha:'4', circunferencia:'36', data_exame:'2018-11-22'},
           {id_panturrilha:'5', circunferencia:'37', data_exame:'2018-11-22'},
           {id_panturrilha:'6', circunferencia:'38', data_exame:'2018-11-23'},
           {id_panturrilha:'7', circunferencia:'39', data_exame:'2018-11-23'},
           {id_panturrilha:'8', circunferencia:'40', data_exame:'2018-11-23'},
           
    ];
      }
      */
    ListPanturrilhaPage.prototype.getData = function () {
        var _this = this;
        this.sqlite.create({
            name: 'idoso.db',
            location: 'default'
        }).then(function (db) {
            var linha = 2;
            db.executeSql('SELECT * FROM (select * from teste_panturrilha ORDER BY  data_exame DESC limit 6) order by data_exame ASC', [])
                .then(function (res) {
                _this.panturrilhas = [];
                _this.num = res.rows.length;
                for (var i = 0; i < res.rows.length; i++) {
                    var data_ex = new Date(res.rows.item(i).data_exame);
                    _this.data_final = _this.datepipe.transform(data_ex, 'dd/MM/yyyy');
                    var nome_teste = res.rows.item(i).nome;
                    if (nome_teste !== nome_teste2) {
                        var nome_teste2 = res.rows.item(i).nome;
                        linha = linha + 1;
                    }
                    var calculo_panturrilha = parseFloat(res.rows.item(i).circunferencia.toFixed(1));
                    var calculo_string = calculo_panturrilha.toString().replace(/[.]/gi, ",");
                    var myDateString = new Date(res.rows.item(i).nascimento);
                    var milisecondsDiff = Date.now() - (myDateString.getTime());
                    var idade = Math.floor((milisecondsDiff) / (1000 * 3600 * 24) / 365);
                    _this.panturrilhas.push({ id_panturrilha: res.rows.item(i).id_panturrilha, nome: res.rows.item(i).nome, data_exame: res.rows.item(i).data_exame, circunferencia: res.rows.item(i).circunferencia, circunferencia2: calculo_string, idade: idade });
                }
            })
                .catch(function (e) { return console.log(e); });
        }).catch(function (e) { return console.log(e); });
    };
    ListPanturrilhaPage.prototype.getData2 = function () {
        var _this = this;
        this.sqlite.create({
            name: 'idoso.db',
            location: 'default'
        }).then(function (db) {
            _this.linha = 2;
            db.executeSql('SELECT * FROM (select * from teste_panturrilha ORDER BY  data_exame ASC) order by data_exame DESC', [])
                .then(function (res) {
                _this.panturrilhas2 = [];
                _this.num2 = res.rows.length;
                for (var i = 0; i < res.rows.length; i++) {
                    //passa parametros para gerar o gráfico
                    var data_ex = new Date(res.rows.item(i).data_exame);
                    _this.data_final = _this.datepipe.transform(data_ex, 'dd/MM/yyyy');
                    _this.nome_teste = res.rows.item(i).nome;
                    var myDateString = new Date(res.rows.item(i).nascimento);
                    var milisecondsDiff = Date.now() - (myDateString.getTime());
                    var idade = Math.floor((milisecondsDiff) / (1000 * 3600 * 24) / 365);
                    _this.panturrilhas2.push({ id_panturrilha: res.rows.item(i).id_panturrilha, nome: res.rows.item(i).nome, data_exame: res.rows.item(i).data_exame, circunferencia: res.rows.item(i).circunferencia, idade: idade });
                }
            })
                .catch(function (e) { return console.log(e); });
        }).catch(function (e) { return console.log(e); });
    };
    ListPanturrilhaPage.prototype.editPanturrilha = function (id_panturrilha) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__resultado_panturrilha_resultado_panturrilha__["a" /* ResultadoPanturrilhaPage */], {
            id_panturrilha: id_panturrilha
        });
    };
    ListPanturrilhaPage.prototype.deletePanturrilha2 = function (id_panturrilha) {
        var _this = this;
        this.sqlite.create({
            name: 'idoso.db',
            location: 'default'
        }).then(function (db) {
            db.executeSql('DELETE FROM teste_panturrilha WHERE id_panturrilha=?', [id_panturrilha])
                .then(function (res) {
                _this.navCtrl.push(ListPanturrilhaPage_1);
                console.log(res);
            })
                .catch(function (e) { return console.log(e); });
        }).catch(function (e) { return console.log(e); });
    };
    ListPanturrilhaPage.prototype.deletePanturrilha = function (id_panturrilha) {
        var _this = this;
        var alert = this.alertCtrl.create({
            title: 'Tem certeza que quer excluir o resultado?',
            message: '',
            buttons: [
                {
                    text: 'Não',
                    role: 'cancel',
                    handler: function () {
                        //console.log('Cancel clicked');
                    }
                },
                {
                    text: 'Sim',
                    handler: function () {
                        // console.log('Buy clicked');
                        _this.deletePanturrilha2(id_panturrilha);
                    }
                }
            ]
        });
        alert.present();
    };
    ListPanturrilhaPage.prototype.addPerfil2 = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__edit_data_edit_data__["a" /* EditDataPage */], {
            rowid: 1
        });
    };
    ListPanturrilhaPage.prototype.getUser = function (rowid) {
        var _this = this;
        this.sqlite.create({
            name: 'idoso.db',
            location: 'default'
        }).then(function (db) {
            db.executeSql('SELECT * FROM perfil WHERE rowid=?', [rowid])
                .then(function (res) {
                if (res.rows.length > 0) {
                    _this.data.rowid = res.rows.item(0).rowid;
                    _this.data.nome = res.rows.item(0).nome;
                    _this.data.sexo = res.rows.item(0).sexo;
                    _this.data.nascimento = res.rows.item(0).nascimento;
                    _this.data.imagem = res.rows.item(0).imagem;
                    _this.photo = res.rows.item(0).imagem;
                }
                if (_this.photo == "") {
                    _this.photo = "assets/imgs/user.png";
                }
            });
        });
    };
    ListPanturrilhaPage = ListPanturrilhaPage_1 = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-list-panturrilha',template:/*ion-inline-start:"/home/rabaco/Documentos/projetos_ionic/idoso_aplicativo_final/src/pages/list-panturrilha/list-panturrilha.html"*/'<ion-header>\n    <ion-navbar>\n      <ion-title>\n        Panturrilha\n      </ion-title>\n      <ion-buttons end>\n        <div class="icone-imagem">\n          <img src="{{ photo }}" class="icone-imagem" (click)="addPerfil2()">\n        </div>\n      </ion-buttons>\n <!--\n        \n      <ion-buttons end>\n        <button ion-button icon-only (click)="addPanturrilha()">\n          <ion-icon name="add-circle"></ion-icon>\n        </button>\n      </ion-buttons>\n\n    -->\n    </ion-navbar>\n  </ion-header>\n\n  <ion-content padding >\n   <div class="titulo-testes">Resultados</div>\n   <ion-item>\n     <div style="text-align: center">\n      <div class="regiao_grafico">\n          <div class="numeros">45<br>40<br>35<br>30<br>25<br>20<br>15\n          </div>\n          <div class="fundo_grafico">\n          </div>\n          <div class="linhas">\n          </div>\n          <div *ngFor="let panturrilha of panturrilhas; let n=index" >\n\n            <div *ngIf="panturrilha.circunferencia<46">\n        <div class="calculo" [ngStyle]="{\'margin-top\': (390-((panturrilha.circunferencia)*8.9)) + \'px\', \'margin-left\': (27+(n*47.8)) + \'px\'}" *ngIf="n<6" (click)="editPanturrilha(panturrilha.id_panturrilha)">\n              <span class=\'texto-grafico\'>{{panturrilha.circunferencia2}}</span>\n              </div>\n              </div>\n              <div *ngIf="panturrilha.circunferencia>=46">\n                  <div class="calculo" [ngStyle]="{\'margin-left\': (27+(n*47.8)) + \'px\'}" *ngIf="n<6" (click)="editPanturrilha(panturrilha.id_panturrilha)" >\n                      <span class=\'texto-grafico\'>{{panturrilha.circunferencia2}}</span>\n                      </div>\n                      </div>\n             <div class="vertical-text"  [ngStyle]="{\'margin-left\': (4+(n*46)) + \'px\'}" *ngIf="n<6">\n                {{panturrilha.data_exame | date:"dd/MM/yyyy"}}\n             </div> \n          </div>\n          \n        </div>\n        </div>\n   </ion-item>\n   <ion-item>\n     <span class="legenda-principal">Legenda:</span><br><br>\n     <span class="legenda-interna"><img src="../assets/imgs/circulo_verde.png" class="circ">Acompanhamento de Rotina</span><br>\n     <span class="legenda-interna"><img src="../assets/imgs/circulo_amarelo.png" class="circ">Atenção</span><br>\n     <span class="legenda-interna"><img src="../assets/imgs/circulo_vermelho.png" class="circ">Ação</span><br>\n   </ion-item>\n   \n    <ion-list *ngFor="let panturrilha2 of panturrilhas2; let i=index"  >\n        <div class="titulo-testes" *ngIf="i==6">Resultados anteriores:</div>\n      <ion-item-sliding *ngIf="i>5" class="transparente ">\n        \n        <ion-item style="margin:0; padding:0; border-bottom: 1px solid; border-color:#B9B9B9" class="transparente">           \n        \n          <div class="quebra" (click)="editPanturrilha(panturrilha2.id_panturrilha)">\n           <span *ngIf="(panturrilha2.circunferencia >= 35)">\n             <div class="circulo_testes"><img src="assets/imgs/circulo_verde.png"></div><div class="texto-data">{{panturrilha2.data_exame | date:"dd/MM/yyyy"}}</div><div class="circulo-texto">Acompanhamento<br>de Rotina</div></span>\n            <span *ngIf="(panturrilha2.circunferencia >= 31) && (panturrilha2.circunferencia <35) "><div class="circulo_testes"><img src="assets/imgs/circulo_amarelo.png"></div><div class="texto-data">{{panturrilha2.data_exame | date:"dd/MM/yyyy"}}</div><div class="circulo-texto2">Atenção<br></div></span>\n            <span *ngIf="(panturrilha2.circunferencia < 31)"><div class="circulo_testes"><img src="assets/imgs/circulo_vermelho.png"></div><div class="texto-data">{{panturrilha2.data_exame | date:"dd/MM/yyyy"}}</div> <div class="circulo-texto2">Ação<br></div></span>\n            \n        \n            <br>\n              </div>\n               <div class="quebra" >\n            \n           \n\n            \n              <button ion-button color="danger" (click)="deletePanturrilha(panturrilha.id_panturrilha)">\n                <ion-icon name="trash"></ion-icon>\n              </button>\n            </div>\n           \n        </ion-item>\n       \n          </ion-item-sliding>\n          \n\n\n    </ion-list>\n  \n\n  </ion-content>\n  <ion-footer>\n  </ion-footer>\n'/*ion-inline-end:"/home/rabaco/Documentos/projetos_ionic/idoso_aplicativo_final/src/pages/list-panturrilha/list-panturrilha.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_2__ionic_native_sqlite__["a" /* SQLite */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* ActionSheetController */],
            __WEBPACK_IMPORTED_MODULE_4__angular_common__["d" /* DatePipe */]])
    ], ListPanturrilhaPage);
    return ListPanturrilhaPage;
    var ListPanturrilhaPage_1;
}());

//# sourceMappingURL=list-panturrilha.js.map

/***/ }),

/***/ 127:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ListImcPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_sqlite__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__resultado_imc_resultado_imc__ = __webpack_require__(57);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_common__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__edit_data_edit_data__ = __webpack_require__(18);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



//import { AddImcPage } from '../add-imc/add-imc';



var ListImcPage = /** @class */ (function () {
    function ListImcPage(navCtrl, sqlite, alertCtrl, actionSheetCtrl, datepipe) {
        this.navCtrl = navCtrl;
        this.sqlite = sqlite;
        this.alertCtrl = alertCtrl;
        this.actionSheetCtrl = actionSheetCtrl;
        this.datepipe = datepipe;
        this.data = { rowid: 1, nascimento: "", sexo: "", nome: "", imagem: "" };
        this.photo = "assets/imgs/user.png";
        this.imcs = [];
        this.imcs2 = [];
        this.nome_teste = "";
        this.nome_teste2 = "";
    }
    ListImcPage_1 = ListImcPage;
    ListImcPage.prototype.ionViewDidLoad = function () {
        this.getData();
        this.getData2();
        this.getUser(1);
    };
    ListImcPage.prototype.getData = function () {
        var _this = this;
        this.sqlite.create({
            name: 'idoso.db',
            location: 'default'
        }).then(function (db) {
            var linha = 2;
            db.executeSql('SELECT * from (select * FROM teste_imc  ORDER BY data_exame DESC limit 6)  ORDER BY data_exame ASC ', [])
                .then(function (res) {
                _this.imcs = [];
                for (var i = 0; i < res.rows.length; i++) {
                    //passa parametros para gerar o gráfico
                    var data_ex = new Date(res.rows.item(i).data_exame);
                    _this.data_final = _this.datepipe.transform(data_ex, 'dd/MM/yyyy HH:mm');
                    var nome_teste = res.rows.item(i).nome;
                    if (nome_teste !== nome_teste2) {
                        var nome_teste2 = res.rows.item(i).nome;
                        linha = linha + 1;
                    }
                    var finalBmi = res.rows.item(i).peso / (res.rows.item(i).altura * res.rows.item(i).altura);
                    var calculo_imc = parseFloat(finalBmi.toFixed(1));
                    var calculo_string = calculo_imc.toString().replace(/[.]/gi, ",");
                    if (!calculo_string.match(/,/)) {
                        calculo_string = calculo_imc.toString() + ",0";
                    }
                    var myDateString = new Date(res.rows.item(i).nascimento);
                    var milisecondsDiff = Date.now() - (myDateString.getTime());
                    var idade = Math.floor((milisecondsDiff) / (1000 * 3600 * 24) / 365);
                    _this.imcs.push({ id_imc: res.rows.item(i).id_imc, nome: res.rows.item(i).nome, data_exame: res.rows.item(i).data_exame, peso: res.rows.item(i).peso, altura: res.rows.item(i).altura, idade: idade, calculo_imc: calculo_imc, calculo_string: calculo_string });
                }
            })
                .catch(function (e) { return console.log(e); });
        }).catch(function (e) { return console.log(e); });
    };
    ListImcPage.prototype.getData2 = function () {
        var _this = this;
        this.sqlite.create({
            name: 'idoso.db',
            location: 'default'
        }).then(function (db) {
            var linha = 2;
            db.executeSql('SELECT * from (select * FROM teste_imc  ORDER BY data_exame ASC) ORDER BY data_exame DESC ', [])
                .then(function (res) {
                _this.imcs2 = [];
                for (var i = 0; i < res.rows.length; i++) {
                    //passa parametros para gerar o gráfico
                    //var data_ex=new Date(res.rows.item(i).data_exame);
                    //var data_final=data_ex.toDateString();
                    var data_ex = new Date(res.rows.item(i).data_exame);
                    _this.data_final = _this.datepipe.transform(data_ex, 'dd/MM/yyyy HH:mm');
                    var nome_teste = res.rows.item(i).nome;
                    /*
                                        this.lineChartData[0].data.push("21.9");
                                        this.lineChartData[1].data.push("50");
                                        this.lineChartData[2].data.push("26.9");
                    */
                    if (nome_teste !== nome_teste2) {
                        var nome_teste2 = res.rows.item(i).nome;
                        linha = linha + 1;
                        //   this.lineChartData[linha].label.push(res.rows.item(i).nome);
                    }
                    var finalBmi = res.rows.item(i).peso / (res.rows.item(i).altura * res.rows.item(i).altura);
                    var calculo_imc = parseFloat(finalBmi.toFixed(1));
                    var calculo_string = calculo_imc.toString().replace(/[.]/gi, ",");
                    //   this.lineChartLabels.push(data_final);
                    //   this.lineChartData[linha].data.push(calculo_imc);
                    var myDateString = new Date(res.rows.item(i).nascimento);
                    var milisecondsDiff = Date.now() - (myDateString.getTime());
                    var idade = Math.floor((milisecondsDiff) / (1000 * 3600 * 24) / 365);
                    //let idade=milisecondsDiff;
                    _this.imcs2.push({ id_imc: res.rows.item(i).id_imc, nome: res.rows.item(i).nome, data_exame: res.rows.item(i).data_exame, peso: res.rows.item(i).peso, altura: res.rows.item(i).altura, idade: idade, calculo_imc: calculo_imc, calculo_string: calculo_string });
                }
            })
                .catch(function (e) { return console.log(e); });
        }).catch(function (e) { return console.log(e); });
    };
    ListImcPage.prototype.editImc = function (id_imc) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__resultado_imc_resultado_imc__["a" /* ResultadoImcPage */], {
            id_imc: id_imc
        });
    };
    ListImcPage.prototype.deleteImc2 = function (id_imc) {
        var _this = this;
        this.sqlite.create({
            name: 'idoso.db',
            location: 'default'
        }).then(function (db) {
            db.executeSql('DELETE FROM teste_imc WHERE id_imc=?', [id_imc])
                .then(function (res) {
                console.log(res);
                _this.navCtrl.push(ListImcPage_1);
            })
                .catch(function (e) { return console.log(e); });
        }).catch(function (e) { return console.log(e); });
    };
    ListImcPage.prototype.deleteImc = function (id_imc) {
        var _this = this;
        var alert = this.alertCtrl.create({
            title: 'Tem certeza que quer excluir o resultado?',
            message: '',
            buttons: [
                {
                    text: 'Não',
                    role: 'cancel',
                    handler: function () {
                    }
                },
                {
                    text: 'Sim',
                    handler: function () {
                        _this.deleteImc2(id_imc);
                    }
                }
            ]
        });
        alert.present();
    };
    ListImcPage.prototype.addPerfil2 = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__edit_data_edit_data__["a" /* EditDataPage */], {
            rowid: 1
        });
    };
    ListImcPage.prototype.getUser = function (rowid) {
        var _this = this;
        this.sqlite.create({
            name: 'idoso.db',
            location: 'default'
        }).then(function (db) {
            db.executeSql('SELECT * FROM perfil WHERE rowid=?', [rowid])
                .then(function (res) {
                if (res.rows.length > 0) {
                    _this.data.rowid = res.rows.item(0).rowid;
                    _this.data.nome = res.rows.item(0).nome;
                    _this.data.sexo = res.rows.item(0).sexo;
                    _this.data.nascimento = res.rows.item(0).nascimento;
                    _this.data.imagem = res.rows.item(0).imagem;
                    _this.photo = res.rows.item(0).imagem;
                }
                if (_this.photo == "") {
                    _this.photo = "assets/imgs/user.png";
                }
            });
        });
    };
    ListImcPage = ListImcPage_1 = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-list-imc',template:/*ion-inline-start:"/home/rabaco/Documentos/projetos_ionic/idoso_aplicativo_final/src/pages/list-imc/list-imc.html"*/'<ion-header>\n    <ion-navbar>\n      <ion-title>\n        Índice de massa corporal\n      </ion-title>\n      <ion-buttons end>\n        <div class="icone-imagem">\n          <img src="{{ photo }}" class="icone-imagem" (click)="addPerfil2()">\n        </div>\n      </ion-buttons>\n      </ion-navbar>\n  </ion-header>\n\n  <ion-content padding >\n\n    <div class="titulo-testes">Resultados</div>\n\n        <ion-item>\n            <div class="regiao_grafico">\n                <div class="numeros">35<br>31<br>28<br>25<br>22<br>19<br>16\n                </div>\n                <div class="fundo_grafico">\n                </div>\n                <div class="linhas">\n                </div>\n                <div *ngFor="let imc of imcs; let n=index">\n                  <div *ngIf="imc.calculo_imc>16 && imc.calculo_imc<35">\n              <div class="calculo" [ngStyle]="{\'margin-top\': (478-((imc.calculo_imc)*14.0)) + \'px\', \'margin-left\': (27+(n*47.8)) + \'px\'}" *ngIf="n<6" (click)="editImc(imc.id_imc)">\n                    <span class=\'texto-grafico\'>{{imc.calculo_string}}</span>\n                    </div>\n                    </div>\n                    <div *ngIf="imc.calculo_imc>35">\n                        <div class="calculo" [ngStyle]="{\'margin-left\': (27+(n*47.8)) + \'px\'}" *ngIf="n<6" (click)="editImc(imc.id_imc)" >\n                            <span class=\'texto-grafico\'>{{imc.calculo_string}}</span>\n                            </div>\n                            </div>\n                            <div *ngIf="imc.calculo_imc<16">\n                              <div class="calculo" [ngStyle]="{\'margin-top\': (280*1) + \'px\', \'margin-left\': (27+(n*47.8)) + \'px\'}" *ngIf="n<6" (click)="editImc(imc.id_imc)" >\n                                  <span class=\'texto-grafico\'>{{imc.calculo_string}}</span>\n                                  </div>\n                                  </div>\n                                  <div class="vertical-text"  [ngStyle]="{\'margin-left\': (4+(n*45.8)) + \'px\'}"  *ngIf="imc.calculo_imc>16 && n<6">\n                                    {{imc.data_exame | date:"dd/MM/yyyy"}}\n                                 </div>\n                   <div class="vertical-text"  [ngStyle]="{\'margin-top\': (360*1) + \'px\',\'margin-left\': (4+(n*45.8)) + \'px\'}" *ngIf="imc.calculo_imc<16 && n<6">\n                      {{imc.data_exame | date:"dd/MM/yyyy"}}\n                   </div>\n                </div>\n                </div>\n         </ion-item>\n         <ion-item>\n          <span class="legenda-principal">Legenda:</span><br><br>\n          <span class="legenda-interna"><img src="../assets/imgs/circulo_verde.png" class="circ">Acompanhamento de Rotina</span><br>\n          <span class="legenda-interna"><img src="../assets/imgs/circulo_amarelo.png" class="circ">Atenção</span><br>\n          <span class="legenda-interna"><img src="../assets/imgs/circulo_vermelho.png" class="circ">Ação</span><br>\n        </ion-item>\n        <ion-list style="margin:0; padding:0; border-bottom: 1px solid; border-color:#B9B9B9" *ngFor="let imc2 of imcs2; let i=index">\n            <div class="titulo-testes" *ngIf="i == 6">Resultados anteriores:</div>\n            <ion-item *ngIf="i > 5">\n        \n          <div class="quebra" (click)="editImc(imc2.id_imc)">\n           <span *ngIf="(imc2.calculo_imc > 27 )"><div class="circulo_testes"><img src="assets/imgs/circulo_vermelho.png"></div> \n            <div class="texto-data">{{imc2.data_exame  | date:"dd/MM/yyyy"}}</div><div class="circulo-texto2">Sobre peso</div></span>\n            <span *ngIf="(imc2.calculo_imc >= 22) && (imc2.calculo_imc <=27) "><div class="circulo_testes"><img src="assets/imgs/circulo_verde.png"></div>\n              <div class="texto-data">{{imc2.data_exame  | date:"dd/MM/yyyy"}}</div>\n              <div class="circulo-texto2">Peso Adequado</div></span>\n            <span *ngIf="(imc2.calculo_imc < 22)"><div class="circulo_testes"><img src="assets/imgs/circulo_vermelho.png"></div>\n              <div class="texto-data">{{imc2.data_exame  | date:"dd/MM/yyyy"}}</div><div class="circulo-texto2">Baixo Peso</div></span>\n            <br>       \n            </div>\n          </ion-item>\n        </ion-list>\n  </ion-content>\n  <ion-footer>\n  </ion-footer>\n'/*ion-inline-end:"/home/rabaco/Documentos/projetos_ionic/idoso_aplicativo_final/src/pages/list-imc/list-imc.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_2__ionic_native_sqlite__["a" /* SQLite */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* ActionSheetController */],
            __WEBPACK_IMPORTED_MODULE_4__angular_common__["d" /* DatePipe */]])
    ], ListImcPage);
    return ListImcPage;
    var ListImcPage_1;
}());

//# sourceMappingURL=list-imc.js.map

/***/ }),

/***/ 128:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ListVes13Page; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_sqlite__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__resultado_ves13_resultado_ves13__ = __webpack_require__(58);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_common__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__edit_data_edit_data__ = __webpack_require__(18);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



//import { AddVes13Page } from '../add-ves13/add-ves13';



var ListVes13Page = /** @class */ (function () {
    function ListVes13Page(navCtrl, navParams, sqlite, alertCtrl, actionSheetCtrl, datepipe) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.sqlite = sqlite;
        this.alertCtrl = alertCtrl;
        this.actionSheetCtrl = actionSheetCtrl;
        this.datepipe = datepipe;
        this.data = { rowid: 1, nascimento: "", sexo: "", nome: "", imagem: "" };
        this.photo = "assets/imgs/user.png";
        this.ves13s = [];
        this.ves13s2 = [];
        this.pontuacaototal = 0;
        this.pontos_total = 0;
        this.linha = 0;
        this.nome_teste = "";
    }
    ListVes13Page_1 = ListVes13Page;
    ListVes13Page.prototype.ionViewDidLoad = function () {
        this.getData();
        this.getData2();
        this.getUser(1);
    };
    ListVes13Page.prototype.getData = function () {
        var _this = this;
        this.sqlite.create({
            name: 'idoso.db',
            location: 'default'
        }).then(function (db) {
            _this.linha = 1;
            db.executeSql('SELECT * FROM ( select * from teste_ves13 ORDER BY data_exame DESC limit 6) order by data_exame ASC ', [])
                .then(function (res) {
                _this.ves13s = [];
                for (var i = 0; i < res.rows.length; i++) {
                    //passa parametros para gerar o gráfico
                    var data_ex = new Date(res.rows.item(i).data_exame);
                    _this.data_final = _this.datepipe.transform(data_ex, 'dd/MM/yyyy HH:mm');
                    _this.nome_teste = res.rows.item(i).nome;
                    //soma parte 1
                    var idade = res.rows.item(i).idade;
                    var somaParcial = null;
                    var pontosIdade = null;
                    var pontosPercepcao = 0;
                    if (idade > 60 && idade < 74) {
                        pontosIdade = 0;
                    }
                    if (idade > 75 && idade < 84) {
                        pontosIdade = 1;
                    }
                    if (idade > 84) {
                        pontosIdade = 3;
                    }
                    if (res.rows.item(i).percepcao_saude >= 4) {
                        pontosPercepcao = 1;
                    }
                    somaParcial = pontosIdade + pontosPercepcao;
                    //limitacoes fisicas
                    var pontosLimitacao1 = 0;
                    var pontosLimitacao2 = 0;
                    var pontosLimitacao3 = 0;
                    var pontosLimitacao4 = 0;
                    var pontosLimitacao5 = 0;
                    var pontosLimitacao6 = 0;
                    if (res.rows.item(i).limitacao1 >= 4) {
                        pontosLimitacao1 = 1;
                    }
                    if (res.rows.item(i).limitacao2 >= 4) {
                        pontosLimitacao2 = 1;
                    }
                    if (res.rows.item(i).limitacao3 >= 4) {
                        pontosLimitacao3 = 1;
                    }
                    if (res.rows.item(i).limitacao4 >= 4) {
                        pontosLimitacao4 = 1;
                    }
                    if (res.rows.item(i).limitacao5 >= 4) {
                        pontosLimitacao5 = 1;
                    }
                    if (res.rows.item(i).limitacao6 >= 4) {
                        pontosLimitacao6 = 1;
                    }
                    var somaLimitacao = pontosLimitacao1 + pontosLimitacao2 + pontosLimitacao3 + pontosLimitacao4 + pontosLimitacao5 + pontosLimitacao6;
                    var maxLimicacao = somaLimitacao;
                    if (somaLimitacao > 2) {
                        maxLimicacao = 2;
                    }
                    //incapacidades Fisicas
                    var pontosIncapacidade1 = 0;
                    var pontosIncapacidade2 = 0;
                    var pontosIncapacidade3 = 0;
                    var pontosIncapacidade4 = 0;
                    var pontosIncapacidade5 = 0;
                    if (res.rows.item(i).incapacidade1 == 1) {
                        pontosIncapacidade1 = 4;
                    }
                    if (res.rows.item(i).incapacidade2 == 1) {
                        pontosIncapacidade2 = 4;
                    }
                    if (res.rows.item(i).incapacidade3 == 1) {
                        pontosIncapacidade3 = 4;
                    }
                    if (res.rows.item(i).incapacidade4 == 1) {
                        pontosIncapacidade4 = 4;
                    }
                    if (res.rows.item(i).incapacidade5 == 1) {
                        pontosIncapacidade5 = 4;
                    }
                    var somaIncapacidade = pontosIncapacidade1 + pontosIncapacidade2 + pontosIncapacidade3 + pontosIncapacidade4 + pontosIncapacidade5;
                    var maxIncapacidade = somaIncapacidade;
                    if (somaIncapacidade > 4) {
                        maxIncapacidade = 4;
                    }
                    var pontuacaofinal = maxIncapacidade + maxLimicacao + somaParcial;
                    _this.pontuacaototal = 0;
                    if (pontuacaofinal >= 3) {
                        _this.pontuacaototal = 3;
                    }
                    else {
                        _this.pontuacaototal = pontuacaofinal;
                    }
                    var pontos_final = somaParcial + maxLimicacao + maxIncapacidade;
                    var pontos_total = pontos_final;
                    if (pontos_final > 10) {
                        _this.pontos_total = 10;
                    }
                    _this.ves13s.push({ id_ves13: res.rows.item(i).id_ves13, nome: res.rows.item(i).nome, data_exame: res.rows.item(i).data_exame, pontos_total: pontos_total, pontuacaototal: pontos_total, idade: idade, incapacidade1: res.rows.item(i).incapacidade1 });
                }
            })
                .catch(function (e) { return console.log(e); });
        }).catch(function (e) { return console.log(e); });
    };
    ListVes13Page.prototype.getData2 = function () {
        var _this = this;
        this.sqlite.create({
            name: 'idoso.db',
            location: 'default'
        }).then(function (db) {
            _this.linha = 1;
            db.executeSql('SELECT * FROM ( select * from teste_ves13 ORDER BY data_exame ASC ) order by data_exame DESC', [])
                .then(function (res) {
                _this.ves13s2 = [];
                for (var i = 0; i < res.rows.length; i++) {
                    var data_ex = new Date(res.rows.item(i).data_exame);
                    _this.data_final = _this.datepipe.transform(data_ex, 'dd/MM/yyyy HH:mm');
                    _this.nome_teste = res.rows.item(i).nome;
                    //soma parte 1
                    var idade = res.rows.item(i).idade;
                    var somaParcial = null;
                    var pontosIdade = null;
                    var pontosPercepcao = 0;
                    if (idade > 60 && idade < 74) {
                        pontosIdade = 0;
                    }
                    if (idade > 75 && idade < 84) {
                        pontosIdade = 1;
                    }
                    if (idade > 84) {
                        pontosIdade = 3;
                    }
                    if (res.rows.item(i).percepcao_saude >= 4) {
                        pontosPercepcao = 1;
                    }
                    somaParcial = pontosIdade + pontosPercepcao;
                    //limitacoes fisicas
                    var pontosLimitacao1 = 0;
                    var pontosLimitacao2 = 0;
                    var pontosLimitacao3 = 0;
                    var pontosLimitacao4 = 0;
                    var pontosLimitacao5 = 0;
                    var pontosLimitacao6 = 0;
                    if (res.rows.item(i).limitacao1 >= 4) {
                        pontosLimitacao1 = 1;
                    }
                    if (res.rows.item(i).limitacao2 >= 4) {
                        pontosLimitacao2 = 1;
                    }
                    if (res.rows.item(i).limitacao3 >= 4) {
                        pontosLimitacao3 = 1;
                    }
                    if (res.rows.item(i).limitacao4 >= 4) {
                        pontosLimitacao4 = 1;
                    }
                    if (res.rows.item(i).limitacao5 >= 4) {
                        pontosLimitacao5 = 1;
                    }
                    if (res.rows.item(i).limitacao6 >= 4) {
                        pontosLimitacao6 = 1;
                    }
                    var somaLimitacao = pontosLimitacao1 + pontosLimitacao2 + pontosLimitacao3 + pontosLimitacao4 + pontosLimitacao5 + pontosLimitacao6;
                    var maxLimicacao = somaLimitacao;
                    if (somaLimitacao > 2) {
                        maxLimicacao = 2;
                    }
                    //incapacidades Fisicas
                    var pontosIncapacidade1 = 0;
                    var pontosIncapacidade2 = 0;
                    var pontosIncapacidade3 = 0;
                    var pontosIncapacidade4 = 0;
                    var pontosIncapacidade5 = 0;
                    if (res.rows.item(i).incapacidade1 == 1) {
                        pontosIncapacidade1 = 4;
                    }
                    if (res.rows.item(i).incapacidade2 == 1) {
                        pontosIncapacidade2 = 4;
                    }
                    if (res.rows.item(i).incapacidade3 == 1) {
                        pontosIncapacidade3 = 4;
                    }
                    if (res.rows.item(i).incapacidade4 == 1) {
                        pontosIncapacidade4 = 4;
                    }
                    if (res.rows.item(i).incapacidade5 == 1) {
                        pontosIncapacidade5 = 4;
                    }
                    var somaIncapacidade = pontosIncapacidade1 + pontosIncapacidade2 + pontosIncapacidade3 + pontosIncapacidade4 + pontosIncapacidade5;
                    var maxIncapacidade = somaIncapacidade;
                    if (somaIncapacidade > 4) {
                        maxIncapacidade = 4;
                    }
                    var pontuacaofinal = maxIncapacidade + maxLimicacao + somaParcial;
                    _this.pontuacaototal = 0;
                    // let pontuacaomensagem=null;
                    if (pontuacaofinal >= 3) {
                        //   pontuacaomensagem="ATENÇÃO/AÇÃO";
                        _this.pontuacaototal = 3;
                    }
                    else {
                        //   pontuacaomensagem="ACOMPANHAMENTO DE ROTINA";
                        _this.pontuacaototal = pontuacaofinal;
                    }
                    var pontos_final = somaParcial + maxLimicacao + maxIncapacidade;
                    var pontos_total = pontos_final;
                    if (pontos_final > 10) {
                        _this.pontos_total = 10;
                    }
                    //let idade=milisecondsDiff;
                    _this.ves13s2.push({ id_ves13: res.rows.item(i).id_ves13, nome: res.rows.item(i).nome, data_exame: res.rows.item(i).data_exame, pontos_total: pontos_total, pontuacaototal: pontos_total, idade: idade, incapacidade1: res.rows.item(i).incapacidade1 });
                }
            })
                .catch(function (e) { return console.log(e); });
        }).catch(function (e) { return console.log(e); });
    };
    ListVes13Page.prototype.editVes13 = function (id_ves13) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__resultado_ves13_resultado_ves13__["a" /* ResultadoVes13Page */], {
            id_ves13: id_ves13
        });
    };
    ListVes13Page.prototype.deleteVes132 = function (id_ves13) {
        var _this = this;
        this.sqlite.create({
            name: 'idoso.db',
            location: 'default'
        }).then(function (db) {
            db.executeSql('DELETE FROM teste_ves13 WHERE id_ves13=?', [id_ves13])
                .then(function (res) {
                console.log(res);
                _this.navCtrl.push(ListVes13Page_1);
            })
                .catch(function (e) { return console.log(e); });
        }).catch(function (e) { return console.log(e); });
    };
    ListVes13Page.prototype.deleteVes13 = function (id_ves13) {
        var _this = this;
        var alert = this.alertCtrl.create({
            title: 'Tem certeza que quer excluir o resultado?',
            message: '',
            buttons: [
                {
                    text: 'Não',
                    role: 'cancel',
                    handler: function () {
                        //console.log('Cancel clicked');
                    }
                },
                {
                    text: 'Sim',
                    handler: function () {
                        // console.log('Buy clicked');
                        _this.deleteVes132(id_ves13);
                    }
                }
            ]
        });
        alert.present();
    };
    ListVes13Page.prototype.addPerfil2 = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__edit_data_edit_data__["a" /* EditDataPage */], {
            rowid: 1
        });
    };
    ListVes13Page.prototype.getUser = function (rowid) {
        var _this = this;
        this.sqlite.create({
            name: 'idoso.db',
            location: 'default'
        }).then(function (db) {
            db.executeSql('SELECT * FROM perfil WHERE rowid=?', [rowid])
                .then(function (res) {
                if (res.rows.length > 0) {
                    _this.data.rowid = res.rows.item(0).rowid;
                    _this.data.nome = res.rows.item(0).nome;
                    _this.data.sexo = res.rows.item(0).sexo;
                    _this.data.nascimento = res.rows.item(0).nascimento;
                    _this.data.imagem = res.rows.item(0).imagem;
                    _this.photo = res.rows.item(0).imagem;
                }
                if (_this.photo == "") {
                    _this.photo = "assets/imgs/user.png";
                }
            });
        });
    };
    ListVes13Page = ListVes13Page_1 = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-list-ves13',template:/*ion-inline-start:"/home/rabaco/Documentos/projetos_ionic/idoso_aplicativo_final/src/pages/list-ves13/list-ves13.html"*/'<ion-header>\n\n  <ion-navbar>\n    <ion-title>Vulnerabilidade</ion-title>\n    <ion-buttons end>\n      <div class="icone-imagem">\n        <img src="{{ photo }}" class="icone-imagem" (click)="addPerfil2()">\n      </div>\n    </ion-buttons>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n\n      <div class="titulo-testes">Resultados</div>\n\n      <ion-item>\n          <div class="regiao_grafico">\n              <div class="numeros">6<br>5<br>4<br>3<br>2<br>1<br>0\n              </div>\n              <div class="fundo_grafico">\n              </div>\n              <div class="linhas">\n              </div>\n              <div *ngFor="let ves13 of ves13s; let n=index">\n                <div *ngIf="ves13.pontuacaototal<=6">\n            <div class="calculo" [ngStyle]="{\'margin-top\': (250-((ves13.pontuacaototal)*44.8)) + \'px\', \'margin-left\': (27+(n*47.8)) + \'px\'}" *ngIf="n<6" (click)="editVes13(ves13.id_ves13)">\n                  <span class=\'texto-grafico\'><span *ngIf="ves13.pontuacaototal<10">0</span>{{ves13.pontuacaototal}}</span>\n                  </div>\n                  </div>\n                  <div *ngIf="ves13.pontuacaototal>6">\n                      <div class="calculo" [ngStyle]="{\'margin-left\': (27+(n*47.8)) + \'px\'}" *ngIf="n<6" (click)="editVes13(ves13.id_ves13)" >\n                          <span class=\'texto-grafico\'><span *ngIf="ves13.pontuacaototal<10">0</span>{{ves13.pontuacaototal}}</span>\n                          </div>\n                          </div>\n                 <div class="vertical-text"  [ngStyle]="{\'margin-left\': (4+(n*46)) + \'px\'}" *ngIf="n<6">\n                    {{ves13.data_exame | date:"dd/MM/yyyy"}}\n                 </div>\n              </div>\n              </div>\n       </ion-item>\n       <ion-item>\n        <span class="legenda-principal">Legenda:</span><br><br>\n        <span class="legenda-interna"><img src="../assets/imgs/circulo_verde.png" class="circ">Acompanhamento de Rotina</span><br>\n         <span class="legenda-interna"><img src="../assets/imgs/circulo_vermelho.png" class="circ">Atenção / Ação</span><br>\n      </ion-item>\n\n\n      <ion-list *ngFor="let ves132 of ves13s2; let i=index">\n          <div class="titulo-testes" *ngIf="i == 6">Resultados anteriores:</div>\n        <ion-item-sliding *ngIf="i > 5">\n          <ion-item style="margin:0; padding:0; border-bottom: 1px solid; border-color:#B9B9B9">\n    \n          <div class="quebra" (click)="editVes13(ves132.id_ves13)">\n            <span *ngIf="(ves132.pontuacaototal<3)">\n              <div class="circulo_testes"><img src="assets/imgs/circulo_verde.png"></div><div class="texto-data"> {{ves132.data_exame | date:"dd/MM/yyyy"}}</div><div class="circulo-texto">Acompanhamento<br>de Rotina</div></span>\n              <span *ngIf="(ves132.pontuacaototal>=3)">\n                <div class="circulo_testes"><img src="assets/imgs/circulo_vermelho.png"></div><div class="texto-data">{{ves132.data_exame | date:"dd/MM/yyyy"}}</div><div class="circulo-texto2">Atenção / Ação</div></span>\n<br>\n          </div>\n\n          </ion-item>\n\n      \n            </ion-item-sliding>\n          </ion-list>\n\n</ion-content>\n'/*ion-inline-end:"/home/rabaco/Documentos/projetos_ionic/idoso_aplicativo_final/src/pages/list-ves13/list-ves13.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__ionic_native_sqlite__["a" /* SQLite */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* ActionSheetController */],
            __WEBPACK_IMPORTED_MODULE_4__angular_common__["d" /* DatePipe */]])
    ], ListVes13Page);
    return ListVes13Page;
    var ListVes13Page_1;
}());

//# sourceMappingURL=list-ves13.js.map

/***/ }),

/***/ 129:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return InicialPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_jquery__ = __webpack_require__(188);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_jquery___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_jquery__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the InicialPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var InicialPage = /** @class */ (function () {
    function InicialPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    InicialPage.prototype.ionViewDidLoad = function () {
        this.subir();
    };
    /*
      subir()
      {
        for (let i = 0; i < 100; i++)
        {
        setTimeout(() => {
          this.content.scrollTo(0,i);
       }, 1);
      }
      */
    InicialPage.prototype.subir = function () {
        __WEBPACK_IMPORTED_MODULE_2_jquery__().ready(function () {
            __WEBPACK_IMPORTED_MODULE_2_jquery__("#corpo").animate({ scrollTop: 16000 }, 300000);
        });
        __WEBPACK_IMPORTED_MODULE_2_jquery__(document).on('touchstart', '#corpo', function (event) {
            __WEBPACK_IMPORTED_MODULE_2_jquery__("#corpo").stop();
        });
        __WEBPACK_IMPORTED_MODULE_2_jquery__(document).on('touchstart', '#corpo', function (event) {
            __WEBPACK_IMPORTED_MODULE_2_jquery__("#corpo").animate({ scrollTop: 16000 }, 300000);
        });
        __WEBPACK_IMPORTED_MODULE_2_jquery__(document).on('touchmove', '#corpo', function (event) {
            __WEBPACK_IMPORTED_MODULE_2_jquery__("#corpo").stop();
        });
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["c" /* Content */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["c" /* Content */])
    ], InicialPage.prototype, "content", void 0);
    InicialPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-inicial',template:/*ion-inline-start:"/home/rabaco/Documentos/projetos_ionic/idoso_aplicativo_final/src/pages/inicial/inicial.html"*/'\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>Sobre</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n<ion-content >\n  <div class="texto" id=\'corpo\' text-wrap>\n    <div class="padding"> \n        <div class="sobre">Sobre o Aplicativo</div>\n        O aplicativo “Atenção ao Idoso” foi baseado em seções Caderneta de Saúde da Pessoa Idosa elaborada pelo Ministério da Saúde em parceria com o LIS/Icict/Fiocruz. A caderneta original é um instrumento de registro e acompanhamento de informações sobre saúde para identificação de vulnerabilidades e para fornecimento de orientações para o autocuidado da pessoa idosa. Este aplicativo é uma adaptação baseada nesse conteúdo e tem o objetivo de expandir o uso desses instrumentos, além de manter a continuidade dos registros e facilitar a portabilidade dos dados.\n<br><br>\nO "Atenção ao Idoso" foi elaborado por meio de uma parceria entre o Laboratório de Informação em Saúde (LIS) e o Centro de Tecnologia da Informação e Comunicação em Saúde (CTIC), ambos do Instituto de Comunicação e Informação Científica e Tecnológica em Saúde da Fundação Oswaldo Cruz (Icict/Fiocruz).\n<br><br><br> \n\n     <div class="sobre">Ficha Técnica</div>\n     <span class="cinza">2019</span>. <strong>Ministério da Saúde. Fundação Oswaldo Cruz</strong><br>\n     Esta obra está de acordo com a <a href="http://portal.fiocruz.br/sites/portal.fiocruz.br/files/documentos/portaria_-_politica_de_acesso_aberto_ao_conhecimento_na_fiocruz.pdf" target="_blank">Política de Acesso Aberto ao Conhecimento</a>, que busca garantir à sociedade o acesso gratuito, público e aberto ao conteúdo integral de toda obra intelectual produzida pela Fiocruz\n     <br><br><br>           \n       \n        <strong>Ministério da Saúde</strong><br>\n        <strong><i>Luiz Henrique Mandetta</i></strong><br>\n        Ministro<br><br>\n        \n        <strong>Fundação Oswaldo Cruz – Fiocruz</strong>\n        <strong><i>Nísia Trindade Lima</i></strong><br>\n        Presidente<br><br>\n        \n        <strong>Vice-Presidência Educação, Informação e Comunicação</strong><br>\n        <strong><i>Cristiani Vieira Machado</i></strong><br>\n        Vice-Presidente<br><br>\n        \n        <strong>Campus Virtual Fiocruz</strong><br>\n        <strong><i>Ana Cristina da Matta Furniel</i></strong><br>\n        Coordenadora<br><br>\n        \n        <strong>Instituto de Informação e Comunicação Científica e Tecnológia – ICICT</strong><br>\n        <strong><i>Rodrigo Murtinho de Martinez Torres</i></strong><br>\n        Diretor<br><br><br>\n\n        <strong>Coordenador Geral</strong><br>\n        <i>Dalia Elena Romero e Jéssica Muzy Rodrigues</i><br><br>\n\n        <strong>Coordenador Acadêmico</strong><br>\n        <i>Jéssica Muzy Rodrigues e Débora Castanheira Pires</i><br><br>\n\n        <strong>Coordenador de Produção</strong><br>\n        <i>Jéssica Muzy Rodrigues e Leticia Sabbadini</i><br><br>\n\n        <strong>Conteudista</strong><br>\n        <i>Débora Castanheira Pires</i><br><br>\n\n        <strong>Revisor Técnico-científico</strong><br>\n        <i>Letícia Sabbadini</i><br><br>\n\n        <strong>Editor técnico</strong><br>\n        <i>Daniela Lessa</i><br><br>\n\n        <strong>Designer Instrucional</strong><br>\n        <i>Cláudio da Silva</i><br><br>\n\n        <strong>Designer Gráfico</strong><br>\n        <i>Cláudio da Silva</i><br><br>\n\n        <strong>Web designer</strong><br>\n        <i>Cláudio da Silva</i><br><br>\n\n        <strong>Desenvolvedor</strong><br>\n        <i>Marcelo Rabaço</i><br><br>\n\n        <strong>Avaliador Técnico-científico</strong><br>\n        <i>Aldo Pontes</i><br><br>\n\n        <strong>Avaliador de Pertinência</strong><br>\n        <i>Aldo Pontes</i><br><br>\n\n        <strong>Consultor</strong><br>\n        <i>Aldo Pontes</i><br><br>\n\n        <strong>Apoio técnico</strong><br>\n        <i>Pedro Teixeira</i><br><br>\n\n        <strong>Engenheiro de Software</strong><br>\n        <i>Marcelo Rabaço</i><br><br>\n\n        <strong>Arquiteto de Software</strong><br>\n        <i>Marcelo Rabaço e Claudio da Silva</i><br><br>\n\n        <strong>Arquiteto de Software</strong><br>\n        <i>Marcelo Rabaço</i><br><br>\n<!--\n        <strong>Pesquisa e assessoria técnico-científica:</strong><br>\n        Leticia Sabbadini, Débora Castanheira, Leo Maia e Aline Marques<br><br>     \n    -->  \n      <br><br><br><br><br>\n \n  </div>\n  <img src="assets/imgs/creditos.png" class="imagem">\n    </div>\n</ion-content>\n'/*ion-inline-end:"/home/rabaco/Documentos/projetos_ionic/idoso_aplicativo_final/src/pages/inicial/inicial.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */]])
    ], InicialPage);
    return InicialPage;
}());

//# sourceMappingURL=inicial.js.map

/***/ }),

/***/ 130:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SobrePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__direitos_direitos__ = __webpack_require__(71);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__direitos_secundaria_direitos_secundaria__ = __webpack_require__(132);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__home_home__ = __webpack_require__(45);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var SobrePage = /** @class */ (function () {
    function SobrePage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.teste = 0;
        this.resultado = "";
        this.voltar = 1;
        this.categoria = "";
        /*var val=this.navCtrl.last().name;
        console.log("Previous Page is called = " + this.navCtrl.last().name);
        */
    }
    SobrePage_1 = SobrePage;
    SobrePage.prototype.goVoltar = function (teste) {
        if (teste == 1) {
            this.navCtrl.push(SobrePage_1);
        }
        else {
            this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__home_home__["a" /* HomePage */]);
        }
    };
    SobrePage.prototype.declareTechnologies = function () {
        this.technologies = [
            { taxonomia: 'Saúde,saude,beneficios', secao: 'Saúde', lei: '', paragrafo: '', descricao: 'Direito à atenção integral à saúde do idoso, por intermédio do SUS', fontes: 'Estatuto do Idoso (Lei nº 10.741, de 1º de outubro de 2003)<br>Artigo 15' },
            { taxonomia: 'Saúde,saude,servicos,beneficios', secao: 'Saúde', lei: '', paragrafo: '', descricao: 'Receber medicamentos, especialmente os de uso continuado, gratuitamente', fontes: 'Estatuto do Idoso (Lei nº 10.741, de 1º de outubro de 2003)<br>Artigo 15 §2' },
            { taxonomia: 'Saúde,casa,servicos,,beneficios', secao: 'Saúde', lei: '', paragrafo: '', descricao: 'Impossibilitados de se locomover, têm direito a atendimento domiciliar, incluindo internação', fontes: 'Estatuto do Idoso (Lei nº 10.741, de 1º de outubro de 2003)<br>Artigo 15' },
            { taxonomia: 'Saúde,tratamento,beneficios', secao: 'Saúde', lei: '', paragrafo: '', descricao: 'É assegurado o direito a optar pelo tratamento de saúde que considerar mais favorável.', fontes: 'Estatuto do Idoso (Lei nº 10.741, de 1º de outubro de 2003)<br>Artigo 17' },
            { taxonomia: 'Saúde,casa,servicos', secao: 'Saúde', lei: '', paragrafo: '', descricao: 'Idosos com necessidade de assistência médica ou multiprofissional têm direito a acolhimento temporário em Hospital-Dia e Centro-Dia.', fontes: 'Decreto 1948/1996<br>Artigo 4' },
            { taxonomia: 'Saúde,casa,servicos', secao: 'Saúde', lei: '', paragrafo: '', descricao: 'Idosos dependentes podem solicitar atendimento domiciliar na sua Unidade Básica de Saúde de referência.', fontes: 'Decreto 1948/1996<br>Artigo 4' },
            { taxonomia: 'Saúde,saude,beneficios', secao: 'Saúde', lei: '', paragrafo: '', descricao: 'Direito a acompanhante em caso de internação ou observação em hospital', fontes: 'Estatuto do Idoso (Lei nº 10.741, de 1º de outubro de 2003)<br>Artigo 16' },
            { taxonomia: 'Saúde,saude,beneficios,planos de saúde', secao: 'Saúde', lei: '', paragrafo: '', descricao: 'Direito a acompanhante em caso de internação ou observação em hospital.', fontes: 'Estatuto do Idoso (Lei nº 10.741, de 1º de outubro de 2003)<br>Artigo 15 §3' },
            { taxonomia: 'Saúde,saude,beneficios,planos de saúde', secao: 'Saúde', lei: '', paragrafo: '', descricao: 'É vedada a cobrança de valores diferenciados em razão da idade nos planos de saúde.', fontes: 'Estatuto do Idoso (Lei nº 10.741, de 1º de outubro de 2003)<br>Artigo 15 §7' },
            { taxonomia: 'Saúde,saude,Prioridades,beneficios', secao: 'Saúde', lei: '', paragrafo: '', descricao: 'Idosos maiores de 80 anos têm preferência especial sobre os demais idosos em todo atendimento, exceto em caso de emergência.', fontes: 'Estatuto do Idoso (Lei nº 10.741, de 1º de outubro de 2003)<br>Artigo 15 §7' },
            { taxonomia: 'Saúde,saude,Prioridades,beneficios,vacinacao', secao: 'Saúde', lei: '', paragrafo: '', descricao: 'Todo idoso tem direito a vacinar-se contra gripe, pneumonia, difteria e febre amarela.', fontes: 'Portaria 1498/2013' },
            { taxonomia: 'Mobilidade,onibus,ônibus,transporte,beneficios', secao: 'Mobilidade', lei: '', paragrafo: '', descricao: 'Gratuidade no transporte coletivo público urbano e semiurbano, com reserva de 10% dos assentos, os quais deverão ser identificados com placa de reserva.', fontes: 'Estatuto do Idoso (Lei nº 10.741, de 1º de outubro de 2003)<br>Artigo 39' },
            { taxonomia: 'Mobilidade,onibus,ônibus,transporte,beneficios', secao: 'Mobilidade', lei: '', paragrafo: '', descricao: 'Reserva de duas vagas gratuitas no transporte interestadual para idosos com renda igual ou inferior a dois salários mínimos e desconto de 50% para os idosos que excedam as vagas garantidas.', fontes: 'Estatuto do Idoso (Lei nº 10.741, de 1º de outubro de 2003)<br>Artigo 40' },
            { taxonomia: 'Mobilidade,transporte,carro,beneficios', secao: 'Mobilidade', lei: '', paragrafo: '', descricao: 'Reserva de 5% das vagas nos estacionamentos públicos e privados.', fontes: 'Estatuto do Idoso (Lei nº 10.741, de 1º de outubro de 2003)<br>Artigo 41' },
            { taxonomia: 'Mobilidade,transporte,carro,beneficios,segurança', secao: 'Prioridades', lei: '', paragrafo: '', descricao: 'O idoso tem direito a prioridade e segurança no embarque e desembarque do transporte público.', fontes: 'Estatuto do Idoso (Lei nº 10.741, de 1º de outubro de 2003)<br>Artigo 42' },
            { taxonomia: 'Prioridades,judiciario,justica,servicos,beneficios', secao: 'Prioridades', lei: '', paragrafo: '', descricao: 'Na tramitação dos processos e procedimentos na execução de atos e diligências judiciais.', fontes: 'Estatuto do Idoso (Lei nº 10.741, de 1º de outubro de 2003)<br>Artigo 71' },
            { taxonomia: 'Prioridades,servicos,beneficios', secao: 'Prioridades', lei: '', paragrafo: '', descricao: 'Em filas de atendimento de serviços nos setores público e privado, tais como bancos, lojas, supermecados etc.', fontes: 'Estatuto do Idoso (Lei nº 10.741, de 1º de outubro de 2003)<br>Artigo 3 §1' },
            { taxonomia: 'Prioridades', secao: 'Prioridades', lei: '', paragrafo: '', descricao: 'Na restituição do Imposto de Renda', fontes: 'Estatuto do Idoso (Lei nº 10.741, de 1º de outubro de 2003)<br>Artigo 71' },
            { taxonomia: 'Renda,beneficios', secao: 'Renda', lei: '', paragrafo: '', descricao: 'Direito de requerer o Benefício de Prestação Continuada (BPC), a partir dos 65 anos de idade, desde que não possua meios para prover sua própria subsistência ou de tê-la provida pela família.', fontes: 'Lei Orgânica de Assistência Social (Lei 8742 de 1993)<br>Artigo 20' },
            { taxonomia: 'Renda,beneficios', secao: 'Renda', lei: '', paragrafo: '', descricao: 'Direito a 25% de acréscimo na aposentadoria por invalidez (casos especiais).', fontes: 'Lei 8213 de 1991.<br>Artigo 45' },
            { taxonomia: 'Renda,beneficios', secao: 'Renda', lei: '', paragrafo: '', descricao: 'O idoso sem condições de subsistência tem direito a solicitar pensão alimentícia de descendente.', fontes: 'Estatuto do Idoso (Lei nº 10.741, de 1º de outubro de 2003)<br>Artigo 11' },
            { taxonomia: 'Outros,Segurança', secao: 'Outros', lei: '', paragrafo: '', descricao: 'Direito de exigir medidas de proteção sempre que seus direitos estiverem ameaçados ou violados por ação ou omissão da sociedade, do Estado, da família, de seu curador ou de entidades de atendimento.', fontes: 'Estatuto do Idoso (Lei nº 10.741, de 1º de outubro de 2003)<br>Artigo 43' },
            { taxonomia: 'Outros', secao: 'Outros', lei: '', paragrafo: '', descricao: 'É vedada a fixação de limite máximo de idade em seleção para qualquer trabalho, emprego ou concurso público, ressalvados os casos em que a natureza do cargo exigir.', fontes: 'Estatuto do Idoso (Lei nº 10.741, de 1º de outubro de 2003)<br>Artigo 27' },
            { taxonomia: 'Lazer,Ingressos,Cinema,shows,espetaculos,futebol,volei,basquete', secao: 'Lazer', lei: '', paragrafo: '', descricao: 'Desconto de pelo menos 50% nos ingressos para eventos artísticos, culturais, esportivos e de lazer.', fontes: 'Estatuto do Idoso (Lei nº 10.741, de 1º de outubro de 2003)<br>Artigo 23' },
            { taxonomia: 'Habitação,Habitacao,casa,moradia', secao: 'Habitação', lei: 'Estatuto do Idoso<br>Decreto 1948/1996', paragrafo: '', descricao: 'Nos programas habitacionais, públicos ou subsidiados com recursos públicos, o idoso tem prioridade na aquisição de imóvel para moradia própria.', fontes: 'Estatuto do Idoso (Lei nº 10.741, de 1º de outubro de 2003)<br>Artigo 38' },
            { taxonomia: 'Habitação,Habitacao,casa,moradia,asilo', secao: 'Habitação', lei: 'Estatuto do Idoso<br>Decreto 1948/1996', paragrafo: '', descricao: 'Idosos sem renda suficiente para sua manutenção e sem família têm direito a acolhimento em Casa-lar.', fontes: 'Decreto 1948/1996<br>Artigo 4' },
        ];
    };
    SobrePage.prototype.filterTechnologies = function (param) {
        var _this = this;
        this.declareTechnologies();
        var val = param;
        // DON'T filter the technologies IF the supplied input is an empty string
        if (val.trim() !== '') {
            this.technologies = this.technologies.filter(function (item) {
                _this.teste = 1;
                _this.resultado = val;
                return _this.removeAccents(item.taxonomia.toLowerCase()).indexOf(val.toLowerCase()) > -1 || _this.removeAccents(item.descricao.toLowerCase()).indexOf(val.toLowerCase()) > -1 || item.taxonomia.toLowerCase().indexOf(val.toLowerCase()) > -1 || item.descricao.toLowerCase().indexOf(val.toLowerCase()) > -1;
            });
        }
    };
    SobrePage.prototype.onFilter = function (category) {
        var _this = this;
        this.declareTechnologies();
        // Only filter the technologies array IF the selection is NOT equal to value of all
        if (category.trim() !== '') {
            this.technologies = this.technologies.filter(function (item) {
                _this.teste = 1;
                _this.resultado = category;
                return item.taxonomia.toLowerCase().indexOf(category.toLowerCase()) > -1;
            });
            this.categoria = '1';
        }
    };
    SobrePage.prototype.filterTechnologies2 = function (param) {
        var val2 = param;
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__direitos_secundaria_direitos_secundaria__["a" /* DireitosSecundariaPage */], { param: val2 });
        console.log(val2);
    };
    SobrePage.prototype.irSaude = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__direitos_secundaria_direitos_secundaria__["a" /* DireitosSecundariaPage */], { category: 'saúde' });
        // this.onFilter('saúde');
    };
    SobrePage.prototype.irMobilidade = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__direitos_secundaria_direitos_secundaria__["a" /* DireitosSecundariaPage */], { category: 'Transporte' });
        //this.onFilter('Transporte');
    };
    SobrePage.prototype.irPrioridade = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__direitos_secundaria_direitos_secundaria__["a" /* DireitosSecundariaPage */], { category: 'Prioridades' });
        //this.onFilter('Prioridades');
    };
    SobrePage.prototype.irRenda = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__direitos_secundaria_direitos_secundaria__["a" /* DireitosSecundariaPage */], { category: 'Renda' });
        //this.onFilter('Renda');
    };
    SobrePage.prototype.irOutros = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__direitos_secundaria_direitos_secundaria__["a" /* DireitosSecundariaPage */], { category: 'Outros' });
        //this.onFilter('Outros');
    };
    SobrePage.prototype.irLazer = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__direitos_secundaria_direitos_secundaria__["a" /* DireitosSecundariaPage */], { category: 'Lazer' });
        //this.onFilter('Lazer');
    };
    SobrePage.prototype.irHabitacao = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__direitos_secundaria_direitos_secundaria__["a" /* DireitosSecundariaPage */], { category: 'Habitação' });
        //this.onFilter('Habitação');
    };
    SobrePage.prototype.irPoliticas = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__direitos_direitos__["a" /* DireitosPage */]);
    };
    /*
 goVoltar()
 {
   this.navCtrl.push(SobrePage)
 }
 */
    SobrePage.prototype.removeAccents = function (value) {
        return value
            .replace(/á/g, 'a')
            .replace(/ã/g, 'a')
            .replace(/â/g, 'a')
            .replace(/é/g, 'e')
            .replace(/è/g, 'e')
            .replace(/ê/g, 'e')
            .replace(/í/g, 'i')
            .replace(/ï/g, 'i')
            .replace(/ì/g, 'i')
            .replace(/ó/g, 'o')
            .replace(/ô/g, 'o')
            .replace(/õ/g, 'o')
            .replace(/ú/g, 'u')
            .replace(/ü/g, 'u')
            .replace(/ç/g, 'c')
            .replace(/ß/g, 's');
    };
    SobrePage = SobrePage_1 = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-sobre',template:/*ion-inline-start:"/home/rabaco/Documentos/projetos_ionic/idoso_aplicativo_final/src/pages/sobre/sobre.html"*/'<ion-header>\n        <ion-navbar hideBackButton="true">\n          <ion-buttons left>\n        <button (click)="goVoltar(teste)" ion-button icon-only style="margin-left: 10px">\n          <ion-icon name="md-arrow-back"></ion-icon>\n        </button>\n      </ion-buttons>\n    <ion-title>\n      <span *ngIf="resultado==\'\'">Direitos</span>\n<span *ngIf="resultado!=\'\' && teste==1 && categoria==1">{{ resultado }}</span>\n<span *ngIf="resultado!=\'\' && categoria!=1">Resultado da busca</span>\n    </ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content>\n  <ion-searchbar\n     (change)="filterTechnologies2($event.target.value)" placeholder="Buscar Ex: ônibus" autocomplete="on"  hideCancelButton="true" autocorrect="on" *ngIf="teste==0" class="busca"></ion-searchbar>\n     <ion-item text-wrap  *ngIf="teste!=0" class="texto-busca"> <!-- {{ resultado }} --></ion-item>\n     <div *ngFor="let technology of technologies; let i=index" >     \n        <ion-item text-wrap class="texto-busca2" >  {{ technology.descricao }} \n            <div class="texto-secao" (click)=onFilter(technology.secao) *ngIf="categoria==0" > {{ technology.secao }}</div>  \n         <div class="fonte" padding *ngIf="technology.fontes"><strong>Fonte:</strong><br><span [innerHTML]="technology.fontes"></span></div>\n         \n        </ion-item>\n      </div>\n      \n      \n\n      <ion-item text-wrap class="texto-busca2" *ngIf="teste!=0 && technologies==false">  Busca por "{{ resultado }}" não retornou nenhum resultado\n          </ion-item>\n  \n  <div *ngIf="teste==0" padding style="margin-top: -30px;"> \n       <button ion-item  no-lines  (click)="irSaude()" color="fundo-item" class="botao_cinza" >Saúde</button> \n        <button ion-item no-lines  (click)="irMobilidade()" color="fundo-item" class="botao_cinza" >Transporte</button>\n        <button ion-item no-lines  (click)="irLazer()" color="fundo-item" class="botao_cinza" >Lazer</button>\n        <button ion-item no-lines  (click)="irPrioridade()" color="fundo-item" class="botao_cinza" >Prioridades</button>\n        <button ion-item no-lines  (click)="irHabitacao()" color="fundo-item" class="botao_cinza" >Habitação</button>\n        <button ion-item no-lines  (click)="irRenda()" color="fundo-item" class="botao_cinza">Renda</button>\n        <button ion-item  no-lines (click)="irOutros()" color="fundo-item" class="botao_cinza">Outros</button>\n        <button ion-item no-lines  (click)="irPoliticas()" color="fundo-item" class="botao_cinza">Políticas</button>\n        \n  </div>     \n     \n      </ion-content>\n\n\n  '/*ion-inline-end:"/home/rabaco/Documentos/projetos_ionic/idoso_aplicativo_final/src/pages/sobre/sobre.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */]])
    ], SobrePage);
    return SobrePage;
    var SobrePage_1;
}());

//# sourceMappingURL=sobre.js.map

/***/ }),

/***/ 131:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DireitosInternoPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_direitos_direitos__ = __webpack_require__(101);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var DireitosInternoPage = /** @class */ (function () {
    function DireitosInternoPage(navCtrl, navParams, direitos) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.direitos = direitos;
        this.politica = 0;
        this.direitos.getByID(this.navParams.get('code')).then(function (result) {
            _this.politica = result;
        });
    }
    DireitosInternoPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-direitos-interno',template:/*ion-inline-start:"/home/rabaco/Documentos/projetos_ionic/idoso_aplicativo_final/src/pages/direitos-interno/direitos-interno.html"*/'<ion-header>\n\n  <ion-navbar>\n    <ion-title>Direitos do Idoso</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n<ion-spinner name="circles" *ngIf="politica == 0">\n</ion-spinner>\n<ion-content padding *ngIf="politica != 0" >\n    <h2>{{politica.nome}}</h2>\n    <strong [innerHTML]="politica.lei">> </strong>\n    <br>\n<br><img src="assets/imgs/brastra.gif" alt="logo presidência da Republica">\n<p [innerHTML]="politica.descricao"></p>\n</ion-content>\n'/*ion-inline-end:"/home/rabaco/Documentos/projetos_ionic/idoso_aplicativo_final/src/pages/direitos-interno/direitos-interno.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__providers_direitos_direitos__["a" /* DireitosProvider */]])
    ], DireitosInternoPage);
    return DireitosInternoPage;
}());

//# sourceMappingURL=direitos-interno.js.map

/***/ }),

/***/ 132:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DireitosSecundariaPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__direitos_direitos__ = __webpack_require__(71);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/*import { HomePage } from '../home/home'; */
var DireitosSecundariaPage = /** @class */ (function () {
    function DireitosSecundariaPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.teste = 0;
        this.resultado = "";
        this.voltar = 1;
        this.categoria = "";
        /*var val=this.navCtrl.last().name;
        console.log("Previous Page is called = " + this.navCtrl.last().name);
        */
        if (navParams.get("category")) {
            var ress = navParams.get("category");
            this.onFilter(ress);
        }
        if (navParams.get("param")) {
            var pa = navParams.get("param");
            this.filterTechnologies(pa);
        }
    }
    DireitosSecundariaPage.prototype.declareTechnologies = function () {
        this.technologies = [
            { taxonomia: 'Saúde,saude,beneficios', secao: 'Saúde', lei: '', paragrafo: '', descricao: 'Direito à atenção integral à saúde do idoso, por intermédio do SUS', fontes: 'Estatuto do Idoso (Lei nº 10.741, de 1º de outubro de 2003)<br>Artigo 15' },
            { taxonomia: 'Saúde,saude,servicos,serviços,beneficios', secao: 'Saúde', lei: '', paragrafo: '', descricao: 'Receber medicamentos, especialmente os de uso continuado, gratuitamente', fontes: 'Estatuto do Idoso (Lei nº 10.741, de 1º de outubro de 2003)<br>Artigo 15 §2' },
            { taxonomia: 'Saúde,casa,servicos,serviços,beneficios', secao: 'Saúde', lei: '', paragrafo: '', descricao: 'Impossibilitados de se locomover, têm direito a atendimento domiciliar, incluindo internação', fontes: 'Estatuto do Idoso (Lei nº 10.741, de 1º de outubro de 2003)<br>Artigo 15' },
            { taxonomia: 'Saúde,tratamento,beneficios', secao: 'Saúde', lei: '', paragrafo: '', descricao: 'É assegurado o direito a optar pelo tratamento de saúde que considerar mais favorável.', fontes: 'Estatuto do Idoso (Lei nº 10.741, de 1º de outubro de 2003)<br>Artigo 17' },
            { taxonomia: 'Saúde,casa,servicos,serviços', secao: 'Saúde', lei: '', paragrafo: '', descricao: 'Idosos com necessidade de assistência médica ou multiprofissional têm direito a acolhimento temporário em Hospital-Dia e Centro-Dia.', fontes: 'Decreto 1948/1996<br>Artigo 4' },
            { taxonomia: 'Saúde,casa,servicos,serviços', secao: 'Saúde', lei: '', paragrafo: '', descricao: 'Idosos dependentes podem solicitar atendimento domiciliar na sua Unidade Básica de Saúde de referência.', fontes: 'Decreto 1948/1996<br>Artigo 4' },
            { taxonomia: 'Saúde,saude,beneficios,planos de saúde', secao: 'Saúde', lei: '', paragrafo: '', descricao: 'Direito a acompanhante em caso de internação ou observação em hospital.', fontes: 'Estatuto do Idoso (Lei nº 10.741, de 1º de outubro de 2003)<br>Artigo 15 §3' },
            { taxonomia: 'Saúde,saude,beneficios,planos de saúde', secao: 'Saúde', lei: '', paragrafo: '', descricao: 'É vedada a cobrança de valores diferenciados em razão da idade nos planos de saúde.', fontes: 'Estatuto do Idoso (Lei nº 10.741, de 1º de outubro de 2003)<br>Artigo 15 §7' },
            { taxonomia: 'Saúde,saude,Prioridades,beneficios', secao: 'Saúde', lei: '', paragrafo: '', descricao: 'Idosos maiores de 80 anos têm preferência especial sobre os demais idosos em todo atendimento, exceto em caso de emergência.', fontes: 'Estatuto do Idoso (Lei nº 10.741, de 1º de outubro de 2003)<br>Artigo 15 §7' },
            { taxonomia: 'Saúde,saude,beneficios,vacinacao', secao: 'Saúde', lei: '', paragrafo: '', descricao: 'Todo idoso tem direito a vacinar-se contra gripe, pneumonia, difteria e febre amarela.', fontes: 'Portaria 1498/2013' },
            { taxonomia: 'Mobilidade,onibus,ônibus,transporte,beneficios', secao: 'Mobilidade', lei: '', paragrafo: '', descricao: 'Gratuidade no transporte coletivo público urbano e semiurbano, com reserva de 10% dos assentos, os quais deverão ser identificados com placa de reserva.', fontes: 'Estatuto do Idoso (Lei nº 10.741, de 1º de outubro de 2003)<br>Artigo 39' },
            { taxonomia: 'Mobilidade,onibus,ônibus,transporte,beneficios', secao: 'Mobilidade', lei: '', paragrafo: '', descricao: 'Reserva de duas vagas gratuitas no transporte interestadual para idosos com renda igual ou inferior a dois salários mínimos e desconto de 50% para os idosos que excedam as vagas garantidas.', fontes: 'Estatuto do Idoso (Lei nº 10.741, de 1º de outubro de 2003)<br>Artigo 40' },
            { taxonomia: 'Mobilidade,transporte,carro,beneficios', secao: 'Mobilidade', lei: '', paragrafo: '', descricao: 'Reserva de 5% das vagas nos estacionamentos públicos e privados.', fontes: 'Estatuto do Idoso (Lei nº 10.741, de 1º de outubro de 2003)<br>Artigo 41' },
            { taxonomia: 'Mobilidade,transporte,carro,beneficios,segurança', secao: 'Prioridades', lei: '', paragrafo: '', descricao: 'O idoso tem direito a prioridade e segurança no embarque e desembarque do transporte público.', fontes: 'Estatuto do Idoso (Lei nº 10.741, de 1º de outubro de 2003)<br>Artigo 42' },
            { taxonomia: 'Prioridades,judiciario,justica,servicos,serviços,beneficios', secao: 'Prioridades', lei: '', paragrafo: '', descricao: 'Na tramitação dos processos e procedimentos na execução de atos e diligências judiciais.', fontes: 'Estatuto do Idoso (Lei nº 10.741, de 1º de outubro de 2003)<br>Artigo 71' },
            { taxonomia: 'Prioridades,servicos,serviços,beneficios', secao: 'Prioridades', lei: '', paragrafo: '', descricao: 'Em filas de atendimento de serviços nos setores público e privado, tais como bancos, lojas, supermecados etc.', fontes: 'Estatuto do Idoso (Lei nº 10.741, de 1º de outubro de 2003)<br>Artigo 3 §1' },
            { taxonomia: 'Prioridades', secao: 'Prioridades', lei: '', paragrafo: '', descricao: 'Na restituição do Imposto de Renda', fontes: 'Estatuto do Idoso (Lei nº 10.741, de 1º de outubro de 2003)<br>Artigo 71' },
            { taxonomia: 'Renda,beneficios', secao: 'Renda', lei: '', paragrafo: '', descricao: 'Direito de requerer o Benefício de Prestação Continuada (BPC), a partir dos 65 anos de idade, desde que não possua meios para prover sua própria subsistência ou de tê-la provida pela família.', fontes: 'Lei Orgânica de Assistência Social (Lei 8742 de 1993)<br>Artigo 20' },
            { taxonomia: 'Renda,beneficios', secao: 'Renda', lei: '', paragrafo: '', descricao: 'Direito a 25% de acréscimo na aposentadoria por invalidez (casos especiais).', fontes: 'Lei 8213 de 1991.<br>Artigo 45' },
            { taxonomia: 'Renda,beneficios', secao: 'Renda', lei: '', paragrafo: '', descricao: 'O idoso sem condições de subsistência tem direito a solicitar pensão alimentícia de descendente.', fontes: 'Estatuto do Idoso (Lei nº 10.741, de 1º de outubro de 2003)<br>Artigo 11' },
            { taxonomia: 'Outros,Segurança', secao: 'Outros', lei: '', paragrafo: '', descricao: 'Direito de exigir medidas de proteção sempre que seus direitos estiverem ameaçados ou violados por ação ou omissão da sociedade, do Estado, da família, de seu curador ou de entidades de atendimento.', fontes: 'Estatuto do Idoso (Lei nº 10.741, de 1º de outubro de 2003)<br>Artigo 43' },
            { taxonomia: 'Outros', secao: 'Outros', lei: '', paragrafo: '', descricao: 'É vedada a fixação de limite máximo de idade em seleção para qualquer trabalho, emprego ou concurso público, ressalvados os casos em que a natureza do cargo exigir.', fontes: 'Estatuto do Idoso (Lei nº 10.741, de 1º de outubro de 2003)<br>Artigo 27' },
            { taxonomia: 'Lazer,Ingressos,Cinema,shows,espetaculos,futebol,volei,basquete', secao: 'Lazer', lei: '', paragrafo: '', descricao: 'Desconto de pelo menos 50% nos ingressos para eventos artísticos, culturais, esportivos e de lazer.', fontes: 'Estatuto do Idoso (Lei nº 10.741, de 1º de outubro de 2003)<br>Artigo 23' },
            { taxonomia: 'Habitação,Habitacao,prioridade,casa,moradia', secao: 'Habitação', lei: 'Estatuto do Idoso<br>Decreto 1948/1996', paragrafo: '', descricao: 'Nos programas habitacionais, públicos ou subsidiados com recursos públicos, o idoso tem prioridade na aquisição de imóvel para moradia própria.', fontes: 'Estatuto do Idoso (Lei nº 10.741, de 1º de outubro de 2003)<br>Artigo 38' },
            { taxonomia: 'Habitação,Habitacao,casa,moradia,asilo', secao: 'Habitação', lei: 'Estatuto do Idoso<br>Decreto 1948/1996', paragrafo: '', descricao: 'Idosos sem renda suficiente para sua manutenção e sem família têm direito a acolhimento em Casa-lar.', fontes: 'Decreto 1948/1996<br>Artigo 4' },
        ];
    };
    DireitosSecundariaPage.prototype.filterTechnologies = function (param) {
        var _this = this;
        this.declareTechnologies();
        var val = param;
        // DON'T filter the technologies IF the supplied input is an empty string
        if (val.trim() !== '') {
            this.technologies = this.technologies.filter(function (item) {
                _this.teste = 1;
                _this.resultado = val;
                return _this.removeAccents(item.taxonomia.toLowerCase()).indexOf(val.toLowerCase()) > -1 || _this.removeAccents(item.descricao.toLowerCase()).indexOf(val.toLowerCase()) > -1 || item.taxonomia.toLowerCase().indexOf(val.toLowerCase()) > -1 || item.descricao.toLowerCase().indexOf(val.toLowerCase()) > -1;
            });
        }
    };
    DireitosSecundariaPage.prototype.onFilter = function (category) {
        var _this = this;
        this.declareTechnologies();
        // Only filter the technologies array IF the selection is NOT equal to value of all
        if (category.trim() !== '') {
            this.technologies = this.technologies.filter(function (item) {
                _this.teste = 1;
                _this.resultado = category;
                return item.taxonomia.toLowerCase().indexOf(category.toLowerCase()) > -1;
            });
            this.categoria = '1';
        }
    };
    DireitosSecundariaPage.prototype.irSaude = function () {
        this.onFilter('saúde');
    };
    DireitosSecundariaPage.prototype.irMobilidade = function () {
        this.onFilter('Transporte');
    };
    DireitosSecundariaPage.prototype.irPrioridade = function () {
        this.onFilter('Prioridades');
    };
    DireitosSecundariaPage.prototype.irRenda = function () {
        this.onFilter('Renda');
    };
    DireitosSecundariaPage.prototype.irOutros = function () {
        this.onFilter('Outros');
    };
    DireitosSecundariaPage.prototype.irLazer = function () {
        this.onFilter('Lazer');
    };
    DireitosSecundariaPage.prototype.irHabitacao = function () {
        this.onFilter('Habitação');
    };
    DireitosSecundariaPage.prototype.irPoliticas = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__direitos_direitos__["a" /* DireitosPage */]);
    };
    /*
 goVoltar()
 {
   this.navCtrl.push(SobrePage)
 }
 */
    DireitosSecundariaPage.prototype.removeAccents = function (value) {
        return value
            .replace(/á/g, 'a')
            .replace(/ã/g, 'a')
            .replace(/â/g, 'a')
            .replace(/é/g, 'e')
            .replace(/è/g, 'e')
            .replace(/ê/g, 'e')
            .replace(/í/g, 'i')
            .replace(/ï/g, 'i')
            .replace(/ì/g, 'i')
            .replace(/ó/g, 'o')
            .replace(/ô/g, 'o')
            .replace(/õ/g, 'o')
            .replace(/ú/g, 'u')
            .replace(/ü/g, 'u')
            .replace(/ç/g, 'c')
            .replace(/ß/g, 's');
    };
    DireitosSecundariaPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-direitos-secundaria',template:/*ion-inline-start:"/home/rabaco/Documentos/projetos_ionic/idoso_aplicativo_final/src/pages/direitos-secundaria/direitos-secundaria.html"*/'<ion-header>\n    <ion-navbar>    \n<ion-title>\n  <span *ngIf="resultado==\'\'">Direitos</span>\n<span *ngIf="resultado!=\'\' && teste==1 && categoria==1">{{ resultado }}</span>\n<span *ngIf="resultado!=\'\' && categoria!=1">Resultado da busca</span>\n</ion-title>\n</ion-navbar>\n</ion-header>\n\n<ion-content>\n<ion-searchbar\n (change)="filterTechnologies($event.target.value)" placeholder="Buscar Ex: ônibus" autocomplete="on"  hideCancelButton="true" autocorrect="on" *ngIf="teste==0" class="busca"></ion-searchbar>\n <ion-item text-wrap  *ngIf="teste!=0 && categoria!=1" class="texto-busca">  {{ resultado }}</ion-item>\n <div *ngFor="let technology of technologies; let i=index" >     \n    <ion-item text-wrap class="texto-busca2" ><br>  {{ technology.descricao }} \n        <div class="texto-secao" (click)=onFilter(technology.secao) *ngIf="categoria==0" > {{ technology.secao }}</div>  \n     <div class="fonte" padding *ngIf="technology.fontes"><strong>Fonte:</strong><br><span [innerHTML]="technology.fontes"></span></div>\n     \n    </ion-item>\n  </div>\n  \n  \n\n  <ion-item text-wrap class="texto-busca2" *ngIf="teste!=0 && technologies==false">  Busca por "{{ resultado }}" não retornou nenhum resultado\n      </ion-item>\n\n<div *ngIf="teste==0" padding style="margin-top: -30px;"> \n   <button ion-item  no-lines  (click)="irSaude()" color="fundo-item" class="botao_cinza" >Saúde</button> \n    <button ion-item no-lines  (click)="irMobilidade()" color="fundo-item" class="botao_cinza" >Transporte</button>\n    <button ion-item no-lines  (click)="irLazer()" color="fundo-item" class="botao_cinza" >Lazer</button>\n    <button ion-item no-lines  (click)="irPrioridade()" color="fundo-item" class="botao_cinza" >Prioridades</button>\n    <button ion-item no-lines  (click)="irHabitacao()" color="fundo-item" class="botao_cinza" >Habitação</button>\n    <button ion-item no-lines  (click)="irRenda()" color="fundo-item" class="botao_cinza">Renda</button>\n    <button ion-item  no-lines (click)="irOutros()" color="fundo-item" class="botao_cinza">Outros</button>\n    <button ion-item no-lines  (click)="irPoliticas()" color="fundo-item" class="botao_cinza">Políticas</button>\n    \n</div>     \n \n  </ion-content>\n\n'/*ion-inline-end:"/home/rabaco/Documentos/projetos_ionic/idoso_aplicativo_final/src/pages/direitos-secundaria/direitos-secundaria.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */]])
    ], DireitosSecundariaPage);
    return DireitosSecundariaPage;
}());

//# sourceMappingURL=direitos-secundaria.js.map

/***/ }),

/***/ 133:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Orientacoes2Page; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__home_home__ = __webpack_require__(45);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__orientacoes2_secundaria_orientacoes2_secundaria__ = __webpack_require__(134);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/**
 * Generated class for the Orientacoes2Page page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var Orientacoes2Page = /** @class */ (function () {
    function Orientacoes2Page(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.teste = 0;
        this.resultado = "";
        this.subtexto = "";
        this.categoria = "";
    }
    Orientacoes2Page_1 = Orientacoes2Page;
    Orientacoes2Page.prototype.ionViewDidLoad = function () {
        // console.log('ionViewDidLoad Orientacoes2Page');
    };
    Orientacoes2Page.prototype.declareTechnologies = function () {
        this.technologies = [
            { taxonomia: 'medicamentos,remedio', secao: 'Medicamentos', etapa: '1', descricao: 'Mantenha os medicamentos em lugares secos e frescos, seguros e específicos para este fim, fora do alcance de crianças e animais. Evite guardá-los com produtos de limpeza, perfumaria e alimentos. ', fontes: '' },
            { taxonomia: 'medicamentos,remedio', secao: 'Medicamentos', etapa: '2', descricao: 'Guarde na geladeira apenas os medicamentos líquidos, conforme orientação de um profissional de saúde. Não guarde medicamentos na porta da geladeira ou próximo do congelador. A insulina, por exemplo, perde o efeito se for congelada. ', fontes: '' },
            { taxonomia: 'medicamentos,remedio', secao: 'Medicamentos', etapa: '3', descricao: 'Se você utiliza porta-comprimidos, deixe somente a quantidade suficiente para 24 horas nos recipientes, que devem ser mantidos cuidadosamente limpos e secos.', fontes: '' },
            { taxonomia: 'medicamentos,remedio', secao: 'Medicamentos', etapa: '4', descricao: 'O armazenamento de medicamentos deve ser individualizado para evitar erros e trocas com medicamentos de outras pessoas.', fontes: '' },
            { taxonomia: 'medicamentos,remedio,higiene', secao: 'Medicamentos', etapa: '5', descricao: 'Lave as mãos antes de manusear qualquer medicamento.', fontes: '' },
            { taxonomia: 'medicamentos,remedio', secao: 'Medicamentos', etapa: '6', descricao: 'Manuseie os medicamentos em lugares claros. Leia sempre o nome para evitar trocas.', fontes: '' },
            { taxonomia: 'medicamentos,remedio', secao: 'Medicamentos', etapa: '7', descricao: 'É importante o uso regular dos medicamentos, observando-se os horários prescritos.', fontes: '' },
            { taxonomia: 'medicamentos,remedio,manuseio', secao: 'Medicamentos', etapa: '8', descricao: 'Tome os comprimidos e as cápsulas sempre com água ou conforme a orientação de um profissional de saúde.', fontes: '' },
            { taxonomia: 'medicamentos,remedio,manuseio', secao: 'Medicamentos', etapa: '9', descricao: 'Consulte seu médico ou farmacêutico caso seja necessário partir ou triturar os comprimidos.', fontes: '' },
            { taxonomia: 'medicamentos,remedio,manuseio', secao: 'Medicamentos', etapa: '10', descricao: 'Abra somente um frasco ou embalagem de cada medicamento por vez.', fontes: '' },
            { taxonomia: 'medicamentos,remedio,manuseio', secao: 'Medicamentos', etapa: '11', descricao: 'Mantenha os medicamentos nas embalagens originais para facilitar sua identificação e o controle da validade.', fontes: '' },
            { taxonomia: 'medicamentos,remedio,manuseio', secao: 'Medicamentos', etapa: '12', descricao: 'Observe frequentemente a data de validade e não tome medicamentos vencidos.', fontes: '' },
            { taxonomia: 'medicamentos,remedio,manuseio,precaucao,precaução,precaucoes,precauções,recomendacao,recomendacoes,recomendações,Recomendação', secao: 'Medicamentos', etapa: '13', descricao: 'Consulte seu médico ou farmacêutico caso observe qualquer mudança no medicamento: cor, mancha ou cheiro estranho.', fontes: '' },
            { taxonomia: 'medicamentos,remedio,manuseio', secao: 'Medicamentos', etapa: '14', descricao: 'Utilize, preferencialmente, o medidor que acompanha o medicamento e lave-o após o uso. Evite colheres caseiras', fontes: '' },
            { taxonomia: 'medicamentos,remedio,manuseio,precaucao,precaução,precaucoes,precauções,recomendacao,recomendacoes,recomendações,Recomendação', secao: 'Medicamentos', etapa: '15', descricao: 'Não passe o bico do tubo nas feridas ou na pele quando for utilizar pomadas. Você pode contaminar o medicamento. ', fontes: '' },
            { taxonomia: 'medicamentos,remedio,manuseio,precaucao,precaução,precaucoes,precauções,recomendacao,recomendacoes,recomendações,Recomendação', secao: 'Medicamentos', etapa: '16', descricao: 'Não encoste os bicos de frascos de colírio e de tubos de pomadas oftamológicas nos olhos ou na pele.', fontes: '' },
            { taxonomia: 'medicamentos,remedio,manuseio,precaucao,precaução,precaucoes,precauções,recomendacao,recomendacoes,recomendações,Recomendação', secao: 'Medicamentos', etapa: '17', descricao: 'Sempre leve as receitas, os exames e os medicamentos em uso nos atendimentos médicos. Informe o médico sobre o uso de chás ou plantas medicinais.', fontes: '' },
            { taxonomia: 'medicamentos,remedio,manuseio,precaucao,precaução,precaucoes,precauções,recomendacao,recomendacoes,recomendações,Recomendação', secao: 'Medicamentos', etapa: '18', descricao: 'Mantenha a receita médica junto dos medicamentos.', fontes: '' },
            { taxonomia: 'medicamentos,remedio,manuseio,precaucao,precaução,precaucoes,precauções,recomendacao,recomendacoes,recomendações,Recomendação', secao: 'Medicamentos', etapa: '19', descricao: 'Nunca espere o medicamento acabar para providenciar nova receita, para comprá-lo ou buscá-lo na unidade de saúde.', fontes: '' },
            { taxonomia: 'medicamentos,remedio,manuseio,precaucao,precaução,precaucoes,precauções,recomendacao,recomendacoes,recomendações,Recomendação', secao: 'Medicamentos', etapa: '20', descricao: 'Os medicamentos que não estão sendo utilizados devem ser guardados em local separado dos que estão em uso.', fontes: '' },
            { taxonomia: 'alimentação,alimentacao,recomendacao,recomendacoes,recomendações,Recomendação', secao: 'Alimentação', etapa: '1° passo', descricao: 'Faça três refeições ao dia (café da manhã, almoço e jantar) e, caso necessite, faça outras refeições nos intervalos.', fontes: '' },
            { taxonomia: 'alimentação,alimentacao,recomendacao,recomendacoes,recomendações,Recomendação', secao: 'Alimentação', etapa: '2° passo', descricao: 'Dê preferência aos grãos integrais e aos alimentos em sua forma mais natural. Inclua alimentos como arroz, milho, batata, mandioca / macaxeira / aipim nas principais refeições. ', fontes: '' },
            { taxonomia: 'alimentação,alimentacao,recomendacao,recomendacoes,recomendações,Recomendação', secao: 'Alimentação', etapa: '3° passo', descricao: 'Inclua frutas, legumes e verduras em todas as refeições ao longo do dia.', fontes: '' },
            { taxonomia: 'alimentação,alimentacao,recomendacao,recomendacoes,recomendações,Recomendação', secao: 'Alimentação', etapa: '4° passo', descricao: 'Coma feijão com arroz, de preferência no almoço ou jantar.', fontes: '' },
            { taxonomia: 'alimentação,alimentacao,recomendacao,recomendacoes,recomendações,Recomendação', secao: 'Alimentação', etapa: '5° passo', descricao: 'Lembre-se de incluir carnes, aves, peixes ou ovos e leite e derivados em pelo menos uma refeição durante o dia.', fontes: '' },
            { taxonomia: 'alimentação,alimentacao,recomendacao,recomendacoes,recomendações,Recomendação', secao: 'Alimentação', etapa: '6° passo', descricao: 'Use pouca quantidade de óleos, gorduras, açúcar e sal no preparo dos alimentos.', fontes: '' },
            { taxonomia: 'alimentação,alimentacao,recomendacao,recomendacoes,recomendações,Recomendação', secao: 'Alimentação', etapa: '7° passo', descricao: 'Beba água mesmo sem sentir sede, de preferência nos intervalos entre as refeições.', fontes: '' },
            { taxonomia: 'alimentação,alimentacao,recomendacao,recomendacoes,recomendações,Recomendação', secao: 'Alimentação', etapa: '8° passo', descricao: 'Evite bebidas açucaradas (refrigerantes, sucos e chás industrializados), bolos e biscoitos recheados, doces e outras guloseimas como regra da alimentação.', fontes: '' },
            { taxonomia: 'alimentação,alimentacao,recomendacao,recomendacoes,recomendações,Recomendação', secao: 'Alimentação', etapa: '9° passo', descricao: 'Fique atento(a) às informações nutricionais disponíveis nos rótulos dos produtos processados e ultraprocessados para favorecer a escolha de produtos alimentícios mais saudáveis.', fontes: '' },
            { taxonomia: 'alimentação,alimentacao,recomendacao,recomendacoes,recomendações,Recomendação', secao: 'Alimentação', etapa: '10° passo', descricao: 'Sempre que possível, coma em companhia.', fontes: '' },
            { taxonomia: 'saúde bucal,saude bucal,recomendacao,recomendacoes,recomendações,Recomendação', secao: 'Saúde bucal', etapa: '1', descricao: 'Escove bem os dentes após cada refeição e antes de dormir.', fontes: '' },
            { taxonomia: 'saúde bucal,saude bucal,recomendacao,recomendacoes,recomendações,Recomendação', secao: 'Saúde bucal', etapa: '2', descricao: 'Use escova de tamanho adequado com cerdas macias e creme dental com flúor.', fontes: '' },
            { taxonomia: 'saúde bucal,saude bucal,recomendacao,recomendacoes,recomendações,Recomendação', secao: 'Saúde bucal', etapa: '3', descricao: 'Passe fio dental entre todos os dentes', fontes: '' },
            { taxonomia: 'saúde bucal,saude bucal,recomendacao,recomendacoes,recomendações,Recomendação', secao: 'Saúde bucal', etapa: '4', descricao: 'Escove a língua', fontes: '' },
            { taxonomia: 'prótese dental,recomendacao,recomendacoes,recomendações,Recomendação', secao: 'Prótese dental', etapa: '1', descricao: 'Limpe-a fora da boca, com sabão ou pasta de dente pouco abrasiva e escova de dentes macia, separada para esta função', fontes: '' },
            { taxonomia: 'prótese dental,recomendacao,recomendacoes,recomendações,Recomendação', secao: 'Prótese dental', etapa: '2', descricao: 'Antes de recolocá-la na boca, escove os dentes e/ou limpe a gengiva, o céu da boca e a língua', fontes: '' },
            { taxonomia: 'prótese dental,recomendacao,recomendacoes,recomendações,Recomendação', secao: 'Prótese dental', etapa: '3', descricao: 'Fique sem a prótese por algumas horas por dia. O ideal é passar a noite sem ela', fontes: '' },
            { taxonomia: 'prótese dental,recomendacao,recomendacoes,recomendações,Recomendação', secao: 'Prótese dental', etapa: '4', descricao: 'Quando a prótese estiver fora da boca, deixe-a sempre em um copo com água', fontes: '' },
            { taxonomia: 'prótese dental,recomendacao,recomendacoes,recomendações,Recomendação', secao: 'Prótese dental', etapa: '5', descricao: 'Procure o dentista para orientação sobre outros produtos complementares de limpeza de dentaduras', fontes: '' },
            { taxonomia: 'prótese dental,recomendacao,recomendacoes,recomendações,Recomendação', secao: 'Prótese dental', etapa: '6', descricao: 'Se a prótese começar a ficar solta, dificultar a mastigação ou casuar irrtações e machucados na gengiva, pode ser hora de trocá-la', fontes: '' },
            { taxonomia: 'Dentista,recomendacao,recomendacoes,recomendações,Recomendação', secao: 'Dentista', etapa: '1', descricao: 'Ajudar da detectar cáries, que são causadas por bactérias que utilizam o açúcar da nossa alimentação para produzir ácidos que destroem os dentes', fontes: '' },
            { taxonomia: 'Dentista,recomendacao,recomendacoes,recomendações,Recomendação', secao: 'Dentista', etapa: '2', descricao: 'Identificar doenças nas gengivas, como a gengivite (inflamação das gengivas) e a periodontite (inflamação que pode chegar a provocar a perda dos dentes)', fontes: '' },
            { taxonomia: 'Dentista,recomendacao,recomendacoes,recomendações,Recomendação', secao: 'Dentista', etapa: '3', descricao: 'Recomendar métodos, tratamentos e produtos para evitar que a "boca seca", o que é comum entre idosos em função de alguns medicamentos ou distúrbios de saúde', fontes: '' },
            { taxonomia: 'Dentista,recomendacao,recomendacoes,recomendações,Recomendação', secao: 'Dentista', etapa: '4', descricao: 'Identificar lesões na boca, tais como manchas, caroços, inchaços, placas esbranquiçadas ou avermelhadas e feridas, avaliar sua gravidade e indicar tratamento', fontes: '' },
            { taxonomia: 'Dentista,recomendacao,recomendacoes,recomendações,Recomendação', secao: 'Dentista', etapa: '5', descricao: 'Verificar a adaptação da prótese periodicamente e indicar a substituição, quando necessário', fontes: '' },
            { taxonomia: 'quedas,recomendacao,recomendacoes,recomendações,Recomendação,precaucao,precaução,precaucoes,precauções,', secao: 'Quedas', etapa: '1', descricao: 'Evite tapetes soltos', fontes: '' },
            { taxonomia: 'quedas,recomendacao,recomendacoes,recomendações,Recomendação,precaucao,precaução,precaucoes,precauções,', secao: 'Quedas', etapa: '2', descricao: 'Escadas devem ter corrimãos dos dois lados', fontes: '' },
            { taxonomia: 'quedas,recomendacao,recomendacoes,recomendações,Recomendação,precaucao,precaução,precaucoes,precauções,', secao: 'Quedas', etapa: '3', descricao: 'Use sapatos fechados e com solado de borracha', fontes: '' },
            { taxonomia: 'quedas,recomendacao,recomendacoes,recomendações,Recomendação,precaucao,precaução,precaucoes,precauções,', secao: 'Quedas', etapa: '4', descricao: 'Coloque tapete anti-derrapante no banheiro', fontes: '' },
            { taxonomia: 'quedas,recomendacao,recomendacoes,recomendações,Recomendação,precaucao,precaução,precaucoes,precauções,', secao: 'Quedas', etapa: '5', descricao: 'Evite andar em áreas com o piso úmido', fontes: '' },
            { taxonomia: 'quedas,recomendacao,recomendacoes,recomendações,Recomendação,precaucao,precaução,precaucoes,precauções,', secao: 'Quedas', etapa: '6', descricao: 'Evite encerar a casa', fontes: '' },
            { taxonomia: 'atividades físicas,recomendacao,recomendacoes,recomendações,Recomendação,precaucao,precaução,precaucoes,precauções,', secao: 'Atividades físicas', etapa: '1', descricao: 'Exercícios posturais.', fontes: '' },
            { taxonomia: 'atividades físicas,recomendacao,recomendacoes,recomendações,Recomendação,precaucao,precaução,precaucoes,precauções,', secao: 'Atividades físicas', etapa: '2', descricao: 'Jogos e modalidades esportivas.', fontes: '' },
            { taxonomia: 'atividades físicas,recomendacao,recomendacoes,recomendações,Recomendação,precaucao,precaução,precaucoes,precauções,', secao: 'Atividades físicas', etapa: '3', descricao: 'Alongamentos e relaxamentos.', fontes: '' },
            { taxonomia: 'atividades físicas,recomendacao,recomendacoes,recomendações,Recomendação,precaucao,precaução,precaucoes,precauções,', secao: 'Atividades físicas', etapa: '4', descricao: 'Exercícios respiratórios.', fontes: '' },
            { taxonomia: 'atividades físicas,recomendacao,recomendacoes,recomendações,Recomendação,precaucao,precaução,precaucoes,precauções,', secao: 'Atividades físicas', etapa: '5', descricao: 'Exercícios resistidos como musculação e ginástica.', fontes: '' },
            { taxonomia: 'atividades físicas,recomendacao,recomendacoes,recomendações,Recomendação,precaucao,precaução,precaucoes,precauções,', secao: 'Atividades físicas', etapa: '6', descricao: 'Caminhada e corrida.', fontes: '' },
            { taxonomia: 'atividades físicas,recomendacao,recomendacoes,recomendações,Recomendação,precaucao,precaução,precaucoes,precauções,', secao: 'Atividades físicas', etapa: '7', descricao: 'Natação e hidroginástica.', fontes: '' },
            { taxonomia: 'atividades físicas,recomendacao,recomendacoes,recomendações,Recomendação,precaucao,precaução,precaucoes,precauções,', secao: 'Atividades físicas', etapa: '8', descricao: 'Práticas corporais orientais, como o <i>tai chi chuan, yoga e lian gong.</i>', fontes: '' },
            { taxonomia: 'atividades físicas,recomendacao,recomendacoes,recomendações,Recomendação,precaucao,precaução,precaucoes,precauções,', secao: 'Atividades físicas', etapa: '9', descricao: 'Capoterapia e danças, como a dança sênior.', fontes: '' },
            { taxonomia: 'sexualidade,sexo,relacionamento,recomendacao,recomendacoes,recomendações,Recomendação,precaucao,precaução,precaucoes,precauções,', secao: 'Sexualidade', etapa: '1', descricao: 'A sexualidade e a sensualidade continuam fazendo parte de nossas vidas, independentemente da idade.', fontes: '' },
            { taxonomia: 'sexualidade,sexo,doença,doenca,doencas,doencas,tabaco,bebida,recomendacao,recomendacoes,recomendações,Recomendação,precaucao,precaução,precaucoes,precauções,', secao: 'Sexualidade', etapa: '2', descricao: 'Algumas condições podem interferir na vida sexual, como diabetes, colesterol alto, fumo, álcool, menopausa e uso de alguns medicamentos.', fontes: '' },
            { taxonomia: 'sexualidade,sexo,medico,médico,relacionamento,recomendacao,recomendacoes,recomendações,Recomendação,precaucao,precaução,precaucoes,precauções,', secao: 'Sexualidade', etapa: '3', descricao: 'Cuidado com o uso de medicamentos que prometem melhorar o desempenho sexual. Todo medicamento só deve ser usado sob orientação médica.', fontes: '' },
            { taxonomia: 'sexualidade,sexo,relacionamento,recomendacao,recomendacoes,recomendações,Recomendação,precaucao,precaução,precaucoes,precauções,', secao: 'Sexualidade', etapa: '4', descricao: 'Se necessário, faça uso de lubrificantes. Eles facilitam a penetração e a tornam mais prazerosa.', fontes: '' },
            { taxonomia: 'sexualidade,sexo,relacionamento,recomendacao,recomendacoes,recomendações,Recomendação,precaucao,precaução,precaucoes,precauções,', secao: 'Sexualidade', etapa: '5', descricao: 'Faça exames para saber como está a sua saúde. Muitas vezes, o desempenho sexual pode estar relacionado a algum problema de saúde.', fontes: '' },
            { taxonomia: 'sexualidade,sexo,preservativo,doenca,doença,doencas,doenças,relacionamento,recomendacao,recomendacoes,recomendações,Recomendação,precaucao,precaução,precaucoes,precauções,', secao: 'Sexualidade', etapa: '6', descricao: 'Não se esqueça de que a idade não lhe dará proteção contra as infecções sexualmente transmissíveis (IST), como gonorreia, sífilis, aids, hepatite C e outras. A camisinha (masculina ou feminina) continua sendo uma das melhores formas de prevenção e deve ser usada nas relações sexuais em qualquer idade.', fontes: '' },
            { taxonomia: 'sexualidade,sexo,doencas,doenças,relacionamento,recomendacao,recomendacoes,recomendações,Recomendação,precaucao,precaução,precaucoes,precauções,', secao: 'Sexualidade', etapa: '7', descricao: 'Outra forma de prevenção é fazer os testes para sífilis, HIV/Aids e hepatite C. Quanto antes esses agravos forem identificados, mais cedo o tratamento pode ser começado, melhorando sua qualidade de vida e impedindo a transmissão de infecções para seus(suas) parceiros(as). Solicite os testes que estão disponíveis na rede pública de saúde. São rápidos, seguros, gratuitos e sigilosos.', fontes: '' },
        ];
    };
    Orientacoes2Page.prototype.filterTechnologies = function (param) {
        var _this = this;
        this.declareTechnologies();
        var val = param;
        // DON'T filter the technologies IF the supplied input is an empty string
        if (val.trim() !== '') {
            this.technologies = this.technologies.filter(function (item) {
                _this.teste = 1;
                _this.resultado = val;
                return _this.removeAccents(item.taxonomia.toLowerCase()).indexOf(val.toLowerCase()) > -1 || _this.removeAccents(item.descricao.toLowerCase()).indexOf(val.toLowerCase()) > -1 || item.taxonomia.toLowerCase().indexOf(val.toLowerCase()) > -1 || item.descricao.toLowerCase().indexOf(val.toLowerCase()) > -1;
            });
        }
    };
    Orientacoes2Page.prototype.onFilter = function (category) {
        var _this = this;
        this.declareTechnologies();
        // Only filter the technologies array IF the selection is NOT equal to value of all
        if (category.trim() !== '') {
            this.technologies = this.technologies.filter(function (item) {
                _this.teste = 1;
                if (category == "Medicamentos") {
                    _this.resultado = "Medicamentos";
                    _this.subtexto = "Orientações quando ao uso e armazenamento de medicamentos";
                }
                if (category == "Alimentação") {
                    _this.resultado = "Alimentação";
                    _this.subtexto = "Dez passos para uma alimentação saudável";
                }
                if (category == "Saúde bucal") {
                    _this.resultado = "Saúde bucal";
                    _this.subtexto = 'Manter uma boa saúde bucal é importante para o bem-estar, a autoestima e a saúde geral de seu corpo. Veja algumas dicas:';
                }
                if (category == "Quedas") {
                    _this.resultado = "Quedas";
                    _this.subtexto = "Para evitar quedas em seu domicílio, alguns cuidados são importantes, tais como:";
                }
                if (category == "Atividades físicas") {
                    _this.resultado = "Atividades físicas";
                    _this.subtexto = "O Programa Academia da Saúde, os Núcleos de Apoio à Saúde da Família (NASFs) e as Unidades Básicas de Saúde (UBS) oferecem opções para a prática de exercícios regulares no Sistema Único de Saúde. Informe-se na sua unidade de saúde. <br>É recomendável buscar orientação de um profissional antes de iniciar um programa de atividades físicas.<br><strong>Dicas de atividades físicas e práticas corporais:</strong>";
                }
                if (category == "Sexualidade") {
                    _this.resultado = "Sexualidade";
                }
                if (category == "Prótese dental") {
                    _this.resultado = "Prótese dental";
                    _this.subtexto = "Se você usa prótese dental (dentadura/ponte móvel):";
                }
                if (category == "Dentista") {
                    _this.resultado = "Dentista";
                    _this.subtexto = "Além da higiene, a ida regular ao dentista contribui para a manutenção da saúde bucal. Veja aspectos que esse profissional pode te ajudar:";
                }
                //this.resultado=category;
                _this.categoria = '1';
                return item.taxonomia.toLowerCase().indexOf(category.toLowerCase()) > -1;
            });
        }
    };
    Orientacoes2Page.prototype.filterTechnologies2 = function (param) {
        var val2 = param;
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__orientacoes2_secundaria_orientacoes2_secundaria__["a" /* Orientacoes2SecundariaPage */], { param: val2 });
        console.log(val2);
    };
    Orientacoes2Page.prototype.irMedicamentos = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__orientacoes2_secundaria_orientacoes2_secundaria__["a" /* Orientacoes2SecundariaPage */], { category: 'Medicamentos' });
        //this.onFilter('Medicamentos');
    };
    Orientacoes2Page.prototype.irAlimentacao = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__orientacoes2_secundaria_orientacoes2_secundaria__["a" /* Orientacoes2SecundariaPage */], { category: 'Alimentação' });
        //this.onFilter('Alimentação');
    };
    Orientacoes2Page.prototype.irBucal = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__orientacoes2_secundaria_orientacoes2_secundaria__["a" /* Orientacoes2SecundariaPage */], { category: 'Saúde bucal' });
        //this.onFilter('Saúde bucal');
    };
    Orientacoes2Page.prototype.irProtese = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__orientacoes2_secundaria_orientacoes2_secundaria__["a" /* Orientacoes2SecundariaPage */], { category: 'Prótese dental' });
        //this.onFilter('Prótese dental');
    };
    Orientacoes2Page.prototype.irDentista = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__orientacoes2_secundaria_orientacoes2_secundaria__["a" /* Orientacoes2SecundariaPage */], { category: 'Dentista' });
        //this.onFilter('Dentista');
    };
    Orientacoes2Page.prototype.irQuedas = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__orientacoes2_secundaria_orientacoes2_secundaria__["a" /* Orientacoes2SecundariaPage */], { category: 'Quedas' });
        //this.onFilter('Quedas');
    };
    Orientacoes2Page.prototype.irAtividades = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__orientacoes2_secundaria_orientacoes2_secundaria__["a" /* Orientacoes2SecundariaPage */], { category: 'Atividades físicas' });
        //this.onFilter('Atividades físicas');
    };
    Orientacoes2Page.prototype.irSexualidade = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__orientacoes2_secundaria_orientacoes2_secundaria__["a" /* Orientacoes2SecundariaPage */], { category: 'Sexualidade' });
        //this.onFilter('Sexualidade');
    };
    /*
      
    goVoltar()
    {
      this.navCtrl.push(Orientacoes2Page)
    }
    */
    Orientacoes2Page.prototype.goVoltar = function (teste) {
        if (teste == 1) {
            this.navCtrl.push(Orientacoes2Page_1);
        }
        else {
            this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_2__home_home__["a" /* HomePage */]);
            this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_2__home_home__["a" /* HomePage */]);
        }
    };
    Orientacoes2Page.prototype.removeAccents = function (item) {
        return item
            .replace(/á/g, 'a')
            .replace(/ã/g, 'a')
            .replace(/â/g, 'a')
            .replace(/é/g, 'e')
            .replace(/è/g, 'e')
            .replace(/ê/g, 'e')
            .replace(/í/g, 'i')
            .replace(/ï/g, 'i')
            .replace(/ì/g, 'i')
            .replace(/ó/g, 'o')
            .replace(/ô/g, 'o')
            .replace(/õ/g, 'o')
            .replace(/ú/g, 'u')
            .replace(/ü/g, 'u')
            .replace(/ç/g, 'c')
            .replace(/ß/g, 's');
    };
    Orientacoes2Page = Orientacoes2Page_1 = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-orientacoes2',template:/*ion-inline-start:"/home/rabaco/Documentos/projetos_ionic/idoso_aplicativo_final/src/pages/orientacoes2/orientacoes2.html"*/'<ion-header>\n  <ion-navbar hideBackButton="true">\n    <ion-buttons left>\n  <button (click)="goVoltar(teste)" ion-button icon-only style="margin-left: 10px">\n    <ion-icon name="md-arrow-back"></ion-icon>\n  </button>\n</ion-buttons>\n<ion-title>\n<span *ngIf="resultado==\'\'">Orientações</span>\n<span *ngIf="resultado!=\'\' && teste==1 && categoria==1">{{ resultado }}</span>\n<span *ngIf="resultado!=\'\' && categoria!=1">Resultado da busca</span>\n</ion-title>\n</ion-navbar>\n</ion-header>\n\n<ion-content>\n<ion-searchbar\n(change)="filterTechnologies2($event.target.value)" placeholder="Buscar Ex: Precaução" autocomplete="on"  hideCancelButton="true" autocorrect="on" *ngIf="teste==0"></ion-searchbar>\n<ion-item text-wrap  *ngIf="teste!=0" class="texto-busca" ><span *ngIf="categoria==\'\'"><!-- {{ resultado }} --></span><div class="texto-bucal" [innerHTML]="subtexto"></div></ion-item>\n<div *ngFor="let technology of technologies; let i=index" >     \n  <ion-item text-wrap class="texto-busca2" > \n    <div *ngIf="(technology.etapa!=\'\' && teste==0) || (technology.etapa!=\'\' && categoria!=\'\') " class="texto-etapa">{{ technology.etapa }}</div>\n    <span [innerHTML]="technology.descricao"></span>   \n    <div class="texto-secao" (click)=onFilter(technology.secao) *ngIf="categoria==0"> {{ technology.secao }}</div>  \n  <!-- <div [innerHTML]="technology.descricao"></div> -->\n  </ion-item>\n  \n</div>\n<div class="fonte" padding *ngIf="teste==1"><strong>Fonte:</strong><br>Caderneta de Saúde da Pessoa Idosa<br><span class="fonte-menor">Ministério da Saúde<br>2017</span></div>\n<ion-item text-wrap class="texto-busca2" *ngIf="teste!=0 && technologies==false">  Busca por "{{ resultado }}" não retornou nenhum resultado\n    </ion-item>\n\n<div *ngIf="teste==0" padding style="margin-top: -30px;"> \n <button ion-item  no-lines  (click)="irMedicamentos()" color="fundo-item" class="botao_cinza" >Medicamentos</button> \n  <button ion-item  no-lines  (click)="irAlimentacao()" color="fundo-item" class="botao_cinza" >Alimentação</button>\n  <button ion-item no-lines  (click)="irBucal()" color="fundo-item" class="botao_cinza" >Saúde bucal</button>\n  <!--<button ion-item no-lines  (click)="irProtese()" color="fundo-item" class="botao_cinza" >Prótese dental</button>\n  <button ion-item no-lines  (click)="irDentista()" color="fundo-item" class="botao_cinza" >Dentista</button>-->\n  <button ion-item no-lines  (click)="irQuedas()" color="fundo-item" class="botao_cinza">Quedas</button>\n  <button ion-item no-lines  (click)="irAtividades()" color="fundo-item" class="botao_cinza">Atividades físicas</button>\n  <button ion-item no-lines  (click)="irSexualidade()" color="fundo-item" class="botao_cinza">Sexualidade</button>\n  \n</div>     \n\n</ion-content>\n\n'/*ion-inline-end:"/home/rabaco/Documentos/projetos_ionic/idoso_aplicativo_final/src/pages/orientacoes2/orientacoes2.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */]])
    ], Orientacoes2Page);
    return Orientacoes2Page;
    var Orientacoes2Page_1;
}());

//# sourceMappingURL=orientacoes2.js.map

/***/ }),

/***/ 134:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Orientacoes2SecundariaPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


//import { HomePage } from '../home/home';
var Orientacoes2SecundariaPage = /** @class */ (function () {
    function Orientacoes2SecundariaPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.teste = 0;
        this.resultado = "";
        this.subtexto = "";
        this.categoria = "";
        this.subsecao = 0;
        if (navParams.get("category")) {
            var ress = navParams.get("category");
            this.onFilter(ress);
        }
        if (navParams.get("param")) {
            var pa = navParams.get("param");
            this.filterTechnologies(pa);
        }
    }
    Orientacoes2SecundariaPage.prototype.ionViewDidLoad = function () {
        this.SaudeBucal();
    };
    Orientacoes2SecundariaPage.prototype.declareTechnologies = function () {
        this.technologies = [
            { taxonomia: 'medicamentos,remedio', secao: 'Medicamentos', etapa: '', descricao: 'Mantenha os medicamentos em lugares secos e frescos, seguros e específicos para este fim, fora do alcance de crianças e animais. Evite guardá-los com produtos de limpeza, perfumaria e alimentos. ', fontes: '' },
            { taxonomia: 'medicamentos,remedio', secao: 'Medicamentos', etapa: '', descricao: 'Guarde na geladeira apenas os medicamentos líquidos, conforme orientação de um profissional de saúde. Não guarde medicamentos na porta da geladeira ou próximo do congelador. A insulina, por exemplo, perde o efeito se for congelada. ', fontes: '' },
            { taxonomia: 'medicamentos,remedio', secao: 'Medicamentos', etapa: '', descricao: 'Se você utiliza porta-comprimidos, deixe somente a quantidade suficiente para 24 horas nos recipientes, que devem ser mantidos cuidadosamente limpos e secos.', fontes: '' },
            { taxonomia: 'medicamentos,remedio', secao: 'Medicamentos', etapa: '', descricao: 'O armazenamento de medicamentos deve ser individualizado para evitar erros e trocas com medicamentos de outras pessoas.', fontes: '' },
            { taxonomia: 'medicamentos,remedio,higiene', secao: 'Medicamentos', etapa: '', descricao: 'Lave as mãos antes de manusear qualquer medicamento.', fontes: '' },
            { taxonomia: 'medicamentos,remedio', secao: 'Medicamentos', etapa: '', descricao: 'Manuseie os medicamentos em lugares claros. Leia sempre o nome para evitar trocas.', fontes: '' },
            { taxonomia: 'medicamentos,remedio', secao: 'Medicamentos', etapa: '', descricao: 'É importante o uso regular dos medicamentos, observando-se os horários prescritos.', fontes: '' },
            { taxonomia: 'medicamentos,remedio,manuseio', secao: 'Medicamentos', etapa: '', descricao: 'Tome os comprimidos e as cápsulas sempre com água ou conforme a orientação de um profissional de saúde.', fontes: '' },
            { taxonomia: 'medicamentos,remedio,manuseio', secao: 'Medicamentos', etapa: '', descricao: 'Consulte seu médico ou farmacêutico caso seja necessário partir ou triturar os comprimidos.', fontes: '' },
            { taxonomia: 'medicamentos,remedio,manuseio', secao: 'Medicamentos', etapa: '', descricao: 'Abra somente um frasco ou embalagem de cada medicamento por vez.', fontes: '' },
            { taxonomia: 'medicamentos,remedio,manuseio', secao: 'Medicamentos', etapa: '', descricao: 'Mantenha os medicamentos nas embalagens originais para facilitar sua identificação e o controle da validade.', fontes: '' },
            { taxonomia: 'medicamentos,remedio,manuseio', secao: 'Medicamentos', etapa: '', descricao: 'Observe frequentemente a data de validade e não tome medicamentos vencidos.', fontes: '' },
            { taxonomia: 'medicamentos,remedio,manuseio,precaucao,precaução,precaucoes,precauções,recomendacao,recomendacoes,recomendações,Recomendação', secao: 'Medicamentos', etapa: '', descricao: 'Consulte seu médico ou farmacêutico caso observe qualquer mudança no medicamento: cor, mancha ou cheiro estranho.', fontes: '' },
            { taxonomia: 'medicamentos,remedio,manuseio', secao: 'Medicamentos', etapa: '', descricao: 'Utilize, preferencialmente, o medidor que acompanha o medicamento e lave-o após o uso. Evite colheres caseiras', fontes: '' },
            { taxonomia: 'medicamentos,remedio,manuseio,precaucao,precaução,precaucoes,precauções,recomendacao,recomendacoes,recomendações,Recomendação', secao: 'Medicamentos', etapa: '', descricao: 'Não passe o bico do tubo nas feridas ou na pele quando for utilizar pomadas. Você pode contaminar o medicamento. ', fontes: '' },
            { taxonomia: 'medicamentos,remedio,manuseio,precaucao,precaução,precaucoes,precauções,recomendacao,recomendacoes,recomendações,Recomendação', secao: 'Medicamentos', etapa: '', descricao: 'Não encoste os bicos de frascos de colírio e de tubos de pomadas oftamológicas nos olhos ou na pele.', fontes: '' },
            { taxonomia: 'medicamentos,remedio,manuseio,precaucao,precaução,precaucoes,precauções,recomendacao,recomendacoes,recomendações,Recomendação', secao: 'Medicamentos', etapa: '', descricao: 'Sempre leve as receitas, os exames e os medicamentos em uso nos atendimentos médicos. Informe o médico sobre o uso de chás ou plantas medicinais.', fontes: '' },
            { taxonomia: 'medicamentos,remedio,manuseio,precaucao,precaução,precaucoes,precauções,recomendacao,recomendacoes,recomendações,Recomendação', secao: 'Medicamentos', etapa: '', descricao: 'Mantenha a receita médica junto dos medicamentos.', fontes: '' },
            { taxonomia: 'medicamentos,remedio,manuseio,precaucao,precaução,precaucoes,precauções,recomendacao,recomendacoes,recomendações,Recomendação', secao: 'Medicamentos', etapa: '', descricao: 'Nunca espere o medicamento acabar para providenciar nova receita, para comprá-lo ou buscá-lo na unidade de saúde.', fontes: '' },
            { taxonomia: 'medicamentos,remedio,manuseio,precaucao,precaução,precaucoes,precauções,recomendacao,recomendacoes,recomendações,Recomendação', secao: 'Medicamentos', etapa: '', descricao: 'Os medicamentos que não estão sendo utilizados devem ser guardados em local separado dos que estão em uso.', fontes: '' },
            { taxonomia: 'alimentação,alimentacao,recomendacao,recomendacoes,recomendações,Recomendação', secao: 'Alimentação', etapa: '1° passo', descricao: 'Faça três refeições ao dia (café da manhã, almoço e jantar) e, caso necessite, faça outras refeições nos intervalos.', fontes: '' },
            { taxonomia: 'alimentação,alimentacao,recomendacao,recomendacoes,recomendações,Recomendação', secao: 'Alimentação', etapa: '2° passo', descricao: 'Dê preferência aos grãos integrais e aos alimentos em sua forma mais natural. Inclua alimentos como arroz, milho, batata, mandioca / macaxeira / aipim nas principais refeições. ', fontes: '' },
            { taxonomia: 'alimentação,alimentacao,recomendacao,recomendacoes,recomendações,Recomendação', secao: 'Alimentação', etapa: '3° passo', descricao: 'Inclua frutas, legumes e verduras em todas as refeições ao longo do dia.', fontes: '' },
            { taxonomia: 'alimentação,alimentacao,recomendacao,recomendacoes,recomendações,Recomendação', secao: 'Alimentação', etapa: '4° passo', descricao: 'Coma feijão com arroz, de preferência no almoço ou jantar.', fontes: '' },
            { taxonomia: 'alimentação,alimentacao,recomendacao,recomendacoes,recomendações,Recomendação', secao: 'Alimentação', etapa: '5° passo', descricao: 'Lembre-se de incluir carnes, aves, peixes ou ovos e leite e derivados em pelo menos uma refeição durante o dia.', fontes: '' },
            { taxonomia: 'alimentação,alimentacao,recomendacao,recomendacoes,recomendações,Recomendação', secao: 'Alimentação', etapa: '6° passo', descricao: 'Use pouca quantidade de óleos, gorduras, açúcar e sal no preparo dos alimentos.', fontes: '' },
            { taxonomia: 'alimentação,alimentacao,recomendacao,recomendacoes,recomendações,Recomendação', secao: 'Alimentação', etapa: '7° passo', descricao: 'Beba água mesmo sem sentir sede, de preferência nos intervalos entre as refeições.', fontes: '' },
            { taxonomia: 'alimentação,alimentacao,recomendacao,recomendacoes,recomendações,Recomendação', secao: 'Alimentação', etapa: '8° passo', descricao: 'Evite bebidas açucaradas (refrigerantes, sucos e chás industrializados), bolos e biscoitos recheados, doces e outras guloseimas como regra da alimentação.', fontes: '' },
            { taxonomia: 'alimentação,alimentacao,recomendacao,recomendacoes,recomendações,Recomendação', secao: 'Alimentação', etapa: '9° passo', descricao: 'Fique atento(a) às informações nutricionais disponíveis nos rótulos dos produtos processados e ultraprocessados para favorecer a escolha de produtos alimentícios mais saudáveis.', fontes: '' },
            { taxonomia: 'alimentação,alimentacao,recomendacao,recomendacoes,recomendações,Recomendação', secao: 'Alimentação', etapa: '10° passo', descricao: 'Sempre que possível, coma em companhia.', fontes: '' },
            { taxonomia: 'saúde bucal,saude bucal,recomendacao,recomendacoes,recomendações,Recomendação', secao: 'Saúde bucal', etapa: '', descricao: 'Escove bem os dentes após cada refeição e antes de dormir.', fontes: '' },
            { taxonomia: 'saúde bucal,saude bucal,recomendacao,recomendacoes,recomendações,Recomendação', secao: 'Saúde bucal', etapa: '', descricao: 'Use escova de tamanho adequado com cerdas macias e creme dental com flúor.', fontes: '' },
            { taxonomia: 'saúde bucal,saude bucal,recomendacao,recomendacoes,recomendações,Recomendação', secao: 'Saúde bucal', etapa: '', descricao: 'Passe fio dental entre todos os dentes', fontes: '' },
            { taxonomia: 'saúde bucal,saude bucal,recomendacao,recomendacoes,recomendações,Recomendação', secao: 'Saúde bucal', etapa: '', descricao: 'Escove a língua', subsecao: '1', fontes: '' },
            { taxonomia: 'prótese dental,recomendacao,recomendacoes,recomendações,Recomendação', secao: 'Saúde bucal', subsecao: 'Prótese dental', etapa: '', descricao: 'Limpe-a fora da boca, com sabão ou pasta de dente pouco abrasiva e escova de dentes macia, separada para esta função', fontes: '' },
            { taxonomia: 'prótese dental,recomendacao,recomendacoes,recomendações,Recomendação', secao: 'Saúde bucal', subsecao: 'Prótese dental', etapa: '', descricao: 'Antes de recolocá-la na boca, escove os dentes e/ou limpe a gengiva, o céu da boca e a língua', fontes: '' },
            { taxonomia: 'prótese dental,recomendacao,recomendacoes,recomendações,Recomendação', secao: 'Saúde bucal', subsecao: 'Prótese dental', etapa: '', descricao: 'Fique sem a prótese por algumas horas por dia. O ideal é passar a noite sem ela', fontes: '' },
            { taxonomia: 'prótese dental,recomendacao,recomendacoes,recomendações,Recomendação', secao: 'Saúde bucal', subsecao: 'Prótese dental', etapa: '', descricao: 'Quando a prótese estiver fora da boca, deixe-a sempre em um copo com água', fontes: '' },
            { taxonomia: 'prótese dental,recomendacao,recomendacoes,recomendações,Recomendação', secao: 'Saúde bucal', subsecao: 'Prótese dental', etapa: '', descricao: 'Procure o dentista para orientação sobre outros produtos complementares de limpeza de dentaduras', fontes: '' },
            { taxonomia: 'prótese dental,recomendacao,recomendacoes,recomendações,Recomendação', secao: 'Saúde bucal', subsecao: 'Prótese dental', etapa: '', descricao: 'Se a prótese começar a ficar solta, dificultar a mastigação ou causar irrtações e machucados na gengiva, pode ser hora de trocá-la', fontes: '' },
            { taxonomia: 'Dentista,recomendacao,recomendacoes,recomendações,Recomendação', secao: 'Saúde bucal', subsecao: 'Dentista', etapa: '', descricao: 'Ajudar da detectar cáries, que são causadas por bactérias que utilizam o açúcar da nossa alimentação para produzir ácidos que destroem os dentes', fontes: '' },
            { taxonomia: 'Dentista,recomendacao,recomendacoes,recomendações,Recomendação', secao: 'Saúde bucal', subsecao: 'Dentista', etapa: '', descricao: 'Identificar doenças nas gengivas, como a gengivite (inflamação das gengivas) e a periodontite (inflamação que pode chegar a provocar a perda dos dentes)', fontes: '' },
            { taxonomia: 'Dentista,recomendacao,recomendacoes,recomendações,Recomendação', secao: 'Saúde bucal', subsecao: 'Dentista', etapa: '', descricao: 'Recomendar métodos, tratamentos e produtos para evitar que a "boca seca", o que é comum entre idosos em função de alguns medicamentos ou distúrbios de saúde', fontes: '' },
            { taxonomia: 'Dentista,recomendacao,recomendacoes,recomendações,Recomendação', secao: 'Saúde bucal', subsecao: 'Dentista', etapa: '', descricao: 'Identificar lesões na boca, tais como manchas, caroços, inchaços, placas esbranquiçadas ou avermelhadas e feridas, avaliar sua gravidade e indicar tratamento', fontes: '' },
            { taxonomia: 'Dentista,recomendacao,recomendacoes,recomendações,Recomendação', secao: 'Saúde bucal', subsecao: 'Dentista', etapa: '', descricao: 'Verificar a adaptação da prótese periodicamente e indicar a substituição, quando necessário', fontes: '' },
            { taxonomia: 'quedas,recomendacao,recomendacoes,recomendações,Recomendação,precaucao,precaução,precaucoes,precauções,', secao: 'Quedas', etapa: '', descricao: 'Evite tapetes soltos', fontes: '' },
            { taxonomia: 'quedas,recomendacao,recomendacoes,recomendações,Recomendação,precaucao,precaução,precaucoes,precauções,', secao: 'Quedas', etapa: '', descricao: 'Escadas devem ter corrimãos dos dois lados', fontes: '' },
            { taxonomia: 'quedas,recomendacao,recomendacoes,recomendações,Recomendação,precaucao,precaução,precaucoes,precauções,', secao: 'Quedas', etapa: '', descricao: 'Use sapatos fechados e com solado de borracha', fontes: '' },
            { taxonomia: 'quedas,recomendacao,recomendacoes,recomendações,Recomendação,precaucao,precaução,precaucoes,precauções,', secao: 'Quedas', etapa: '', descricao: 'Coloque tapete anti-derrapante no banheiro', fontes: '' },
            { taxonomia: 'quedas,recomendacao,recomendacoes,recomendações,Recomendação,precaucao,precaução,precaucoes,precauções,', secao: 'Quedas', etapa: '', descricao: 'Evite andar em áreas com o piso úmido', fontes: '' },
            { taxonomia: 'quedas,recomendacao,recomendacoes,recomendações,Recomendação,precaucao,precaução,precaucoes,precauções,', secao: 'Quedas', etapa: '', descricao: 'Evite encerar a casa', fontes: '' },
            { taxonomia: 'atividades físicas,recomendacao,recomendacoes,recomendações,Recomendação,precaucao,precaução,precaucoes,precauções,', secao: 'Atividades físicas', etapa: '', descricao: 'Exercícios posturais.', fontes: '' },
            { taxonomia: 'atividades físicas,recomendacao,recomendacoes,recomendações,Recomendação,precaucao,precaução,precaucoes,precauções,', secao: 'Atividades físicas', etapa: '', descricao: 'Jogos e modalidades esportivas.', fontes: '' },
            { taxonomia: 'atividades físicas,recomendacao,recomendacoes,recomendações,Recomendação,precaucao,precaução,precaucoes,precauções,', secao: 'Atividades físicas', etapa: '', descricao: 'Alongamentos e relaxamentos.', fontes: '' },
            { taxonomia: 'atividades físicas,recomendacao,recomendacoes,recomendações,Recomendação,precaucao,precaução,precaucoes,precauções,', secao: 'Atividades físicas', etapa: '', descricao: 'Exercícios respiratórios.', fontes: '' },
            { taxonomia: 'atividades físicas,recomendacao,recomendacoes,recomendações,Recomendação,precaucao,precaução,precaucoes,precauções,', secao: 'Atividades físicas', etapa: '', descricao: 'Exercícios resistidos como musculação e ginástica.', fontes: '' },
            { taxonomia: 'atividades físicas,recomendacao,recomendacoes,recomendações,Recomendação,precaucao,precaução,precaucoes,precauções,', secao: 'Atividades físicas', etapa: '', descricao: 'Caminhada e corrida.', fontes: '' },
            { taxonomia: 'atividades físicas,recomendacao,recomendacoes,recomendações,Recomendação,precaucao,precaução,precaucoes,precauções,', secao: 'Atividades físicas', etapa: '', descricao: 'Natação e hidroginástica.', fontes: '' },
            { taxonomia: 'atividades físicas,recomendacao,recomendacoes,recomendações,Recomendação,precaucao,precaução,precaucoes,precauções,', secao: 'Atividades físicas', etapa: '', descricao: 'Práticas corporais orientais, como o <i>tai chi chuan, yoga e lian gong.</i>', fontes: '' },
            { taxonomia: 'atividades físicas,recomendacao,recomendacoes,recomendações,Recomendação,precaucao,precaução,precaucoes,precauções,', secao: 'Atividades físicas', etapa: '', descricao: 'Capoterapia e danças, como a dança sênior.', fontes: '' },
            { taxonomia: 'sexualidade,sexo,relacionamento,recomendacao,recomendacoes,recomendações,Recomendação,precaucao,precaução,precaucoes,precauções,', secao: 'Sexualidade', etapa: '', descricao: 'A sexualidade e a sensualidade continuam fazendo parte de nossas vidas, independentemente da idade.', fontes: '' },
            { taxonomia: 'sexualidade,sexo,doença,doenca,doencas,doencas,tabaco,bebida,recomendacao,recomendacoes,recomendações,Recomendação,precaucao,precaução,precaucoes,precauções,', secao: 'Sexualidade', etapa: '', descricao: 'Algumas condições podem interferir na vida sexual, como diabetes, colesterol alto, fumo, álcool, menopausa e uso de alguns medicamentos.', fontes: '' },
            { taxonomia: 'sexualidade,sexo,medico,médico,relacionamento,recomendacao,recomendacoes,recomendações,Recomendação,precaucao,precaução,precaucoes,precauções,', secao: 'Sexualidade', etapa: '', descricao: 'Cuidado com o uso de medicamentos que prometem melhorar o desempenho sexual. Todo medicamento só deve ser usado sob orientação médica.', fontes: '' },
            { taxonomia: 'sexualidade,sexo,relacionamento,recomendacao,recomendacoes,recomendações,Recomendação,precaucao,precaução,precaucoes,precauções,', secao: 'Sexualidade', etapa: '', descricao: 'Se necessário, faça uso de lubrificantes. Eles facilitam a penetração e a tornam mais prazerosa.', fontes: '' },
            { taxonomia: 'sexualidade,sexo,relacionamento,recomendacao,recomendacoes,recomendações,Recomendação,precaucao,precaução,precaucoes,precauções,', secao: 'Sexualidade', etapa: '', descricao: 'Faça exames para saber como está a sua saúde. Muitas vezes, o desempenho sexual pode estar relacionado a algum problema de saúde.', fontes: '' },
            { taxonomia: 'sexualidade,sexo,preservativo,doenca,doença,doencas,doenças,relacionamento,recomendacao,recomendacoes,recomendações,Recomendação,precaucao,precaução,precaucoes,precauções,', secao: 'Sexualidade', etapa: '', descricao: 'Não se esqueça de que a idade não lhe dará proteção contra as infecções sexualmente transmissíveis (IST), como gonorreia, sífilis, aids, hepatite C e outras. A camisinha (masculina ou feminina) continua sendo uma das melhores formas de prevenção e deve ser usada nas relações sexuais em qualquer idade.', fontes: '' },
            { taxonomia: 'sexualidade,sexo,doencas,doenças,relacionamento,recomendacao,recomendacoes,recomendações,Recomendação,precaucao,precaução,precaucoes,precauções,', secao: 'Sexualidade', etapa: '', descricao: 'Outra forma de prevenção é fazer os testes para sífilis, HIV/Aids e hepatite C. Quanto antes esses agravos forem identificados, mais cedo o tratamento pode ser começado, melhorando sua qualidade de vida e impedindo a transmissão de infecções para seus(suas) parceiros(as). Solicite os testes que estão disponíveis na rede pública de saúde. São rápidos, seguros, gratuitos e sigilosos.', fontes: '' },
        ];
    };
    Orientacoes2SecundariaPage.prototype.SaudeBucal = function () {
        this.saudebucais = [
            { taxonomia: 'prótese dental,recomendacao,recomendacoes,recomendações,Recomendação', secao: 'Saúde bucal', subsecao: 'Prótese dental', etapa: '', descricao: 'Limpe-a fora da boca, com sabão ou pasta de dente pouco abrasiva e escova de dentes macia, separada para esta função', subtitulo: 'Se você usa prótese dental (dentadura/ponte móvel):', fontes: '' },
            { taxonomia: 'prótese dental,recomendacao,recomendacoes,recomendações,Recomendação', secao: 'Saúde bucal', subsecao: 'Prótese dental', etapa: '', descricao: 'Antes de recolocá-la na boca, escove os dentes e/ou limpe a gengiva, o céu da boca e a língua', fontes: '' },
            { taxonomia: 'prótese dental,recomendacao,recomendacoes,recomendações,Recomendação', secao: 'Saúde bucal', subsecao: 'Prótese dental', etapa: '', descricao: 'Fique sem a prótese por algumas horas por dia. O ideal é passar a noite sem ela', fontes: '' },
            { taxonomia: 'prótese dental,recomendacao,recomendacoes,recomendações,Recomendação', secao: 'Saúde bucal', subsecao: 'Prótese dental', etapa: '', descricao: 'Quando a prótese estiver fora da boca, deixe-a sempre em um copo com água', fontes: '' },
            { taxonomia: 'prótese dental,recomendacao,recomendacoes,recomendações,Recomendação', secao: 'Saúde bucal', subsecao: 'Prótese dental', etapa: '', descricao: 'Procure o dentista para orientação sobre outros produtos complementares de limpeza de dentaduras', fontes: '' },
            { taxonomia: 'prótese dental,recomendacao,recomendacoes,recomendações,Recomendação', secao: 'Saúde bucal', subsecao: 'Prótese dental', etapa: '', descricao: 'Se a prótese começar a ficar solta, dificultar a mastigação ou casuar irrtações e machucados na gengiva, pode ser hora de trocá-la', fontes: '' },
            { taxonomia: 'Dentista,recomendacao,recomendacoes,recomendações,Recomendação', secao: 'Saúde bucal', subsecao: 'Dentista', etapa: '', descricao: 'Ajudar da detectar cáries, que são causadas por bactérias que utilizam o açúcar da nossa alimentação para produzir ácidos que destroem os dentes', subtitulo: 'Além da higiene, a ida regular ao dentista contribui para a manutenção da saúde bucal. Veja aspectos que esse profissional pode te ajudar:', fontes: '' },
            { taxonomia: 'Dentista,recomendacao,recomendacoes,recomendações,Recomendação', secao: 'Saúde bucal', subsecao: 'Dentista', etapa: '', descricao: 'Identificar doenças nas gengivas, como a gengivite (inflamação das gengivas) e a periodontite (inflamação que pode chegar a provocar a perda dos dentes)', fontes: '' },
            { taxonomia: 'Dentista,recomendacao,recomendacoes,recomendações,Recomendação', secao: 'Saúde bucal', subsecao: 'Dentista', etapa: '', descricao: 'Recomendar métodos, tratamentos e produtos para evitar que a "boca seca", o que é comum entre idosos em função de alguns medicamentos ou distúrbios de saúde', fontes: '' },
            { taxonomia: 'Dentista,recomendacao,recomendacoes,recomendações,Recomendação', secao: 'Saúde bucal', subsecao: 'Dentista', etapa: '', descricao: 'Identificar lesões na boca, tais como manchas, caroços, inchaços, placas esbranquiçadas ou avermelhadas e feridas, avaliar sua gravidade e indicar tratamento', fontes: '' },
            { taxonomia: 'Dentista,recomendacao,recomendacoes,recomendações,Recomendação', secao: 'Saúde bucal', subsecao: 'Dentista', etapa: '', descricao: 'Verificar a adaptação da prótese periodicamente e indicar a substituição, quando necessário', fontes: '' }
        ];
    };
    Orientacoes2SecundariaPage.prototype.filterTechnologies = function (param) {
        var _this = this;
        this.declareTechnologies();
        var val = param;
        // DON'T filter the technologies IF the supplied input is an empty string
        if (val.trim() !== '') {
            this.technologies = this.technologies.filter(function (item) {
                _this.teste = 1;
                _this.resultado = val;
                return _this.removeAccents(item.taxonomia.toLowerCase()).indexOf(val.toLowerCase()) > -1 || _this.removeAccents(item.descricao.toLowerCase()).indexOf(val.toLowerCase()) > -1 || item.taxonomia.toLowerCase().indexOf(val.toLowerCase()) > -1 || item.descricao.toLowerCase().indexOf(val.toLowerCase()) > -1;
            });
        }
    };
    Orientacoes2SecundariaPage.prototype.onFilter = function (category) {
        var _this = this;
        this.declareTechnologies();
        // Only filter the technologies array IF the selection is NOT equal to value of all
        if (category.trim() !== '') {
            this.technologies = this.technologies.filter(function (item) {
                _this.teste = 1;
                if (category == "Medicamentos") {
                    _this.resultado = "Medicamentos";
                    _this.subtexto = "Orientações quanto ao uso e armazenamento de medicamentos:";
                }
                if (category == "Alimentação") {
                    _this.resultado = "Alimentação";
                    _this.subtexto = "Dez passos para uma alimentação saudável:";
                }
                if (category == "Saúde bucal") {
                    _this.resultado = "Saúde bucal";
                    _this.subsecao = 1;
                    _this.subtexto = 'Manter uma boa saúde bucal é importante para o bem-estar, a autoestima e a saúde geral de seu corpo. Veja algumas dicas:';
                }
                if (category == "Quedas") {
                    _this.resultado = "Quedas";
                    _this.subtexto = "Para evitar quedas em seu domicílio, alguns cuidados são importantes, tais como:";
                }
                if (category == "Atividades físicas") {
                    _this.resultado = "Atividades físicas";
                    _this.subtexto = "O Programa Academia da Saúde, os Núcleos de Apoio à Saúde da Família (NASFs) e as Unidades Básicas de Saúde (UBS) oferecem opções para a prática de exercícios regulares no Sistema Único de Saúde. Informe-se na sua unidade de saúde. <br>É recomendável buscar orientação de um profissional antes de iniciar um programa de atividades físicas.<br><strong>Dicas de atividades físicas e práticas corporais:</strong>";
                }
                if (category == "Sexualidade") {
                    _this.resultado = "Sexualidade";
                }
                if (category == "Prótese dental") {
                    _this.resultado = "Prótese dental";
                    _this.subtexto = "Se você usa prótese dental (dentadura/ponte móvel):";
                }
                if (category == "Dentista") {
                    _this.resultado = "Dentista";
                    _this.subtexto = "Além da higiene, a ida regular ao dentista contribui para a manutenção da saúde bucal. Veja aspectos que esse profissional pode te ajudar:";
                }
                //this.resultado=category;
                _this.categoria = '1';
                return item.taxonomia.toLowerCase().indexOf(category.toLowerCase()) > -1;
            });
        }
    };
    Orientacoes2SecundariaPage.prototype.irMedicamentos = function () {
        this.onFilter('Medicamentos');
    };
    Orientacoes2SecundariaPage.prototype.irAlimentacao = function () {
        this.onFilter('Alimentação');
    };
    Orientacoes2SecundariaPage.prototype.irBucal = function () {
        this.onFilter('Saúde bucal');
    };
    Orientacoes2SecundariaPage.prototype.irProtese = function () {
        this.onFilter('Prótese dental');
    };
    Orientacoes2SecundariaPage.prototype.irDentista = function () {
        this.onFilter('Dentista');
    };
    Orientacoes2SecundariaPage.prototype.irQuedas = function () {
        this.onFilter('Quedas');
    };
    Orientacoes2SecundariaPage.prototype.irAtividades = function () {
        this.onFilter('Atividades físicas');
    };
    Orientacoes2SecundariaPage.prototype.irSexualidade = function () {
        this.onFilter('Sexualidade');
    };
    /*
      
    goVoltar()
    {
      this.navCtrl.push(Orientacoes2Page)
    }
    */
    Orientacoes2SecundariaPage.prototype.removeAccents = function (item) {
        return item
            .replace(/á/g, 'a')
            .replace(/ã/g, 'a')
            .replace(/â/g, 'a')
            .replace(/é/g, 'e')
            .replace(/è/g, 'e')
            .replace(/ê/g, 'e')
            .replace(/í/g, 'i')
            .replace(/ï/g, 'i')
            .replace(/ì/g, 'i')
            .replace(/ó/g, 'o')
            .replace(/ô/g, 'o')
            .replace(/õ/g, 'o')
            .replace(/ú/g, 'u')
            .replace(/ü/g, 'u')
            .replace(/ç/g, 'c')
            .replace(/ß/g, 's');
    };
    Orientacoes2SecundariaPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-orientacoes2-secundaria',template:/*ion-inline-start:"/home/rabaco/Documentos/projetos_ionic/idoso_aplicativo_final/src/pages/orientacoes2-secundaria/orientacoes2-secundaria.html"*/'<ion-header>\n    <ion-navbar>      \n  <ion-title>\n  <span *ngIf="resultado==\'\'">Orientações</span>\n  <span *ngIf="resultado!=\'\' && teste==1 && categoria==1">{{ resultado }}</span>\n  <span *ngIf="resultado!=\'\' && categoria!=1">Resultado da busca</span>\n  </ion-title>\n  </ion-navbar>\n  </ion-header>\n  \n  <ion-content>\n  <ion-searchbar\n  (change)="filterTechnologies($event.target.value)" placeholder="Buscar Ex: Precaução" autocomplete="on"  hideCancelButton="true" autocorrect="on" *ngIf="teste==0"></ion-searchbar>\n  <ion-item text-wrap  *ngIf="teste!=0 && categoria!=1" class="texto-busca" > {{ resultado }} <div class="texto-bucal" [innerHTML]="subtexto"></div></ion-item>\n  <!--<div>{{ technologies.length }}</div>-->\n  <div *ngFor="let technology of technologies; let i=index; " >     \n    <ion-item text-wrap class="texto-busca2" ><br>\n      <div *ngIf="(technology.etapa!=\'\' && teste==0) || (technology.etapa!=\'\' && categoria!=\'\');" class="texto-etapa">{{ technology.etapa }}</div>\n      <span [innerHTML]="technology.descricao"></span>   \n      <div class="texto-secao" (click)=onFilter(technology.secao) *ngIf="categoria==0"> {{ technology.secao }}</div>  \n    <!-- <div [innerHTML]="technology.descricao"></div> -->\n    </ion-item>    \n  \n\n    <div *ngFor="let saudebucal of saudebucais; let g=index" >   \n        <ion-item text-wrap class="texto-busca2" *ngIf="technology.subsecao==1" > <br>\n            <div *ngIf="(g==0 || g==6)"  class="texto-etapa"><br><br>{{ saudebucal.subsecao }}<br><span class="subtitulo"> {{ saudebucal.subtitulo }} <br><br> </span></div>\n          <div *ngIf="(saudebucal.etapa!=\'\' && teste==0) || (saudebucal.etapa!=\'\' && categoria!=\'\') " class="texto-etapa">{{ saudebucal.etapa }}</div>\n          <span [innerHTML]="saudebucal.descricao"></span>   \n          <div class="texto-secao" (click)=onFilter(saudebucal.secao) *ngIf="categoria==0">{{ saudebucal.secao }} | {{ saudebucal.subsecao }}</div>  \n        <!-- <div [innerHTML]="technology.descricao"></div> -->\n        </ion-item>\n        \n      </div>\n  </div>\n \n\n \n  <div class="fonte" padding *ngIf="teste==1"><strong>Fonte:</strong><br>Caderneta de Saúde da Pessoa Idosa<br><span class="fonte-menor">Ministério da Saúde<br>2017</span></div>\n  <ion-item text-wrap class="texto-busca2" *ngIf="teste!=0 && technologies==false">  Busca por "{{ resultado }}" não retornou nenhum resultado\n      </ion-item>\n  \n  <div *ngIf="teste==0" padding style="margin-top: -30px;"> \n   <button ion-item  no-lines  (click)="irMedicamentos()" color="fundo-item" class="botao_cinza" >Medicamentos</button> \n    <button ion-item  no-lines  (click)="irAlimentacao()" color="fundo-item" class="botao_cinza" >Alimentação</button>\n    <button ion-item no-lines  (click)="irBucal()" color="fundo-item" class="botao_cinza" >Saúde bucal</button>\n    <button ion-item no-lines  (click)="irProtese()" color="fundo-item" class="botao_cinza" >Prótese dental</button>\n    <button ion-item no-lines  (click)="irDentista()" color="fundo-item" class="botao_cinza" >Dentista</button>\n    <button ion-item no-lines  (click)="irQuedas()" color="fundo-item" class="botao_cinza">Quedas</button>\n    <button ion-item no-lines  (click)="irAtividades()" color="fundo-item" class="botao_cinza">Atividades físicas</button>\n    <button ion-item no-lines  (click)="irSexualidade()" color="fundo-item" class="botao_cinza">Sexualidade</button>\n    \n  </div>     \n  \n  </ion-content>\n  '/*ion-inline-end:"/home/rabaco/Documentos/projetos_ionic/idoso_aplicativo_final/src/pages/orientacoes2-secundaria/orientacoes2-secundaria.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */]])
    ], Orientacoes2SecundariaPage);
    return Orientacoes2SecundariaPage;
}());

//# sourceMappingURL=orientacoes2-secundaria.js.map

/***/ }),

/***/ 135:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TelefonesPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_call_number__ = __webpack_require__(190);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__home_home__ = __webpack_require__(45);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/**
 * Generated class for the TelefonesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var TelefonesPage = /** @class */ (function () {
    function TelefonesPage(navCtrl, navParams, callNumber, alertCtrl, actionSheetCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.callNumber = callNumber;
        this.alertCtrl = alertCtrl;
        this.actionSheetCtrl = actionSheetCtrl;
        this.tabBarElement = document.querySelector('#Tabs');
        this.navCtrl.parent.select(5);
    }
    TelefonesPage.prototype.goVoltar = function () {
        //this.navCtrl.popToRoot(); 
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_3__home_home__["a" /* HomePage */]);
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_3__home_home__["a" /* HomePage */]);
        //this.navCtrl.setRoot(TabsPage);
    };
    TelefonesPage.prototype.ligarAns = function () {
        var _this = this;
        var alert = this.alertCtrl.create({
            title: 'Deseja realmente fazer esta ligação telefônica?',
            message: '<div class="message">Este serviço poderá ser cobrado!!!</div>',
            cssClass: 'referencias',
            buttons: [
                {
                    text: 'Não',
                    role: 'cancel',
                    handler: function () {
                        //console.log('Cancel clicked');
                    }
                },
                {
                    text: 'Sim',
                    handler: function () {
                        // console.log('Buy clicked');
                        _this.callNumber.callNumber("08007019656", true);
                    }
                }
            ]
        });
        alert.present();
    };
    TelefonesPage.prototype.ligarBombeiros = function () {
        var _this = this;
        var alert = this.alertCtrl.create({
            title: 'Deseja realmente fazer esta ligação telefônica?',
            message: '<div class="message">Este serviço poderá ser cobrado!!!</div>',
            cssClass: 'referencias',
            buttons: [
                {
                    text: 'Não',
                    role: 'cancel',
                    handler: function () {
                        //console.log('Cancel clicked');
                    }
                },
                {
                    text: 'Sim',
                    handler: function () {
                        // console.log('Buy clicked');
                        _this.callNumber.callNumber("193", true);
                    }
                }
            ]
        });
        alert.present();
    };
    TelefonesPage.prototype.ligarViolencia = function () {
        var _this = this;
        var alert = this.alertCtrl.create({
            title: 'Deseja realmente fazer esta ligação telefônica?',
            message: '<div class="message">Este serviço poderá ser cobrado!!!</div>',
            cssClass: 'referencias',
            buttons: [
                {
                    text: 'Não',
                    role: 'cancel',
                    handler: function () {
                        //console.log('Cancel clicked');
                    }
                },
                {
                    text: 'Sim',
                    handler: function () {
                        // console.log('Buy clicked');
                        _this.callNumber.callNumber("100", true);
                    }
                }
            ]
        });
        alert.present();
    };
    TelefonesPage.prototype.ligarDisqueSaude = function () {
        var _this = this;
        var alert = this.alertCtrl.create({
            title: 'Deseja realmente fazer esta ligação telefônica?',
            message: '<div class="message">Este serviço poderá ser cobrado!!!</div>',
            cssClass: 'referencias',
            buttons: [
                {
                    text: 'Não',
                    role: 'cancel',
                    handler: function () {
                        //console.log('Cancel clicked');
                    }
                },
                {
                    text: 'Sim',
                    handler: function () {
                        // console.log('Buy clicked');
                        _this.callNumber.callNumber("136", true);
                    }
                }
            ]
        });
        alert.present();
    };
    TelefonesPage.prototype.ligarSamu = function () {
        var _this = this;
        var alert = this.alertCtrl.create({
            title: 'Deseja realmente fazer esta ligação telefônica?',
            message: '<div class="message">Este serviço poderá ser cobrado!!!</div>',
            cssClass: 'referencias',
            buttons: [
                {
                    text: 'Não',
                    role: 'cancel',
                    handler: function () {
                        //console.log('Cancel clicked');
                    }
                },
                {
                    text: 'Sim',
                    handler: function () {
                        // console.log('Buy clicked');
                        _this.callNumber.callNumber("192", true);
                    }
                }
            ]
        });
        alert.present();
    };
    TelefonesPage.prototype.ligarViolenciaMulher = function () {
        var _this = this;
        var alert = this.alertCtrl.create({
            title: '<div class="message">Deseja realmente fazer esta ligação telefônica?</div>',
            message: 'Este serviço poderá ser cobrado!!!',
            cssClass: 'referencias',
            buttons: [
                {
                    text: 'Não',
                    role: 'cancel',
                    handler: function () {
                        //console.log('Cancel clicked');
                    }
                },
                {
                    text: 'Sim',
                    handler: function () {
                        // console.log('Buy clicked');
                        _this.callNumber.callNumber("180", true);
                    }
                }
            ]
        });
        alert.present();
    };
    TelefonesPage.prototype.ligarSuicidio = function () {
        var _this = this;
        var alert = this.alertCtrl.create({
            title: 'Deseja realmente fazer esta ligação telefônica?',
            message: '<div class="message">Este serviço poderá ser cobrado!!!</div>',
            cssClass: 'referencias',
            buttons: [
                {
                    text: 'Não',
                    role: 'cancel',
                    handler: function () {
                        //console.log('Cancel clicked');
                    }
                },
                {
                    text: 'Sim',
                    handler: function () {
                        // console.log('Buy clicked');
                        _this.callNumber.callNumber("188", true);
                    }
                }
            ]
        });
        alert.present();
    };
    TelefonesPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-telefones',template:/*ion-inline-start:"/home/rabaco/Documentos/projetos_ionic/idoso_aplicativo_final/src/pages/telefones/telefones.html"*/'<!--\n  Generated template for the TelefonesPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar hideBackButton="true">\n      <ion-buttons left >\n          <button (click)="goVoltar()" ion-button icon-only style="margin-left: 10px">\n            <ion-icon name="md-arrow-back"></ion-icon>\n          </button>\n        </ion-buttons>\n    <ion-title>Telefones úteis</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n  <ion-grid>\n    <ion-row>\n      <ion-col class="contorno" >Agência Nacional de Saúde Suplementar (ANS)<br>\n        <span class="telefonar" (click)="ligarAns()">0800 701 9656</span>\n     \n      </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col class="contorno" >Corpo de Bombeiros<br>\n        <span class="telefonar" (click)="ligarBombeiros()">193</span>\n      </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col class="contorno" >Denúncias de violência<br>\n        <span class="telefonar" (click)="ligarViolencia()">100</span>\n      </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col class="contorno" >Disque saúde<br>\n        <span class="menor">Serviço gratuito.<br>Funciona de segunda a sexta-feira  (exceto feriados), das 7h às 22h.</span> <br>\n        <span class="telefonar" (click)="ligarDisqueSaude()">136</span></ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col class="contorno" >SAMU<br>\n        <span class="telefonar" (click)="ligarSamu()">192</span></ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col class="contorno" >Violência contra a mulher<br>\n        <span class="telefonar" (click)="ligarViolenciaMulher()">180</span></ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col class="contorno" >Prevenção ao suicídio<br>\n        <span class="menor">Serviço gratuito.<br>Você pode conversar com um voluntário em todo o território nacional, 24 horas todos os dias.</span><br>\n        <span class="telefonar" (click)="ligarSuicidio()">188</span></ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col class="contorno" >Site do Ministério da Saúde<br><a href="https://www.saude.gov.br">https://www.saude.gov.br</a></ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col class="contorno" >E-mail da Coordenação de Saúde da Pessoa Idosa do Ministério da Saúde<br><a href="mailto:idoso@saude.gov.br">idoso@saude.gov.br</a></ion-col>\n    </ion-row>\n  </ion-grid>\n</ion-content>\n'/*ion-inline-end:"/home/rabaco/Documentos/projetos_ionic/idoso_aplicativo_final/src/pages/telefones/telefones.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__ionic_native_call_number__["a" /* CallNumber */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* ActionSheetController */]])
    ], TelefonesPage);
    return TelefonesPage;
}());

//# sourceMappingURL=telefones.js.map

/***/ }),

/***/ 145:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 145;

/***/ }),

/***/ 18:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return EditDataPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_sqlite__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_toast__ = __webpack_require__(44);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_camera__ = __webpack_require__(100);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_forms__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__avaliacao_avaliacao__ = __webpack_require__(39);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







//import localePtBr from '@angular/common/locales/pt';
//import { registerLocaleData } from '@angular/common';
//registerLocaleData(localePtBr);
var EditDataPage = /** @class */ (function () {
    function EditDataPage(navCtrl, navParams, sqlite, toast, camera, formbuilder) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.sqlite = sqlite;
        this.toast = toast;
        this.camera = camera;
        this.formbuilder = formbuilder;
        this.data = { rowid: 1, nascimento: "", sexo: "", nome: "", imagem: "" };
        this.photo = '';
        this.getCurrentData(navParams.get("rowid"));
        this.formgroup = formbuilder.group({
            //nome: ['',Validators.required,Validators.minLength(5)]
            nome: ['', __WEBPACK_IMPORTED_MODULE_5__angular_forms__["f" /* Validators */].compose([__WEBPACK_IMPORTED_MODULE_5__angular_forms__["f" /* Validators */].maxLength(30), __WEBPACK_IMPORTED_MODULE_5__angular_forms__["f" /* Validators */].pattern('[A-Za-záàâãéèêíïóôõöúçñÁÀÂÃÉÈÍÏÓÔÕÖÚÇÑ ]*'), __WEBPACK_IMPORTED_MODULE_5__angular_forms__["f" /* Validators */].required, __WEBPACK_IMPORTED_MODULE_5__angular_forms__["f" /* Validators */].minLength(2)])]
            /*  sex: ['', Validators.required],*/
            /*  nasce: ['',Validators.required]   */
        });
        this.nome = this.formgroup.controls['nome'];
        /*  this.nasce = this.formgroup.controls['nasce'];*/
        /* this.sex = this.formgroup.controls['sex']; */
    }
    EditDataPage.prototype.takePicture = function () {
        var _this = this;
        var options = {
            quality: 70,
            destinationType: this.camera.DestinationType.DATA_URL,
            encodingType: this.camera.EncodingType.JPEG,
            mediaType: this.camera.MediaType.PICTURE,
            allowEdit: true,
            targetWidth: 640,
            targetHeight: 640
        };
        this.camera.getPicture(options)
            .then(function (imageData) {
            var base64image = 'data:image/jpeg;base64,' + imageData;
            _this.photo = base64image;
        }, function (error) {
            console.error(error);
        })
            .catch(function (error) {
            console.error(error);
        });
    };
    EditDataPage.prototype.getCurrentData = function (rowid) {
        var _this = this;
        this.sqlite.create({
            name: 'idoso.db',
            location: 'default'
        }).then(function (db) {
            db.executeSql('SELECT * FROM perfil WHERE rowid=?', [rowid])
                .then(function (res) {
                if (res.rows.length > 0) {
                    _this.data.rowid = res.rows.item(0).rowid;
                    _this.data.nome = res.rows.item(0).nome;
                    if (_this.data.nome == " ") {
                        _this.data.nome = "";
                    }
                    _this.data.sexo = res.rows.item(0).sexo;
                    _this.data.nascimento = res.rows.item(0).nascimento;
                    _this.data.imagem = res.rows.item(0).imagem;
                    _this.photo = res.rows.item(0).imagem;
                }
            })
                .catch(function (e) {
                console.log(e);
                _this.toast.show(e, '5000', 'center').subscribe(function (toast) {
                    console.log(toast);
                });
            });
        }).catch(function (e) {
            console.log(e);
            _this.toast.show(e, '5000', 'center').subscribe(function (toast) {
                console.log(toast);
            });
        });
    };
    EditDataPage.prototype.updateData = function () {
        var _this = this;
        this.sqlite.create({
            name: 'idoso.db',
            location: 'default'
        }).then(function (db) {
            db.executeSql('UPDATE perfil SET nascimento=?,sexo=?,nome=?,imagem=? WHERE rowid=?', [_this.data.nascimento, _this.data.sexo, _this.data.nome, _this.photo, _this.data.rowid])
                .then(function (res) {
                _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_6__avaliacao_avaliacao__["a" /* AvaliacaoPage */]);
                console.log(res);
                _this.toast.show('Dados Atualizados', '5000', 'center').subscribe(function (toast) {
                });
            })
                .catch(function (e) {
                console.log(e);
                _this.toast.show(e, '5000', 'center').subscribe(function (toast) {
                    console.log(toast);
                });
            });
        }).catch(function (e) {
            console.log(e);
            _this.toast.show(e, '5000', 'center').subscribe(function (toast) {
                console.log(toast);
            });
        });
    };
    EditDataPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-edit-data',template:/*ion-inline-start:"/home/rabaco/Documentos/projetos_ionic/idoso_aplicativo_final/src/pages/edit-data/edit-data.html"*/'<ion-header>\n\n  <ion-navbar>\n    <ion-title>Dados pessoais</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n<ion-content padding>\n\n    <ion-item no-lines>\n        <button ion-item large  (click)="takePicture()"  outline color="branco" float-left>\n          <div *ngIf="photo==0">\n          <img src="assets/imgs/user.png" class="icone">\n        </div>\n        <div *ngIf="photo!=0">\n        <img class="circle-pic" src="{{ photo }}"  />\n        </div>\n        </button>\n    </ion-item>\n    <form [formGroup]="formgroup" >\n    <ion-item>\n      <ion-label>Nome</ion-label>\n      <ion-input type="text" placeholder="|" [(ngModel)]="data.nome" name="description" formControlName="nome" class="campos-testes" ></ion-input>\n    </ion-item>\n    <ion-item  *ngIf="nome.hasError(\'required\') && nome.touched" text-warp no-line>\n      <p class="error"> *Nome é um campo obrigado de ser preenchido</p>\n    </ion-item>\n    <ion-item *ngIf="nome.hasError(\'minlength\') && nome.touched" text-warp no-line>\n        <p class="error">Seu nome precisa ter mais que 4 caracteres</p>\n    </ion-item>\n    <ion-item *ngIf="nome.hasError(\'maxlength\') && nome.touched" text-warp no-line>\n        <p  class="error">Máximo 30 caracteres</p>\n    </ion-item>\n    <ion-item>\n      <ion-label >Data de Nascimento</ion-label>\n      <ion-datetime displayFormat="DD/MMM/YYYY" [(ngModel)]="data.nascimento" name="nascimento" min="1880" max="1970" doneText="Confirmar" cancelText="Cancelar"  \n      monthShortNames="jan, fev, mar, abr, mai, jun, jul, ago, set, out, nov, dez"\n      [ngModelOptions]="{standalone: true}">\n    </ion-datetime>\n    </ion-item>\n    <!--\n    <ion-item>      \n      <ion-label>Sexo</ion-label>\n      <ion-select [(ngModel)]="data.sexo" name="sexo" formControlName="sex">\n        <ion-option value="Masculino">Masculino</ion-option>\n        <ion-option value="Feminino">Feminino</ion-option>\n      </ion-select>    \n    </ion-item>\n    -->\n    <ion-input type="hidden" [(ngModel)]="data.rowid" name="rowid" [ngModelOptions]="{standalone: true}"></ion-input>\n    <button class="botao" ion-item no-lines type="submit" block (click)="updateData()" color="botao" [disabled]="formgroup.invalid">Confirmar</button>\n</form>\n</ion-content>\n'/*ion-inline-end:"/home/rabaco/Documentos/projetos_ionic/idoso_aplicativo_final/src/pages/edit-data/edit-data.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__ionic_native_sqlite__["a" /* SQLite */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_native_toast__["a" /* Toast */],
            __WEBPACK_IMPORTED_MODULE_4__ionic_native_camera__["a" /* Camera */],
            __WEBPACK_IMPORTED_MODULE_5__angular_forms__["a" /* FormBuilder */]])
    ], EditDataPage);
    return EditDataPage;
}());

//# sourceMappingURL=edit-data.js.map

/***/ }),

/***/ 186:
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"../pages/add-avaliacao/add-avaliacao.module": [
		485,
		20
	],
	"../pages/add-data/add-data.module": [
		486,
		19
	],
	"../pages/add-imc/add-imc.module": [
		487,
		18
	],
	"../pages/add-panturrilha/add-panturrilha.module": [
		488,
		17
	],
	"../pages/add-ves13/add-ves13.module": [
		498,
		16
	],
	"../pages/avaliacao/avaliacao.module": [
		499,
		15
	],
	"../pages/direitos-interno/direitos-interno.module": [
		489,
		14
	],
	"../pages/direitos-secundaria/direitos-secundaria.module": [
		500,
		13
	],
	"../pages/direitos/direitos.module": [
		490,
		12
	],
	"../pages/edit-data/edit-data.module": [
		491,
		11
	],
	"../pages/inicial/inicial.module": [
		492,
		10
	],
	"../pages/list-imc/list-imc.module": [
		493,
		9
	],
	"../pages/list-panturrilha/list-panturrilha.module": [
		494,
		8
	],
	"../pages/list-ves13/list-ves13.module": [
		501,
		7
	],
	"../pages/orientacoes2-secundaria/orientacoes2-secundaria.module": [
		504,
		6
	],
	"../pages/orientacoes2/orientacoes2.module": [
		505,
		5
	],
	"../pages/resultado-imc/resultado-imc.module": [
		495,
		4
	],
	"../pages/resultado-panturrilha/resultado-panturrilha.module": [
		496,
		3
	],
	"../pages/resultado-ves13/resultado-ves13.module": [
		502,
		2
	],
	"../pages/sobre/sobre.module": [
		503,
		1
	],
	"../pages/telefones/telefones.module": [
		497,
		0
	]
};
function webpackAsyncContext(req) {
	var ids = map[req];
	if(!ids)
		return Promise.reject(new Error("Cannot find module '" + req + "'."));
	return __webpack_require__.e(ids[1]).then(function() {
		return __webpack_require__(ids[0]);
	});
};
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = 186;
module.exports = webpackAsyncContext;

/***/ }),

/***/ 363:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AddAvaliacaoPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the AddAvaliacaoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var AddAvaliacaoPage = /** @class */ (function () {
    function AddAvaliacaoPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    AddAvaliacaoPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad AddAvaliacaoPage');
    };
    AddAvaliacaoPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-add-avaliacao',template:/*ion-inline-start:"/home/rabaco/Documentos/projetos_ionic/idoso_aplicativo_final/src/pages/add-avaliacao/add-avaliacao.html"*/'<!--\n  Generated template for the AddAvaliacaoPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>add-avaliacao</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n\n</ion-content>\n'/*ion-inline-end:"/home/rabaco/Documentos/projetos_ionic/idoso_aplicativo_final/src/pages/add-avaliacao/add-avaliacao.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */]])
    ], AddAvaliacaoPage);
    return AddAvaliacaoPage;
}());

//# sourceMappingURL=add-avaliacao.js.map

/***/ }),

/***/ 364:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(365);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(387);


Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 387:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__ = __webpack_require__(36);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_component__ = __webpack_require__(437);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ng2_charts__ = __webpack_require__(438);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ng2_charts___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_ng2_charts__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_common__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_camera__ = __webpack_require__(100);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ionic_native_call_number__ = __webpack_require__(190);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__pages_contact_contact__ = __webpack_require__(484);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__pages_home_home__ = __webpack_require__(45);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__pages_inicial_inicial__ = __webpack_require__(129);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__pages_sobre_sobre__ = __webpack_require__(130);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__pages_tabs_tabs__ = __webpack_require__(64);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__pages_add_data_add_data__ = __webpack_require__(70);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__pages_edit_data_edit_data__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__pages_avaliacao_avaliacao__ = __webpack_require__(39);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__pages_add_avaliacao_add_avaliacao__ = __webpack_require__(363);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__pages_direitos_direitos__ = __webpack_require__(71);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__pages_direitos_secundaria_direitos_secundaria__ = __webpack_require__(132);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__pages_direitos_interno_direitos_interno__ = __webpack_require__(131);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__pages_orientacoes2_secundaria_orientacoes2_secundaria__ = __webpack_require__(134);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__pages_orientacoes2_orientacoes2__ = __webpack_require__(133);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_22__pages_telefones_telefones__ = __webpack_require__(135);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_23__ionic_native_status_bar__ = __webpack_require__(230);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_24__ionic_native_splash_screen__ = __webpack_require__(231);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_25__ionic_native_sqlite__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_26__ionic_native_toast__ = __webpack_require__(44);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_27__angular_http__ = __webpack_require__(189);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_28__providers_direitos_direitos__ = __webpack_require__(101);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_29__pages_add_panturrilha_add_panturrilha__ = __webpack_require__(123);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_30__pages_list_panturrilha_list_panturrilha__ = __webpack_require__(126);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_31__pages_resultado_panturrilha_resultado_panturrilha__ = __webpack_require__(56);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_32__pages_add_imc_add_imc__ = __webpack_require__(124);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_33__pages_list_imc_list_imc__ = __webpack_require__(127);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_34__pages_resultado_imc_resultado_imc__ = __webpack_require__(57);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_35__pages_add_ves13_add_ves13__ = __webpack_require__(125);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_36__pages_list_ves13_list_ves13__ = __webpack_require__(128);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_37__pages_resultado_ves13_resultado_ves13__ = __webpack_require__(58);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



























//import { HttpClientModule } from '@angular/common/http';











var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_8__pages_contact_contact__["a" /* ContactPage */],
                __WEBPACK_IMPORTED_MODULE_9__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_11__pages_sobre_sobre__["a" /* SobrePage */],
                __WEBPACK_IMPORTED_MODULE_10__pages_inicial_inicial__["a" /* InicialPage */],
                __WEBPACK_IMPORTED_MODULE_12__pages_tabs_tabs__["a" /* TabsPage */],
                __WEBPACK_IMPORTED_MODULE_13__pages_add_data_add_data__["a" /* AddDataPage */],
                __WEBPACK_IMPORTED_MODULE_14__pages_edit_data_edit_data__["a" /* EditDataPage */],
                __WEBPACK_IMPORTED_MODULE_15__pages_avaliacao_avaliacao__["a" /* AvaliacaoPage */],
                __WEBPACK_IMPORTED_MODULE_16__pages_add_avaliacao_add_avaliacao__["a" /* AddAvaliacaoPage */],
                __WEBPACK_IMPORTED_MODULE_17__pages_direitos_direitos__["a" /* DireitosPage */],
                __WEBPACK_IMPORTED_MODULE_19__pages_direitos_interno_direitos_interno__["a" /* DireitosInternoPage */],
                __WEBPACK_IMPORTED_MODULE_18__pages_direitos_secundaria_direitos_secundaria__["a" /* DireitosSecundariaPage */],
                __WEBPACK_IMPORTED_MODULE_21__pages_orientacoes2_orientacoes2__["a" /* Orientacoes2Page */],
                __WEBPACK_IMPORTED_MODULE_20__pages_orientacoes2_secundaria_orientacoes2_secundaria__["a" /* Orientacoes2SecundariaPage */],
                __WEBPACK_IMPORTED_MODULE_22__pages_telefones_telefones__["a" /* TelefonesPage */],
                __WEBPACK_IMPORTED_MODULE_29__pages_add_panturrilha_add_panturrilha__["a" /* AddPanturrilhaPage */],
                __WEBPACK_IMPORTED_MODULE_32__pages_add_imc_add_imc__["a" /* AddImcPage */],
                __WEBPACK_IMPORTED_MODULE_35__pages_add_ves13_add_ves13__["a" /* AddVes13Page */],
                __WEBPACK_IMPORTED_MODULE_30__pages_list_panturrilha_list_panturrilha__["a" /* ListPanturrilhaPage */],
                __WEBPACK_IMPORTED_MODULE_33__pages_list_imc_list_imc__["a" /* ListImcPage */],
                __WEBPACK_IMPORTED_MODULE_36__pages_list_ves13_list_ves13__["a" /* ListVes13Page */],
                __WEBPACK_IMPORTED_MODULE_34__pages_resultado_imc_resultado_imc__["a" /* ResultadoImcPage */],
                __WEBPACK_IMPORTED_MODULE_37__pages_resultado_ves13_resultado_ves13__["a" /* ResultadoVes13Page */],
                __WEBPACK_IMPORTED_MODULE_31__pages_resultado_panturrilha_resultado_panturrilha__["a" /* ResultadoPanturrilhaPage */],
                __WEBPACK_IMPORTED_MODULE_11__pages_sobre_sobre__["a" /* SobrePage */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_27__angular_http__["b" /* HttpModule */],
                __WEBPACK_IMPORTED_MODULE_4_ng2_charts__["ChartsModule"],
                //  HttpClientModule,
                __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["f" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* MyApp */], {
                    navExitApp: false,
                }, {
                    links: [
                        { loadChildren: '../pages/add-avaliacao/add-avaliacao.module#AddAvaliacaoPageModule', name: 'AddAvaliacaoPage', segment: 'add-avaliacao', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/add-data/add-data.module#AddDataPageModule', name: 'AddDataPage', segment: 'add-data', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/add-imc/add-imc.module#AddImcPageModule', name: 'AddImcPage', segment: 'add-imc', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/add-panturrilha/add-panturrilha.module#AddPanturrilhaPageModule', name: 'AddPanturrilhaPage', segment: 'add-panturrilha', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/direitos-interno/direitos-interno.module#DireitosInternoPageModule', name: 'DireitosInternoPage', segment: 'direitos-interno', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/direitos/direitos.module#DireitosPageModule', name: 'DireitosPage', segment: 'direitos', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/edit-data/edit-data.module#EditDataPageModule', name: 'EditDataPage', segment: 'edit-data', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/inicial/inicial.module#InicialPageModule', name: 'InicialPage', segment: 'inicial', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/list-imc/list-imc.module#ListImcPageModule', name: 'ListImcPage', segment: 'list-imc', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/list-panturrilha/list-panturrilha.module#ListPanturrilhaPageModule', name: 'ListPanturrilhaPage', segment: 'list-panturrilha', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/resultado-imc/resultado-imc.module#ResultadoImcPageModule', name: 'ResultadoImcPage', segment: 'resultado-imc', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/resultado-panturrilha/resultado-panturrilha.module#ResultadoPanturrilhaPageModule', name: 'ResultadoPanturrilhaPage', segment: 'resultado-panturrilha', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/telefones/telefones.module#TelefonesPageModule', name: 'TelefonesPage', segment: 'telefones', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/add-ves13/add-ves13.module#AddVes13PageModule', name: 'AddVes13Page', segment: 'add-ves13', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/avaliacao/avaliacao.module#AvaliacaoPageModule', name: 'AvaliacaoPage', segment: 'avaliacao', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/direitos-secundaria/direitos-secundaria.module#DireitosSecundariaPageModule', name: 'DireitosSecundariaPage', segment: 'direitos-secundaria', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/list-ves13/list-ves13.module#ListVes13PageModule', name: 'ListVes13Page', segment: 'list-ves13', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/resultado-ves13/resultado-ves13.module#ResultadoVes13PageModule', name: 'ResultadoVes13Page', segment: 'resultado-ves13', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/sobre/sobre.module#SobrePageModule', name: 'SobrePage', segment: 'sobre', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/orientacoes2-secundaria/orientacoes2-secundaria.module#Orientacoes2SecundariaPageModule', name: 'Orientacoes2SecundariaPage', segment: 'orientacoes2-secundaria', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/orientacoes2/orientacoes2.module#Orientacoes2PageModule', name: 'Orientacoes2Page', segment: 'orientacoes2', priority: 'low', defaultHistory: [] }
                    ]
                }),
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["d" /* IonicApp */]],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_8__pages_contact_contact__["a" /* ContactPage */],
                __WEBPACK_IMPORTED_MODULE_9__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_12__pages_tabs_tabs__["a" /* TabsPage */],
                __WEBPACK_IMPORTED_MODULE_13__pages_add_data_add_data__["a" /* AddDataPage */],
                __WEBPACK_IMPORTED_MODULE_14__pages_edit_data_edit_data__["a" /* EditDataPage */],
                __WEBPACK_IMPORTED_MODULE_15__pages_avaliacao_avaliacao__["a" /* AvaliacaoPage */],
                __WEBPACK_IMPORTED_MODULE_16__pages_add_avaliacao_add_avaliacao__["a" /* AddAvaliacaoPage */],
                __WEBPACK_IMPORTED_MODULE_17__pages_direitos_direitos__["a" /* DireitosPage */],
                __WEBPACK_IMPORTED_MODULE_19__pages_direitos_interno_direitos_interno__["a" /* DireitosInternoPage */],
                __WEBPACK_IMPORTED_MODULE_18__pages_direitos_secundaria_direitos_secundaria__["a" /* DireitosSecundariaPage */],
                __WEBPACK_IMPORTED_MODULE_21__pages_orientacoes2_orientacoes2__["a" /* Orientacoes2Page */],
                __WEBPACK_IMPORTED_MODULE_20__pages_orientacoes2_secundaria_orientacoes2_secundaria__["a" /* Orientacoes2SecundariaPage */],
                __WEBPACK_IMPORTED_MODULE_22__pages_telefones_telefones__["a" /* TelefonesPage */],
                __WEBPACK_IMPORTED_MODULE_29__pages_add_panturrilha_add_panturrilha__["a" /* AddPanturrilhaPage */],
                __WEBPACK_IMPORTED_MODULE_32__pages_add_imc_add_imc__["a" /* AddImcPage */],
                __WEBPACK_IMPORTED_MODULE_35__pages_add_ves13_add_ves13__["a" /* AddVes13Page */],
                __WEBPACK_IMPORTED_MODULE_30__pages_list_panturrilha_list_panturrilha__["a" /* ListPanturrilhaPage */],
                __WEBPACK_IMPORTED_MODULE_33__pages_list_imc_list_imc__["a" /* ListImcPage */],
                __WEBPACK_IMPORTED_MODULE_36__pages_list_ves13_list_ves13__["a" /* ListVes13Page */],
                __WEBPACK_IMPORTED_MODULE_34__pages_resultado_imc_resultado_imc__["a" /* ResultadoImcPage */],
                __WEBPACK_IMPORTED_MODULE_37__pages_resultado_ves13_resultado_ves13__["a" /* ResultadoVes13Page */],
                __WEBPACK_IMPORTED_MODULE_31__pages_resultado_panturrilha_resultado_panturrilha__["a" /* ResultadoPanturrilhaPage */],
                __WEBPACK_IMPORTED_MODULE_11__pages_sobre_sobre__["a" /* SobrePage */],
                __WEBPACK_IMPORTED_MODULE_10__pages_inicial_inicial__["a" /* InicialPage */]
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_23__ionic_native_status_bar__["a" /* StatusBar */],
                __WEBPACK_IMPORTED_MODULE_24__ionic_native_splash_screen__["a" /* SplashScreen */],
                { provide: __WEBPACK_IMPORTED_MODULE_0__angular_core__["ErrorHandler"], useClass: __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["e" /* IonicErrorHandler */] },
                __WEBPACK_IMPORTED_MODULE_25__ionic_native_sqlite__["a" /* SQLite */],
                __WEBPACK_IMPORTED_MODULE_26__ionic_native_toast__["a" /* Toast */],
                __WEBPACK_IMPORTED_MODULE_28__providers_direitos_direitos__["a" /* DireitosProvider */],
                __WEBPACK_IMPORTED_MODULE_5__angular_common__["d" /* DatePipe */],
                __WEBPACK_IMPORTED_MODULE_6__ionic_native_camera__["a" /* Camera */],
                __WEBPACK_IMPORTED_MODULE_7__ionic_native_call_number__["a" /* CallNumber */]
            ]
        })
    ], AppModule);
    return AppModule;
}());

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 39:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AvaliacaoPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ionic_native_sqlite__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__add_panturrilha_add_panturrilha__ = __webpack_require__(123);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__add_imc_add_imc__ = __webpack_require__(124);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__add_ves13_add_ves13__ = __webpack_require__(125);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__list_panturrilha_list_panturrilha__ = __webpack_require__(126);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__resultado_panturrilha_resultado_panturrilha__ = __webpack_require__(56);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__resultado_imc_resultado_imc__ = __webpack_require__(57);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__resultado_ves13_resultado_ves13__ = __webpack_require__(58);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__list_imc_list_imc__ = __webpack_require__(127);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__list_ves13_list_ves13__ = __webpack_require__(128);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__edit_data_edit_data__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__home_home__ = __webpack_require__(45);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14_jquery__ = __webpack_require__(188);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14_jquery___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_14_jquery__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



//import { ContactPage } from '../contact/contact';











//import { TabsPage } from '../tabs/tabs';


var AvaliacaoPage = /** @class */ (function () {
    function AvaliacaoPage(navCtrl, navParams, alertCtrl, sqlite) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.alertCtrl = alertCtrl;
        this.sqlite = sqlite;
        this.data = { rowid: 1, nascimento: "", sexo: "", nome: "", imagem: "" };
        this.photo = "assets/imgs/user.png";
        this.panturrilhas = [];
        this.imcs = [];
        this.ves13s = [];
        this.totalIncome = 0;
        this.totalExpense = 0;
        this.balance = 0;
        this.num_ves13 = 0;
        this.num_imc = 0;
        this.num_panturrilha = 0;
        this.circunferencia = 0;
        this.cod_panturrilha = 0;
        this.imc = 0;
        this.cod_imc = 0;
        this.pontosfinal = 0;
        this.cod_ves13 = 0;
        this.pontuacaototal = 0;
        this.pontos_total = 0;
        this.tabBarElement = document.querySelector('#Tabs');
        this.navCtrl.parent.select(1);
    }
    AvaliacaoPage.prototype.ionViewDidEnter = function () {
        this.getCurrentData(1);
        this.getPanturrilha();
        this.getPanturrilha2();
        this.getImc();
        this.getImc2();
        this.getVes13();
        this.getVes132();
    };
    AvaliacaoPage.prototype.subir = function () {
        __WEBPACK_IMPORTED_MODULE_14_jquery__().ready(function () {
            __WEBPACK_IMPORTED_MODULE_14_jquery__(".alert-message").animate({ scrollTop: 1600 }, 160000);
        });
        __WEBPACK_IMPORTED_MODULE_14_jquery__(document).on('touchstart', '.alert-message', function (event) {
            __WEBPACK_IMPORTED_MODULE_14_jquery__(".alert-message").stop();
        });
        __WEBPACK_IMPORTED_MODULE_14_jquery__(document).on('touchstart', '.alert-message', function (event) {
            __WEBPACK_IMPORTED_MODULE_14_jquery__(".alert-message").animate({ scrollTop: 1600 }, 160000);
        });
        __WEBPACK_IMPORTED_MODULE_14_jquery__(document).on('touchmove', '.alert-message', function (event) {
            __WEBPACK_IMPORTED_MODULE_14_jquery__(".alert-message").stop();
        });
    };
    AvaliacaoPage.prototype.infoPanturrilha = function () {
        var alert = this.alertCtrl.create({
            title: 'Perímetro da panturrilha',
            message: '<div class="message">Essa é uma medida de triagem indicada para avaliação da massa muscular, cuja redução correlaciona-se com risco de desenvolver a síndrome chamada sarcopenia e desfechos desfavoráveis. sarcopenia é uma síndrome clínica caracterizada pela perda progressiva e generalizada da massa, força e desempenho muscular, com risco de desfechos adversos como incapacidade física, baixa qualidade de vida e óbito. É diagnosticada através da densidade corporal total ou inferida através de medidas indiretas.</div>'
                + '<br><br><div class="referencia"><strong>Fonte:</strong><br>Brasil. Ministério da Saúde. Secretaria de Atenção à Saúde. Departamento de Ações Programáticas Estratégicas.<br><br>Manual para utilização da Caderneta de Saúde da Pessoa Idosa / Ministério da Saúde, Secretaria de Atenção à Saúde, Departamento de Ações Programáticas Estratégicas. – Brasília : Ministério da Saúde, 2018.<br>96 p. : il. <br><br>(ISBN 978-85-334-2613-9)</div>',
            cssClass: 'referencias',
            buttons: ['OK']
        });
        alert.present();
        this.subir();
    };
    AvaliacaoPage.prototype.infoImc = function () {
        var alert = this.alertCtrl.create({
            title: 'Índice de massa corporal',
            message: '<div class="message">Nos procedimentos de diagnóstico e acompanhamento do estado nutricional de pessoas idosas, o critério prioritário a ser utilizado deve ser a classificação do Índice de Massa Corporal (IMC), recomendado pela Organização Mundial de Saúde (OMS), considerando os pontos de corte diferenciados para a população com 60 anos ou mais de idade. Assim, é importante conhecer algumas alterações fisiológicas características dessa fase do curso da vida.</div>'
                + '<br><br><div class="referencia"><strong>Fonte:</strong><br>Brasil. Ministério da Saúde. Secretaria de Atenção à Saúde. Departamento de Ações Programáticas Estratégicas.<br><br>Manual para utilização da Caderneta de Saúde da Pessoa Idosa / Ministério da Saúde, Secretaria de Atenção à Saúde, Departamento de Ações Programáticas Estratégicas. – Brasília : Ministério da Saúde, 2018.<br>96 p. : il. <br><br>(ISBN 978-85-334-2613-9)</div>',
            cssClass: 'referencias',
            buttons: ['OK']
        });
        alert.present();
        this.subir();
    };
    AvaliacaoPage.prototype.infoVes13 = function () {
        var alert = this.alertCtrl.create({
            title: 'Índice de vulnerabilidade',
            message: '<div class="message">O VES-13 é um instrumento simples e eficaz, capaz de identificar o idoso vulnerável residente na comunidade, com base na idade, auto-percepção da saúde, presença de limitações físicas e incapacidades (SALIBA et al., 2001). É um questionário de fácil aplicabilidade, que pode ser respondido pelos próprios profissionais de saúde, pela pessoa idosa ou pelos familiares/cuidadores, dispensando a observação direta do usuário. Baseia-se no registro das habilidades necessárias para a realização das tarefas do cotidiano. Cada item recebe uma determinada pontuação e o somatório final pode variar de 0 a 10 pontos.</div>'
                + '<br><br><div class="referencia"><strong>Fonte:</strong><br>Brasil. Ministério da Saúde. Secretaria de Atenção à Saúde. Departamento de Ações Programáticas Estratégicas.<br><br>Manual para utilização da Caderneta de Saúde da Pessoa Idosa / Ministério da Saúde, Secretaria de Atenção à Saúde, Departamento de Ações Programáticas Estratégicas. – Brasília : Ministério da Saúde, 2018.<br>96 p. : il. <br><br>(ISBN 978-85-334-2613-9)</div>',
            cssClass: 'referencias',
            buttons: ['OK']
        });
        alert.present();
        this.subir();
    };
    /*
      ionViewWillEnter() {
          this.getCurrentData(1);
        this.getPanturrilha();
        this.getPanturrilha2();
        this.getImc();
        this.getImc2();
        this.getVes13();
        this.getVes132();
      }
    */
    AvaliacaoPage.prototype.goVoltar = function () {
        //this.navCtrl.popToRoot(); 
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_13__home_home__["a" /* HomePage */]);
        //this.navCtrl.setRoot(TabsPage);
    };
    AvaliacaoPage.prototype.getPanturrilha = function () {
        var _this = this;
        this.sqlite.create({
            name: 'idoso.db',
            location: 'default'
        }).then(function (db) {
            db.executeSql('SELECT COUNT(*) as num FROM teste_panturrilha', [])
                .then(function (res) {
                _this.num_panturrilha = res.rows.item(0).num;
            });
        });
    };
    AvaliacaoPage.prototype.getPanturrilha2 = function () {
        var _this = this;
        this.sqlite.create({
            name: 'idoso.db',
            location: 'default'
        }).then(function (db) {
            db.executeSql('SELECT id_panturrilha,circunferencia FROM teste_panturrilha order by id_panturrilha DESC limit 1', [])
                .then(function (res) {
                _this.panturrilhas = [];
                for (var i = 0; i < res.rows.length; i++) {
                    _this.circunferencia = res.rows.item(i).circunferencia;
                    _this.cod_panturrilha = res.rows.item(i).id_panturrilha;
                }
            });
        });
    };
    AvaliacaoPage.prototype.getImc = function () {
        var _this = this;
        this.sqlite.create({
            name: 'idoso.db',
            location: 'default'
        }).then(function (db) {
            db.executeSql('SELECT COUNT(*) as num FROM teste_imc', [])
                .then(function (res) {
                _this.num_imc = res.rows.item(0).num;
            });
        });
    };
    AvaliacaoPage.prototype.getImc2 = function () {
        var _this = this;
        this.sqlite.create({
            name: 'idoso.db',
            location: 'default'
        }).then(function (db) {
            db.executeSql('SELECT id_imc, altura,peso FROM teste_imc order by id_imc DESC limit 1', [])
                .then(function (res) {
                _this.imcs = [];
                for (var i = 0; i < res.rows.length; i++) {
                    var finalBmi = res.rows.item(i).peso / (res.rows.item(i).altura * res.rows.item(i).altura);
                    var calculo_imc = parseFloat(finalBmi.toFixed(1));
                    _this.imc = calculo_imc;
                    _this.cod_imc = res.rows.item(i).id_imc;
                }
            });
        });
    };
    AvaliacaoPage.prototype.getVes13 = function () {
        var _this = this;
        this.sqlite.create({
            name: 'idoso.db',
            location: 'default'
        }).then(function (db) {
            db.executeSql('SELECT COUNT(*) as num FROM teste_ves13', [])
                .then(function (res) {
                _this.num_ves13 = res.rows.item(0).num;
            });
        });
    };
    AvaliacaoPage.prototype.getVes132 = function () {
        var _this = this;
        this.sqlite.create({
            name: 'idoso.db',
            location: 'default'
        }).then(function (db) {
            db.executeSql('SELECT id_ves13,id_perfil, idade,  percepcao_saude,  limitacao1,  limitacao2, limitacao3, limitacao4, limitacao5, limitacao6,  incapacidade1,incapacidade2, incapacidade3,incapacidade4,incapacidade5, data_exame from teste_ves13 order by id_ves13 DESC limit 1', [])
                .then(function (res) {
                _this.ves13s = [];
                for (var i = 0; i < res.rows.length; i++) {
                    _this.cod_ves13 = res.rows.item(i).id_ves13;
                    var idade = res.rows.item(i).idade;
                    var somaParcial = null;
                    var pontosIdade = null;
                    var pontosPercepcao = 0;
                    if (idade > 60 && idade < 74) {
                        pontosIdade = 0;
                    }
                    if (idade > 75 && idade < 84) {
                        pontosIdade = 1;
                    }
                    if (idade > 84) {
                        pontosIdade = 3;
                    }
                    if (res.rows.item(i).percepcao_saude >= 4) {
                        pontosPercepcao = 1;
                    }
                    somaParcial = pontosIdade + pontosPercepcao;
                    //limitacoes fisicas
                    var pontosLimitacao1 = 0;
                    var pontosLimitacao2 = 0;
                    var pontosLimitacao3 = 0;
                    var pontosLimitacao4 = 0;
                    var pontosLimitacao5 = 0;
                    var pontosLimitacao6 = 0;
                    if (res.rows.item(i).limitacao1 >= 4) {
                        pontosLimitacao1 = 1;
                    }
                    if (res.rows.item(i).limitacao2 >= 4) {
                        pontosLimitacao2 = 1;
                    }
                    if (res.rows.item(i).limitacao3 >= 4) {
                        pontosLimitacao3 = 1;
                    }
                    if (res.rows.item(i).limitacao4 >= 4) {
                        pontosLimitacao4 = 1;
                    }
                    if (res.rows.item(i).limitacao5 >= 4) {
                        pontosLimitacao5 = 1;
                    }
                    if (res.rows.item(i).limitacao6 >= 4) {
                        pontosLimitacao6 = 1;
                    }
                    var somaLimitacao = pontosLimitacao1 + pontosLimitacao2 + pontosLimitacao3 + pontosLimitacao4 + pontosLimitacao5 + pontosLimitacao6;
                    var maxLimicacao = somaLimitacao;
                    if (somaLimitacao > 2) {
                        maxLimicacao = 2;
                    }
                    //incapacidades Fisicas
                    var pontosIncapacidade1 = 0;
                    var pontosIncapacidade2 = 0;
                    var pontosIncapacidade3 = 0;
                    var pontosIncapacidade4 = 0;
                    var pontosIncapacidade5 = 0;
                    if (res.rows.item(i).incapacidade1 == 1) {
                        pontosIncapacidade1 = 4;
                    }
                    if (res.rows.item(i).incapacidade2 == 1) {
                        pontosIncapacidade2 = 4;
                    }
                    if (res.rows.item(i).incapacidade3 == 1) {
                        pontosIncapacidade3 = 4;
                    }
                    if (res.rows.item(i).incapacidade4 == 1) {
                        pontosIncapacidade4 = 4;
                    }
                    if (res.rows.item(i).incapacidade5 == 1) {
                        pontosIncapacidade5 = 4;
                    }
                    var somaIncapacidade = pontosIncapacidade1 + pontosIncapacidade2 + pontosIncapacidade3 + pontosIncapacidade4 + pontosIncapacidade5;
                    var maxIncapacidade = somaIncapacidade;
                    if (somaIncapacidade > 4) {
                        maxIncapacidade = 4;
                    }
                    var pontuacaofinal = maxIncapacidade + maxLimicacao + somaParcial;
                    _this.pontuacaototal = 0;
                    if (pontuacaofinal >= 3) {
                        _this.pontuacaototal = 3;
                    }
                    else {
                        _this.pontuacaototal = pontuacaofinal;
                    }
                    var pontos_final = somaParcial + maxLimicacao + maxIncapacidade;
                    var pontos_total = pontos_final;
                    if (pontos_final > 10) {
                        _this.pontos_total = 10;
                    }
                    _this.pontosfinal = pontos_total;
                }
            });
        });
    };
    AvaliacaoPage.prototype.getCurrentData = function (rowid) {
        var _this = this;
        this.sqlite.create({
            name: 'idoso.db',
            location: 'default'
        }).then(function (db) {
            db.executeSql('SELECT * FROM perfil WHERE rowid=?', [rowid])
                .then(function (res) {
                if (res.rows.length > 0) {
                    _this.data.rowid = res.rows.item(0).rowid;
                    _this.data.nome = res.rows.item(0).nome;
                    _this.data.sexo = res.rows.item(0).sexo;
                    _this.data.nascimento = res.rows.item(0).nascimento;
                    _this.data.imagem = res.rows.item(0).imagem;
                    _this.photo = res.rows.item(0).imagem;
                }
                if (_this.photo == "") {
                    _this.photo = "assets/imgs/user.png";
                }
            });
        });
    };
    /*
      addPerfil() {
        this.navCtrl.push(ContactPage);
      }
    */
    AvaliacaoPage.prototype.addPanturrilha = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__add_panturrilha_add_panturrilha__["a" /* AddPanturrilhaPage */]);
    };
    AvaliacaoPage.prototype.addImc = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__add_imc_add_imc__["a" /* AddImcPage */]);
    };
    AvaliacaoPage.prototype.addVes13 = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__add_ves13_add_ves13__["a" /* AddVes13Page */]);
    };
    AvaliacaoPage.prototype.listPanturrilha = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_6__list_panturrilha_list_panturrilha__["a" /* ListPanturrilhaPage */]);
    };
    AvaliacaoPage.prototype.listImc = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_10__list_imc_list_imc__["a" /* ListImcPage */]);
    };
    AvaliacaoPage.prototype.listVes13 = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_11__list_ves13_list_ves13__["a" /* ListVes13Page */]);
    };
    AvaliacaoPage.prototype.addPerfil2 = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_12__edit_data_edit_data__["a" /* EditDataPage */], {
            rowid: 1
        });
    };
    AvaliacaoPage.prototype.verResultado = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_7__resultado_panturrilha_resultado_panturrilha__["a" /* ResultadoPanturrilhaPage */], {
            rowid: 1
        });
    };
    AvaliacaoPage.prototype.verPanturrilha = function (id_panturrilha) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_7__resultado_panturrilha_resultado_panturrilha__["a" /* ResultadoPanturrilhaPage */], {
            id_panturrilha: this.cod_panturrilha
        });
    };
    AvaliacaoPage.prototype.verImc = function (id_imc) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_8__resultado_imc_resultado_imc__["a" /* ResultadoImcPage */], {
            id_imc: this.cod_imc
        });
    };
    AvaliacaoPage.prototype.verVes13 = function (id_ves13) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_9__resultado_ves13_resultado_ves13__["a" /* ResultadoVes13Page */], {
            id_ves13: this.cod_ves13
        });
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["c" /* Content */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["c" /* Content */])
    ], AvaliacaoPage.prototype, "content", void 0);
    AvaliacaoPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-avaliacao',template:/*ion-inline-start:"/home/rabaco/Documentos/projetos_ionic/idoso_aplicativo_final/src/pages/avaliacao/avaliacao.html"*/'<ion-header>\n  <ion-navbar hideBackButton="true">\n    <ion-buttons left >\n  <button (click)="goVoltar()" ion-button icon-only style="margin-left: 10px">\n    <ion-icon name="md-arrow-back"></ion-icon>\n  </button>\n</ion-buttons>\n    <ion-title>Avaliações</ion-title>\n  </ion-navbar> \n</ion-header>\n\n\n<ion-content padding>\n\n  <div class="usuario">\n  <div class="icone-imagem">\n    <img src="{{ photo }}" class="icone-imagem" (click)="addPerfil2()">\n  </div>\n  <div class="nome-idoso" (click)="addPerfil2()" *ngIf="data.nome==\' \'">\n   <ion-item><span class="nome-idoso2">|</span></ion-item>\n  \n  </div>\n  <div class="nome-idoso" (click)="addPerfil2()" *ngIf="data.nome!=\' \'">\n    {{data.nome}}\n  </div>\n  </div>\n  <div class="espacamento"></div>\n  <ion-card color="fundo-item" class="avaliacao">\n  <ion-card-content> <span class="icone-direita" (click)="infoPanturrilha()"><ion-icon name="md-information-circle"></ion-icon></span>\n    <ion-card-title class="texto-titulo-avaliacao">\n    <strong class="texto-avaliacao-t">Perímetro da panturrilha</strong>\n    </ion-card-title>\n  <!--<ion-item no-lines style="background-color: transparent;" *ngIf="num_panturrilha>0"> -->\n      <ion-item no-lines style="background-color: transparent;" *ngIf="num_panturrilha>0">\n          <span *ngIf="(circunferencia > 45)" (click)="verPanturrilha(cod_panturrilha)">\n            <div class="circulo_testes"><img src="assets/imgs/circulo_branco.png"></div><div class="circulo-texto" (click)="verPanturrilha(cod_panturrilha)">Acompanhamento<br>de rotina</div></span>\n      <span *ngIf="(circunferencia >= 35 && circunferencia<=45)" (click)="verPanturrilha(cod_panturrilha)">\n          <div class="circulo_testes"><img src="assets/imgs/circulo_verde.png"></div><div class="circulo-texto" (click)="verPanturrilha(cod_panturrilha)">Acompanhamento<br>de rotina</div></span>\n         <span *ngIf="(circunferencia >= 31) && (circunferencia <35) " (click)="verPanturrilha(cod_panturrilha)"><div class="circulo_testes"><img src="assets/imgs/circulo_amarelo.png"></div><div class="circulo-texto2">Atenção<br></div></span>\n         <span *ngIf="(circunferencia < 31)" (click)="verPanturrilha(cod_panturrilha)"><div class="circulo_testes"><img src="assets/imgs/circulo_vermelho.png"></div> <div class="circulo-texto2" >Ação<br></div></span>\n         \n  \n  </ion-item>\n<button ion-item round icon-start color="botao" class="botao-menor" text-wrap (click)="addPanturrilha()" mode="md" no-lines>\n    <div class="icone-avaliacao"><img src="assets/imgs/mais.png"></div><div class="texto-avaliacao">Nova Avaliação</div>\n  </button>\n\n  <!--<button ion-item round icon-start color="botao" class="botao-menor" text-wrap (click)="listPanturrilha()" *ngIf="num_panturrilha>0" mode="md"> -->\n      <button ion-item round icon-start color="botao" class="botao-menor" text-wrap (click)="listPanturrilha()" mode="md" no-lines *ngIf="num_panturrilha>0" >\n    <div class="icone-avaliacao"><img src="assets/imgs/olho.png"></div><div class="texto-avaliacao">Testes Anteriores</div>\n  </button>\n </ion-card-content>\n</ion-card>\n<ion-card color="fundo-item" class="avaliacao">\n<ion-card-content> <span class="icone-direita" (click)="infoImc()"><ion-icon name="md-information-circle"></ion-icon></span>\n  <ion-card-title class="texto-titulo-avaliacao" > \n  <strong class="texto-avaliacao-t">Índice de massa corporal </strong>\n   \n  \n  </ion-card-title>\n<ion-item no-lines style="background-color: transparent;" *ngIf="num_imc>0">\n   \n  <span *ngIf="(imc> 27 )" (click)="verImc(cod_imc)"><div class="circulo_testes" ><img src="assets/imgs/circulo_vermelho.png"></div><div class="circulo-texto2">Sobrepeso</div></span>\n    <span *ngIf="(imc >= 22) && (imc <=27) " (click)="verImc(cod_imc)"><div class="circulo_testes"><img src="assets/imgs/circulo_verde.png"></div><div class="circulo-texto2">Peso Adequado</div></span>\n    <span *ngIf="(imc < 22)" (click)="verImc(cod_imc)"><div class="circulo_testes"><img src="assets/imgs/circulo_vermelho.png"></div> <div class="circulo-texto2">Baixo Peso</div></span>\n  </ion-item>\n<button ion-item round icon-start color="botao" class="botao-menor" text-wrap (click)="addImc()" mode="md" no-lines>\n  <div class="icone-avaliacao"><img src="assets/imgs/mais.png"></div><div class="texto-avaliacao">Nova Avaliação</div>\n</button>\n\n<button ion-item round icon-start color="botao" class="botao-menor" text-wrap (click)="listImc()"  *ngIf="num_imc>0" mode="md" no-lines> \n    <div class="icone-avaliacao"><img src="assets/imgs/olho.png"></div><div class="texto-avaliacao">Testes Anteriores</div>\n</button>\n</ion-card-content>\n</ion-card>\n\n<ion-card color="fundo-item" class="avaliacao">\n<ion-card-content><span class="icone-direita" (click)="infoVes13()"><ion-icon name="md-information-circle"></ion-icon></span>\n  <ion-card-title class="texto-titulo-avaliacao">\n  <strong class="texto-avaliacao-t">Índice de vulnerabilidade</strong>\n  </ion-card-title>\n  <ion-item no-lines style="background-color: transparent;" *ngIf="num_ves13>0"> \n          <span *ngIf="(pontosfinal<3)" (click)="verVes13(cod_ves13)">\n          <div class="circulo_testes"><img src="assets/imgs/circulo_verde.png"></div><div class="circulo-texto" text-wrap>Acompanhamento<br>de Rotina</div></span>\n          <span *ngIf="(pontosfinal>=3)" (click)="verVes13(cod_ves13)">\n            <div class="circulo_testes"><img src="assets/imgs/circulo_vermelho.png"></div><div class="circulo-texto2">Atenção / Ação</div></span>\n\n  </ion-item>\n<button ion-item round icon-start color="botao" class="botao-menor" text-wrap (click)="addVes13()" mode="md" no-lines>\n  <div class="icone-avaliacao"><img src="assets/imgs/mais.png"></div><div class="texto-avaliacao">Nova Avaliação</div>\n</button>\n\n<button ion-item round icon-start color="botao" class="botao-menor" text-wrap (click)="listVes13()"  *ngIf="num_ves13>0" mode="md" no-lines>\n    <div class="icone-avaliacao"><img src="assets/imgs/olho.png"></div><div class="texto-avaliacao">Testes Anteriores</div>\n</button>\n</ion-card-content>\n</ion-card>\n\n</ion-content>\n'/*ion-inline-end:"/home/rabaco/Documentos/projetos_ionic/idoso_aplicativo_final/src/pages/avaliacao/avaliacao.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["h" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["i" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["b" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_1__ionic_native_sqlite__["a" /* SQLite */]])
    ], AvaliacaoPage);
    return AvaliacaoPage;
}());

//# sourceMappingURL=avaliacao.js.map

/***/ }),

/***/ 437:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__ = __webpack_require__(230);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(231);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_tabs_tabs__ = __webpack_require__(64);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





//import { HomePage } from '../pages/home/home';
var MyApp = /** @class */ (function () {
    function MyApp(platform, statusBar, splashScreen) {
        this.rootPage = __WEBPACK_IMPORTED_MODULE_4__pages_tabs_tabs__["a" /* TabsPage */];
        platform.ready().then(function () {
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            statusBar.styleDefault();
            splashScreen.hide();
            /* platform.registerBackButtonAction(() => {
               debugger;
               this.nav.push(TabsPage);
               /*this.nav.popTo(this.nav.getByIndex(this.nav.length()-2));*/
            /*this.nav.last();*/
            /*alert (this.nav);*/
            /*
             });
         */
        });
    }
    MyApp = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({template:/*ion-inline-start:"/home/rabaco/Documentos/projetos_ionic/idoso_aplicativo_final/src/app/app.html"*/'<ion-nav [root]="rootPage"></ion-nav>\n'/*ion-inline-end:"/home/rabaco/Documentos/projetos_ionic/idoso_aplicativo_final/src/app/app.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* Platform */], __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__["a" /* StatusBar */], __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */]])
    ], MyApp);
    return MyApp;
}());

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 45:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_sqlite__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__add_data_add_data__ = __webpack_require__(70);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__tabs_tabs__ = __webpack_require__(64);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__edit_data_edit_data__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__inicial_inicial__ = __webpack_require__(129);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





//import { AvaliacaoPage } from '../avaliacao/avaliacao';
//import { Orientacoes2Page } from '../orientacoes2/orientacoes2';
//import { TelefonesPage } from '../telefones/telefones';

//import { SobrePage } from '../sobre/sobre';

var HomePage = /** @class */ (function () {
    function HomePage(navCtrl, navParams, platform, sqlite) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.platform = platform;
        this.sqlite = sqlite;
        this.usuarios = [];
        this.num = 0;
        this.tabBarElement = document.querySelector('#Tabs');
        this.navCtrl.parent.select(0);
    }
    HomePage.prototype.ionViewDidEnter = function () {
        this.verificarCadastro();
        this.getInterno();
    };
    HomePage.prototype.ionViewWillLeave = function () {
        this.getPanturrilha();
        this.getImc();
        this.getVes13();
    };
    HomePage.prototype.verPerfil = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__tabs_tabs__["a" /* TabsPage */]);
    };
    HomePage.prototype.verAvaliacao = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_4__tabs_tabs__["a" /* TabsPage */], { selectedTab: 1 });
    };
    HomePage.prototype.verDireitos = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_4__tabs_tabs__["a" /* TabsPage */], { selectedTab: 2 });
    };
    HomePage.prototype.verOrientacao = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_4__tabs_tabs__["a" /* TabsPage */], { selectedTab: 3 });
    };
    HomePage.prototype.verTelefones = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_4__tabs_tabs__["a" /* TabsPage */], { selectedTab: 4 });
    };
    HomePage.prototype.verSobre = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_6__inicial_inicial__["a" /* InicialPage */]);
    };
    HomePage.prototype.verCadastro = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__add_data_add_data__["a" /* AddDataPage */]);
    };
    HomePage.prototype.editarCadastro = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__edit_data_edit_data__["a" /* EditDataPage */], {
            rowid: 1
        });
    };
    HomePage.prototype.sair = function () {
        this.platform.exitApp();
    };
    HomePage.prototype.verificarCadastro = function () {
        var _this = this;
        this.sqlite.create({
            name: 'idoso.db',
            location: 'default'
        }).then(function (db) {
            db.executeSql('CREATE TABLE IF NOT EXISTS perfil(rowid INTEGER PRIMARY KEY, nascimento Date, nome TEXT, sexo TEXT, imagem Text)', [])
                .then(function (res) { return console.log('Criou a tabela perfil'); })
                .catch(function (e) { return console.log(e); });
            db.executeSql('SELECT COUNT(*) as num,nome FROM perfil where nome=" "', [])
                .then(function (res) {
                _this.num = res.rows.item(0).num;
            })
                .then(function (res) { return console.log('Executou select perfil'); })
                .catch(function (e) { return _this.usuarios.push({ num: 1 }); });
        }).catch(function (e) { return console.log(e); });
    };
    HomePage.prototype.getInterno = function () {
        this.sqlite.create({
            name: 'idoso.db',
            location: 'default'
        }).then(function (db) {
            db.sqlBatch([
                ['INSERT INTO perfil VALUES(?,?,?,?,?)', [1, '', ' ', '', '']]
            ])
                .then(function (res) { return console.log('Criado perfil'); })
                .catch(function (e) { return console.log(e); });
        }).catch(function (e) { return console.log(e); });
    };
    HomePage.prototype.getPanturrilha = function () {
        this.sqlite.create({
            name: 'idoso.db',
            location: 'default'
        }).then(function (db) {
            db.executeSql('CREATE TABLE IF NOT EXISTS teste_panturrilha(id_panturrilha INTEGER PRIMARY KEY, id_perfil INTEGER, circunferencia REAL, recomendacao TEXT, data_exame DATE)', [])
                .then(function (res) { return console.log('Criado tabela teste_panturrilha'); })
                .catch(function (e) { return console.log(e); });
        }).catch(function (e) { return console.log(e); });
    };
    HomePage.prototype.getImc = function () {
        this.sqlite.create({
            name: 'idoso.db',
            location: 'default'
        }).then(function (db) {
            db.executeSql('CREATE TABLE IF NOT EXISTS teste_imc (id_imc INTEGER PRIMARY KEY, id_perfil INTEGER, peso REAL, altura REAL, recomendacao TEXT, data_exame DATE)', [])
                .then(function (res) { return console.log('Criado tabela teste_imc'); })
                .catch(function (e) { return console.log(e); });
        }).catch(function (e) { return console.log(e); });
    };
    HomePage.prototype.getVes13 = function () {
        this.sqlite.create({
            name: 'idoso.db',
            location: 'default'
        }).then(function (db) {
            db.executeSql('CREATE TABLE IF NOT EXISTS teste_ves13 ( id_ves13 INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, id_perfil INTEGER, idade INTEGER,  percepcao_saude INTEGER, limitacao1 INTEGER, limitacao2 TEXT, limitacao3 INTEGER, limitacao4 INTEGER, limitacao5 INTEGER, limitacao6 INTEGER,  incapacidade1 INTEGER, incapacidade2 INTEGER, incapacidade3 INTEGER, incapacidade4 INTEGER, incapacidade5 INTEGER, data_exame DATE)', [])
                .then(function (res) { return console.log('Criado tabela teste_ves13'); })
                .catch(function (e) { return console.log(e); });
        }).catch(function (e) { return console.log(e); });
    };
    HomePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-home',template:/*ion-inline-start:"/home/rabaco/Documentos/projetos_ionic/idoso_aplicativo_final/src/pages/home/home.html"*/'<ion-header>\n    <div class="top"></div>\n    <div class="titulo-caderneta-virtual">\n    Atenção ao Idoso<br>\n    <span class="titulo-caderneta-menor">Instrumento de apoio do cuidado</span>\n    </div>\n  \n</ion-header>\n\n<ion-content >\n \n  <div padding class="fundo_principal">\n   <div class="texto-caderneta-virtual" >\nSeja bem vindo!<br><br>\n    <span>Monitore sua saúde, conheça seus direitos e veja orientações para auxiliar no seu autocuidado.</span><br><br>\n   </div>\n\n\n<!--\n    <button ion-item color="botao" class="botao" (click)="verCadastro()" >\n     <ion-icon name="custom-cadastro" class="icones"></ion-icon><div class="altura-texto">Cadastro do Idoso\n     </div>\n      </button>\n    -->\n\n    \n\n   <button ion-item no-lines  color="botao" class="botao" (click)="verAvaliacao()" >\n      <ion-icon name="custom-clipboard"  class="icones"></ion-icon><div class="altura-texto">\n      Avaliações\n   </div>\n    </button>\n\n    <button ion-item no-lines color="botao" class="botao" (click)="verDireitos()" >\n        <ion-icon name="custom-justica"  class="icones"></ion-icon><div class="altura-texto">\n        Direitos\n     </div>\n      </button>\n\n      <button ion-item no-lines color="botao" class="botao" (click)="verOrientacao()" >\n          <ion-icon name="custom-compasso"  class="icones"></ion-icon><div class="altura-texto">\n         Orientações\n       </div>\n        </button>\n\n        <button ion-item no-lines color="botao" class="botao" (click)="verTelefones()" >\n            <ion-icon name="custom-telefone"  class="icones"></ion-icon><div class="altura-texto">\n           Telefones\n         </div>\n          </button>\n\n          <button ion-item no-lines color="botao" class="botao" (click)="verSobre()"  >\n            <ion-icon name="md-information-circle"  class="icones2"></ion-icon><div class="altura-texto">\n             Sobre\n           </div>\n            </button>\n            <button ion-item no-lines color="botao" class="botao" (click)="sair()"  >\n                <ion-icon name="md-exit"  class="icones2"></ion-icon><div class="altura-texto">\n               Sair do aplicativo\n             </div>\n              </button>\n            <!--\n            <button ion-item color="botao" class="botao" (click)="editarCadastro()" >\n                <ion-icon name="custom-cadastro" class="icones"></ion-icon><div class="altura-texto">Editar dados do Idoso\n                </div>\n                 </button>\n                -->\n        \n\n\n\n\n\n\n</div>\n\n\n  </ion-content>\n'/*ion-inline-end:"/home/rabaco/Documentos/projetos_ionic/idoso_aplicativo_final/src/pages/home/home.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_2__ionic_native_sqlite__["a" /* SQLite */]])
    ], HomePage);
    return HomePage;
}());

//# sourceMappingURL=home.js.map

/***/ }),

/***/ 465:
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./af": 239,
	"./af.js": 239,
	"./ar": 240,
	"./ar-dz": 241,
	"./ar-dz.js": 241,
	"./ar-kw": 242,
	"./ar-kw.js": 242,
	"./ar-ly": 243,
	"./ar-ly.js": 243,
	"./ar-ma": 244,
	"./ar-ma.js": 244,
	"./ar-sa": 245,
	"./ar-sa.js": 245,
	"./ar-tn": 246,
	"./ar-tn.js": 246,
	"./ar.js": 240,
	"./az": 247,
	"./az.js": 247,
	"./be": 248,
	"./be.js": 248,
	"./bg": 249,
	"./bg.js": 249,
	"./bm": 250,
	"./bm.js": 250,
	"./bn": 251,
	"./bn.js": 251,
	"./bo": 252,
	"./bo.js": 252,
	"./br": 253,
	"./br.js": 253,
	"./bs": 254,
	"./bs.js": 254,
	"./ca": 255,
	"./ca.js": 255,
	"./cs": 256,
	"./cs.js": 256,
	"./cv": 257,
	"./cv.js": 257,
	"./cy": 258,
	"./cy.js": 258,
	"./da": 259,
	"./da.js": 259,
	"./de": 260,
	"./de-at": 261,
	"./de-at.js": 261,
	"./de-ch": 262,
	"./de-ch.js": 262,
	"./de.js": 260,
	"./dv": 263,
	"./dv.js": 263,
	"./el": 264,
	"./el.js": 264,
	"./en-au": 265,
	"./en-au.js": 265,
	"./en-ca": 266,
	"./en-ca.js": 266,
	"./en-gb": 267,
	"./en-gb.js": 267,
	"./en-ie": 268,
	"./en-ie.js": 268,
	"./en-il": 269,
	"./en-il.js": 269,
	"./en-nz": 270,
	"./en-nz.js": 270,
	"./eo": 271,
	"./eo.js": 271,
	"./es": 272,
	"./es-do": 273,
	"./es-do.js": 273,
	"./es-us": 274,
	"./es-us.js": 274,
	"./es.js": 272,
	"./et": 275,
	"./et.js": 275,
	"./eu": 276,
	"./eu.js": 276,
	"./fa": 277,
	"./fa.js": 277,
	"./fi": 278,
	"./fi.js": 278,
	"./fo": 279,
	"./fo.js": 279,
	"./fr": 280,
	"./fr-ca": 281,
	"./fr-ca.js": 281,
	"./fr-ch": 282,
	"./fr-ch.js": 282,
	"./fr.js": 280,
	"./fy": 283,
	"./fy.js": 283,
	"./gd": 284,
	"./gd.js": 284,
	"./gl": 285,
	"./gl.js": 285,
	"./gom-latn": 286,
	"./gom-latn.js": 286,
	"./gu": 287,
	"./gu.js": 287,
	"./he": 288,
	"./he.js": 288,
	"./hi": 289,
	"./hi.js": 289,
	"./hr": 290,
	"./hr.js": 290,
	"./hu": 291,
	"./hu.js": 291,
	"./hy-am": 292,
	"./hy-am.js": 292,
	"./id": 293,
	"./id.js": 293,
	"./is": 294,
	"./is.js": 294,
	"./it": 295,
	"./it.js": 295,
	"./ja": 296,
	"./ja.js": 296,
	"./jv": 297,
	"./jv.js": 297,
	"./ka": 298,
	"./ka.js": 298,
	"./kk": 299,
	"./kk.js": 299,
	"./km": 300,
	"./km.js": 300,
	"./kn": 301,
	"./kn.js": 301,
	"./ko": 302,
	"./ko.js": 302,
	"./ku": 303,
	"./ku.js": 303,
	"./ky": 304,
	"./ky.js": 304,
	"./lb": 305,
	"./lb.js": 305,
	"./lo": 306,
	"./lo.js": 306,
	"./lt": 307,
	"./lt.js": 307,
	"./lv": 308,
	"./lv.js": 308,
	"./me": 309,
	"./me.js": 309,
	"./mi": 310,
	"./mi.js": 310,
	"./mk": 311,
	"./mk.js": 311,
	"./ml": 312,
	"./ml.js": 312,
	"./mn": 313,
	"./mn.js": 313,
	"./mr": 314,
	"./mr.js": 314,
	"./ms": 315,
	"./ms-my": 316,
	"./ms-my.js": 316,
	"./ms.js": 315,
	"./mt": 317,
	"./mt.js": 317,
	"./my": 318,
	"./my.js": 318,
	"./nb": 319,
	"./nb.js": 319,
	"./ne": 320,
	"./ne.js": 320,
	"./nl": 321,
	"./nl-be": 322,
	"./nl-be.js": 322,
	"./nl.js": 321,
	"./nn": 323,
	"./nn.js": 323,
	"./pa-in": 324,
	"./pa-in.js": 324,
	"./pl": 325,
	"./pl.js": 325,
	"./pt": 326,
	"./pt-br": 327,
	"./pt-br.js": 327,
	"./pt.js": 326,
	"./ro": 328,
	"./ro.js": 328,
	"./ru": 329,
	"./ru.js": 329,
	"./sd": 330,
	"./sd.js": 330,
	"./se": 331,
	"./se.js": 331,
	"./si": 332,
	"./si.js": 332,
	"./sk": 333,
	"./sk.js": 333,
	"./sl": 334,
	"./sl.js": 334,
	"./sq": 335,
	"./sq.js": 335,
	"./sr": 336,
	"./sr-cyrl": 337,
	"./sr-cyrl.js": 337,
	"./sr.js": 336,
	"./ss": 338,
	"./ss.js": 338,
	"./sv": 339,
	"./sv.js": 339,
	"./sw": 340,
	"./sw.js": 340,
	"./ta": 341,
	"./ta.js": 341,
	"./te": 342,
	"./te.js": 342,
	"./tet": 343,
	"./tet.js": 343,
	"./tg": 344,
	"./tg.js": 344,
	"./th": 345,
	"./th.js": 345,
	"./tl-ph": 346,
	"./tl-ph.js": 346,
	"./tlh": 347,
	"./tlh.js": 347,
	"./tr": 348,
	"./tr.js": 348,
	"./tzl": 349,
	"./tzl.js": 349,
	"./tzm": 350,
	"./tzm-latn": 351,
	"./tzm-latn.js": 351,
	"./tzm.js": 350,
	"./ug-cn": 352,
	"./ug-cn.js": 352,
	"./uk": 353,
	"./uk.js": 353,
	"./ur": 354,
	"./ur.js": 354,
	"./uz": 355,
	"./uz-latn": 356,
	"./uz-latn.js": 356,
	"./uz.js": 355,
	"./vi": 357,
	"./vi.js": 357,
	"./x-pseudo": 358,
	"./x-pseudo.js": 358,
	"./yo": 359,
	"./yo.js": 359,
	"./zh-cn": 360,
	"./zh-cn.js": 360,
	"./zh-hk": 361,
	"./zh-hk.js": 361,
	"./zh-tw": 362,
	"./zh-tw.js": 362
};
function webpackContext(req) {
	return __webpack_require__(webpackContextResolve(req));
};
function webpackContextResolve(req) {
	var id = map[req];
	if(!(id + 1)) // check for number or string
		throw new Error("Cannot find module '" + req + "'.");
	return id;
};
webpackContext.keys = function webpackContextKeys() {
	return Object.keys(map);
};
webpackContext.resolve = webpackContextResolve;
module.exports = webpackContext;
webpackContext.id = 465;

/***/ }),

/***/ 484:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ContactPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_sqlite__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__add_data_add_data__ = __webpack_require__(70);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__edit_data_edit_data__ = __webpack_require__(18);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var ContactPage = /** @class */ (function () {
    function ContactPage(navCtrl, sqlite) {
        this.navCtrl = navCtrl;
        this.sqlite = sqlite;
        this.usuarios = [];
        this.totalIncome = 0;
        this.totalExpense = 0;
        this.balance = 0;
    }
    ContactPage.prototype.ionViewDidLoad = function () {
        this.getData();
    };
    ContactPage.prototype.ionViewWillEnter = function () {
        this.getData();
    };
    ContactPage.prototype.getData = function () {
        var _this = this;
        this.sqlite.create({
            name: 'idoso.db',
            location: 'default'
        }).then(function (db) {
            db.executeSql('CREATE TABLE IF NOT EXISTS perfil(rowid INTEGER PRIMARY KEY, nascimento Date, nome TEXT, sexo TEXT, imagem TEXT)', [])
                .then(function (res) { return console.log('Executed SQL'); })
                .catch(function (e) { return console.log(e); });
            db.executeSql('SELECT * FROM perfil ORDER BY nome DESC', [])
                .then(function (res) {
                _this.usuarios = [];
                for (var i = 0; i < res.rows.length; i++) {
                    _this.usuarios.push({ rowid: res.rows.item(i).rowid, nascimento: res.rows.item(i).nascimento, nome: res.rows.item(i).nome, sexo: res.rows.item(i).sexo, imagem: res.rows.item(i).imagem });
                }
            })
                .catch(function (e) { return console.log(e); });
            /*     db.executeSql('SELECT SUM(amount) AS totalIncome FROM expense WHERE type="Income"', [])
                 .then(res => {
                   if(res.rows.length>0) {
                     this.totalIncome = parseInt(res.rows.item(0).totalIncome);
                     this.balance = this.totalIncome-this.totalExpense;
                   }
                 })
                 .catch(e => console.log(e));
                 db.executeSql('SELECT SUM(amount) AS totalExpense FROM expense WHERE type="Expense"', [])
                 .then(res => {
                   if(res.rows.length>0) {
                     this.totalExpense = parseInt(res.rows.item(0).totalExpense);
                     this.balance = this.totalIncome-this.totalExpense;
                   }
                 })
                 */
        }).catch(function (e) { return console.log(e); });
    };
    ContactPage.prototype.addData = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__add_data_add_data__["a" /* AddDataPage */]);
    };
    ContactPage.prototype.editData = function (rowid) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__edit_data_edit_data__["a" /* EditDataPage */], {
            rowid: rowid
        });
    };
    ContactPage.prototype.deleteData = function (rowid) {
        var _this = this;
        this.sqlite.create({
            name: 'idoso.db',
            location: 'default'
        }).then(function (db) {
            db.executeSql('DELETE FROM perfil WHERE rowid=?', [rowid])
                .then(function (res) {
                console.log(res);
                _this.getData();
            })
                .catch(function (e) { return console.log(e); });
        }).catch(function (e) { return console.log(e); });
    };
    ContactPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-contact',template:/*ion-inline-start:"/home/rabaco/Documentos/projetos_ionic/idoso_aplicativo_final/src/pages/contact/contact.html"*/'<ion-header>\n  <ion-navbar>\n    <ion-title>\n      Perfil\n    </ion-title>\n    <ion-buttons end>\n      <button ion-button icon-only (click)="addData()">\n        <ion-icon name="add-circle"></ion-icon>\n      </button>\n    </ion-buttons>\n  </ion-navbar>\n</ion-header>\n\n<ion-content padding>\n  <h2>Lista de Idosos</h2>\n  <ion-list>\n    <ion-item-sliding *ngFor="let usuario of usuarios; let i=index">\n      <ion-item nopadding>\n        <p>\n          <span>{{usuario.nome}}</span><br>\n          Nascimento: {{usuario.nascimento| date:"dd/MM/yyyy"}}<br>\n          {{usuario.sexo}}\n          <br>\n          \n\n            <button ion-button color="primary" (click)="editData(usuario.rowid)">\n              <ion-icon name="paper"></ion-icon>\n            </button>\n            <button ion-button color="danger" (click)="deleteData(usuario.rowid)">\n              <ion-icon name="trash"></ion-icon>\n            </button>\n\n        </p>\n\n      </ion-item>\n        </ion-item-sliding>\n  </ion-list>\n</ion-content>\n<ion-footer>\n</ion-footer>\n'/*ion-inline-end:"/home/rabaco/Documentos/projetos_ionic/idoso_aplicativo_final/src/pages/contact/contact.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_2__ionic_native_sqlite__["a" /* SQLite */]])
    ], ContactPage);
    return ContactPage;
}());

//# sourceMappingURL=contact.js.map

/***/ }),

/***/ 56:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ResultadoPanturrilhaPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_sqlite__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_common__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__avaliacao_avaliacao__ = __webpack_require__(39);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__edit_data_edit_data__ = __webpack_require__(18);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var ResultadoPanturrilhaPage = /** @class */ (function () {
    function ResultadoPanturrilhaPage(navCtrl, sqlite, navParams, alertCtrl, actionSheetCtrl, datepipe) {
        this.navCtrl = navCtrl;
        this.sqlite = sqlite;
        this.navParams = navParams;
        this.alertCtrl = alertCtrl;
        this.actionSheetCtrl = actionSheetCtrl;
        this.datepipe = datepipe;
        this.data = { rowid: 1, nascimento: "", sexo: "", nome: "", imagem: "" };
        this.photo = "assets/imgs/user.png";
        this.panturrilhas = [];
        this.nome_teste = "";
        this.nome_teste2 = "";
        this.num = 0;
        this.voltar = 2;
        if (navParams.get("id_panturrilha")) {
            this.getData(navParams.get("id_panturrilha"));
            this.voltar = 2;
        }
        else {
            this.getData2();
            this.voltar = 3;
        }
    }
    ResultadoPanturrilhaPage.prototype.ionViewDidLoad = function () {
        this.getUser(1);
    };
    ResultadoPanturrilhaPage.prototype.goBack = function () {
        this.navCtrl.popTo(this.navCtrl.getByIndex(this.navCtrl.length() - this.voltar));
        //console.log("aa" +this.voltar);
    };
    ResultadoPanturrilhaPage.prototype.getData = function (id_panturrilha) {
        var _this = this;
        this.sqlite.create({
            name: 'idoso.db',
            location: 'default'
        }).then(function (db) {
            var linha = 2;
            db.executeSql('SELECT id_panturrilha, circunferencia, data_exame FROM teste_panturrilha where id_panturrilha=?', [id_panturrilha])
                .then(function (res) {
                _this.panturrilhas = [];
                _this.num = res.rows.length;
                for (var i = 0; i < res.rows.length; i++) {
                    //passa parametros para gerar o gráfico
                    //var data_ex=new Date(res.rows.item(i).data_exame);
                    //var data_final=data_ex.toDateString();
                    var data_ex = new Date(res.rows.item(i).data_exame);
                    _this.data_final = _this.datepipe.transform(data_ex, 'dd/MM/yyyy');
                    var nome_teste = res.rows.item(i).nome;
                    if (nome_teste !== nome_teste2) {
                        var nome_teste2 = res.rows.item(i).nome;
                        linha = linha + 1;
                    }
                    var calculo_panturrilha = parseFloat(res.rows.item(i).circunferencia.toFixed(1));
                    var calculo_string = calculo_panturrilha.toString().replace(/[.]/gi, ",");
                    var myDateString = new Date(res.rows.item(i).nascimento);
                    var milisecondsDiff = Date.now() - (myDateString.getTime());
                    var idade = Math.floor((milisecondsDiff) / (1000 * 3600 * 24) / 365);
                    //let idade=milisecondsDiff;
                    _this.panturrilhas.push({ id_panturrilha: res.rows.item(i).id_panturrilha, nome: res.rows.item(i).nome, data_exame: res.rows.item(i).data_exame, circunferencia: res.rows.item(i).circunferencia, circunferencia2: calculo_string, idade: idade });
                }
            })
                .catch(function (e) { return console.log(e); });
        }).catch(function (e) { return console.log(e); });
    };
    ResultadoPanturrilhaPage.prototype.getData2 = function () {
        var _this = this;
        this.sqlite.create({
            name: 'idoso.db',
            location: 'default'
        }).then(function (db) {
            var linha = 2;
            db.executeSql('SELECT id_panturrilha, circunferencia, data_exame FROM teste_panturrilha order by id_panturrilha DESC limit 1', [])
                .then(function (res) {
                _this.panturrilhas = [];
                _this.num = res.rows.length;
                for (var i = 0; i < res.rows.length; i++) {
                    //passa parametros para gerar o gráfico
                    //var data_ex=new Date(res.rows.item(i).data_exame);
                    //var data_final=data_ex.toDateString();
                    var data_ex = new Date(res.rows.item(i).data_exame);
                    _this.data_final = _this.datepipe.transform(data_ex, 'dd/MM/yyyy');
                    var nome_teste = res.rows.item(i).nome;
                    if (nome_teste !== nome_teste2) {
                        var nome_teste2 = res.rows.item(i).nome;
                        linha = linha + 1;
                    }
                    var calculo_panturrilha = parseFloat(res.rows.item(i).circunferencia.toFixed(1));
                    var calculo_string = calculo_panturrilha.toString().replace(/[.]/gi, ",");
                    var myDateString = new Date(res.rows.item(i).nascimento);
                    var milisecondsDiff = Date.now() - (myDateString.getTime());
                    var idade = Math.floor((milisecondsDiff) / (1000 * 3600 * 24) / 365);
                    //let idade=milisecondsDiff;
                    _this.panturrilhas.push({ id_panturrilha: res.rows.item(i).id_panturrilha, nome: res.rows.item(i).nome, data_exame: res.rows.item(i).data_exame, circunferencia: res.rows.item(i).circunferencia, circunferencia2: calculo_string, idade: idade });
                }
            })
                .catch(function (e) { return console.log(e); });
        }).catch(function (e) { return console.log(e); });
    };
    /*
       addPanturrilha() {
         this.navCtrl.push(AddPanturrilhaPage);
       }
    */
    ResultadoPanturrilhaPage.prototype.deletePanturrilha2 = function (id_panturrilha) {
        var _this = this;
        this.sqlite.create({
            name: 'idoso.db',
            location: 'default'
        }).then(function (db) {
            db.executeSql('DELETE FROM teste_panturrilha WHERE id_panturrilha=?', [id_panturrilha])
                .then(function (res) {
                console.log(res);
                _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__avaliacao_avaliacao__["a" /* AvaliacaoPage */]);
            })
                .catch(function (e) { return console.log(e); });
        }).catch(function (e) { return console.log(e); });
    };
    ResultadoPanturrilhaPage.prototype.deletePanturrilha = function (id_panturrilha) {
        var _this = this;
        var alert = this.alertCtrl.create({
            title: 'Tem certeza que quer excluir o resultado?',
            message: '',
            buttons: [
                {
                    text: 'Não',
                    role: 'cancel',
                    handler: function () {
                        //console.log('Cancel clicked');
                    }
                },
                {
                    text: 'Sim',
                    handler: function () {
                        // console.log('Buy clicked');
                        _this.deletePanturrilha2(id_panturrilha);
                    }
                }
            ]
        });
        alert.present();
    };
    ResultadoPanturrilhaPage.prototype.addPerfil2 = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__edit_data_edit_data__["a" /* EditDataPage */], {
            rowid: 1
        });
    };
    ResultadoPanturrilhaPage.prototype.getUser = function (rowid) {
        var _this = this;
        this.sqlite.create({
            name: 'idoso.db',
            location: 'default'
        }).then(function (db) {
            db.executeSql('SELECT * FROM perfil WHERE rowid=?', [rowid])
                .then(function (res) {
                if (res.rows.length > 0) {
                    _this.data.rowid = res.rows.item(0).rowid;
                    _this.data.nome = res.rows.item(0).nome;
                    _this.data.sexo = res.rows.item(0).sexo;
                    _this.data.nascimento = res.rows.item(0).nascimento;
                    _this.data.imagem = res.rows.item(0).imagem;
                    _this.photo = res.rows.item(0).imagem;
                }
                if (_this.photo == "") {
                    _this.photo = "assets/imgs/user.png";
                }
            });
        });
    };
    ResultadoPanturrilhaPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-resultado-panturrilha',template:/*ion-inline-start:"/home/rabaco/Documentos/projetos_ionic/idoso_aplicativo_final/src/pages/resultado-panturrilha/resultado-panturrilha.html"*/'<ion-header>\n    <ion-navbar hideBackButton="true">\n      <ion-buttons left>\n    <button (click)="goBack()" ion-button icon-only style="margin-left: 10px">\n      <ion-icon name="md-arrow-back"></ion-icon>\n    </button>\n  </ion-buttons>\n      <ion-title>\n        Panturrilha\n      </ion-title>\n      <ion-buttons end>\n        <div class="icone-imagem">\n          <img src="{{ photo }}" class="icone-imagem" (click)="addPerfil2()">\n        </div>\n      </ion-buttons>\n      <!--\n      <ion-buttons end>\n        <button ion-button icon-only (click)="addPanturrilha()" >\n          <ion-icon name="add-circle"></ion-icon>\n        </button>\n      </ion-buttons>\n    -->\n    </ion-navbar>\n  </ion-header>\n\n  <ion-content padding >\n   <div class="titulo-testes">Resultado</div>\n      <ion-list *ngFor="let panturrilha of panturrilhas; let i=index" > <div class="texto-data">{{panturrilha.data_exame | date:"dd/MM/yyyy"}} \n         <button ion-button icon-only  (click)="deletePanturrilha(panturrilha.id_panturrilha)" class="button"  >\n          <ion-icon name="ios-trash" color="danger"></ion-icon>\n        </button></div>\n      <ion-item-sliding >\n        <ion-item style="margin:0; padding:0">\n        <div class="valor-teste">{{panturrilha.circunferencia2}}<span class="cm"> cm</span></div>\n         <div class="quebra">\n            <span *ngIf="(panturrilha.circunferencia > 45)">\n                <div class="circulo_testes"><img src="assets/imgs/circulo_branco.png"></div><div class="circulo-texto">Acompanhamento<br>de Rotina</div></span>\n           <span *ngIf="(panturrilha.circunferencia >= 35 && panturrilha.circunferencia <= 45)">\n             <div class="circulo_testes"><img src="assets/imgs/circulo_verde.png"></div><div class="circulo-texto">Acompanhamento<br>de Rotina</div></span>\n            <span *ngIf="(panturrilha.circunferencia >= 31) && (panturrilha.circunferencia <35) "><div class="circulo_testes"><img src="assets/imgs/circulo_amarelo.png"></div><div class="circulo-texto2">Atenção<br></div></span>\n            <span *ngIf="(panturrilha.circunferencia < 31)"><div class="circulo_testes"><img src="assets/imgs/circulo_vermelho.png"></div> <div class="circulo-texto2">Ação<br></div></span>\n\n           <!-- <span>{{panturrilha.nome}} ({{panturrilha.idade }}) Anos</span><br>\n            Data do Exame: {{panturrilha.data_exame | date:"dd/MM/yyyy"}} -->\n            <br>\n              </div>\n               <div class="quebra" >\n                 <ion-card color="fundo-item" nopadding>\n                 <ion-card-content>\n                   <ion-card-title class="texto-titulo-avaliacao">\n                   <strong>Recomendações</strong>\n                   </ion-card-title>\n                   <span *ngIf="(panturrilha.circunferencia >45 )" class="texto-recomendacaoes" text-wrap>A circunferência da sua panturrilha (batata da perna) sugere que sua condição muscular está satisfatória. Mantenha acompanhamento de rotina, medindo esse dado novamente daqui a cerca de seis meses.</span>\n                   <span *ngIf="(panturrilha.circunferencia >= 35 && panturrilha.circunferencia <= 45 )" class="texto-recomendacaoes" text-wrap>Parabéns! A circunferência da sua panturrilha (batata da perna) sugere que sua condição muscular está satisfatória. Mantenha acompanhamento de rotina, medindo esse dado novamente daqui a cerca de seis meses.</span>\n                   <span *ngIf="(panturrilha.circunferencia >= 31) && (panturrilha.circunferencia <35)" class="texto-recomendacaoes" text-wrap>A circunferência da sua panturrilha (batata da perna) sugere que sua condição muscular pode estar apresentando perdas e requer atenção. Procure um profissional de saúde para obter orientações sobre como proceder.</span>\n                   <span *ngIf="(panturrilha.circunferencia < 31)" class="texto-recomendacaoes" text-wrap>A circunferência da sua panturrilha (batata da perna) sugere sua que condição muscular está apresentando perdas (sarcopenia) e isso pode agravar dificuldades de autonomia e independência. Procure um médico ou profissional de saúde para obter orientações sobre como proceder.</span>\n                 <div class="referencia" text-wrap>\n                  <span class="fontes">Fonte(s):<br></span>\n                  Caderneta Virtual de Saúde da Pessoa Idosa<br><br>\n                  \n                  Caderno de Atenção Básica nº 19 - Envelhecimento e\n                  Saúde da Pessoa Idosa<br><br><br>\n                  <span class="atencao">Obs: As avaliações e orientações fornecidas nesse aplicativo não substituem a consulta médica.</span>\n                 </div>\n                  </ion-card-content>\n               </ion-card>\n               <!--\n              <button ion-button color="primary" (click)="editPanturrilha(panturrilha.id_panturrilha)">\n                <ion-icon name="paper"></ion-icon>\n              </button>\n            -->\n\n\n             \n            </div>\n\n        </ion-item>\n\n          </ion-item-sliding>\n\n\n\n    </ion-list>\n    <div class="row">\n      <div class="col-md-6">\n        <div style="display: block;">\n        <canvas baseChart height="250"\n                    [datasets]="lineChartData"\n                    *ngIf="i<1"\n                    [labels]="lineChartLabels"\n                    [options]="lineChartOptions"\n                    [colors]="lineChartColors"\n                    [legend]="lineChartLegend"\n                    [chartType]="lineChartType"\n                    (chartHover)="chartHovered($event)"\n                    (chartClick)="chartClicked($event)"></canvas>\n        </div>\n      </div>\n    </div>\n\n  </ion-content>\n  <ion-footer>\n  </ion-footer>\n'/*ion-inline-end:"/home/rabaco/Documentos/projetos_ionic/idoso_aplicativo_final/src/pages/resultado-panturrilha/resultado-panturrilha.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_2__ionic_native_sqlite__["a" /* SQLite */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* ActionSheetController */],
            __WEBPACK_IMPORTED_MODULE_3__angular_common__["d" /* DatePipe */]])
    ], ResultadoPanturrilhaPage);
    return ResultadoPanturrilhaPage;
}());

//# sourceMappingURL=resultado-panturrilha.js.map

/***/ }),

/***/ 57:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ResultadoImcPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_sqlite__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__avaliacao_avaliacao__ = __webpack_require__(39);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_common__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__edit_data_edit_data__ = __webpack_require__(18);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



//import { AddImcPage } from '../add-imc/add-imc';




var ResultadoImcPage = /** @class */ (function () {
    function ResultadoImcPage(navCtrl, sqlite, navParams, alertCtrl, datepipe) {
        this.navCtrl = navCtrl;
        this.sqlite = sqlite;
        this.navParams = navParams;
        this.alertCtrl = alertCtrl;
        this.datepipe = datepipe;
        this.data = { rowid: 1, nascimento: "", sexo: "", nome: "", imagem: "" };
        this.photo = "assets/imgs/user.png";
        this.imcs = [];
        this.nome_teste = "";
        this.nome_teste2 = "";
        this.voltar = 2;
        if (navParams.get("id_imc")) {
            this.getData(navParams.get("id_imc"));
            this.voltar = 2;
        }
        else {
            this.getData2();
            this.voltar = 3;
        }
    }
    ResultadoImcPage_1 = ResultadoImcPage;
    ResultadoImcPage.prototype.ionViewDidLoad = function () {
        this.getUser(1);
    };
    ResultadoImcPage.prototype.goBack = function () {
        this.navCtrl.popTo(this.navCtrl.getByIndex(this.navCtrl.length() - this.voltar));
        //console.log("aa" +this.voltar);
    };
    ResultadoImcPage.prototype.getData = function (id_imc) {
        var _this = this;
        this.sqlite.create({
            name: 'idoso.db',
            location: 'default'
        }).then(function (db) {
            var linha = 2;
            db.executeSql('SELECT id_imc, peso,altura, data_exame FROM teste_imc where id_imc=?', [id_imc])
                .then(function (res) {
                _this.imcs = [];
                for (var i = 0; i < res.rows.length; i++) {
                    //passa parametros para gerar o gráfico
                    //var data_ex=new Date(res.rows.item(i).data_exame);
                    //var data_final=data_ex.toDateString();
                    var data_ex = new Date(res.rows.item(i).data_exame);
                    _this.data_final = _this.datepipe.transform(data_ex, 'dd/MM/yyyy HH:mm');
                    var nome_teste = res.rows.item(i).nome;
                    if (nome_teste !== nome_teste2) {
                        var nome_teste2 = res.rows.item(i).nome;
                        linha = linha + 1;
                    }
                    var finalBmi = res.rows.item(i).peso / (res.rows.item(i).altura * res.rows.item(i).altura);
                    var calculo_imc = parseFloat(finalBmi.toFixed(1));
                    var calculo_string = calculo_imc.toString().replace(/[.]/gi, ",");
                    var myDateString = new Date(res.rows.item(i).nascimento);
                    var milisecondsDiff = Date.now() - (myDateString.getTime());
                    var idade = Math.floor((milisecondsDiff) / (1000 * 3600 * 24) / 365);
                    //let idade=milisecondsDiff;
                    _this.imcs.push({ id_imc: res.rows.item(i).id_imc, nome: res.rows.item(i).nome, data_exame: res.rows.item(i).data_exame, peso: res.rows.item(i).peso, altura: res.rows.item(i).altura, idade: idade, calculo_imc: calculo_imc, calculo_string: calculo_string });
                }
            })
                .catch(function (e) { return console.log(e); });
        }).catch(function (e) { return console.log(e); });
    };
    ResultadoImcPage.prototype.getData2 = function () {
        var _this = this;
        this.sqlite.create({
            name: 'idoso.db',
            location: 'default'
        }).then(function (db) {
            var linha = 2;
            db.executeSql('SELECT id_imc,  peso,  altura, data_exame FROM teste_imc order by id_imc DESC limit 1', [])
                .then(function (res) {
                _this.imcs = [];
                for (var i = 0; i < res.rows.length; i++) {
                    //passa parametros para gerar o gráfico
                    //var data_ex=new Date(res.rows.item(i).data_exame);
                    //var data_final=data_ex.toDateString();
                    var data_ex = new Date(res.rows.item(i).data_exame);
                    _this.data_final = _this.datepipe.transform(data_ex, 'dd/MM/yyyy HH:mm');
                    var nome_teste = res.rows.item(i).nome;
                    if (nome_teste !== nome_teste2) {
                        var nome_teste2 = res.rows.item(i).nome;
                        linha = linha + 1;
                    }
                    var finalBmi = res.rows.item(i).peso / (res.rows.item(i).altura * res.rows.item(i).altura);
                    var calculo_imc = parseFloat(finalBmi.toFixed(1));
                    var calculo_string = calculo_imc.toString().replace(/[.]/gi, ",");
                    var myDateString = new Date(res.rows.item(i).nascimento);
                    var milisecondsDiff = Date.now() - (myDateString.getTime());
                    var idade = Math.floor((milisecondsDiff) / (1000 * 3600 * 24) / 365);
                    //let idade=milisecondsDiff;
                    _this.imcs.push({ id_imc: res.rows.item(i).id_imc, nome: res.rows.item(i).nome, data_exame: res.rows.item(i).data_exame, peso: res.rows.item(i).peso, altura: res.rows.item(i).altura, idade: idade, calculo_imc: calculo_imc, calculo_string: calculo_string });
                }
            })
                .catch(function (e) { return console.log(e); });
        }).catch(function (e) { return console.log(e); });
    };
    /*
         addImc() {
           this.navCtrl.push(AddImcPage);
         }
    */
    ResultadoImcPage.prototype.editImc = function (id_imc) {
        this.navCtrl.push(ResultadoImcPage_1, {
            id_imc: id_imc
        });
    };
    ResultadoImcPage.prototype.deleteImc2 = function (id_imc) {
        var _this = this;
        this.sqlite.create({
            name: 'idoso.db',
            location: 'default'
        }).then(function (db) {
            db.executeSql('DELETE FROM teste_imc WHERE id_imc=?', [id_imc])
                .then(function (res) {
                console.log(res);
                _this.getData(id_imc);
                _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__avaliacao_avaliacao__["a" /* AvaliacaoPage */]);
            })
                .catch(function (e) { return console.log(e); });
        }).catch(function (e) { return console.log(e); });
    };
    ResultadoImcPage.prototype.deleteImc = function (id_imc) {
        var _this = this;
        var alert = this.alertCtrl.create({
            title: 'Tem certeza que quer excluir o resultado?',
            message: '',
            buttons: [
                {
                    text: 'Não',
                    role: 'cancel',
                    handler: function () {
                        //console.log('Cancel clicked');
                    }
                },
                {
                    text: 'Sim',
                    handler: function () {
                        // console.log('Buy clicked');
                        _this.deleteImc2(id_imc);
                    }
                }
            ]
        });
        alert.present();
    };
    ResultadoImcPage.prototype.addPerfil2 = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__edit_data_edit_data__["a" /* EditDataPage */], {
            rowid: 1
        });
    };
    ResultadoImcPage.prototype.getUser = function (rowid) {
        var _this = this;
        this.sqlite.create({
            name: 'idoso.db',
            location: 'default'
        }).then(function (db) {
            db.executeSql('SELECT * FROM perfil WHERE rowid=?', [rowid])
                .then(function (res) {
                if (res.rows.length > 0) {
                    _this.data.rowid = res.rows.item(0).rowid;
                    _this.data.nome = res.rows.item(0).nome;
                    _this.data.sexo = res.rows.item(0).sexo;
                    _this.data.nascimento = res.rows.item(0).nascimento;
                    _this.data.imagem = res.rows.item(0).imagem;
                    _this.photo = res.rows.item(0).imagem;
                }
                if (_this.photo == "") {
                    _this.photo = "assets/imgs/user.png";
                }
            });
        });
    };
    ResultadoImcPage = ResultadoImcPage_1 = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-resultado-imc',template:/*ion-inline-start:"/home/rabaco/Documentos/projetos_ionic/idoso_aplicativo_final/src/pages/resultado-imc/resultado-imc.html"*/'<ion-header>\n  <ion-navbar hideBackButton="true">\n    <ion-buttons left>\n  <button (click)="goBack()" ion-button icon-only style="margin-left: 10px">\n    <ion-icon name="md-arrow-back"></ion-icon>\n  </button>\n</ion-buttons>\n    <ion-title>Índice de massa corporal</ion-title>\n    <ion-buttons end>\n      <div class="icone-imagem">\n        <img src="{{ photo }}" class="icone-imagem" (click)="addPerfil2()">\n      </div>\n    </ion-buttons>\n    <!--\n    <ion-buttons end>\n      <button ion-button icon-only (click)="addImc()" >\n        <ion-icon name="add-circle"></ion-icon>\n      </button>\n    </ion-buttons>\n  -->\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n    <div class="titulo-testes">Resultado</div>\n    <ion-list style="margin:0; padding:0" *ngFor="let imc of imcs; let i=index">\n        <div class="texto-data">{{imc.data_exame | date:"dd/MM/yyyy"}} \n            <button ion-button icon-only  (click)="deleteImc(imc.id_imc)" class="button"  >\n             <ion-icon name="ios-trash" color="danger"></ion-icon>\n           </button></div>\n\n     \n      <div class="valor-teste">{{ imc.calculo_string }}<span class="cm"> Kg/m²</span></div>\n      <div class="quebra">\n       <span *ngIf="(imc.calculo_imc > 27 )"><div class="circulo_testes"><img src="assets/imgs/circulo_vermelho.png"></div><div class="circulo-texto2">Sobrepeso</div></span>\n        <span *ngIf="(imc.calculo_imc >= 22) && (imc.calculo_imc <=27) "><div class="circulo_testes"><img src="assets/imgs/circulo_verde.png"></div><div class="circulo-texto2">Peso Adequado</div></span>\n        <span *ngIf="(imc.calculo_imc < 22)"><div class="circulo_testes"><img src="assets/imgs/circulo_vermelho.png"></div> <div class="circulo-texto2">Baixo Peso</div></span>\n        <br>\n        <div class="quebra">\n            <ion-card color="fundo-item">\n                <ion-card-content>\n                  <ion-card-title class="texto-titulo-avaliacao">\n                  <strong>Recomendações</strong>\n                  </ion-card-title>\n            <span *ngIf="(imc.calculo_imc > 27 )" class="texto-recomendacaoes" text-wrap>Atenção. Seu IMC está acima do adequado. Talvez seja necessário uma reorientação nutricional e verificação de outros fatores que dificultem o equilíbrio do IMC. Seria indicado que procurasse o serviço de saúde mais próximo para verificar questões nutricionais, possíveis distúrbios e avaliar a possibilidade de ingressar e/ou adaptar a participação em atividades físicas.</span>\n            <span *ngIf="(imc.calculo_imc >= 22) && (imc.calculo_imc <=27)" class="texto-recomendacaoes" text-wrap>Parabéns! Você está com o peso adequado. É provável que tenha uma dieta saudável, pratique atividades físicas e tenha fatores genéticos favoráveis. Mantenha seus hábitos e verifique seu IMC a cada seis meses.</span>\n            <span *ngIf="(imc.calculo_imc < 22)" class="texto-recomendacaoes" text-wrap>Atenção. Seu IMC está abaixo do adequado. Talvez existam fatores interferindo no seu apetite ou estado nutricional. A falta de exercícios, especialmente exercícios de resistência e força, contribuem para perdas musculares (sarcopenia) e ósseas (osteoporose). Seria indicado que procurasse o serviço de saúde mais próximo para verificar essas possibilidades.</span>\n            <div class="referencia" text-wrap>\n              <span class="fontes">Fonte(s):<br></span>\n              Caderneta Virtual de Saúde da Pessoa Idosa<br><br>\n              \n              Caderno de Atenção Básica nº 19 - Envelhecimento e\n              Saúde da Pessoa Idosa<br><br><br>\n              <span class="atencao">Obs: As avaliações e orientações fornecidas nesse aplicativo não substituem a consulta médica.</span>\n             </div>\n            <br>\n                </ion-card-content>\n            </ion-card>\n\n        </div>\n        </div>\n\n    </ion-list>\n\n</ion-content>\n'/*ion-inline-end:"/home/rabaco/Documentos/projetos_ionic/idoso_aplicativo_final/src/pages/resultado-imc/resultado-imc.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_2__ionic_native_sqlite__["a" /* SQLite */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_4__angular_common__["d" /* DatePipe */]])
    ], ResultadoImcPage);
    return ResultadoImcPage;
    var ResultadoImcPage_1;
}());

//# sourceMappingURL=resultado-imc.js.map

/***/ }),

/***/ 58:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ResultadoVes13Page; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_sqlite__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__avaliacao_avaliacao__ = __webpack_require__(39);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_common__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__edit_data_edit_data__ = __webpack_require__(18);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





//import { ThrowStmt } from '@angular/compiler';

var ResultadoVes13Page = /** @class */ (function () {
    function ResultadoVes13Page(navCtrl, sqlite, navParams, alertCtrl, actionSheetCtrl, datepipe) {
        this.navCtrl = navCtrl;
        this.sqlite = sqlite;
        this.navParams = navParams;
        this.alertCtrl = alertCtrl;
        this.actionSheetCtrl = actionSheetCtrl;
        this.datepipe = datepipe;
        this.data = { rowid: 1, nascimento: "", sexo: "", nome: "", imagem: "" };
        this.photo = "assets/imgs/user.png";
        this.ves13s = [];
        this.voltar = 2;
        this.linha = 0;
        this.pontuacaototal = 0;
        this.pontos_total = 0;
        if (navParams.get("id_ves13")) {
            this.getData(navParams.get("id_ves13"));
            this.voltar = 2;
        }
        else {
            this.getData2();
            this.voltar = 3;
        }
    }
    ResultadoVes13Page.prototype.ionViewDidLoad = function () {
        this.getUser(1);
    };
    ResultadoVes13Page.prototype.goBack = function () {
        this.navCtrl.popTo(this.navCtrl.getByIndex(this.navCtrl.length() - this.voltar));
        //console.log("aa" +this.voltar);
    };
    ResultadoVes13Page.prototype.getData = function (id_ves13) {
        var _this = this;
        this.sqlite.create({
            name: 'idoso.db',
            location: 'default'
        }).then(function (db) {
            _this.linha = 1;
            db.executeSql('SELECT id_ves13, id_perfil, idade,  percepcao_saude,  limitacao1,  limitacao2, limitacao3, limitacao4, limitacao5, limitacao6,  incapacidade1,incapacidade2, incapacidade3,incapacidade4,incapacidade5, data_exame from teste_ves13 where id_ves13=?', [id_ves13])
                .then(function (res) {
                _this.ves13s = [];
                for (var i = 0; i < res.rows.length; i++) {
                    //passa parametros para gerar o gráfico
                    var data_ex = new Date(res.rows.item(i).data_exame);
                    _this.data_final = _this.datepipe.transform(data_ex, 'dd/MM/yyyy HH:mm');
                    //soma parte 1
                    var idade = res.rows.item(i).idade;
                    var somaParcial = null;
                    var pontosIdade = null;
                    var pontosPercepcao = 0;
                    if (idade > 60 && idade < 74) {
                        pontosIdade = 0;
                    }
                    if (idade > 75 && idade < 84) {
                        pontosIdade = 1;
                    }
                    if (idade > 84) {
                        pontosIdade = 3;
                    }
                    if (res.rows.item(i).percepcao_saude >= 4) {
                        pontosPercepcao = 1;
                    }
                    somaParcial = pontosIdade + pontosPercepcao;
                    //limitacoes fisicas
                    var pontosLimitacao1 = 0;
                    var pontosLimitacao2 = 0;
                    var pontosLimitacao3 = 0;
                    var pontosLimitacao4 = 0;
                    var pontosLimitacao5 = 0;
                    var pontosLimitacao6 = 0;
                    if (res.rows.item(i).limitacao1 >= 4) {
                        pontosLimitacao1 = 1;
                    }
                    if (res.rows.item(i).limitacao2 >= 4) {
                        pontosLimitacao2 = 1;
                    }
                    if (res.rows.item(i).limitacao3 >= 4) {
                        pontosLimitacao3 = 1;
                    }
                    if (res.rows.item(i).limitacao4 >= 4) {
                        pontosLimitacao4 = 1;
                    }
                    if (res.rows.item(i).limitacao5 >= 4) {
                        pontosLimitacao5 = 1;
                    }
                    if (res.rows.item(i).limitacao6 >= 4) {
                        pontosLimitacao6 = 1;
                    }
                    var somaLimitacao = pontosLimitacao1 + pontosLimitacao2 + pontosLimitacao3 + pontosLimitacao4 + pontosLimitacao5 + pontosLimitacao6;
                    var maxLimicacao = somaLimitacao;
                    if (somaLimitacao > 2) {
                        maxLimicacao = 2;
                    }
                    //incapacidades Fisicas
                    var pontosIncapacidade1 = 0;
                    var pontosIncapacidade2 = 0;
                    var pontosIncapacidade3 = 0;
                    var pontosIncapacidade4 = 0;
                    var pontosIncapacidade5 = 0;
                    if (res.rows.item(i).incapacidade1 == 1) {
                        pontosIncapacidade1 = 4;
                    }
                    if (res.rows.item(i).incapacidade2 == 1) {
                        pontosIncapacidade2 = 4;
                    }
                    if (res.rows.item(i).incapacidade3 == 1) {
                        pontosIncapacidade3 = 4;
                    }
                    if (res.rows.item(i).incapacidade4 == 1) {
                        pontosIncapacidade4 = 4;
                    }
                    if (res.rows.item(i).incapacidade5 == 1) {
                        pontosIncapacidade5 = 4;
                    }
                    var somaIncapacidade = pontosIncapacidade1 + pontosIncapacidade2 + pontosIncapacidade3 + pontosIncapacidade4 + pontosIncapacidade5;
                    var maxIncapacidade = somaIncapacidade;
                    if (somaIncapacidade > 4) {
                        maxIncapacidade = 4;
                    }
                    var pontuacaofinal = maxIncapacidade + maxLimicacao + somaParcial;
                    _this.pontuacaototal = 0;
                    if (pontuacaofinal >= 3) {
                        _this.pontuacaototal = 3;
                    }
                    else {
                        _this.pontuacaototal = pontuacaofinal;
                    }
                    var pontos_final = somaParcial + maxLimicacao + maxIncapacidade;
                    var pontos_total = pontos_final;
                    if (pontos_final > 10) {
                        _this.pontos_total = 10;
                    }
                    _this.ves13s.push({ id_ves13: res.rows.item(i).id_ves13, nome: res.rows.item(i).nome, data_exame: res.rows.item(i).data_exame, pontos_total: pontos_total, pontuacaototal: pontos_total, idade: idade, incapacidade1: res.rows.item(i).incapacidade1 });
                }
            })
                .catch(function (e) { return console.log(e); });
        }).catch(function (e) { return console.log(e); });
    };
    ResultadoVes13Page.prototype.getData2 = function () {
        var _this = this;
        this.sqlite.create({
            name: 'idoso.db',
            location: 'default'
        }).then(function (db) {
            _this.linha = 1;
            db.executeSql('SELECT id_ves13,id_perfil, idade,  percepcao_saude,  limitacao1,  limitacao2, limitacao3, limitacao4, limitacao5, limitacao6,  incapacidade1,incapacidade2, incapacidade3,incapacidade4,incapacidade5, data_exame from teste_ves13 order by id_ves13 DESC limit 1', [])
                .then(function (res) {
                _this.ves13s = [];
                for (var i = 0; i < res.rows.length; i++) {
                    //passa parametros para gerar o gráfico
                    var data_ex = new Date(res.rows.item(i).data_exame);
                    _this.data_final = _this.datepipe.transform(data_ex, 'dd/MM/yyyy HH:mm');
                    //soma parte 1
                    var idade = res.rows.item(i).idade;
                    var somaParcial = null;
                    var pontosIdade = null;
                    var pontosPercepcao = 0;
                    if (idade > 60 && idade < 74) {
                        pontosIdade = 0;
                    }
                    if (idade > 75 && idade < 84) {
                        pontosIdade = 1;
                    }
                    if (idade > 84) {
                        pontosIdade = 3;
                    }
                    if (res.rows.item(i).percepcao_saude >= 4) {
                        pontosPercepcao = 1;
                    }
                    somaParcial = pontosIdade + pontosPercepcao;
                    //limitacoes fisicas
                    var pontosLimitacao1 = 0;
                    var pontosLimitacao2 = 0;
                    var pontosLimitacao3 = 0;
                    var pontosLimitacao4 = 0;
                    var pontosLimitacao5 = 0;
                    var pontosLimitacao6 = 0;
                    if (res.rows.item(i).limitacao1 >= 4) {
                        pontosLimitacao1 = 1;
                    }
                    if (res.rows.item(i).limitacao2 >= 4) {
                        pontosLimitacao2 = 1;
                    }
                    if (res.rows.item(i).limitacao3 >= 4) {
                        pontosLimitacao3 = 1;
                    }
                    if (res.rows.item(i).limitacao4 >= 4) {
                        pontosLimitacao4 = 1;
                    }
                    if (res.rows.item(i).limitacao5 >= 4) {
                        pontosLimitacao5 = 1;
                    }
                    if (res.rows.item(i).limitacao6 >= 4) {
                        pontosLimitacao6 = 1;
                    }
                    var somaLimitacao = pontosLimitacao1 + pontosLimitacao2 + pontosLimitacao3 + pontosLimitacao4 + pontosLimitacao5 + pontosLimitacao6;
                    var maxLimicacao = somaLimitacao;
                    if (somaLimitacao > 2) {
                        maxLimicacao = 2;
                    }
                    //incapacidades Fisicas
                    var pontosIncapacidade1 = 0;
                    var pontosIncapacidade2 = 0;
                    var pontosIncapacidade3 = 0;
                    var pontosIncapacidade4 = 0;
                    var pontosIncapacidade5 = 0;
                    if (res.rows.item(i).incapacidade1 == 1) {
                        pontosIncapacidade1 = 4;
                    }
                    if (res.rows.item(i).incapacidade2 == 1) {
                        pontosIncapacidade2 = 4;
                    }
                    if (res.rows.item(i).incapacidade3 == 1) {
                        pontosIncapacidade3 = 4;
                    }
                    if (res.rows.item(i).incapacidade4 == 1) {
                        pontosIncapacidade4 = 4;
                    }
                    if (res.rows.item(i).incapacidade5 == 1) {
                        pontosIncapacidade5 = 4;
                    }
                    var somaIncapacidade = pontosIncapacidade1 + pontosIncapacidade2 + pontosIncapacidade3 + pontosIncapacidade4 + pontosIncapacidade5;
                    var maxIncapacidade = somaIncapacidade;
                    if (somaIncapacidade > 4) {
                        maxIncapacidade = 4;
                    }
                    var pontuacaofinal = maxIncapacidade + maxLimicacao + somaParcial;
                    _this.pontuacaototal = 0;
                    if (pontuacaofinal >= 3) {
                        _this.pontuacaototal = 3;
                    }
                    else {
                        _this.pontuacaototal = pontuacaofinal;
                    }
                    var pontos_final = somaParcial + maxLimicacao + maxIncapacidade;
                    var pontos_total = pontos_final;
                    if (pontos_final > 10) {
                        _this.pontos_total = 10;
                    }
                    _this.ves13s.push({ id_ves13: res.rows.item(i).id_ves13, nome: res.rows.item(i).nome, data_exame: res.rows.item(i).data_exame, pontos_total: pontos_total, pontuacaototal: pontos_total, idade: idade, incapacidade1: res.rows.item(i).incapacidade1 });
                }
            })
                .catch(function (e) { return console.log(e); });
        }).catch(function (e) { return console.log(e); });
    };
    ResultadoVes13Page.prototype.deleteVes132 = function (id_ves13) {
        var _this = this;
        this.sqlite.create({
            name: 'idoso.db',
            location: 'default'
        }).then(function (db) {
            db.executeSql('DELETE FROM teste_ves13 WHERE id_ves13=?', [id_ves13])
                .then(function (res) {
                console.log(res);
                _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__avaliacao_avaliacao__["a" /* AvaliacaoPage */]);
            })
                .catch(function (e) { return console.log(e); });
        }).catch(function (e) { return console.log(e); });
    };
    ResultadoVes13Page.prototype.deleteVes13 = function (id_ves13) {
        var _this = this;
        var alert = this.alertCtrl.create({
            title: 'Tem certeza que quer excluir o resultado?',
            message: '',
            buttons: [
                {
                    text: 'Não',
                    role: 'cancel',
                    handler: function () {
                    }
                },
                {
                    text: 'Sim',
                    handler: function () {
                        _this.deleteVes132(id_ves13);
                    }
                }
            ]
        });
        alert.present();
    };
    ResultadoVes13Page.prototype.addPerfil2 = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__edit_data_edit_data__["a" /* EditDataPage */], {
            rowid: 1
        });
    };
    ResultadoVes13Page.prototype.getUser = function (rowid) {
        var _this = this;
        this.sqlite.create({
            name: 'idoso.db',
            location: 'default'
        }).then(function (db) {
            db.executeSql('SELECT * FROM perfil WHERE rowid=?', [rowid])
                .then(function (res) {
                if (res.rows.length > 0) {
                    _this.data.rowid = res.rows.item(0).rowid;
                    _this.data.nome = res.rows.item(0).nome;
                    _this.data.sexo = res.rows.item(0).sexo;
                    _this.data.nascimento = res.rows.item(0).nascimento;
                    _this.data.imagem = res.rows.item(0).imagem;
                    _this.photo = res.rows.item(0).imagem;
                }
                if (_this.photo == "") {
                    _this.photo = "assets/imgs/user.png";
                }
            });
        });
    };
    ResultadoVes13Page = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-resultado-ves13',template:/*ion-inline-start:"/home/rabaco/Documentos/projetos_ionic/idoso_aplicativo_final/src/pages/resultado-ves13/resultado-ves13.html"*/'<ion-header>\n\n  <ion-navbar hideBackButton="true">\n    <ion-buttons left>\n  <button (click)="goBack()" ion-button icon-only style="margin-left: 10px">\n    <ion-icon name="md-arrow-back"></ion-icon>\n  </button>\n</ion-buttons>\n    <ion-title>Vulnerabilidade</ion-title>\n    <ion-buttons end>\n      <div class="icone-imagem">\n        <img src="{{ photo }}" class="icone-imagem" (click)="addPerfil2()">\n      </div>\n    </ion-buttons>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n    <div class="titulo-testes">Resultado</div>\n  <ion-list *ngFor="let ves13 of ves13s; let i=index">\n      <div class="texto-data">{{ves13.data_exame | date:"dd/MM/yyyy"}} \n          <button ion-button icon-only  (click)="deleteVes13(ves13.id_ves13)" class="button"  >\n           <ion-icon name="ios-trash" color="danger"></ion-icon>\n           </button>\n           </div>\n\n    <ion-item-sliding>\n      <ion-item style="margin:0; padding:0">\n      \n      <div class="valor-teste"><span *ngIf="ves13.pontos_total<10">0</span>{{ves13.pontos_total}}<span class="cm"> Pontos</span></div>\n      <div class="quebra">\n        <span *ngIf="(ves13.pontuacaototal<3)">\n          <div class="circulo_testes"><img src="assets/imgs/circulo_verde.png"></div><div class="circulo-texto">Acompanhamento<br>de Rotina</div></span>\n          <span *ngIf="(ves13.pontuacaototal>=3)">\n            <div class="circulo_testes"><img src="assets/imgs/circulo_vermelho.png"></div><div class="circulo-texto2">Atenção / Ação</div></span>\n<br>\n      </div>\n\n\n      <div class="quebra" >\n        <ion-card color="fundo-item" nopadding>\n        <ion-card-content>\n          <ion-card-title class="texto-titulo-avaliacao">\n          <strong>Recomendações</strong>\n          </ion-card-title>\n          <span *ngIf="(ves13.pontuacaototal<3)" class="texto-recomendacaoes" text-wrap>Segundo o protocolo VES-13, você não se encontra em situação de vulnerabilidade. Provavelmente você realiza suas atividades do dia a dia com certa facilidade e tem a percepção de apresentar boa saúde quando comparado(a) a pessoas da sua idade. Apenas mantenha o acompanhamento de rotina.</span>\n          <span *ngIf="(ves13.pontuacaototal>=3)" class="texto-recomendacaoes" text-wrap>Segundo o protocolo VES-13, você se encontra em situação de algum grau de vulnerabilidade. Provavelmente você deixa de realizar algumas atividades do dia a dia em função de limitações físicas e/ou psíquicas. Procure o serviço de saúde para verificar sua condição de saúde e adotar providências para recuperar sua qualidade de vida.</span>\n        \n          <div class="referencia" text-wrap>\n            <span class="fontes">Fonte(s):<br></span>\n            Caderneta Virtual de Saúde da Pessoa Idosa<br><br>\n            \n            Caderno de Atenção Básica nº 19 - Envelhecimento e\n            Saúde da Pessoa Idosa<br><br><br>\n            <span class="atencao">Obs: As avaliações e orientações fornecidas nesse aplicativo não substituem a consulta médica.</span>\n           </div>\n\n        </ion-card-content>\n        </ion-card>\n      </div>\n\n\n\n\n      </ion-item>\n    </ion-item-sliding>\n  </ion-list>\n</ion-content>\n'/*ion-inline-end:"/home/rabaco/Documentos/projetos_ionic/idoso_aplicativo_final/src/pages/resultado-ves13/resultado-ves13.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_2__ionic_native_sqlite__["a" /* SQLite */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* ActionSheetController */],
            __WEBPACK_IMPORTED_MODULE_4__angular_common__["d" /* DatePipe */]])
    ], ResultadoVes13Page);
    return ResultadoVes13Page;
}());

//# sourceMappingURL=resultado-ves13.js.map

/***/ }),

/***/ 64:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TabsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__home_home__ = __webpack_require__(45);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__avaliacao_avaliacao__ = __webpack_require__(39);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__sobre_sobre__ = __webpack_require__(130);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__orientacoes2_orientacoes2__ = __webpack_require__(133);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__telefones_telefones__ = __webpack_require__(135);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
//import { Component } from '@angular/core';


//import { AboutPage } from '../about/about';
//import { ContactPage } from '../contact/contact';





var TabsPage = /** @class */ (function () {
    function TabsPage(navParams, navCtrl) {
        this.navParams = navParams;
        this.navCtrl = navCtrl;
        this.teste = 0;
        this.tab1Root = __WEBPACK_IMPORTED_MODULE_2__home_home__["a" /* HomePage */];
        this.tab2Root = __WEBPACK_IMPORTED_MODULE_4__sobre_sobre__["a" /* SobrePage */];
        this.tab3Root = __WEBPACK_IMPORTED_MODULE_5__orientacoes2_orientacoes2__["a" /* Orientacoes2Page */];
        this.tab4Root = __WEBPACK_IMPORTED_MODULE_6__telefones_telefones__["a" /* TelefonesPage */];
        // tab5Root = ContactPage;
        this.tab5Root = __WEBPACK_IMPORTED_MODULE_3__avaliacao_avaliacao__["a" /* AvaliacaoPage */];
        this.tabBarElement = document.querySelector('#Tabs');
        this.selectedTab = this.navParams.get('selectedTab');
    }
    TabsPage.prototype.ionViewDidLoad = function () {
        // console.log("aa" +this.selectedTab);
        if (this.selectedTab) {
            // this.mainTabs.select(this.selectedTab);
            this.navCtrl.parent.select(this.selectedTab);
            //alert(this.selectedTab) ;     
        }
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('mainTabs'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* Tabs */])
    ], TabsPage.prototype, "mainTabs", void 0);
    TabsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({template:/*ion-inline-start:"/home/rabaco/Documentos/projetos_ionic/idoso_aplicativo_final/src/pages/tabs/tabs.html"*/'<ion-tabs color="fundo-menu" #mainTabs id="tabs">    \n  <ion-tab [root]="tab1Root" tabTitle="Início" tabIcon="custom-home"></ion-tab>  \n  <ion-tab [root]="tab5Root" tabTitle="Avaliação" tabIcon="custom-clipboard"></ion-tab>\n  <ion-tab [root]="tab2Root" tabTitle="Direitos" tabIcon="custom-justica"></ion-tab>\n  <ion-tab [root]="tab3Root" tabTitle="Orientações" tabIcon="custom-compasso"></ion-tab>\n  <ion-tab [root]="tab4Root" tabTitle="Telefones" tabIcon="custom-telefone"></ion-tab>\n  <!--<ion-tab [root]="tab5Root" tabTitle="Perfil" tabIcon="contacts"></ion-tab>-->\n\n\n</ion-tabs>\n'/*ion-inline-end:"/home/rabaco/Documentos/projetos_ionic/idoso_aplicativo_final/src/pages/tabs/tabs.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */]])
    ], TabsPage);
    return TabsPage;
}());

//# sourceMappingURL=tabs.js.map

/***/ }),

/***/ 70:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AddDataPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_sqlite__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_toast__ = __webpack_require__(44);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__tabs_tabs__ = __webpack_require__(64);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_camera__ = __webpack_require__(100);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__angular_forms__ = __webpack_require__(15);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







/*import { HomePage } from '../home/home';*/
var AddDataPage = /** @class */ (function () {
    function AddDataPage(navCtrl, navParams, sqlite, toast, camera, formbuilder) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.sqlite = sqlite;
        this.toast = toast;
        this.camera = camera;
        this.formbuilder = formbuilder;
        this.photo = '';
        this.data = { nascimento: "", sexo: "", nome: "", imagem: "", amount: 0 };
        this.formgroup = formbuilder.group({
            //nome: ['',Validators.required,Validators.minLength(5)]
            nome: ['', __WEBPACK_IMPORTED_MODULE_6__angular_forms__["f" /* Validators */].compose([__WEBPACK_IMPORTED_MODULE_6__angular_forms__["f" /* Validators */].maxLength(30), __WEBPACK_IMPORTED_MODULE_6__angular_forms__["f" /* Validators */].pattern('[a-zA-Z ]*'), __WEBPACK_IMPORTED_MODULE_6__angular_forms__["f" /* Validators */].required, __WEBPACK_IMPORTED_MODULE_6__angular_forms__["f" /* Validators */].minLength(5)])],
            sex: ['', __WEBPACK_IMPORTED_MODULE_6__angular_forms__["f" /* Validators */].required],
            nasce: ['', __WEBPACK_IMPORTED_MODULE_6__angular_forms__["f" /* Validators */].required]
        });
        this.nome = this.formgroup.controls['nome'];
        this.nasce = this.formgroup.controls['nasce'];
        this.sex = this.formgroup.controls['sex'];
    }
    /*
        ionViewWillLoad() {
          this.marceloForm = this.formBuilder.group({
            nome: new FormControl('', Validators.compose([
              UsernameValidator.validUsername,
              Validators.maxLength(25),
              Validators.minLength(5),
              Validators.pattern('^(?=.*[a-zA-Z])(?=.*[0-9])[a-zA-Z0-9]+$'),
              Validators.required
            ]))
          });
        }
    */
    AddDataPage.prototype.takePicture = function () {
        var _this = this;
        var options = {
            quality: 80,
            destinationType: this.camera.DestinationType.DATA_URL,
            encodingType: this.camera.EncodingType.JPEG,
            mediaType: this.camera.MediaType.PICTURE,
            allowEdit: true,
            targetWidth: 640,
            targetHeight: 360
        };
        this.camera.getPicture(options)
            .then(function (imageData) {
            var base64image = 'data:image/jpeg;base64,' + imageData;
            _this.photo = base64image;
        }, function (error) {
            console.error(error);
        })
            .catch(function (error) {
            console.error(error);
        });
    };
    AddDataPage.prototype.saveData = function () {
        var _this = this;
        this.sqlite.create({
            name: 'idoso.db',
            location: 'default'
        }).then(function (db) {
            db.executeSql('INSERT INTO perfil VALUES(?,?,?,?,?)', [1, _this.data.nascimento, _this.data.nome, _this.data.sexo, _this.photo])
                .then(function (res) {
                _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__tabs_tabs__["a" /* TabsPage */]);
                console.log(res);
                _this.toast.show('Perfil Salvo', '5000', 'center').subscribe(function (toast) {
                    //this.navCtrl.popToRoot();
                });
            })
                .catch(function (e) {
                console.log(e);
                _this.toast.show(e, '5000', 'center').subscribe(function (toast) {
                    console.log(toast);
                });
            });
        }).catch(function (e) {
            console.log(e);
            _this.toast.show(e, '5000', 'center').subscribe(function (toast) {
                console.log(toast);
            });
        });
    };
    AddDataPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-add-data',template:/*ion-inline-start:"/home/rabaco/Documentos/projetos_ionic/idoso_aplicativo_final/src/pages/add-data/add-data.html"*/'<ion-header>\n\n  <ion-navbar>\n    <ion-title>Dados pessoais</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n<ion-content padding class="ion-item">\n\n    <ion-item no-lines>\n        <button ion-item large  (click)="takePicture()" *ngIf="photo==0" outline color="branco" float-left><img src="assets/imgs/user.png" class="icone"></button>\n        <img class="circle-pic" src="{{ photo }}" *ngIf="photo!=0" float-left/>\n    </ion-item>\n    <form [formGroup]="formgroup" >\n    <ion-item>\n      <ion-label>Nome</ion-label>\n      <ion-input type="text" placeholder="Nome do idoso" [(ngModel)]="data.nome" formControlName="nome"  ></ion-input>\n    </ion-item>\n    <ion-item class="error" *ngIf="nome.hasError(\'required\') && nome.touched">\n        <p> *Nome é um campo obrigado de ser preenchido</p>\n      </ion-item>\n      <ion-item class="error" *ngIf="nome.hasError(\'minlength\') && nome.touched">\n          <p> Seu nome precisa ter mais que 4 caracteres</p>\n      </ion-item>\n      <ion-item class="error" *ngIf="nome.hasError(\'maxlength\') && nome.touched">\n          <p>Máximo 30 caracteres</p>\n      </ion-item>\n    <ion-item>\n      <ion-label >Data Nascimento</ion-label>\n      <ion-datetime displayFormat="DD/MM/YYYY" [(ngModel)]="data.nascimento" min="1880" max="1970" formControlName="nasce"></ion-datetime>\n    </ion-item>\n    <!--\n    <ion-item>\n      <ion-label >Sexo</ion-label>\n      <ion-select [(ngModel)]="data.sexo" formControlName="sex"  >\n        <ion-option value="Masculino">Masculino</ion-option>\n        <ion-option value="Feminino">Feminino</ion-option>\n      </ion-select>\n    </ion-item>\n    -->\n\n    <button ion-button type="submit" block color="botao" [disabled]="formgroup.invalid" (click)="saveData()">Confirmar</button>\n  </form>\n</ion-content>\n'/*ion-inline-end:"/home/rabaco/Documentos/projetos_ionic/idoso_aplicativo_final/src/pages/add-data/add-data.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__ionic_native_sqlite__["a" /* SQLite */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_native_toast__["a" /* Toast */],
            __WEBPACK_IMPORTED_MODULE_5__ionic_native_camera__["a" /* Camera */],
            __WEBPACK_IMPORTED_MODULE_6__angular_forms__["a" /* FormBuilder */]])
    ], AddDataPage);
    return AddDataPage;
}());

//# sourceMappingURL=add-data.js.map

/***/ }),

/***/ 71:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DireitosPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_direitos_direitos__ = __webpack_require__(101);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__direitos_interno_direitos_interno__ = __webpack_require__(131);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var DireitosPage = /** @class */ (function () {
    function DireitosPage(navCtrl, direitos) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.direitos = direitos;
        this.politicas = 0;
        this.direitos.loadAll().then(function (result) {
            _this.politicas = result;
        });
    }
    DireitosPage.prototype.detailsPage = function (id) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__direitos_interno_direitos_interno__["a" /* DireitosInternoPage */], { code: id });
    };
    ;
    DireitosPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-direitos',template:/*ion-inline-start:"/home/rabaco/Documentos/projetos_ionic/idoso_aplicativo_final/src/pages/direitos/direitos.html"*/'<ion-header>\n\n  <ion-navbar>\n    <ion-title>Direitos do Idoso</ion-title>\n  </ion-navbar>\n\n</ion-header>\n<ion-content padding >\n\n<ion-spinner name="circles" *ngIf="politicas == 0">\n  </ion-spinner>\n  <div class="texto-inicial" padding>A legislação brasileira assegura determinados direitos para a população de 60 anos ou mais de idade. Para comprovar a idade basta apresentar um documento que contenha sua foto, como a Carteira de Identidade ou Carteira de Habilitação. \n  </div>\n  <ion-list *ngIf="politicas != 0" lines>\n    <ion-item *ngFor="let Politicas of politicas" (click)="detailsPage(Politicas.code);" detail-push class="titulo-politica" text-wrap >\n      {{Politicas.nome}}\n      <div class="lei" [innerHTML]="Politicas.lei"></div>\n\n    </ion-item>\n  </ion-list>\n \n  </ion-content>\n'/*ion-inline-end:"/home/rabaco/Documentos/projetos_ionic/idoso_aplicativo_final/src/pages/direitos/direitos.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */], __WEBPACK_IMPORTED_MODULE_2__providers_direitos_direitos__["a" /* DireitosProvider */]])
    ], DireitosPage);
    return DireitosPage;
}());

//# sourceMappingURL=direitos.js.map

/***/ })

},[364]);
//# sourceMappingURL=main.js.map