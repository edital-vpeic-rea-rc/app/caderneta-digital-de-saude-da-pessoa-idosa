import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ActionSheetController, AlertController } from 'ionic-angular';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';
import { AvaliacaoPage } from '../avaliacao/avaliacao';
import { DatePipe } from '@angular/common'
//import { ThrowStmt } from '@angular/compiler';
import { EditDataPage } from '../edit-data/edit-data';




@IonicPage()
@Component({
  selector: 'page-resultado-ves13',
  templateUrl: 'resultado-ves13.html',
})
export class ResultadoVes13Page {
  data = { rowid:1, nascimento:"", sexo:"", nome:"", imagem:""};
  photo: string = "assets/imgs/user.png";
  ves13s: any = [];
  voltar: number=2;
  linha: number=0;
  data_final: any;
  pontuacaototal: number=0;
  pontos_total: number=0;

  constructor(public navCtrl: NavController,
    private sqlite: SQLite,
    public navParams: NavParams,
    public alertCtrl: AlertController,
    public actionSheetCtrl: ActionSheetController,
    public datepipe: DatePipe) {

      if (navParams.get("id_ves13"))
    {
      this.getData(navParams.get("id_ves13"));
      this.voltar=2;
    }
    else
    {
    this.getData2();
    this.voltar=3;
    }
     }
 
     ionViewDidLoad() {      
      this.getUser(1);
  }

     goBack()
     {
       this.navCtrl.popTo(this.navCtrl.getByIndex(this.navCtrl.length()-this.voltar));
       //console.log("aa" +this.voltar);
     }


  getData(id_ves13) {


       this.sqlite.create({

         name: 'idoso.db',
         location: 'default'
       }).then((db: SQLiteObject) => {

        this.linha=1;

          db.executeSql('SELECT id_ves13, id_perfil, idade,  percepcao_saude,  limitacao1,  limitacao2, limitacao3, limitacao4, limitacao5, limitacao6,  incapacidade1,incapacidade2, incapacidade3,incapacidade4,incapacidade5, data_exame from teste_ves13 where id_ves13=?', [id_ves13])
          .then(res => {
           this.ves13s = [];

             for(var i=0; i<res.rows.length; i++) {

            //passa parametros para gerar o gráfico
          
            var data_ex=new Date(res.rows.item(i).data_exame);
            this.data_final =this.datepipe.transform(data_ex, 'dd/MM/yyyy HH:mm');

     
             //soma parte 1
              let idade=res.rows.item(i).idade;
               let somaParcial= null;
               let pontosIdade= null;
               let pontosPercepcao= 0;
             if (idade>60 && idade<74)
             {
              pontosIdade=0;
             }
             if (idade>75 && idade<84)
             {
               pontosIdade=1;
             }
             if (idade>84)
             {
              pontosIdade=3;
             }

             if (res.rows.item(i).percepcao_saude>=4)
             {
               pontosPercepcao=1;
             }

             somaParcial=pontosIdade + pontosPercepcao;


             //limitacoes fisicas

             let pontosLimitacao1=0;
             let pontosLimitacao2=0;
             let pontosLimitacao3=0;
             let pontosLimitacao4=0;
             let pontosLimitacao5=0;
             let pontosLimitacao6=0;

             if (res.rows.item(i).limitacao1>=4)
             {
               pontosLimitacao1=1;
             }

             if (res.rows.item(i).limitacao2>=4)
             {
               pontosLimitacao2=1;
             }

             if (res.rows.item(i).limitacao3>=4)
             {
               pontosLimitacao3=1;
             }

             if (res.rows.item(i).limitacao4>=4)
             {
               pontosLimitacao4=1;
             }
             if (res.rows.item(i).limitacao5>=4)
             {
               pontosLimitacao5=1;
             }
             if (res.rows.item(i).limitacao6>=4)
             {
               pontosLimitacao6=1;
             }

             let somaLimitacao=pontosLimitacao1+pontosLimitacao2+pontosLimitacao3+pontosLimitacao4+pontosLimitacao5+pontosLimitacao6;
             let maxLimicacao=somaLimitacao;
             if (somaLimitacao>2) {
               maxLimicacao=2;
             }

             //incapacidades Fisicas

             let pontosIncapacidade1=0;
             let pontosIncapacidade2=0;
             let pontosIncapacidade3=0;
             let pontosIncapacidade4=0;
             let pontosIncapacidade5=0;

             if (res.rows.item(i).incapacidade1==1)
             {
               pontosIncapacidade1=4;
             }
             if (res.rows.item(i).incapacidade2==1)
             {
               pontosIncapacidade2=4;
             }
             if (res.rows.item(i).incapacidade3==1)
             {
               pontosIncapacidade3=4;
             }
             if (res.rows.item(i).incapacidade4==1)
             {
               pontosIncapacidade4=4;
             }
             if (res.rows.item(i).incapacidade5==1)
             {
               pontosIncapacidade5=4;
             }

             let somaIncapacidade=pontosIncapacidade1+pontosIncapacidade2+pontosIncapacidade3+pontosIncapacidade4+pontosIncapacidade5;

             let maxIncapacidade=somaIncapacidade;
             if (somaIncapacidade>4) {
               maxIncapacidade=4;
             }

             let pontuacaofinal=maxIncapacidade+maxLimicacao+somaParcial;
             this.pontuacaototal=0;
            
             if (pontuacaofinal>=3)
             {
            
              this.pontuacaototal=3;
             }
             else
             {
        
             this.pontuacaototal=pontuacaofinal;
             }
           let pontos_final=somaParcial+maxLimicacao+maxIncapacidade;
           var pontos_total=pontos_final;
           if (pontos_final>10) {
           this.pontos_total=10;
           }
             
            this.ves13s.push({id_ves13:res.rows.item(i).id_ves13,nome:res.rows.item(i).nome,data_exame:res.rows.item(i).data_exame,pontos_total:pontos_total,pontuacaototal:pontos_total,idade:idade,incapacidade1:res.rows.item(i).incapacidade1})

           }
            })
         .catch(e => console.log(e));

       }

      ).catch(e => console.log(e));

     }



       getData2() {


            this.sqlite.create({

              name: 'idoso.db',
              location: 'default'
            }).then((db: SQLiteObject) => {

             this.linha=1;

               db.executeSql('SELECT id_ves13,id_perfil, idade,  percepcao_saude,  limitacao1,  limitacao2, limitacao3, limitacao4, limitacao5, limitacao6,  incapacidade1,incapacidade2, incapacidade3,incapacidade4,incapacidade5, data_exame from teste_ves13 order by id_ves13 DESC limit 1', [])
              .then(res => {
                this.ves13s = [];

                  for(var i=0; i<res.rows.length; i++) {

                 //passa parametros para gerar o gráfico
                

                 var data_ex=new Date(res.rows.item(i).data_exame);
                 this.data_final =this.datepipe.transform(data_ex, 'dd/MM/yyyy HH:mm');
        
                  //soma parte 1
                   let idade=res.rows.item(i).idade;
                    let somaParcial= null;
                    let pontosIdade= null;
                    let pontosPercepcao= 0;
                  if (idade>60 && idade<74)
                  {
                   pontosIdade=0;
                  }
                  if (idade>75 && idade<84)
                  {
                    pontosIdade=1;
                  }
                  if (idade>84)
                  {
                   pontosIdade=3;
                  }

                  if (res.rows.item(i).percepcao_saude>=4)
                  {
                    pontosPercepcao=1;
                  }

                  somaParcial=pontosIdade + pontosPercepcao;


                  //limitacoes fisicas

                  let pontosLimitacao1=0;
                  let pontosLimitacao2=0;
                  let pontosLimitacao3=0;
                  let pontosLimitacao4=0;
                  let pontosLimitacao5=0;
                  let pontosLimitacao6=0;

                  if (res.rows.item(i).limitacao1>=4)
                  {
                    pontosLimitacao1=1;
                  }

                  if (res.rows.item(i).limitacao2>=4)
                  {
                    pontosLimitacao2=1;
                  }

                  if (res.rows.item(i).limitacao3>=4)
                  {
                    pontosLimitacao3=1;
                  }

                  if (res.rows.item(i).limitacao4>=4)
                  {
                    pontosLimitacao4=1;
                  }
                  if (res.rows.item(i).limitacao5>=4)
                  {
                    pontosLimitacao5=1;
                  }
                  if (res.rows.item(i).limitacao6>=4)
                  {
                    pontosLimitacao6=1;
                  }

                  let somaLimitacao=pontosLimitacao1+pontosLimitacao2+pontosLimitacao3+pontosLimitacao4+pontosLimitacao5+pontosLimitacao6;
                  let maxLimicacao=somaLimitacao;
                  if (somaLimitacao>2) {
                    maxLimicacao=2;
                  }

                  //incapacidades Fisicas

                  let pontosIncapacidade1=0;
                  let pontosIncapacidade2=0;
                  let pontosIncapacidade3=0;
                  let pontosIncapacidade4=0;
                  let pontosIncapacidade5=0;

                  if (res.rows.item(i).incapacidade1==1)
                  {
                    pontosIncapacidade1=4;
                  }
                  if (res.rows.item(i).incapacidade2==1)
                  {
                    pontosIncapacidade2=4;
                  }
                  if (res.rows.item(i).incapacidade3==1)
                  {
                    pontosIncapacidade3=4;
                  }
                  if (res.rows.item(i).incapacidade4==1)
                  {
                    pontosIncapacidade4=4;
                  }
                  if (res.rows.item(i).incapacidade5==1)
                  {
                    pontosIncapacidade5=4;
                  }

                  let somaIncapacidade=pontosIncapacidade1+pontosIncapacidade2+pontosIncapacidade3+pontosIncapacidade4+pontosIncapacidade5;

                  let maxIncapacidade=somaIncapacidade;
                  if (somaIncapacidade>4) {
                    maxIncapacidade=4;
                  }

                  let pontuacaofinal=maxIncapacidade+maxLimicacao+somaParcial;
                  this.pontuacaototal=0;
                 
                  if (pontuacaofinal>=3)
                  {
                 
                  this.pontuacaototal=3;
                  }
                  else
                  {
                 
                  this.pontuacaototal=pontuacaofinal;
                  }
                let pontos_final=somaParcial+maxLimicacao+maxIncapacidade;
                var pontos_total=pontos_final;
                if (pontos_final>10) {
                this.pontos_total=10;
                }
                
                 this.ves13s.push({id_ves13:res.rows.item(i).id_ves13,nome:res.rows.item(i).nome,data_exame:res.rows.item(i).data_exame,pontos_total:pontos_total,pontuacaototal:pontos_total,idade:idade,incapacidade1:res.rows.item(i).incapacidade1})

                }
                 })
              .catch(e => console.log(e));

            }

           ).catch(e => console.log(e));

          }


     deleteVes132(id_ves13) {
       this.sqlite.create({
         name: 'idoso.db',
         location: 'default'
       }).then((db: SQLiteObject) => {
         db.executeSql('DELETE FROM teste_ves13 WHERE id_ves13=?', [id_ves13])
         .then(res => {
           console.log(res);
           this.navCtrl.push(AvaliacaoPage)
         })
         .catch(e => console.log(e));
       }).catch(e => console.log(e));
     }

     
     deleteVes13(id_ves13) {
      let alert = this.alertCtrl.create({
        title: 'Tem certeza que quer excluir o resultado?',
        message: '',
        buttons: [
          {
            text: 'Não',
            role: 'cancel',
            handler: () => {
            
            }
          },
          {
            text: 'Sim',
            handler: () => {
           
             this.deleteVes132(id_ves13);
            }
          }
        ]
      });
      alert.present();
    }

    addPerfil2() {
      this.navCtrl.push(EditDataPage, {
        rowid:1
      });
    }
    
     getUser(rowid) {
      this.sqlite.create({
        name: 'idoso.db',
        location: 'default'
      }).then((db: SQLiteObject) => {
        db.executeSql('SELECT * FROM perfil WHERE rowid=?', [rowid])
          .then(res => {
            if(res.rows.length > 0) {
              this.data.rowid = res.rows.item(0).rowid;
              this.data.nome = res.rows.item(0).nome;
              this.data.sexo = res.rows.item(0).sexo;
              this.data.nascimento = res.rows.item(0).nascimento;
              this.data.imagem = res.rows.item(0).imagem;
              this.photo=res.rows.item(0).imagem;
            }
            if (this.photo=="") {this.photo="assets/imgs/user.png"}
          })
          
        })
      }
    }









