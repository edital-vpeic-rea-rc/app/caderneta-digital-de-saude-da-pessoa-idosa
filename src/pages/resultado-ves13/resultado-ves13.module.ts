import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ResultadoVes13Page } from './resultado-ves13';

@NgModule({
  declarations: [
    ResultadoVes13Page,
  ],
  imports: [
    IonicPageModule.forChild(ResultadoVes13Page),
  ],
})
export class ResultadoVes13PageModule {}
