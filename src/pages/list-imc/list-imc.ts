import { Component } from '@angular/core';
import { IonicPage, NavController, ActionSheetController, AlertController } from 'ionic-angular';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';
//import { AddImcPage } from '../add-imc/add-imc';
import { ResultadoImcPage } from '../resultado-imc/resultado-imc';
import { DatePipe } from '@angular/common';
import { EditDataPage } from '../edit-data/edit-data';

@IonicPage()
@Component({
  selector: 'page-list-imc',
  templateUrl: 'list-imc.html',
})
export class ListImcPage {
  data = { rowid:1, nascimento:"", sexo:"", nome:"", imagem:""};
  photo: string = "assets/imgs/user.png";
  imcs: any = [];
  imcs2: any = [];
  nome_teste:string ="";
  nome_teste2:string ="";
  data_final: any;


  constructor(public navCtrl: NavController,
    private sqlite: SQLite,
    public alertCtrl: AlertController,
    public actionSheetCtrl: ActionSheetController,
    public datepipe: DatePipe) {  }

    ionViewDidLoad() {
     this.getData();
     this.getData2();
     this.getUser(1);
 }


  getData() {


       this.sqlite.create({

         name: 'idoso.db',
         location: 'default'
       }).then((db: SQLiteObject) => {

        let linha=2;

          db.executeSql('SELECT * from (select * FROM teste_imc  ORDER BY data_exame DESC limit 6)  ORDER BY data_exame ASC ', [])
         .then(res => {
           this.imcs = [];

             for(var i=0; i<res.rows.length; i++) {

            //passa parametros para gerar o gráfico
       

            var data_ex=new Date(res.rows.item(i).data_exame);
            this.data_final =this.datepipe.transform(data_ex, 'dd/MM/yyyy HH:mm');

             var nome_teste = res.rows.item(i).nome;


          if (nome_teste !== nome_teste2) {
              var nome_teste2 = res.rows.item(i).nome;
               linha=linha+1;
       
             }

             let finalBmi = res.rows.item(i).peso / (res.rows.item(i).altura * res.rows.item(i).altura );
             var calculo_imc=parseFloat(finalBmi.toFixed(1));
             var calculo_string = calculo_imc.toString().replace(/[.]/gi,",");         
             
             if(!calculo_string.match(/,/)) { calculo_string=calculo_imc.toString()+",0"; }
             var myDateString = new Date(res.rows.item(i).nascimento);
             var milisecondsDiff = Date.now() - (myDateString.getTime());
             let idade=Math.floor(( milisecondsDiff )/(1000 * 3600 * 24)/365);
             this.imcs.push({id_imc:res.rows.item(i).id_imc,nome:res.rows.item(i).nome,data_exame:res.rows.item(i).data_exame,peso:res.rows.item(i).peso,altura:res.rows.item(i).altura,idade:idade,calculo_imc:calculo_imc,calculo_string:calculo_string})

           }



         })
         .catch(e => console.log(e));

       }

      ).catch(e => console.log(e));

     }

     getData2() {


      this.sqlite.create({

        name: 'idoso.db',
        location: 'default'
      }).then((db: SQLiteObject) => {

       let linha=2;

         db.executeSql('SELECT * from (select * FROM teste_imc  ORDER BY data_exame ASC) ORDER BY data_exame DESC ', [])
        .then(res => {
          this.imcs2 = [];

            for(var i=0; i<res.rows.length; i++) {

           //passa parametros para gerar o gráfico
           //var data_ex=new Date(res.rows.item(i).data_exame);
           //var data_final=data_ex.toDateString();

           var data_ex=new Date(res.rows.item(i).data_exame);
           this.data_final =this.datepipe.transform(data_ex, 'dd/MM/yyyy HH:mm');

            var nome_teste = res.rows.item(i).nome;



/*
                    this.lineChartData[0].data.push("21.9");
                    this.lineChartData[1].data.push("50");
                    this.lineChartData[2].data.push("26.9");
*/
           if (nome_teste !== nome_teste2) {
             var nome_teste2 = res.rows.item(i).nome;
              linha=linha+1;
        //   this.lineChartData[linha].label.push(res.rows.item(i).nome);
            }

            let finalBmi = res.rows.item(i).peso / (res.rows.item(i).altura * res.rows.item(i).altura );
            var calculo_imc=parseFloat(finalBmi.toFixed(1));
            var calculo_string = calculo_imc.toString().replace(/[.]/gi,","); 
         //   this.lineChartLabels.push(data_final);
         //   this.lineChartData[linha].data.push(calculo_imc);


            var myDateString = new Date(res.rows.item(i).nascimento);
            var milisecondsDiff = Date.now() - (myDateString.getTime());
            let idade=Math.floor(( milisecondsDiff )/(1000 * 3600 * 24)/365);
            //let idade=milisecondsDiff;
           this.imcs2.push({id_imc:res.rows.item(i).id_imc,nome:res.rows.item(i).nome,data_exame:res.rows.item(i).data_exame,peso:res.rows.item(i).peso,altura:res.rows.item(i).altura,idade:idade,calculo_imc:calculo_imc,calculo_string:calculo_string})

          }



        })
        .catch(e => console.log(e));

      }

     ).catch(e => console.log(e));

    }



     editImc(id_imc) {
       this.navCtrl.push(ResultadoImcPage, {
         id_imc:id_imc
       });
     }

     deleteImc2(id_imc) {
      this.sqlite.create({
        name: 'idoso.db',
        location: 'default'
      }).then((db: SQLiteObject) => {
        db.executeSql('DELETE FROM teste_imc WHERE id_imc=?', [id_imc])
        .then(res => {
          console.log(res);         
          this.navCtrl.push(ListImcPage)
        })
        .catch(e => console.log(e));
      }).catch(e => console.log(e));
    }

    deleteImc(id_imc) {
     let alert = this.alertCtrl.create({
       title: 'Tem certeza que quer excluir o resultado?',
       message: '',
       buttons: [
         {
           text: 'Não',
           role: 'cancel',
           handler: () => {
   
           }
         },
         {
           text: 'Sim',
           handler: () => {
           
            this.deleteImc2(id_imc);
           }
         }
       ]
     });
     alert.present();
   }

   addPerfil2() {
    this.navCtrl.push(EditDataPage, {
      rowid:1
    });
  }

   getUser(rowid) {
    this.sqlite.create({
      name: 'idoso.db',
      location: 'default'
    }).then((db: SQLiteObject) => {
      db.executeSql('SELECT * FROM perfil WHERE rowid=?', [rowid])
        .then(res => {
          if(res.rows.length > 0) {
            this.data.rowid = res.rows.item(0).rowid;
            this.data.nome = res.rows.item(0).nome;
            this.data.sexo = res.rows.item(0).sexo;
            this.data.nascimento = res.rows.item(0).nascimento;
            this.data.imagem = res.rows.item(0).imagem;
            this.photo=res.rows.item(0).imagem;
          }
          if (this.photo=="") {this.photo="assets/imgs/user.png"}
        })
        
      })
    }

}
