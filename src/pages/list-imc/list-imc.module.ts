import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ListImcPage } from './list-imc';

@NgModule({
  declarations: [
    ListImcPage,
  ],
  imports: [
    IonicPageModule.forChild(ListImcPage),
  ],
})
export class ListImcPageModule {}
