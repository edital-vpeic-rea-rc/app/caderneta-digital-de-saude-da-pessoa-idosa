import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Orientacoes2Page } from './orientacoes2';

@NgModule({
  declarations: [
    Orientacoes2Page,
  ],
  imports: [
    IonicPageModule.forChild(Orientacoes2Page),
  ],
})
export class Orientacoes2PageModule {}
