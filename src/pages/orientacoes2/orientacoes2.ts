import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { HomePage } from '../home/home';
import { Orientacoes2SecundariaPage } from '../orientacoes2-secundaria/orientacoes2-secundaria';
/**
 * Generated class for the Orientacoes2Page page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-orientacoes2',
  templateUrl: 'orientacoes2.html',
})
export class Orientacoes2Page {
  public technologies : Array<any>;
  teste : number=0;
  resultado: string="";
  subtexto: string="";
  categoria: string="";
  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
   // console.log('ionViewDidLoad Orientacoes2Page');
  }


  declareTechnologies() : void
  {
      this.technologies = [
        {taxonomia: 'medicamentos,remedio', secao:'Medicamentos', etapa:'1', descricao: 'Mantenha os medicamentos em lugares secos e frescos, seguros e específicos para este fim, fora do alcance de crianças e animais. Evite guardá-los com produtos de limpeza, perfumaria e alimentos. ',  fontes:''},
        {taxonomia: 'medicamentos,remedio', secao:'Medicamentos',etapa:'2', descricao: 'Guarde na geladeira apenas os medicamentos líquidos, conforme orientação de um profissional de saúde. Não guarde medicamentos na porta da geladeira ou próximo do congelador. A insulina, por exemplo, perde o efeito se for congelada. ',  fontes:''},
        {taxonomia: 'medicamentos,remedio', secao:'Medicamentos',etapa:'3', descricao: 'Se você utiliza porta-comprimidos, deixe somente a quantidade suficiente para 24 horas nos recipientes, que devem ser mantidos cuidadosamente limpos e secos.',  fontes:''},
        {taxonomia: 'medicamentos,remedio', secao:'Medicamentos',etapa:'4', descricao: 'O armazenamento de medicamentos deve ser individualizado para evitar erros e trocas com medicamentos de outras pessoas.',  fontes:''},
        {taxonomia: 'medicamentos,remedio,higiene', secao:'Medicamentos',etapa:'5', descricao: 'Lave as mãos antes de manusear qualquer medicamento.',  fontes:''},
        {taxonomia: 'medicamentos,remedio', secao:'Medicamentos',etapa:'6', descricao: 'Manuseie os medicamentos em lugares claros. Leia sempre o nome para evitar trocas.',  fontes:''},
        {taxonomia: 'medicamentos,remedio', secao:'Medicamentos',etapa:'7', descricao: 'É importante o uso regular dos medicamentos, observando-se os horários prescritos.',  fontes:''},
        {taxonomia: 'medicamentos,remedio,manuseio', secao:'Medicamentos',etapa:'8', descricao: 'Tome os comprimidos e as cápsulas sempre com água ou conforme a orientação de um profissional de saúde.',  fontes:''},
        {taxonomia: 'medicamentos,remedio,manuseio', secao:'Medicamentos',etapa:'9', descricao: 'Consulte seu médico ou farmacêutico caso seja necessário partir ou triturar os comprimidos.',  fontes:''},
        {taxonomia: 'medicamentos,remedio,manuseio', secao:'Medicamentos',etapa:'10', descricao: 'Abra somente um frasco ou embalagem de cada medicamento por vez.',  fontes:''},
        {taxonomia: 'medicamentos,remedio,manuseio', secao:'Medicamentos',etapa:'11', descricao: 'Mantenha os medicamentos nas embalagens originais para facilitar sua identificação e o controle da validade.',  fontes:''},
        {taxonomia: 'medicamentos,remedio,manuseio', secao:'Medicamentos',etapa:'12', descricao: 'Observe frequentemente a data de validade e não tome medicamentos vencidos.',  fontes:''},
        {taxonomia: 'medicamentos,remedio,manuseio,precaucao,precaução,precaucoes,precauções,recomendacao,recomendacoes,recomendações,Recomendação', secao:'Medicamentos',etapa:'13', descricao: 'Consulte seu médico ou farmacêutico caso observe qualquer mudança no medicamento: cor, mancha ou cheiro estranho.',  fontes:''},
        {taxonomia: 'medicamentos,remedio,manuseio', secao:'Medicamentos',etapa:'14', descricao: 'Utilize, preferencialmente, o medidor que acompanha o medicamento e lave-o após o uso. Evite colheres caseiras',  fontes:''},
        {taxonomia: 'medicamentos,remedio,manuseio,precaucao,precaução,precaucoes,precauções,recomendacao,recomendacoes,recomendações,Recomendação', secao:'Medicamentos',etapa:'15', descricao: 'Não passe o bico do tubo nas feridas ou na pele quando for utilizar pomadas. Você pode contaminar o medicamento. ',  fontes:''},
        {taxonomia: 'medicamentos,remedio,manuseio,precaucao,precaução,precaucoes,precauções,recomendacao,recomendacoes,recomendações,Recomendação', secao:'Medicamentos',etapa:'16', descricao: 'Não encoste os bicos de frascos de colírio e de tubos de pomadas oftamológicas nos olhos ou na pele.',  fontes:''},
        {taxonomia: 'medicamentos,remedio,manuseio,precaucao,precaução,precaucoes,precauções,recomendacao,recomendacoes,recomendações,Recomendação', secao:'Medicamentos',etapa:'17', descricao: 'Sempre leve as receitas, os exames e os medicamentos em uso nos atendimentos médicos. Informe o médico sobre o uso de chás ou plantas medicinais.',  fontes:''},
        {taxonomia: 'medicamentos,remedio,manuseio,precaucao,precaução,precaucoes,precauções,recomendacao,recomendacoes,recomendações,Recomendação', secao:'Medicamentos',etapa:'18', descricao: 'Mantenha a receita médica junto dos medicamentos.',  fontes:''},
        {taxonomia: 'medicamentos,remedio,manuseio,precaucao,precaução,precaucoes,precauções,recomendacao,recomendacoes,recomendações,Recomendação', secao:'Medicamentos',etapa:'19', descricao: 'Nunca espere o medicamento acabar para providenciar nova receita, para comprá-lo ou buscá-lo na unidade de saúde.',  fontes:''},
        {taxonomia: 'medicamentos,remedio,manuseio,precaucao,precaução,precaucoes,precauções,recomendacao,recomendacoes,recomendações,Recomendação', secao:'Medicamentos',etapa:'20', descricao: 'Os medicamentos que não estão sendo utilizados devem ser guardados em local separado dos que estão em uso.',  fontes:''},
        
        {taxonomia: 'alimentação,alimentacao,recomendacao,recomendacoes,recomendações,Recomendação',secao:'Alimentação',etapa:'1° passo', descricao: 'Faça três refeições ao dia (café da manhã, almoço e jantar) e, caso necessite, faça outras refeições nos intervalos.',  fontes:''},
        {taxonomia: 'alimentação,alimentacao,recomendacao,recomendacoes,recomendações,Recomendação', secao:'Alimentação',etapa:'2° passo', descricao: 'Dê preferência aos grãos integrais e aos alimentos em sua forma mais natural. Inclua alimentos como arroz, milho, batata, mandioca / macaxeira / aipim nas principais refeições. ',  fontes:''},        
        {taxonomia: 'alimentação,alimentacao,recomendacao,recomendacoes,recomendações,Recomendação', secao:'Alimentação',etapa:'3° passo', descricao: 'Inclua frutas, legumes e verduras em todas as refeições ao longo do dia.',  fontes:''},        
        {taxonomia: 'alimentação,alimentacao,recomendacao,recomendacoes,recomendações,Recomendação', secao:'Alimentação',etapa:'4° passo', descricao: 'Coma feijão com arroz, de preferência no almoço ou jantar.',  fontes:''},        
        {taxonomia: 'alimentação,alimentacao,recomendacao,recomendacoes,recomendações,Recomendação', secao:'Alimentação',etapa:'5° passo', descricao: 'Lembre-se de incluir carnes, aves, peixes ou ovos e leite e derivados em pelo menos uma refeição durante o dia.',  fontes:''},                    
        {taxonomia: 'alimentação,alimentacao,recomendacao,recomendacoes,recomendações,Recomendação', secao:'Alimentação',etapa:'6° passo', descricao: 'Use pouca quantidade de óleos, gorduras, açúcar e sal no preparo dos alimentos.',  fontes:''},                    
        {taxonomia: 'alimentação,alimentacao,recomendacao,recomendacoes,recomendações,Recomendação', secao:'Alimentação',etapa:'7° passo', descricao: 'Beba água mesmo sem sentir sede, de preferência nos intervalos entre as refeições.',  fontes:''},             
        {taxonomia: 'alimentação,alimentacao,recomendacao,recomendacoes,recomendações,Recomendação', secao:'Alimentação',etapa:'8° passo', descricao: 'Evite bebidas açucaradas (refrigerantes, sucos e chás industrializados), bolos e biscoitos recheados, doces e outras guloseimas como regra da alimentação.',  fontes:''},             
        {taxonomia: 'alimentação,alimentacao,recomendacao,recomendacoes,recomendações,Recomendação', secao:'Alimentação',etapa:'9° passo', descricao: 'Fique atento(a) às informações nutricionais disponíveis nos rótulos dos produtos processados e ultraprocessados para favorecer a escolha de produtos alimentícios mais saudáveis.',  fontes:''},             
        {taxonomia: 'alimentação,alimentacao,recomendacao,recomendacoes,recomendações,Recomendação', secao:'Alimentação',etapa:'10° passo', descricao: 'Sempre que possível, coma em companhia.',  fontes:''},             
        
        {taxonomia: 'saúde bucal,saude bucal,recomendacao,recomendacoes,recomendações,Recomendação', secao:'Saúde bucal',etapa:'1',  descricao: 'Escove bem os dentes após cada refeição e antes de dormir.',  fontes:''},             
        {taxonomia: 'saúde bucal,saude bucal,recomendacao,recomendacoes,recomendações,Recomendação', secao:'Saúde bucal',etapa:'2',  descricao: 'Use escova de tamanho adequado com cerdas macias e creme dental com flúor.',  fontes:''},  
        {taxonomia: 'saúde bucal,saude bucal,recomendacao,recomendacoes,recomendações,Recomendação', secao:'Saúde bucal',etapa:'3',  descricao: 'Passe fio dental entre todos os dentes',  fontes:''},  
        {taxonomia: 'saúde bucal,saude bucal,recomendacao,recomendacoes,recomendações,Recomendação', secao:'Saúde bucal',etapa:'4',  descricao: 'Escove a língua',  fontes:''},  
        {taxonomia: 'prótese dental,recomendacao,recomendacoes,recomendações,Recomendação', secao:'Prótese dental',etapa:'1',  descricao: 'Limpe-a fora da boca, com sabão ou pasta de dente pouco abrasiva e escova de dentes macia, separada para esta função',  fontes:''},       
        {taxonomia: 'prótese dental,recomendacao,recomendacoes,recomendações,Recomendação', secao:'Prótese dental',etapa:'2',  descricao: 'Antes de recolocá-la na boca, escove os dentes e/ou limpe a gengiva, o céu da boca e a língua',  fontes:''},       
        {taxonomia: 'prótese dental,recomendacao,recomendacoes,recomendações,Recomendação', secao:'Prótese dental',etapa:'3',  descricao: 'Fique sem a prótese por algumas horas por dia. O ideal é passar a noite sem ela',  fontes:''},       
        {taxonomia: 'prótese dental,recomendacao,recomendacoes,recomendações,Recomendação', secao:'Prótese dental',etapa:'4',  descricao: 'Quando a prótese estiver fora da boca, deixe-a sempre em um copo com água',  fontes:''},       
        {taxonomia: 'prótese dental,recomendacao,recomendacoes,recomendações,Recomendação', secao:'Prótese dental',etapa:'5',  descricao: 'Procure o dentista para orientação sobre outros produtos complementares de limpeza de dentaduras',  fontes:''},       
        {taxonomia: 'prótese dental,recomendacao,recomendacoes,recomendações,Recomendação', secao:'Prótese dental',etapa:'6',  descricao: 'Se a prótese começar a ficar solta, dificultar a mastigação ou casuar irrtações e machucados na gengiva, pode ser hora de trocá-la',  fontes:''},       
        {taxonomia: 'Dentista,recomendacao,recomendacoes,recomendações,Recomendação', secao:'Dentista',etapa:'1',  descricao: 'Ajudar da detectar cáries, que são causadas por bactérias que utilizam o açúcar da nossa alimentação para produzir ácidos que destroem os dentes',  fontes:''},       
        {taxonomia: 'Dentista,recomendacao,recomendacoes,recomendações,Recomendação', secao:'Dentista',etapa:'2',  descricao: 'Identificar doenças nas gengivas, como a gengivite (inflamação das gengivas) e a periodontite (inflamação que pode chegar a provocar a perda dos dentes)',  fontes:''},       
        {taxonomia: 'Dentista,recomendacao,recomendacoes,recomendações,Recomendação', secao:'Dentista',etapa:'3',  descricao: 'Recomendar métodos, tratamentos e produtos para evitar que a "boca seca", o que é comum entre idosos em função de alguns medicamentos ou distúrbios de saúde',  fontes:''},       
        {taxonomia: 'Dentista,recomendacao,recomendacoes,recomendações,Recomendação', secao:'Dentista',etapa:'4',  descricao: 'Identificar lesões na boca, tais como manchas, caroços, inchaços, placas esbranquiçadas ou avermelhadas e feridas, avaliar sua gravidade e indicar tratamento',  fontes:''},       
        {taxonomia: 'Dentista,recomendacao,recomendacoes,recomendações,Recomendação', secao:'Dentista',etapa:'5',  descricao: 'Verificar a adaptação da prótese periodicamente e indicar a substituição, quando necessário',  fontes:''},       
   
       

        {taxonomia: 'quedas,recomendacao,recomendacoes,recomendações,Recomendação,precaucao,precaução,precaucoes,precauções,', secao:'Quedas',etapa:'1', descricao: 'Evite tapetes soltos',  fontes:''},       
        {taxonomia: 'quedas,recomendacao,recomendacoes,recomendações,Recomendação,precaucao,precaução,precaucoes,precauções,', secao:'Quedas',etapa:'2', descricao: 'Escadas devem ter corrimãos dos dois lados',  fontes:''},           
        {taxonomia: 'quedas,recomendacao,recomendacoes,recomendações,Recomendação,precaucao,precaução,precaucoes,precauções,', secao:'Quedas',etapa:'3', descricao: 'Use sapatos fechados e com solado de borracha',  fontes:''},           
        {taxonomia: 'quedas,recomendacao,recomendacoes,recomendações,Recomendação,precaucao,precaução,precaucoes,precauções,', secao:'Quedas',etapa:'4', descricao: 'Coloque tapete anti-derrapante no banheiro',  fontes:''},           
        {taxonomia: 'quedas,recomendacao,recomendacoes,recomendações,Recomendação,precaucao,precaução,precaucoes,precauções,', secao:'Quedas',etapa:'5', descricao: 'Evite andar em áreas com o piso úmido',  fontes:''},           
        {taxonomia: 'quedas,recomendacao,recomendacoes,recomendações,Recomendação,precaucao,precaução,precaucoes,precauções,', secao:'Quedas',etapa:'6', descricao: 'Evite encerar a casa',  fontes:''},           
    
        {taxonomia: 'atividades físicas,recomendacao,recomendacoes,recomendações,Recomendação,precaucao,precaução,precaucoes,precauções,', secao:'Atividades físicas',etapa:'1', descricao: 'Exercícios posturais.',  fontes:''},           
        {taxonomia: 'atividades físicas,recomendacao,recomendacoes,recomendações,Recomendação,precaucao,precaução,precaucoes,precauções,', secao:'Atividades físicas',etapa:'2', descricao: 'Jogos e modalidades esportivas.',  fontes:''}, 
        {taxonomia: 'atividades físicas,recomendacao,recomendacoes,recomendações,Recomendação,precaucao,precaução,precaucoes,precauções,', secao:'Atividades físicas',etapa:'3', descricao: 'Alongamentos e relaxamentos.',  fontes:''}, 
        {taxonomia: 'atividades físicas,recomendacao,recomendacoes,recomendações,Recomendação,precaucao,precaução,precaucoes,precauções,', secao:'Atividades físicas',etapa:'4', descricao: 'Exercícios respiratórios.',  fontes:''}, 
        {taxonomia: 'atividades físicas,recomendacao,recomendacoes,recomendações,Recomendação,precaucao,precaução,precaucoes,precauções,', secao:'Atividades físicas',etapa:'5', descricao: 'Exercícios resistidos como musculação e ginástica.',  fontes:''}, 
        {taxonomia: 'atividades físicas,recomendacao,recomendacoes,recomendações,Recomendação,precaucao,precaução,precaucoes,precauções,', secao:'Atividades físicas',etapa:'6', descricao: 'Caminhada e corrida.',  fontes:''}, 
        {taxonomia: 'atividades físicas,recomendacao,recomendacoes,recomendações,Recomendação,precaucao,precaução,precaucoes,precauções,', secao:'Atividades físicas',etapa:'7', descricao: 'Natação e hidroginástica.',  fontes:''}, 
        {taxonomia: 'atividades físicas,recomendacao,recomendacoes,recomendações,Recomendação,precaucao,precaução,precaucoes,precauções,', secao:'Atividades físicas',etapa:'8', descricao: 'Práticas corporais orientais, como o <i>tai chi chuan, yoga e lian gong.</i>',  fontes:''}, 
        {taxonomia: 'atividades físicas,recomendacao,recomendacoes,recomendações,Recomendação,precaucao,precaução,precaucoes,precauções,', secao:'Atividades físicas',etapa:'9', descricao: 'Capoterapia e danças, como a dança sênior.',  fontes:''}, 

        {taxonomia: 'sexualidade,sexo,relacionamento,recomendacao,recomendacoes,recomendações,Recomendação,precaucao,precaução,precaucoes,precauções,', secao:'Sexualidade',etapa:'1', descricao: 'A sexualidade e a sensualidade continuam fazendo parte de nossas vidas, independentemente da idade.',  fontes:''}, 
        {taxonomia: 'sexualidade,sexo,doença,doenca,doencas,doencas,tabaco,bebida,recomendacao,recomendacoes,recomendações,Recomendação,precaucao,precaução,precaucoes,precauções,',secao:'Sexualidade', etapa:'2', descricao: 'Algumas condições podem interferir na vida sexual, como diabetes, colesterol alto, fumo, álcool, menopausa e uso de alguns medicamentos.',  fontes:''}, 
        {taxonomia: 'sexualidade,sexo,medico,médico,relacionamento,recomendacao,recomendacoes,recomendações,Recomendação,precaucao,precaução,precaucoes,precauções,', secao:'Sexualidade',etapa:'3', descricao: 'Cuidado com o uso de medicamentos que prometem melhorar o desempenho sexual. Todo medicamento só deve ser usado sob orientação médica.',  fontes:''}, 
        {taxonomia: 'sexualidade,sexo,relacionamento,recomendacao,recomendacoes,recomendações,Recomendação,precaucao,precaução,precaucoes,precauções,', secao:'Sexualidade',etapa:'4', descricao: 'Se necessário, faça uso de lubrificantes. Eles facilitam a penetração e a tornam mais prazerosa.',  fontes:''}, 
        {taxonomia: 'sexualidade,sexo,relacionamento,recomendacao,recomendacoes,recomendações,Recomendação,precaucao,precaução,precaucoes,precauções,', secao:'Sexualidade',etapa:'5', descricao: 'Faça exames para saber como está a sua saúde. Muitas vezes, o desempenho sexual pode estar relacionado a algum problema de saúde.',  fontes:''}, 
        {taxonomia: 'sexualidade,sexo,preservativo,doenca,doença,doencas,doenças,relacionamento,recomendacao,recomendacoes,recomendações,Recomendação,precaucao,precaução,precaucoes,precauções,', secao:'Sexualidade',etapa:'6', descricao: 'Não se esqueça de que a idade não lhe dará proteção contra as infecções sexualmente transmissíveis (IST), como gonorreia, sífilis, aids, hepatite C e outras. A camisinha (masculina ou feminina) continua sendo uma das melhores formas de prevenção e deve ser usada nas relações sexuais em qualquer idade.',  fontes:''}, 
        {taxonomia: 'sexualidade,sexo,doencas,doenças,relacionamento,recomendacao,recomendacoes,recomendações,Recomendação,precaucao,precaução,precaucoes,precauções,', secao:'Sexualidade',etapa:'7', descricao: 'Outra forma de prevenção é fazer os testes para sífilis, HIV/Aids e hepatite C. Quanto antes esses agravos forem identificados, mais cedo o tratamento pode ser começado, melhorando sua qualidade de vida e impedindo a transmissão de infecções para seus(suas) parceiros(as). Solicite os testes que estão disponíveis na rede pública de saúde. São rápidos, seguros, gratuitos e sigilosos.',  fontes:''}, 




 

       
        
        
      ];
   }

   filterTechnologies(param : any) : void
   {
      this.declareTechnologies();


      let val : string 	= param;

      // DON'T filter the technologies IF the supplied input is an empty string
      if (val.trim() !== '')
      {
         this.technologies = this.technologies.filter((item) =>
         {
           this.teste=1;
           this.resultado=val;
           return this.removeAccents(item.taxonomia.toLowerCase()).indexOf(val.toLowerCase()) > -1 || this.removeAccents(item.descricao.toLowerCase()).indexOf(val.toLowerCase()) > -1 || item.taxonomia.toLowerCase().indexOf(val.toLowerCase()) > -1 || item.descricao.toLowerCase().indexOf(val.toLowerCase()) > -1;
         })
      }
   }

   onFilter(category : string) : void
   {
     this.declareTechnologies();

      // Only filter the technologies array IF the selection is NOT equal to value of all
      if (category.trim() !== '')
      {
         this.technologies = this.technologies.filter((item) =>
         { this.teste=1;
          if (category=="Medicamentos") {this.resultado="Medicamentos"; this.subtexto="Orientações quando ao uso e armazenamento de medicamentos";  }
          if (category=="Alimentação") {this.resultado="Alimentação";this.subtexto="Dez passos para uma alimentação saudável"; }
          if (category=="Saúde bucal") {this.resultado="Saúde bucal"; this.subtexto='Manter uma boa saúde bucal é importante para o bem-estar, a autoestima e a saúde geral de seu corpo. Veja algumas dicas:';}
          if (category=="Quedas") {this.resultado="Quedas"; this.subtexto="Para evitar quedas em seu domicílio, alguns cuidados são importantes, tais como:";}                  
          if (category=="Atividades físicas") {this.resultado="Atividades físicas"; this.subtexto="O Programa Academia da Saúde, os Núcleos de Apoio à Saúde da Família (NASFs) e as Unidades Básicas de Saúde (UBS) oferecem opções para a prática de exercícios regulares no Sistema Único de Saúde. Informe-se na sua unidade de saúde. <br>É recomendável buscar orientação de um profissional antes de iniciar um programa de atividades físicas.<br><strong>Dicas de atividades físicas e práticas corporais:</strong>" }    
          if (category=="Sexualidade") {this.resultado="Sexualidade"; }     
          if (category=="Prótese dental") {this.resultado="Prótese dental"; this.subtexto="Se você usa prótese dental (dentadura/ponte móvel):" }    
          if (category=="Dentista") {this.resultado="Dentista"; this.subtexto="Além da higiene, a ida regular ao dentista contribui para a manutenção da saúde bucal. Veja aspectos que esse profissional pode te ajudar:" }    
          
          
          //this.resultado=category;
          this.categoria='1'       
          
           return item.taxonomia.toLowerCase().indexOf(category.toLowerCase()) > -1;
         })
      }
   }

   filterTechnologies2(param : any) : void
   {
    let val2 : string 	= param;
    this.navCtrl.push(Orientacoes2SecundariaPage, {param: val2});
    console.log(val2);
   }

   irMedicamentos()
   {
    this.navCtrl.push(Orientacoes2SecundariaPage, {category:'Medicamentos'});
    //this.onFilter('Medicamentos');
   }

   irAlimentacao()
   {
    this.navCtrl.push(Orientacoes2SecundariaPage, {category:'Alimentação'});
    //this.onFilter('Alimentação');
   }


   irBucal()
   {
    this.navCtrl.push(Orientacoes2SecundariaPage, {category:'Saúde bucal'});
    //this.onFilter('Saúde bucal');
   }

   
   irProtese()
   {
    this.navCtrl.push(Orientacoes2SecundariaPage, {category:'Prótese dental'});
    //this.onFilter('Prótese dental');
   }

   irDentista()
   {
    this.navCtrl.push(Orientacoes2SecundariaPage, {category:'Dentista'}); 
    //this.onFilter('Dentista');
   }


   irQuedas()
   {
    this.navCtrl.push(Orientacoes2SecundariaPage, {category:'Quedas'}); 
    //this.onFilter('Quedas');
   }

   irAtividades()
   {
    this.navCtrl.push(Orientacoes2SecundariaPage, {category:'Atividades físicas'}); 
    //this.onFilter('Atividades físicas');
   }

   irSexualidade()
   {
    this.navCtrl.push(Orientacoes2SecundariaPage, {category:'Sexualidade'}); 
    //this.onFilter('Sexualidade');
   }

/*
  
goVoltar()
{
  this.navCtrl.push(Orientacoes2Page)
}
*/

goVoltar(teste)
{
  if (teste==1)
  {            
    this.navCtrl.push(Orientacoes2Page);       
              
  }
  else
  {
    this.navCtrl.setRoot(HomePage);
    this.navCtrl.setRoot(HomePage);  
  }
  
}

 
   removeAccents(item) {
    return item
         .replace(/á/g, 'a') 
         .replace(/ã/g, 'a') 
         .replace(/â/g, 'a')            
         .replace(/é/g, 'e')
         .replace(/è/g, 'e') 
         .replace(/ê/g, 'e')
         .replace(/í/g, 'i')
         .replace(/ï/g, 'i')
         .replace(/ì/g, 'i')
         .replace(/ó/g, 'o')
         .replace(/ô/g, 'o')
         .replace(/õ/g, 'o')
         .replace(/ú/g, 'u')
         .replace(/ü/g, 'u')
         .replace(/ç/g, 'c')
         .replace(/ß/g, 's');
}
}
