import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Orientacoes2SecundariaPage } from './orientacoes2-secundaria';

@NgModule({
  declarations: [
    Orientacoes2SecundariaPage,
  ],
  imports: [
    IonicPageModule.forChild(Orientacoes2SecundariaPage),
  ],
})
export class Orientacoes2SecundariaPageModule {}
