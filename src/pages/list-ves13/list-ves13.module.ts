import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ListVes13Page } from './list-ves13';

@NgModule({
  declarations: [
    ListVes13Page,
  ],
  imports: [
    IonicPageModule.forChild(ListVes13Page),
  ],
})
export class ListVes13PageModule {}
