import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';

import { AddDataPage } from '../add-data/add-data';
import { EditDataPage } from '../edit-data/edit-data';

@Component({
  selector: 'page-contact',
  templateUrl: 'contact.html'
})
export class ContactPage {

  usuarios: any = [];
   totalIncome = 0;
   totalExpense = 0;
   balance = 0;

   constructor(public navCtrl: NavController,
     private sqlite: SQLite) {}

   ionViewDidLoad() {
     this.getData();
   }

   ionViewWillEnter() {
     this.getData();
   }

   getData() {
     this.sqlite.create({
       name: 'idoso.db',
       location: 'default'
     }).then((db: SQLiteObject) => {
       db.executeSql('CREATE TABLE IF NOT EXISTS perfil(rowid INTEGER PRIMARY KEY, nascimento Date, nome TEXT, sexo TEXT, imagem TEXT)', [])
       .then(res => console.log('Executed SQL'))
       .catch(e => console.log(e));
       db.executeSql('SELECT * FROM perfil ORDER BY nome DESC', [])
       .then(res => {
         this.usuarios = [];
         for(var i=0; i<res.rows.length; i++) {
           this.usuarios.push({rowid:res.rows.item(i).rowid,nascimento:res.rows.item(i).nascimento,nome:res.rows.item(i).nome,sexo:res.rows.item(i).sexo,imagem:res.rows.item(i).imagem})
         }
       })
       .catch(e => console.log(e));
  /*     db.executeSql('SELECT SUM(amount) AS totalIncome FROM expense WHERE type="Income"', [])
       .then(res => {
         if(res.rows.length>0) {
           this.totalIncome = parseInt(res.rows.item(0).totalIncome);
           this.balance = this.totalIncome-this.totalExpense;
         }
       })
       .catch(e => console.log(e));
       db.executeSql('SELECT SUM(amount) AS totalExpense FROM expense WHERE type="Expense"', [])
       .then(res => {
         if(res.rows.length>0) {
           this.totalExpense = parseInt(res.rows.item(0).totalExpense);
           this.balance = this.totalIncome-this.totalExpense;
         }
       })
       */
     }).catch(e => console.log(e));
   }

   addData() {
     this.navCtrl.push(AddDataPage);
   }

   editData(rowid) {
     this.navCtrl.push(EditDataPage, {
       rowid:rowid
     });
   }

   deleteData(rowid) {
     this.sqlite.create({
       name: 'idoso.db',
       location: 'default'
     }).then((db: SQLiteObject) => {
       db.executeSql('DELETE FROM perfil WHERE rowid=?', [rowid])
       .then(res => {
         console.log(res);
         this.getData();
       })
       .catch(e => console.log(e));
     }).catch(e => console.log(e));
   }

 }
