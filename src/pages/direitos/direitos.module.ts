import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DireitosPage } from './direitos';

@NgModule({
  declarations: [
    DireitosPage,
  ],
  imports: [
    IonicPageModule.forChild(DireitosPage),
  ],
})
export class DireitosPageModule {}
