import { Component } from '@angular/core';
import { IonicPage, NavController} from 'ionic-angular';
import { DireitosProvider } from '../../providers/direitos/direitos';
import { DireitosInternoPage } from '../direitos-interno/direitos-interno';


@IonicPage()
@Component({
  selector: 'page-direitos',
  templateUrl: 'direitos.html',
})
export class DireitosPage {
  politicas: any = 0;
  constructor(public navCtrl: NavController, public direitos: DireitosProvider) {
        this.direitos.loadAll().then(result => {
            this.politicas =  result;
        });
  }
  detailsPage(id){
    this.navCtrl.push(DireitosInternoPage, {code: id});
  };
}
