import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DireitosSecundariaPage } from './direitos-secundaria';

@NgModule({
  declarations: [
    DireitosSecundariaPage,
  ],
  imports: [
    IonicPageModule.forChild(DireitosSecundariaPage),
  ],
})
export class DireitosSecundariaPageModule {}
