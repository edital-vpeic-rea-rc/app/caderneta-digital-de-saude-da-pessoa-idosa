import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams} from 'ionic-angular';

import { DireitosPage } from '../direitos/direitos';
/*import { HomePage } from '../home/home'; */

@IonicPage()
@Component({
  selector: 'page-direitos-secundaria',
  templateUrl: 'direitos-secundaria.html',
})
export class DireitosSecundariaPage{

  public technologies : Array<any>;
  teste : number=0;
  resultado: string="";
  voltar: number=1;
  categoria: string="";
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    /*var val=this.navCtrl.last().name;
    console.log("Previous Page is called = " + this.navCtrl.last().name);
    */
   if (navParams.get("category")) {
   var ress=navParams.get("category");
   this.onFilter(ress);
   }

   if (navParams.get("param")) {
    var pa=navParams.get("param");
    this.filterTechnologies(pa);
   }
    }

    




  declareTechnologies() : void
  {
      this.technologies = [
        {taxonomia:'Saúde,saude,beneficios', secao:'Saúde', lei:'', paragrafo:'', descricao:'Direito à atenção integral à saúde do idoso, por intermédio do SUS',  fontes:'Estatuto do Idoso (Lei nº 10.741, de 1º de outubro de 2003)<br>Artigo 15'},
        {taxonomia:'Saúde,saude,servicos,serviços,beneficios',secao:'Saúde', lei:'', paragrafo:'', descricao:'Receber medicamentos, especialmente os de uso continuado, gratuitamente',  fontes:'Estatuto do Idoso (Lei nº 10.741, de 1º de outubro de 2003)<br>Artigo 15 §2'},
        {taxonomia:'Saúde,casa,servicos,serviços,beneficios',secao:'Saúde', lei:'', paragrafo:'', descricao:'Impossibilitados de se locomover, têm direito a atendimento domiciliar, incluindo internação',  fontes:'Estatuto do Idoso (Lei nº 10.741, de 1º de outubro de 2003)<br>Artigo 15'},
        {taxonomia:'Saúde,tratamento,beneficios',secao:'Saúde', lei:'', paragrafo:'', descricao:'É assegurado o direito a optar pelo tratamento de saúde que considerar mais favorável.',  fontes:'Estatuto do Idoso (Lei nº 10.741, de 1º de outubro de 2003)<br>Artigo 17'},
        {taxonomia:'Saúde,casa,servicos,serviços',secao:'Saúde', lei:'', paragrafo:'', descricao:'Idosos com necessidade de assistência médica ou multiprofissional têm direito a acolhimento temporário em Hospital-Dia e Centro-Dia.',  fontes:'Decreto 1948/1996<br>Artigo 4'},
        {taxonomia:'Saúde,casa,servicos,serviços',secao:'Saúde', lei:'', paragrafo:'', descricao:'Idosos dependentes podem solicitar atendimento domiciliar na sua Unidade Básica de Saúde de referência.',  fontes:'Decreto 1948/1996<br>Artigo 4'},
        {taxonomia:'Saúde,saude,beneficios,planos de saúde', secao:'Saúde', lei:'', paragrafo:'', descricao:'Direito a acompanhante em caso de internação ou observação em hospital.',  fontes:'Estatuto do Idoso (Lei nº 10.741, de 1º de outubro de 2003)<br>Artigo 15 §3'},
        {taxonomia:'Saúde,saude,beneficios,planos de saúde',secao:'Saúde', lei:'', paragrafo:'', descricao:'É vedada a cobrança de valores diferenciados em razão da idade nos planos de saúde.',  fontes:'Estatuto do Idoso (Lei nº 10.741, de 1º de outubro de 2003)<br>Artigo 15 §7'},
        {taxonomia:'Saúde,saude,Prioridades,beneficios',secao:'Saúde', lei:'', paragrafo:'', descricao:'Idosos maiores de 80 anos têm preferência especial sobre os demais idosos em todo atendimento, exceto em caso de emergência.',  fontes:'Estatuto do Idoso (Lei nº 10.741, de 1º de outubro de 2003)<br>Artigo 15 §7'},
        {taxonomia:'Saúde,saude,beneficios,vacinacao',secao:'Saúde', lei:'', paragrafo:'', descricao:'Todo idoso tem direito a vacinar-se contra gripe, pneumonia, difteria e febre amarela.',  fontes:'Portaria 1498/2013'},
        {taxonomia:'Mobilidade,onibus,ônibus,transporte,beneficios',secao:'Mobilidade', lei:'', paragrafo:'', descricao:'Gratuidade no transporte coletivo público urbano e semiurbano, com reserva de 10% dos assentos, os quais deverão ser identificados com placa de reserva.',  fontes:'Estatuto do Idoso (Lei nº 10.741, de 1º de outubro de 2003)<br>Artigo 39'},
        {taxonomia:'Mobilidade,onibus,ônibus,transporte,beneficios',secao:'Mobilidade', lei:'', paragrafo:'', descricao:'Reserva de duas vagas gratuitas no transporte interestadual para idosos com renda igual ou inferior a dois salários mínimos e desconto de 50% para os idosos que excedam as vagas garantidas.',  fontes:'Estatuto do Idoso (Lei nº 10.741, de 1º de outubro de 2003)<br>Artigo 40'},
        {taxonomia:'Mobilidade,transporte,carro,beneficios',secao:'Mobilidade', lei:'', paragrafo:'', descricao:'Reserva de 5% das vagas nos estacionamentos públicos e privados.',  fontes:'Estatuto do Idoso (Lei nº 10.741, de 1º de outubro de 2003)<br>Artigo 41'},
        {taxonomia:'Mobilidade,transporte,carro,beneficios,segurança',secao:'Prioridades', lei:'', paragrafo:'', descricao:'O idoso tem direito a prioridade e segurança no embarque e desembarque do transporte público.',  fontes:'Estatuto do Idoso (Lei nº 10.741, de 1º de outubro de 2003)<br>Artigo 42'},
        {taxonomia:'Prioridades,judiciario,justica,servicos,serviços,beneficios',secao:'Prioridades', lei:'', paragrafo:'', descricao:'Na tramitação dos processos e procedimentos na execução de atos e diligências judiciais.',  fontes:'Estatuto do Idoso (Lei nº 10.741, de 1º de outubro de 2003)<br>Artigo 71'},
        {taxonomia:'Prioridades,servicos,serviços,beneficios',secao:'Prioridades', lei:'', paragrafo:'', descricao:'Em filas de atendimento de serviços nos setores público e privado, tais como bancos, lojas, supermecados etc.',  fontes:'Estatuto do Idoso (Lei nº 10.741, de 1º de outubro de 2003)<br>Artigo 3 §1'},
        {taxonomia:'Prioridades',secao:'Prioridades', lei:'', paragrafo:'', descricao:'Na restituição do Imposto de Renda',  fontes:'Estatuto do Idoso (Lei nº 10.741, de 1º de outubro de 2003)<br>Artigo 71'},
        {taxonomia:'Renda,beneficios',secao:'Renda', lei:'', paragrafo:'', descricao:'Direito de requerer o Benefício de Prestação Continuada (BPC), a partir dos 65 anos de idade, desde que não possua meios para prover sua própria subsistência ou de tê-la provida pela família.',  fontes:'Lei Orgânica de Assistência Social (Lei 8742 de 1993)<br>Artigo 20'},
        {taxonomia:'Renda,beneficios',secao:'Renda', lei:'', paragrafo:'', descricao:'Direito a 25% de acréscimo na aposentadoria por invalidez (casos especiais).',  fontes:'Lei 8213 de 1991.<br>Artigo 45'},
        {taxonomia:'Renda,beneficios',secao:'Renda', lei:'', paragrafo:'', descricao:'O idoso sem condições de subsistência tem direito a solicitar pensão alimentícia de descendente.',  fontes:'Estatuto do Idoso (Lei nº 10.741, de 1º de outubro de 2003)<br>Artigo 11'},
        {taxonomia:'Outros,Segurança',secao:'Outros', lei:'', paragrafo:'', descricao:'Direito de exigir medidas de proteção sempre que seus direitos estiverem ameaçados ou violados por ação ou omissão da sociedade, do Estado, da família, de seu curador ou de entidades de atendimento.',  fontes:'Estatuto do Idoso (Lei nº 10.741, de 1º de outubro de 2003)<br>Artigo 43'},
        {taxonomia:'Outros',secao:'Outros', lei:'', paragrafo:'', descricao:'É vedada a fixação de limite máximo de idade em seleção para qualquer trabalho, emprego ou concurso público, ressalvados os casos em que a natureza do cargo exigir.',  fontes:'Estatuto do Idoso (Lei nº 10.741, de 1º de outubro de 2003)<br>Artigo 27'},
        {taxonomia:'Lazer,Ingressos,Cinema,shows,espetaculos,futebol,volei,basquete',secao:'Lazer', lei:'', paragrafo:'', descricao:'Desconto de pelo menos 50% nos ingressos para eventos artísticos, culturais, esportivos e de lazer.',  fontes:'Estatuto do Idoso (Lei nº 10.741, de 1º de outubro de 2003)<br>Artigo 23'},
        {taxonomia:'Habitação,Habitacao,prioridade,casa,moradia',secao:'Habitação', lei:'Estatuto do Idoso<br>Decreto 1948/1996', paragrafo:'', descricao:'Nos programas habitacionais, públicos ou subsidiados com recursos públicos, o idoso tem prioridade na aquisição de imóvel para moradia própria.',  fontes:'Estatuto do Idoso (Lei nº 10.741, de 1º de outubro de 2003)<br>Artigo 38'},
        {taxonomia:'Habitação,Habitacao,casa,moradia,asilo',secao:'Habitação', lei:'Estatuto do Idoso<br>Decreto 1948/1996', paragrafo:'', descricao:'Idosos sem renda suficiente para sua manutenção e sem família têm direito a acolhimento em Casa-lar.',  fontes:'Decreto 1948/1996<br>Artigo 4'},
      ];
   }

   filterTechnologies(param : any) : void
   {
      this.declareTechnologies();


      let val : string 	= param;

      // DON'T filter the technologies IF the supplied input is an empty string
      if (val.trim() !== '')
      {
         this.technologies = this.technologies.filter((item) =>
         {
           this.teste=1;
           this.resultado=val;
           return this.removeAccents(item.taxonomia.toLowerCase()).indexOf(val.toLowerCase()) > -1 || this.removeAccents(item.descricao.toLowerCase()).indexOf(val.toLowerCase()) > -1 || item.taxonomia.toLowerCase().indexOf(val.toLowerCase()) > -1 || item.descricao.toLowerCase().indexOf(val.toLowerCase()) > -1;
         })
      }
   }

   onFilter(category : string) : void
   {
     this.declareTechnologies();

      // Only filter the technologies array IF the selection is NOT equal to value of all
      if (category.trim() !== '')
      {
         this.technologies = this.technologies.filter((item) =>
         { this.teste=1;
          this.resultado=category;
           return item.taxonomia.toLowerCase().indexOf(category.toLowerCase()) > -1;
         })
         this.categoria='1';
      }
   }

   irSaude()
   {
    this.onFilter('saúde');
   }

   irMobilidade()
   {
    this.onFilter('Transporte');
   }


   irPrioridade()
   {
    this.onFilter('Prioridades');
   }

   irRenda()
   {
    this.onFilter('Renda');
   }

   irOutros()
   {
    this.onFilter('Outros');
   }

   irLazer()
   {
    this.onFilter('Lazer');
   }

   irHabitacao()
   {
    this.onFilter('Habitação');
   }

   irPoliticas()
   {
    this.navCtrl.push(DireitosPage)
   }
   /*
goVoltar()
{
  this.navCtrl.push(SobrePage)
}
*/
 
   removeAccents(value) {
    return value
    .replace(/á/g, 'a') 
    .replace(/ã/g, 'a') 
    .replace(/â/g, 'a')            
    .replace(/é/g, 'e')
    .replace(/è/g, 'e') 
    .replace(/ê/g, 'e')
    .replace(/í/g, 'i')
    .replace(/ï/g, 'i')
    .replace(/ì/g, 'i')
    .replace(/ó/g, 'o')
    .replace(/ô/g, 'o')
    .replace(/õ/g, 'o')
    .replace(/ú/g, 'u')
    .replace(/ü/g, 'u')
    .replace(/ç/g, 'c')
    .replace(/ß/g, 's');
}

}


