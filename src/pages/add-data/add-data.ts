import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';
import { Toast } from '@ionic-native/toast';
import { TabsPage } from '../tabs/tabs';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { Validators, FormBuilder, FormGroup,AbstractControl } from '@angular/forms';
/*import { HomePage } from '../home/home';*/


@IonicPage()
@Component({
  selector: 'page-add-data',
  templateUrl: 'add-data.html',
})
export class AddDataPage {
  formgroup:FormGroup;
  photo: string = '';
  nome: AbstractControl;
  nasce: AbstractControl;
  sex: AbstractControl;
  data = { nascimento:"", sexo:"", nome:"", imagem:"", amount:0 };

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    private sqlite: SQLite,
    private toast: Toast,
    private camera: Camera,
    public formbuilder:FormBuilder
  ) {

      this.formgroup = formbuilder.group({
        //nome: ['',Validators.required,Validators.minLength(5)]
       nome: ['', Validators.compose([Validators.maxLength(30), Validators.pattern('[a-zA-Z ]*'), Validators.required,Validators.minLength(5)])],
       sex: ['', Validators.required],
       nasce: ['',Validators.required]   
      });

      this.nome = this.formgroup.controls['nome'];
      this.nasce = this.formgroup.controls['nasce'];
      this.sex = this.formgroup.controls['sex'];
      

    }
/*
    ionViewWillLoad() {
      this.marceloForm = this.formBuilder.group({
        nome: new FormControl('', Validators.compose([
          UsernameValidator.validUsername,
          Validators.maxLength(25),
          Validators.minLength(5),
          Validators.pattern('^(?=.*[a-zA-Z])(?=.*[0-9])[a-zA-Z0-9]+$'),
          Validators.required
        ]))        
      });
    }
*/
  takePicture() {


    const options: CameraOptions = {
      quality: 80,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      allowEdit: true,
      targetWidth: 640,
      targetHeight: 360

    }

    this.camera.getPicture(options)
      .then((imageData) => {
        let base64image = 'data:image/jpeg;base64,' + imageData;
        this.photo = base64image;

      }, (error) => {
        console.error(error);
      })
      .catch((error) => {
        console.error(error);
      })
  }


  saveData() {
    this.sqlite.create({
      name: 'idoso.db',
      location: 'default'
    }).then((db: SQLiteObject) => {
      db.executeSql('INSERT INTO perfil VALUES(?,?,?,?,?)',[1,this.data.nascimento,this.data.nome,this.data.sexo,this.photo])
        .then(res => { this.navCtrl.push(TabsPage);
          console.log(res);
          this.toast.show('Perfil Salvo', '5000', 'center').subscribe(
            toast => {
              //this.navCtrl.popToRoot();

            }
          );
        })
        .catch(e => {
          console.log(e);
          this.toast.show(e, '5000', 'center').subscribe(
            toast => {
              console.log(toast);
            }
          );
        });
    }).catch(e => {
      console.log(e);
      this.toast.show(e, '5000', 'center').subscribe(
        toast => {
          console.log(toast);
        }
      );
    });
  }

}
