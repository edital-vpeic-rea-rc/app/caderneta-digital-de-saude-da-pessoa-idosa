import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AddAvaliacaoPage } from './add-avaliacao';

@NgModule({
  declarations: [
    AddAvaliacaoPage,
  ],
  imports: [
    IonicPageModule.forChild(AddAvaliacaoPage),
  ],
})
export class AddAvaliacaoPageModule {}
