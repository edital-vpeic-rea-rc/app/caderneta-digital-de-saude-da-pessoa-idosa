import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';
import { Toast } from '@ionic-native/toast';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { Validators, FormBuilder, FormGroup, AbstractControl } from '@angular/forms';
import { AvaliacaoPage } from '../avaliacao/avaliacao';

//import localePtBr from '@angular/common/locales/pt';

//import { registerLocaleData } from '@angular/common';

//registerLocaleData(localePtBr);

@IonicPage()
@Component({
  selector: 'page-edit-data',
  templateUrl: 'edit-data.html',
})
export class EditDataPage {

  data = { rowid:1, nascimento:"", sexo:"", nome:"", imagem:""};
  
  photo: string = '';
  nome: AbstractControl;
  nasce: AbstractControl;
  sex: AbstractControl;
  formgroup:FormGroup;

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    private sqlite: SQLite,
    private toast: Toast,
    public camera: Camera,
    public formbuilder:FormBuilder

  ) {
      this.getCurrentData(navParams.get("rowid"));
      
      this.formgroup = formbuilder.group({
        //nome: ['',Validators.required,Validators.minLength(5)]
       nome: ['', Validators.compose([Validators.maxLength(30), Validators.pattern('[A-Za-záàâãéèêíïóôõöúçñÁÀÂÃÉÈÍÏÓÔÕÖÚÇÑ ]*'), Validators.required,Validators.minLength(2)])]
     /*  sex: ['', Validators.required],*/
     /*  nasce: ['',Validators.required]   */
      });

      this.nome = this.formgroup.controls['nome'];
    /*  this.nasce = this.formgroup.controls['nasce'];*/
     /* this.sex = this.formgroup.controls['sex']; */
  }



  takePicture() {


    const options: CameraOptions = {
      quality: 70,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      allowEdit: true,
      targetWidth: 640,
      targetHeight: 640

    }

    this.camera.getPicture(options)
      .then((imageData) => {
        let base64image = 'data:image/jpeg;base64,' + imageData;
        this.photo = base64image;

      }, (error) => {
        console.error(error);
      })
      .catch((error) => {
        console.error(error);
      })
  }

  getCurrentData(rowid) {
    this.sqlite.create({
      name: 'idoso.db',
      location: 'default'
    }).then((db: SQLiteObject) => {
      db.executeSql('SELECT * FROM perfil WHERE rowid=?', [rowid])
        .then(res => {
          if(res.rows.length > 0) {
            this.data.rowid = res.rows.item(0).rowid;
            this.data.nome = res.rows.item(0).nome;
            if (this.data.nome==" " )
            {
              this.data.nome="";
            }
            this.data.sexo = res.rows.item(0).sexo;
            this.data.nascimento = res.rows.item(0).nascimento;
            this.data.imagem = res.rows.item(0).imagem;
            this.photo=res.rows.item(0).imagem;
          }
        })
        .catch(e => {
          console.log(e);
          this.toast.show(e, '5000', 'center').subscribe(
            toast => {
              console.log(toast);
            }
          );
        });
    }).catch(e => {
      console.log(e);
      this.toast.show(e, '5000', 'center').subscribe(
        toast => {
          console.log(toast);
        }
      );
    });
  }

  updateData() {
    this.sqlite.create({
      name: 'idoso.db',
      location: 'default'
    }).then((db: SQLiteObject) => {
      db.executeSql('UPDATE perfil SET nascimento=?,sexo=?,nome=?,imagem=? WHERE rowid=?',[this.data.nascimento,this.data.sexo,this.data.nome,this.photo,this.data.rowid])
        .then(res => {this.navCtrl.push(AvaliacaoPage);
          console.log(res);
          this.toast.show('Dados Atualizados', '5000', 'center').subscribe(
            toast => {

            }
          );
        })
        .catch(e => {
          console.log(e);
          this.toast.show(e, '5000', 'center').subscribe(
            toast => {
              console.log(toast);
            }
          );
        });
    }).catch(e => {
      console.log(e);
      this.toast.show(e, '5000', 'center').subscribe(
        toast => {
          console.log(toast);
        }
      );
    });
  }

}
