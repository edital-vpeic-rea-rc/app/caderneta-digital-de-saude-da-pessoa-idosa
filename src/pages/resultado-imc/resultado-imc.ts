import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';
//import { AddImcPage } from '../add-imc/add-imc';
import { AvaliacaoPage } from '../avaliacao/avaliacao';
import { DatePipe } from '@angular/common';
import { AlertController } from 'ionic-angular';
import { EditDataPage } from '../edit-data/edit-data';



@IonicPage()
@Component({
  selector: 'page-resultado-imc',
  templateUrl: 'resultado-imc.html',
})
export class ResultadoImcPage {
  data = { rowid:1, nascimento:"", sexo:"", nome:"", imagem:""};
  photo: string = "assets/imgs/user.png";
  imcs: any = [];
  nome_teste:string ="";
  nome_teste2:string ="";
  voltar: number=2;
  data_final: any;


  constructor(public navCtrl: NavController,
    private sqlite: SQLite,
    public navParams: NavParams,
    private alertCtrl: AlertController,
    public datepipe: DatePipe) { 

      if (navParams.get("id_imc"))
    {
      this.getData(navParams.get("id_imc"))
      this.voltar=2;
    }
    else
    {
    this.getData2();
    this.voltar=3;
    }
     }

     ionViewDidLoad() {      
      this.getUser(1);
  }


     goBack()
     {
       this.navCtrl.popTo(this.navCtrl.getByIndex(this.navCtrl.length()-this.voltar));
       //console.log("aa" +this.voltar);
     }

  getData(id_imc) {


       this.sqlite.create({

         name: 'idoso.db',
         location: 'default'
       }).then((db: SQLiteObject) => {

        let linha=2;

          db.executeSql('SELECT id_imc, peso,altura, data_exame FROM teste_imc where id_imc=?', [id_imc])
         .then(res => {
           this.imcs = [];

             for(var i=0; i<res.rows.length; i++) {

            //passa parametros para gerar o gráfico
            //var data_ex=new Date(res.rows.item(i).data_exame);
            //var data_final=data_ex.toDateString();

            var data_ex=new Date(res.rows.item(i).data_exame);
            this.data_final =this.datepipe.transform(data_ex, 'dd/MM/yyyy HH:mm');

             var nome_teste = res.rows.item(i).nome;

            if (nome_teste !== nome_teste2) {
              var nome_teste2 = res.rows.item(i).nome;
               linha=linha+1;
            
             }

             let finalBmi = res.rows.item(i).peso / (res.rows.item(i).altura * res.rows.item(i).altura );
             var calculo_imc=parseFloat(finalBmi.toFixed(1));
             var calculo_string = calculo_imc.toString().replace(/[.]/gi,",");   
             var myDateString = new Date(res.rows.item(i).nascimento);
             var milisecondsDiff = Date.now() - (myDateString.getTime());
             let idade=Math.floor(( milisecondsDiff )/(1000 * 3600 * 24)/365);
             //let idade=milisecondsDiff;
             
             this.imcs.push({id_imc:res.rows.item(i).id_imc,nome:res.rows.item(i).nome,data_exame:res.rows.item(i).data_exame,peso:res.rows.item(i).peso,altura:res.rows.item(i).altura,idade:idade,calculo_imc:calculo_imc,calculo_string:calculo_string})

           }



         })
         .catch(e => console.log(e));

       }

      ).catch(e => console.log(e));

     }


     getData2() {


      this.sqlite.create({

        name: 'idoso.db',
        location: 'default'
      }).then((db: SQLiteObject) => {

       let linha=2;

         db.executeSql('SELECT id_imc,  peso,  altura, data_exame FROM teste_imc order by id_imc DESC limit 1', [])
        .then(res => {
          this.imcs = [];

            for(var i=0; i<res.rows.length; i++) {

           //passa parametros para gerar o gráfico
           //var data_ex=new Date(res.rows.item(i).data_exame);
           //var data_final=data_ex.toDateString();

           var data_ex=new Date(res.rows.item(i).data_exame);
           this.data_final =this.datepipe.transform(data_ex, 'dd/MM/yyyy HH:mm');

            var nome_teste = res.rows.item(i).nome;

           if (nome_teste !== nome_teste2) {
             var nome_teste2 = res.rows.item(i).nome;
              linha=linha+1;
           
            }

            let finalBmi = res.rows.item(i).peso / (res.rows.item(i).altura * res.rows.item(i).altura );
            var calculo_imc=parseFloat(finalBmi.toFixed(1));   
            var calculo_string = calculo_imc.toString().replace(/[.]/gi,",");      
           
            var myDateString = new Date(res.rows.item(i).nascimento);
            var milisecondsDiff = Date.now() - (myDateString.getTime());
            let idade=Math.floor(( milisecondsDiff )/(1000 * 3600 * 24)/365);
            //let idade=milisecondsDiff;
           this.imcs.push({id_imc:res.rows.item(i).id_imc,nome:res.rows.item(i).nome,data_exame:res.rows.item(i).data_exame,peso:res.rows.item(i).peso,altura:res.rows.item(i).altura,idade:idade,calculo_imc:calculo_imc,calculo_string:calculo_string})

          }



        })
        .catch(e => console.log(e));

      }

     ).catch(e => console.log(e));

    }

/*
     addImc() {
       this.navCtrl.push(AddImcPage);
     }
*/
     editImc(id_imc) {
       this.navCtrl.push(ResultadoImcPage, {
         id_imc:id_imc
       });
     }

  
     deleteImc2(id_imc) {
       this.sqlite.create({
         name: 'idoso.db',
         location: 'default'
       }).then((db: SQLiteObject) => {
         db.executeSql('DELETE FROM teste_imc WHERE id_imc=?', [id_imc])
         .then(res => {
           console.log(res);
           this.getData(id_imc);
           this.navCtrl.push(AvaliacaoPage);
         })
         .catch(e => console.log(e));
       }).catch(e => console.log(e));
     }

     deleteImc(id_imc) {
      let alert = this.alertCtrl.create({
        title: 'Tem certeza que quer excluir o resultado?',
        message: '',
        buttons: [
          {
            text: 'Não',
            role: 'cancel',
            handler: () => {
              //console.log('Cancel clicked');
            }
          },
          {
            text: 'Sim',
            handler: () => {
             // console.log('Buy clicked');
             this.deleteImc2(id_imc);
            }
          }
        ]
      });
      alert.present();
    }

    addPerfil2() {
      this.navCtrl.push(EditDataPage, {
        rowid:1
      });
    }
    
     getUser(rowid) {
      this.sqlite.create({
        name: 'idoso.db',
        location: 'default'
      }).then((db: SQLiteObject) => {
        db.executeSql('SELECT * FROM perfil WHERE rowid=?', [rowid])
          .then(res => {
            if(res.rows.length > 0) {
              this.data.rowid = res.rows.item(0).rowid;
              this.data.nome = res.rows.item(0).nome;
              this.data.sexo = res.rows.item(0).sexo;
              this.data.nascimento = res.rows.item(0).nascimento;
              this.data.imagem = res.rows.item(0).imagem;
              this.photo=res.rows.item(0).imagem;
            }
            if (this.photo=="") {this.photo="assets/imgs/user.png"}
          })
          
        })
      }


}
