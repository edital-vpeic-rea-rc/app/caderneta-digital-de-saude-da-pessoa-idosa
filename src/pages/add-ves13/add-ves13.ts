import { Component,ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';
import { Toast } from '@ionic-native/toast';
import { ResultadoVes13Page } from '../resultado-ves13/resultado-ves13'
import { Validators, FormBuilder, FormGroup, AbstractControl } from '@angular/forms';
import { EditDataPage } from '../edit-data/edit-data';
import { Slides } from 'ionic-angular';
import { AlertController } from 'ionic-angular';

@IonicPage()

@Component({
  selector: 'page-add-ves13',
  templateUrl: 'add-ves13.html',
})
export class AddVes13Page {
  @ViewChild(Slides) slides: Slides;
  data = { rowid:1, nascimento:"", sexo:"", nome:"", imagem:""};
  photo: string = "assets/imgs/user.png";
  ves13 = { data_exame:"", recomendacao:"", id_perfil:"", idade:"",percepcao_saude:"",limitacao1:"",limitacao2:"",limitacao3:"",limitacao4:"",limitacao5:"",limitacao6:"",incapacidade1:"",incapacidade2:"", incapacidade3:"", incapacidade4:"", incapacidade5:"", marcelo:"" };
  perfils: any = [];
  perfil = {idade:""}
  categories: any = [];  
  marcelo: string;
  ida:number =  0;
  public percepcao_saude : Array<any>;
  public limitacoes : Array<any>;
  public incapacidades1 : Array<any>;
  public incapacidades2 : Array<any>;
  public incapacidades3 : Array<any>;
  public incapacidades4 : Array<any>;
  public incapacidades5 : Array<any>;

  slide0Form:FormGroup;
  slide1Form:FormGroup;
  slide2Form:FormGroup;
  slide3Form:FormGroup;
  slide4Form:FormGroup;
  slide5Form:FormGroup;
  slide6Form:FormGroup;
  slide7Form:FormGroup;
  slide8Form:FormGroup;
  slide9Form:FormGroup;
  slide10Form:FormGroup;
  slide11Form:FormGroup;
  slide12Form:FormGroup;

  idad: AbstractControl;
  percep: AbstractControl;
  limita1: AbstractControl;
  limita2: AbstractControl;
  limita3: AbstractControl;
  limita4: AbstractControl;
  limita5: AbstractControl;
  limita6: AbstractControl;
  incapa1: AbstractControl;
  incapa2: AbstractControl;
  incapa3: AbstractControl;
  incapa4: AbstractControl;
  incapa5: AbstractControl;



  @ViewChild('signupSlider') signupSlider: any;

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    private sqlite: SQLite,
    private toast: Toast,
    public alertCtrl: AlertController,
    public formbuilder:FormBuilder
  ) {
   

    this.slide0Form = formbuilder.group({
    idad: ['',Validators.compose([Validators.maxLength(3), Validators.minLength(2),Validators.max(140),Validators.pattern('[^, .-]+'), Validators.required])]
    });
    this.idad = this.slide0Form.controls['idad'];

    this.slide1Form = formbuilder.group({    
    percep: ['', Validators.required]
    });  
    this.percep = this.slide1Form.controls['percep'];

    this.slide2Form = formbuilder.group({
    
    limita1: ['', Validators.required]
    });
    this.limita1 = this.slide2Form.controls['limita1'];

    this.slide3Form = formbuilder.group({      
      limita2: ['', Validators.required]
      });
    
      this.limita2 = this.slide3Form.controls['limita2'];


    this.slide4Form = formbuilder.group({
      
      limita3: ['', Validators.required]
      });
      this. limita3 = this.slide4Form.controls['limita3'];

    this.slide5Form = formbuilder.group({
     
      limita4: ['', Validators.required]
      });
      this. limita4 = this.slide5Form.controls['limita4'];

      this.slide6Form = formbuilder.group({
       
        limita5: ['', Validators.required]
        });
        this. limita5 = this.slide6Form.controls['limita5'];

        this.slide7Form = formbuilder.group({
          
          limita6: ['', Validators.required]
          });
          this. limita6 = this.slide7Form.controls['limita6'];

          this.slide8Form = formbuilder.group({
            
            incapa1: ['', Validators.required]
            });
            this. incapa1 = this.slide8Form.controls['incapa1'];

            this.slide9Form = formbuilder.group({
              
              incapa2: ['', Validators.required]
              });
              this. incapa2 = this.slide9Form.controls['incapa2'];

              this.slide10Form = formbuilder.group({
                
                incapa3: ['', Validators.required]
                });
                this. incapa3 = this.slide10Form.controls['incapa3'];

                this.slide11Form = formbuilder.group({
                  
                  incapa4: ['', Validators.required]
                  });
                  this. incapa4 = this.slide11Form.controls['incapa4'];

                  this.slide12Form = formbuilder.group({                    
                   
                    incapa5: ['', Validators.required]
                    

                    });
                    this. incapa5 = this.slide12Form.controls['incapa5'];

  }

  ionViewDidLoad() {  
    this.slides.lockSwipeToNext(true);
    this.getLimitacao();
    this.getIncapacidade1();
    this.getIncapacidade2();
    this.getIncapacidade3();
    this.getIncapacidade4();
    this.getIncapacidade5();
    this.getPercepcao() 
    this.getPerfil()
    this.getUser(1);
  }

  infoVes13() {
    const alert = this.alertCtrl.create({
      title: 'Índice de vulnerabilidade',
      message:  '<div class="message">O VES-13 é um instrumento simples e eficaz, capaz de identificar o idoso vulnerável residente na comunidade, com base na idade, auto-percepção da saúde, presença de limitações físicas e incapacidades (SALIBA et al., 2001). É um questionário de fácil aplicabilidade, que pode ser respondido pelos próprios profissionais de saúde, pela pessoa idosa ou pelos familiares/cuidadores, dispensando a observação direta do usuário. Baseia-se no registro das habilidades necessárias para a realização das tarefas do cotidiano. Cada item recebe uma determinada pontuação e o somatório final pode variar de 0 a 10 pontos.</div>'
      + '<br><br><div class="referencia"><strong>Fonte:</strong><br>Brasil. Ministério da Saúde. Secretaria de Atenção à Saúde. Departamento de Ações Programáticas Estratégicas.<br><br>Manual para utilização da Caderneta de Saúde da Pessoa Idosa / Ministério da Saúde, Secretaria de Atenção à Saúde, Departamento de Ações Programáticas Estratégicas. – Brasília : Ministério da Saúde, 2018.<br>96 p. : il. <br><br>(ISBN 978-85-334-2613-9)</div>',
      cssClass: 'referencias',
      buttons: ['OK']
    });
    alert.present();
  }

  getPercepcao() : void
  {
      this.percepcao_saude = [
        {id:'1', lei:'', name:'Excelente'},
        {id:'2', lei:'', name:'Muito Bom'},
        {id:'3', lei:'', name:'Boa'},
        {id:'4', lei:'', name:'Regular'},
        {id:'5', lei:'', name:'Ruim '}
 ];
   }

   getLimitacao() : void
   {
       this.limitacoes = [
         {id:'1', lei:'', name:'Nenhuma dificuldade'},
         {id:'2', lei:'', name:'Pouca dificuldade'},
         {id:'3', lei:'', name:'Média'},
         {id:'4', lei:'', name:'Muita dificuldade'},
         {id:'5', lei:'', name:'Incapaz '}
  ];
    }

    getIncapacidade1() : void
    {
        this.incapacidades1 = [
          {id:'1', lei:'', name:'Sim'},
          {id:'2', lei:'', name:'Não ou não faz compras por outros motivos que não a saúde.'}
         
   ];
     }

     getIncapacidade2() : void
     {
         this.incapacidades2 = [
           {id:'1', lei:'', name:'Sim'},
           {id:'2', lei:'', name:'Não ou não controla o dinheiro por outros motivos que não a saúde.'}
          
    ];
      }

      getIncapacidade3() : void
      {
          this.incapacidades3 = [
            {id:'1', lei:'', name:'Sim'},
            {id:'2', lei:'', name:'Não ou não caminha dentro de casa por outros motivos que não a saúde.'}
           
     ];
       }

       getIncapacidade4() : void
       {
           this.incapacidades4 = [
             {id:'1', lei:'', name:'Sim'},
             {id:'2', lei:'', name:'Não ou não realiza tarefas domésticas leves por outros motivos que não a saúde.'}
            
      ];
        }

        getIncapacidade5() : void
        {
            this.incapacidades5 = [
              {id:'1', lei:'', name:'Sim'},
              {id:'2', lei:'', name:'Não ou não toma banho sozinho(a) por outros motivos que não a saúde.'}
             
       ];
         }

  getPerfil() {
    this.sqlite.create({
      name: 'idoso.db',
      location: 'default'
    }).then((db: SQLiteObject) => {

      db.executeSql('SELECT rowid, nome,nascimento FROM perfil where rowid=1', [])
      .then(res => {
        this.perfils = [];
        for(var i=0; i<res.rows.length; i++) {
        var myDateString = new Date(res.rows.item(i).nascimento);
          var milisecondsDiff = Date.now() - (myDateString.getTime());
          let idade=Math.floor(( milisecondsDiff )/(1000 * 3600 * 24)/365);
          this.ida=idade;
          this.ves13.idade=""+idade;
          if (this.ves13.idade=="NaN")
          {
            this.ves13.idade="";            
          }
          this.perfils.push({rowid:res.rows.item(i).rowid,nascimento:res.rows.item(i).nascimento,nome:res.rows.item(i).nome,sexo:res.rows.item(i).sexo,idade:idade})


        }
      })
      .catch(e => console.log(e));
    }).catch(e => console.log(e));
  }




    saveVes13() {
          this.sqlite.create({
        name: 'idoso.db',
        location: 'default'
      }).then((db: SQLiteObject) => {
        db.executeSql('INSERT INTO teste_ves13 VALUES(NULL,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)',[1,this.ves13.idade,this.ves13.percepcao_saude,this.ves13.limitacao1,this.ves13.limitacao2,this.ves13.limitacao3,this.ves13.limitacao4,this.ves13.limitacao5,this.ves13.limitacao6,this.ves13.incapacidade1,this.ves13.incapacidade2,this.ves13.incapacidade3,this.ves13.incapacidade4,this.ves13.incapacidade5,Date.now()])
        .then(res => { this.navCtrl.push(ResultadoVes13Page);
            console.log(res);
            this.toast.show('Avaliação salva', '5000', 'center').subscribe(
              toast => {
                //this.navCtrl.popToRoot();

              }
            );
          })
          .catch(e => {
            console.log(e);
            this.toast.show(e, '5000', 'center').subscribe(
              toast => {
                console.log(toast);
              }
            );
          });
      }).catch(e => {
        console.log(e);
        this.toast.show(e, '5000', 'center').subscribe(
          toast => {
            console.log(toast);
          }
        );
      });
    }

    next(){
      
      this.slides.lockSwipeToNext(false);
      this.signupSlider.slideNext();
      this.slides.lockSwipeToNext(true);
      
  }

  prev(){
    
      this.signupSlider.slidePrev();
      
  }

  addPerfil2() {
    this.navCtrl.push(EditDataPage, {
      rowid:1
    });
  }

  getUser(rowid) {
    this.sqlite.create({
      name: 'idoso.db',
      location: 'default'
    }).then((db: SQLiteObject) => {
      db.executeSql('SELECT * FROM perfil WHERE rowid=?', [rowid])
        .then(res => {
          if(res.rows.length > 0) {
            this.data.rowid = res.rows.item(0).rowid;
            this.data.nome = res.rows.item(0).nome;
            this.data.sexo = res.rows.item(0).sexo;
            this.data.nascimento = res.rows.item(0).nascimento;
            this.data.imagem = res.rows.item(0).imagem;
            this.photo=res.rows.item(0).imagem;
          }
          if (this.photo=="") {this.photo="assets/imgs/user.png"}
        })
        
      })
    }
}
