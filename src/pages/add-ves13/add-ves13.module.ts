import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AddVes13Page } from './add-ves13';

@NgModule({
  declarations: [
    AddVes13Page,
  ],
  imports: [
    IonicPageModule.forChild(AddVes13Page),
  ],
})
export class AddVes13PageModule {}
