import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ActionSheetController, AlertController } from 'ionic-angular';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';
import { DatePipe } from '@angular/common'
import { AvaliacaoPage } from '../avaliacao/avaliacao';
import { EditDataPage } from '../edit-data/edit-data';


@IonicPage()
@Component({
  selector: 'page-resultado-panturrilha',
  templateUrl: 'resultado-panturrilha.html',
})

export class ResultadoPanturrilhaPage  {
  data = { rowid:1, nascimento:"", sexo:"", nome:"", imagem:""};
  photo: string = "assets/imgs/user.png";
  panturrilhas: any = [];
  nome_teste:string ="";
  nome_teste2:string ="";
  num: number =0;
  voltar: number=2;
  data_final: any;


  constructor(public navCtrl: NavController,
    private sqlite: SQLite,
    public navParams: NavParams,    
    public alertCtrl: AlertController,
    public actionSheetCtrl: ActionSheetController,
    public datepipe: DatePipe) {

    if (navParams.get("id_panturrilha"))
    {

      this.getData(navParams.get("id_panturrilha"));
     this.voltar=2;
  }
    else
    {
    this.getData2();

  this.voltar=3;
    }

    }

    ionViewDidLoad() {      
      this.getUser(1);
  }

    goBack()
        {
          this.navCtrl.popTo(this.navCtrl.getByIndex(this.navCtrl.length()-this.voltar));
          //console.log("aa" +this.voltar);
        }


getData(id_panturrilha) {


     this.sqlite.create({

       name: 'idoso.db',
       location: 'default'
     }).then((db: SQLiteObject) => {

      let linha=2;

        db.executeSql('SELECT id_panturrilha, circunferencia, data_exame FROM teste_panturrilha where id_panturrilha=?', [id_panturrilha])
       .then(res => {
         this.panturrilhas = [];
         this.num = res.rows.length;
           for(var i=0; i<res.rows.length; i++) {

          //passa parametros para gerar o gráfico
          //var data_ex=new Date(res.rows.item(i).data_exame);
          //var data_final=data_ex.toDateString();

          var data_ex=new Date(res.rows.item(i).data_exame);
          this.data_final =this.datepipe.transform(data_ex, 'dd/MM/yyyy');

           var nome_teste = res.rows.item(i).nome;


                          if (nome_teste !== nome_teste2) {
            var nome_teste2 = res.rows.item(i).nome;
             linha=linha+1;

           }
           var calculo_panturrilha=parseFloat(res.rows.item(i).circunferencia.toFixed(1));
           var calculo_string = calculo_panturrilha.toString().replace(/[.]/gi,",");

           var myDateString = new Date(res.rows.item(i).nascimento);
           var milisecondsDiff = Date.now() - (myDateString.getTime());
           let idade=Math.floor(( milisecondsDiff )/(1000 * 3600 * 24)/365);
           //let idade=milisecondsDiff;
          this.panturrilhas.push({id_panturrilha:res.rows.item(i).id_panturrilha,nome:res.rows.item(i).nome,data_exame:res.rows.item(i).data_exame,circunferencia:res.rows.item(i).circunferencia,circunferencia2:calculo_string,idade:idade})

         }



       }


)
       .catch(e => console.log(e));


     }
    ).catch(e => console.log(e));

   }

   getData2() {


    this.sqlite.create({

      name: 'idoso.db',
      location: 'default'
    }).then((db: SQLiteObject) => {

     let linha=2;

       db.executeSql('SELECT id_panturrilha, circunferencia, data_exame FROM teste_panturrilha order by id_panturrilha DESC limit 1', [])
      .then(res => {
        this.panturrilhas = [];
        this.num = res.rows.length;
          for(var i=0; i<res.rows.length; i++) {

         //passa parametros para gerar o gráfico
         //var data_ex=new Date(res.rows.item(i).data_exame);
         //var data_final=data_ex.toDateString();

         var data_ex=new Date(res.rows.item(i).data_exame);
         this.data_final =this.datepipe.transform(data_ex, 'dd/MM/yyyy');

          var nome_teste = res.rows.item(i).nome;


                         if (nome_teste !== nome_teste2) {
           var nome_teste2 = res.rows.item(i).nome;
            linha=linha+1;

          }
          var calculo_panturrilha=parseFloat(res.rows.item(i).circunferencia.toFixed(1));
          var calculo_string = calculo_panturrilha.toString().replace(/[.]/gi,",");


          var myDateString = new Date(res.rows.item(i).nascimento);
          var milisecondsDiff = Date.now() - (myDateString.getTime());
          let idade=Math.floor(( milisecondsDiff )/(1000 * 3600 * 24)/365);
          //let idade=milisecondsDiff;
         this.panturrilhas.push({id_panturrilha:res.rows.item(i).id_panturrilha,nome:res.rows.item(i).nome,data_exame:res.rows.item(i).data_exame,circunferencia:res.rows.item(i).circunferencia,circunferencia2:calculo_string,idade:idade})

        }



      })
      .catch(e => console.log(e));

    }

   ).catch(e => console.log(e));

  }


/*
   addPanturrilha() {
     this.navCtrl.push(AddPanturrilhaPage);
   }
*/
 
   deletePanturrilha2(id_panturrilha) {
    this.sqlite.create({
      name: 'idoso.db',
      location: 'default'
    }).then((db: SQLiteObject) => {
      db.executeSql('DELETE FROM teste_panturrilha WHERE id_panturrilha=?', [id_panturrilha])
      .then(res => {
        console.log(res);        
        this.navCtrl.push(AvaliacaoPage)
      })
      .catch(e => console.log(e));
    }).catch(e => console.log(e));
  }

  deletePanturrilha(id_panturrilha) {
    let alert = this.alertCtrl.create({
      title: 'Tem certeza que quer excluir o resultado?',
      message: '',
      buttons: [
        {
          text: 'Não',
          role: 'cancel',         
          handler: () => {
            //console.log('Cancel clicked');
          }
        },
        {
          text: 'Sim',
          handler: () => {
           // console.log('Buy clicked');
           this.deletePanturrilha2(id_panturrilha)
          }
        }
      ]
    });
    alert.present();
  }

  addPerfil2() {
    this.navCtrl.push(EditDataPage, {
      rowid:1
    });
  }
  
   getUser(rowid) {
    this.sqlite.create({
      name: 'idoso.db',
      location: 'default'
    }).then((db: SQLiteObject) => {
      db.executeSql('SELECT * FROM perfil WHERE rowid=?', [rowid])
        .then(res => {
          if(res.rows.length > 0) {
            this.data.rowid = res.rows.item(0).rowid;
            this.data.nome = res.rows.item(0).nome;
            this.data.sexo = res.rows.item(0).sexo;
            this.data.nascimento = res.rows.item(0).nascimento;
            this.data.imagem = res.rows.item(0).imagem;
            this.photo=res.rows.item(0).imagem;
          }
          if (this.photo=="") {this.photo="assets/imgs/user.png"}
        })
        
      })
    }

 }
