import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ResultadoPanturrilhaPage } from './resultado-panturrilha';

@NgModule({
  declarations: [
    ResultadoPanturrilhaPage,
  ],
  imports: [
    IonicPageModule.forChild(ResultadoPanturrilhaPage),
  ],
})
export class ResultadoPanturrilhaPageModule {}
