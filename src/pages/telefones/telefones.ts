import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,ActionSheetController, AlertController } from 'ionic-angular';
import { CallNumber } from '@ionic-native/call-number';
import { HomePage } from '../home/home';
/**
 * Generated class for the TelefonesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-telefones',
  templateUrl: 'telefones.html',
})
export class TelefonesPage {

  tabBarElement: any;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams, 
    public callNumber: CallNumber,   
    public alertCtrl: AlertController,
    public actionSheetCtrl: ActionSheetController
  ) {
    this.tabBarElement = document.querySelector('#Tabs');
      this.navCtrl.parent.select(5); 
  }

  goVoltar() {       
    //this.navCtrl.popToRoot(); 
  
    this.navCtrl.setRoot(HomePage);
    this.navCtrl.setRoot(HomePage);
    //this.navCtrl.setRoot(TabsPage);
   
  
  }

  ligarAns() {
    let alert = this.alertCtrl.create({
      title: 'Deseja realmente fazer esta ligação telefônica?',
      message: '<div class="message">Este serviço poderá ser cobrado!!!</div>',
      cssClass: 'referencias',
      buttons: [
        {
          text: 'Não',
          role: 'cancel',
          handler: () => {
            //console.log('Cancel clicked');
          }
        },
        {
          text: 'Sim',
          handler: () => {
           // console.log('Buy clicked');
           this.callNumber.callNumber("08007019656", true);
          }
        }
      ]
    });
    alert.present();
    
  }

  ligarBombeiros() {
    let alert = this.alertCtrl.create({
      title: 'Deseja realmente fazer esta ligação telefônica?',
      message: '<div class="message">Este serviço poderá ser cobrado!!!</div>',
      cssClass: 'referencias',
      buttons: [
        {
          text: 'Não',
          role: 'cancel',
          handler: () => {
            //console.log('Cancel clicked');
          }
        },
        {
          text: 'Sim',
          handler: () => {
           // console.log('Buy clicked');
           this.callNumber.callNumber("193", true);
          }
        }
      ]
    });
    alert.present();
    
  }

  ligarViolencia() {
    let alert = this.alertCtrl.create({
      title: 'Deseja realmente fazer esta ligação telefônica?',
      message: '<div class="message">Este serviço poderá ser cobrado!!!</div>',
      cssClass: 'referencias',
      buttons: [
        {
          text: 'Não',
          role: 'cancel',
          handler: () => {
            //console.log('Cancel clicked');
          }
        },
        {
          text: 'Sim',
          handler: () => {
           // console.log('Buy clicked');
           this.callNumber.callNumber("100", true);
          }
        }
      ]
    });
    alert.present();
    
    
  }

  ligarDisqueSaude() {
    let alert = this.alertCtrl.create({
      title: 'Deseja realmente fazer esta ligação telefônica?',
      message: '<div class="message">Este serviço poderá ser cobrado!!!</div>',
      cssClass: 'referencias',
      buttons: [
        {
          text: 'Não',
          role: 'cancel',
          handler: () => {
            //console.log('Cancel clicked');
          }
        },
        {
          text: 'Sim',
          handler: () => {
           // console.log('Buy clicked');
           this.callNumber.callNumber("136", true);
          }
        }
      ]
    });
    alert.present();
    
  }

  ligarSamu() {
    let alert = this.alertCtrl.create({
      title: 'Deseja realmente fazer esta ligação telefônica?',
      message: '<div class="message">Este serviço poderá ser cobrado!!!</div>',
      cssClass: 'referencias',
      buttons: [
        {
          text: 'Não',
          role: 'cancel',
          handler: () => {
            //console.log('Cancel clicked');
          }
        },
        {
          text: 'Sim',
          handler: () => {
           // console.log('Buy clicked');
           this.callNumber.callNumber("192", true);
          }
        }
      ]
    });
    alert.present();
    
  }
  ligarViolenciaMulher() {
    let alert = this.alertCtrl.create({
      title: '<div class="message">Deseja realmente fazer esta ligação telefônica?</div>',
      message: 'Este serviço poderá ser cobrado!!!',
      cssClass: 'referencias',
      buttons: [
        {
          text: 'Não',
          role: 'cancel',
          handler: () => {
            //console.log('Cancel clicked');
          }
        },
        {
          text: 'Sim',
          handler: () => {
           // console.log('Buy clicked');
           this.callNumber.callNumber("180", true);
          }
        }
      ]
    });
    alert.present();
    
  }

  ligarSuicidio() {
    let alert = this.alertCtrl.create({
      title: 'Deseja realmente fazer esta ligação telefônica?',
      message: '<div class="message">Este serviço poderá ser cobrado!!!</div>',
      cssClass: 'referencias',
      buttons: [
        {
          text: 'Não',
          role: 'cancel',
          handler: () => {
            //console.log('Cancel clicked');
          }
        },
        {
          text: 'Sim',
          handler: () => {
           // console.log('Buy clicked');
           this.callNumber.callNumber("188", true);
          }
        }
      ]
    });
    alert.present();
    
  }

}
