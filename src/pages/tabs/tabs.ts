//import { Component } from '@angular/core';
import { Component, ViewChild } from '@angular/core';
import { NavController, Tabs, NavParams } from 'ionic-angular';
//import { AboutPage } from '../about/about';
//import { ContactPage } from '../contact/contact';
import { HomePage } from '../home/home';
import { AvaliacaoPage } from '../avaliacao/avaliacao';
import { SobrePage } from '../sobre/sobre';
import { Orientacoes2Page } from '../orientacoes2/orientacoes2';
import { TelefonesPage } from '../telefones/telefones';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {
  
  @ViewChild('mainTabs') mainTabs: Tabs;

  public selectedTab: number;
  teste: number=0;
  tabBarElement: any;
  tab1Root = HomePage;
  tab2Root = SobrePage
  tab3Root = Orientacoes2Page;
  tab4Root = TelefonesPage
 // tab5Root = ContactPage;
  tab5Root = AvaliacaoPage;

  constructor(public navParams: NavParams,
 public navCtrl: NavController
   
    ) {

      this.tabBarElement = document.querySelector('#Tabs');
      
    this.selectedTab = this.navParams.get('selectedTab') ; 
    
  }
  
 ionViewDidLoad() {
   // console.log("aa" +this.selectedTab);
    if(this.selectedTab) {
     // this.mainTabs.select(this.selectedTab);
      this.navCtrl.parent.select(this.selectedTab);    
      //alert(this.selectedTab) ;     
      
    }
    
  
    
 
    
  }
  

}
