import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DireitosInternoPage } from './direitos-interno';

@NgModule({
  declarations: [
    DireitosInternoPage,
  ],
  imports: [
    IonicPageModule.forChild(DireitosInternoPage),
  ],
})
export class DireitosInternoPageModule {}
