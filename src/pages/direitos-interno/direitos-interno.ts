import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { DireitosProvider } from '../../providers/direitos/direitos';

@IonicPage()
@Component({
  selector: 'page-direitos-interno',
  templateUrl: 'direitos-interno.html',
})
export class DireitosInternoPage {
  politica: any = 0;
  constructor(public navCtrl: NavController, 
              public navParams: NavParams,
              public direitos: DireitosProvider) {
    this.direitos.getByID(this.navParams.get('code')).then(result => {
      this.politica = result;
    });
  }
}
