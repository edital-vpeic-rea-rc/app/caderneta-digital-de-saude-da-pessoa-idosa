import { Component,ViewChild  } from '@angular/core';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';
import { IonicPage, NavController, NavParams, Content } from 'ionic-angular';
//import { ContactPage } from '../contact/contact';
import { AddPanturrilhaPage } from '../add-panturrilha/add-panturrilha';
import { AddImcPage } from '../add-imc/add-imc';
import { AddVes13Page } from '../add-ves13/add-ves13';
import { ListPanturrilhaPage } from '../list-panturrilha/list-panturrilha';
import { ResultadoPanturrilhaPage } from '../resultado-panturrilha/resultado-panturrilha';
import { ResultadoImcPage } from '../resultado-imc/resultado-imc';
import { ResultadoVes13Page } from '../resultado-ves13/resultado-ves13';
import { ListImcPage } from '../list-imc/list-imc';
import { ListVes13Page } from '../list-ves13/list-ves13';
import { EditDataPage } from '../edit-data/edit-data';
import { HomePage } from '../home/home';
//import { TabsPage } from '../tabs/tabs';
import { AlertController } from 'ionic-angular';
import * as $ from "jquery";


@IonicPage()
@Component({
  selector: 'page-avaliacao',
  templateUrl: 'avaliacao.html',
})

export class AvaliacaoPage {
  data = { rowid:1, nascimento:"", sexo:"", nome:"", imagem:""};
  photo: string = "assets/imgs/user.png";
  panturrilhas: any = [];
  imcs: any = [];
  ves13s: any = [];
  totalIncome = 0;
  totalExpense = 0;
  balance = 0;
  num_ves13: number=0;
  num_imc: number=0;
  num_panturrilha: number=0;
  circunferencia: number=0;
  cod_panturrilha: number=0;
  imc: number =0;
  cod_imc: number =0;
  pontosfinal: number=0;
  cod_ves13: number=0;
  tabBarElement: any;
  selectedTab: any;
  pontuacaototal: number=0;
  pontos_total: number=0;
  
  @ViewChild(Content) content: Content; 
  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public alertCtrl: AlertController,
    private sqlite: SQLite) {
      this.tabBarElement = document.querySelector('#Tabs');
      this.navCtrl.parent.select(1); 
      
    }

  ionViewDidEnter() {
    this.getCurrentData(1);
    this.getPanturrilha();
    this.getPanturrilha2();
    this.getImc();
    this.getImc2();
    this.getVes13();
    this.getVes132();
    

  }


  subir()
  {
   
   $().ready(function(){
     $(".alert-message").animate({ scrollTop: 1600 }, 160000);
     });
     
     $(document).on('touchstart', '.alert-message', (event) => {
       $( ".alert-message" ).stop();
       });
 
      $(document).on('touchstart', '.alert-message', (event) => {
       $(".alert-message").animate({ scrollTop: 1600 }, 160000);
       });
 
       $(document).on('touchmove', '.alert-message', (event) => {
         $( ".alert-message" ).stop();
         });
         
  }

  infoPanturrilha() {
    const alert = this.alertCtrl.create({
      title: 'Perímetro da panturrilha',
      message: '<div class="message">Essa é uma medida de triagem indicada para avaliação da massa muscular, cuja redução correlaciona-se com risco de desenvolver a síndrome chamada sarcopenia e desfechos desfavoráveis. sarcopenia é uma síndrome clínica caracterizada pela perda progressiva e generalizada da massa, força e desempenho muscular, com risco de desfechos adversos como incapacidade física, baixa qualidade de vida e óbito. É diagnosticada através da densidade corporal total ou inferida através de medidas indiretas.</div>'
       + '<br><br><div class="referencia"><strong>Fonte:</strong><br>Brasil. Ministério da Saúde. Secretaria de Atenção à Saúde. Departamento de Ações Programáticas Estratégicas.<br><br>Manual para utilização da Caderneta de Saúde da Pessoa Idosa / Ministério da Saúde, Secretaria de Atenção à Saúde, Departamento de Ações Programáticas Estratégicas. – Brasília : Ministério da Saúde, 2018.<br>96 p. : il. <br><br>(ISBN 978-85-334-2613-9)</div>',
      cssClass: 'referencias',
      buttons: ['OK']
    });
    alert.present();
    this.subir();
  }

  infoImc() {
    const alert = this.alertCtrl.create({
      title: 'Índice de massa corporal',
      message:  '<div class="message">Nos procedimentos de diagnóstico e acompanhamento do estado nutricional de pessoas idosas, o critério prioritário a ser utilizado deve ser a classificação do Índice de Massa Corporal (IMC), recomendado pela Organização Mundial de Saúde (OMS), considerando os pontos de corte diferenciados para a população com 60 anos ou mais de idade. Assim, é importante conhecer algumas alterações fisiológicas características dessa fase do curso da vida.</div>'
      + '<br><br><div class="referencia"><strong>Fonte:</strong><br>Brasil. Ministério da Saúde. Secretaria de Atenção à Saúde. Departamento de Ações Programáticas Estratégicas.<br><br>Manual para utilização da Caderneta de Saúde da Pessoa Idosa / Ministério da Saúde, Secretaria de Atenção à Saúde, Departamento de Ações Programáticas Estratégicas. – Brasília : Ministério da Saúde, 2018.<br>96 p. : il. <br><br>(ISBN 978-85-334-2613-9)</div>',
      cssClass: 'referencias',
      buttons: ['OK']
    });
    alert.present();
    this.subir();
  }

  infoVes13() {
    const alert = this.alertCtrl.create({
      title: 'Índice de vulnerabilidade',
      message:  '<div class="message">O VES-13 é um instrumento simples e eficaz, capaz de identificar o idoso vulnerável residente na comunidade, com base na idade, auto-percepção da saúde, presença de limitações físicas e incapacidades (SALIBA et al., 2001). É um questionário de fácil aplicabilidade, que pode ser respondido pelos próprios profissionais de saúde, pela pessoa idosa ou pelos familiares/cuidadores, dispensando a observação direta do usuário. Baseia-se no registro das habilidades necessárias para a realização das tarefas do cotidiano. Cada item recebe uma determinada pontuação e o somatório final pode variar de 0 a 10 pontos.</div>'
      + '<br><br><div class="referencia"><strong>Fonte:</strong><br>Brasil. Ministério da Saúde. Secretaria de Atenção à Saúde. Departamento de Ações Programáticas Estratégicas.<br><br>Manual para utilização da Caderneta de Saúde da Pessoa Idosa / Ministério da Saúde, Secretaria de Atenção à Saúde, Departamento de Ações Programáticas Estratégicas. – Brasília : Ministério da Saúde, 2018.<br>96 p. : il. <br><br>(ISBN 978-85-334-2613-9)</div>',
      cssClass: 'referencias',
      buttons: ['OK']
    });
    alert.present();
    this.subir();
  }
  
/*
  ionViewWillEnter() {
      this.getCurrentData(1);
    this.getPanturrilha();
    this.getPanturrilha2();
    this.getImc();
    this.getImc2();
    this.getVes13();
    this.getVes132();
  }
*/
goVoltar() {       
  //this.navCtrl.popToRoot(); 

  this.navCtrl.push(HomePage);  
  //this.navCtrl.setRoot(TabsPage);
 

}

  getPanturrilha() {
    this.sqlite.create({
      name: 'idoso.db',
      location: 'default'
    }).then((db: SQLiteObject) => {
       db.executeSql('SELECT COUNT(*) as num FROM teste_panturrilha', [])
      .then(res => { this.num_panturrilha=res.rows.item(0).num;       
      })      
    })
  }

  getPanturrilha2() {
    this.sqlite.create({
      name: 'idoso.db',
      location: 'default'
    }).then((db: SQLiteObject) => {
       db.executeSql('SELECT id_panturrilha,circunferencia FROM teste_panturrilha order by id_panturrilha DESC limit 1', [])
      .then(res => {
        this.panturrilhas = [];
        for(var i=0; i<res.rows.length; i++) {
          this.circunferencia=res.rows.item(i).circunferencia;
          this.cod_panturrilha=res.rows.item(i).id_panturrilha;
        }
      })      
    })
  }

  getImc() {
    this.sqlite.create({
      name: 'idoso.db',
      location: 'default'
    }).then((db: SQLiteObject) => {
     
      db.executeSql('SELECT COUNT(*) as num FROM teste_imc', [])
      .then(res => {this.num_imc=res.rows.item(0).num;
       
      })
    })
  }

  getImc2() {
    this.sqlite.create({
      name: 'idoso.db',
      location: 'default'
    }).then((db: SQLiteObject) => {
     
      db.executeSql('SELECT id_imc, altura,peso FROM teste_imc order by id_imc DESC limit 1', [])
      .then(res => {
        this.imcs = [];
        for(var i=0; i<res.rows.length; i++) {
          let finalBmi = res.rows.item(i).peso / (res.rows.item(i).altura * res.rows.item(i).altura );
          var calculo_imc=parseFloat(finalBmi.toFixed(1));
          this.imc=calculo_imc;
          this.cod_imc=res.rows.item(i).id_imc;
          
        }
      })
    })
  }

  getVes13() {
    this.sqlite.create({
      name: 'idoso.db',
      location: 'default'
    }).then((db: SQLiteObject) => {
      db.executeSql('SELECT COUNT(*) as num FROM teste_ves13', [])
      .then(res => {this.num_ves13=res.rows.item(0).num;       
      })
    })
  }

  getVes132() {
    this.sqlite.create({
      name: 'idoso.db',
      location: 'default'
    }).then((db: SQLiteObject) => {
      db.executeSql('SELECT id_ves13,id_perfil, idade,  percepcao_saude,  limitacao1,  limitacao2, limitacao3, limitacao4, limitacao5, limitacao6,  incapacidade1,incapacidade2, incapacidade3,incapacidade4,incapacidade5, data_exame from teste_ves13 order by id_ves13 DESC limit 1', [])
      .then(res => {
        this.ves13s = [];
        for(var i=0; i<res.rows.length; i++) {
          this.cod_ves13=res.rows.item(i).id_ves13;
             let idade=res.rows.item(i).idade;
             let somaParcial= null;
             let pontosIdade= null;
             let pontosPercepcao= 0;

           if (idade>60 && idade<74)
           {
            pontosIdade=0;
           }
           if (idade>75 && idade<84)
           {
             pontosIdade=1;
           }
           if (idade>84)
           {
            pontosIdade=3;
           }

           if (res.rows.item(i).percepcao_saude>=4)
           {
             pontosPercepcao=1;
           }

           somaParcial=pontosIdade + pontosPercepcao;


           //limitacoes fisicas

           let pontosLimitacao1=0;
           let pontosLimitacao2=0;
           let pontosLimitacao3=0;
           let pontosLimitacao4=0;
           let pontosLimitacao5=0;
           let pontosLimitacao6=0;

           if (res.rows.item(i).limitacao1>=4)
           {
             pontosLimitacao1=1;
           }

           if (res.rows.item(i).limitacao2>=4)
           {
             pontosLimitacao2=1;
           }

           if (res.rows.item(i).limitacao3>=4)
           {
             pontosLimitacao3=1;
           }

           if (res.rows.item(i).limitacao4>=4)
           {
             pontosLimitacao4=1;
           }
           if (res.rows.item(i).limitacao5>=4)
           {
             pontosLimitacao5=1;
           }
           if (res.rows.item(i).limitacao6>=4)
           {
             pontosLimitacao6=1;
           }

           let somaLimitacao=pontosLimitacao1+pontosLimitacao2+pontosLimitacao3+pontosLimitacao4+pontosLimitacao5+pontosLimitacao6;
           let maxLimicacao=somaLimitacao;
           if (somaLimitacao>2) {
             maxLimicacao=2;
           }

           //incapacidades Fisicas

           let pontosIncapacidade1=0;
           let pontosIncapacidade2=0;
           let pontosIncapacidade3=0;
           let pontosIncapacidade4=0;
           let pontosIncapacidade5=0;

           if (res.rows.item(i).incapacidade1==1)
           {
             pontosIncapacidade1=4;
           }
           if (res.rows.item(i).incapacidade2==1)
           {
             pontosIncapacidade2=4;
           }
           if (res.rows.item(i).incapacidade3==1)
           {
             pontosIncapacidade3=4;
           }
           if (res.rows.item(i).incapacidade4==1)
           {
             pontosIncapacidade4=4;
           }
           if (res.rows.item(i).incapacidade5==1)
           {
             pontosIncapacidade5=4;
           }

           let somaIncapacidade=pontosIncapacidade1+pontosIncapacidade2+pontosIncapacidade3+pontosIncapacidade4+pontosIncapacidade5;

           let maxIncapacidade=somaIncapacidade;
           if (somaIncapacidade>4) {
             maxIncapacidade=4;
           }

           let pontuacaofinal=maxIncapacidade+maxLimicacao+somaParcial;
           this.pontuacaototal=0;
          
           if (pontuacaofinal>=3)
           {
         
            this.pontuacaototal=3;
           }
           else
           {
          
            this.pontuacaototal=pontuacaofinal;
           }
         let pontos_final=somaParcial+maxLimicacao+maxIncapacidade;
         var pontos_total=pontos_final;
         if (pontos_final>10) {
         this.pontos_total=10;
         }
          
          this.pontosfinal=pontos_total;
        }
      })
    })
  }


  getCurrentData(rowid) {
    this.sqlite.create({
      name: 'idoso.db',
      location: 'default'
    }).then((db: SQLiteObject) => {
      db.executeSql('SELECT * FROM perfil WHERE rowid=?', [rowid])
        .then(res => {
          if(res.rows.length > 0) {
            this.data.rowid = res.rows.item(0).rowid;
            this.data.nome = res.rows.item(0).nome;
            this.data.sexo = res.rows.item(0).sexo;
            this.data.nascimento = res.rows.item(0).nascimento;
            this.data.imagem = res.rows.item(0).imagem;
            this.photo=res.rows.item(0).imagem;
          }
          if (this.photo=="") {this.photo="assets/imgs/user.png"}
        })
        
      })
    }


/*
  addPerfil() {
    this.navCtrl.push(ContactPage);
  }
*/
  addPanturrilha() {
    this.navCtrl.push(AddPanturrilhaPage);
  }

  addImc() {
    this.navCtrl.push(AddImcPage);
  }

  addVes13() {
    this.navCtrl.push(AddVes13Page);
  }

  listPanturrilha() {
    this.navCtrl.push(ListPanturrilhaPage);
  }

  listImc() {
    this.navCtrl.push(ListImcPage);
  }

  listVes13() {
    this.navCtrl.push(ListVes13Page);
  }

  addPerfil2() {
    this.navCtrl.push(EditDataPage, {
      rowid:1
    });
  }
  verResultado() {
    this.navCtrl.push(ResultadoPanturrilhaPage, {
      rowid:1
    });
  }

  verPanturrilha(id_panturrilha) {  
    this.navCtrl.push(ResultadoPanturrilhaPage, {
     id_panturrilha:this.cod_panturrilha
    });
  }

  verImc(id_imc) {  
    this.navCtrl.push(ResultadoImcPage, {
     id_imc:this.cod_imc
    });
  }

  verVes13(id_ves13) {  
    this.navCtrl.push(ResultadoVes13Page, {
     id_ves13:this.cod_ves13
    });
  }
}
