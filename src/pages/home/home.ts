import { Component } from '@angular/core';
import { Platform,NavController, NavParams} from 'ionic-angular';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';
import { AddDataPage } from '../add-data/add-data';
import { TabsPage } from '../tabs/tabs';
//import { AvaliacaoPage } from '../avaliacao/avaliacao';
//import { Orientacoes2Page } from '../orientacoes2/orientacoes2';
//import { TelefonesPage } from '../telefones/telefones';
import { EditDataPage } from '../edit-data/edit-data';
//import { SobrePage } from '../sobre/sobre';
import { InicialPage } from '../inicial/inicial';



@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  usuarios: any = [];
  num: number =0;
  tabBarElement: any;

  constructor(
    
    public navCtrl: NavController,
    public navParams: NavParams,
    private platform:Platform,
    private sqlite: SQLite) {
      
      this.tabBarElement = document.querySelector('#Tabs');
      this.navCtrl.parent.select(0); 
  }


  ionViewDidEnter()
  { this.verificarCadastro(); 
    this.getInterno();    
    }

  ionViewWillLeave()
    {
    this. getPanturrilha();
    this.getImc();
    this.getVes13();
    }
  


  verPerfil() {
    this.navCtrl.push(TabsPage);
  }

  public verAvaliacao(): void {       
    this.navCtrl.setRoot(TabsPage, {selectedTab:1});
    
    
  }

  verDireitos() {
    this.navCtrl.setRoot(TabsPage, {selectedTab:2});
    
  }

  verOrientacao() {
    this.navCtrl.setRoot(TabsPage, {selectedTab:3});
   
  }

  verTelefones() {
    this.navCtrl.setRoot(TabsPage, {selectedTab:4});
    
  }

  verSobre() {
    this.navCtrl.push(InicialPage);
  }

  verCadastro() {
    this.navCtrl.push(AddDataPage);
  }

  editarCadastro() {
    this.navCtrl.push(EditDataPage, {
      rowid:1
    });
  }

  sair()
  {
    this.platform.exitApp();
  }


  verificarCadastro() {
    this.sqlite.create({
      name: 'idoso.db',
      location: 'default'
    }).then((db: SQLiteObject) => {
      db.executeSql('CREATE TABLE IF NOT EXISTS perfil(rowid INTEGER PRIMARY KEY, nascimento Date, nome TEXT, sexo TEXT, imagem Text)', [])
      .then(res => console.log('Criou a tabela perfil'))
      .catch(e => console.log(e));
       db.executeSql('SELECT COUNT(*) as num,nome FROM perfil where nome=" "', [])
      .then(res => {
        this.num = res.rows.item(0).num;

      })
      .then(res => console.log('Executou select perfil'))
      .catch(e => this.usuarios.push({num:1}));
    }).catch(e => console.log(e));
  }

  getInterno() {
    this.sqlite.create({
      name: 'idoso.db',
      location: 'default'
    }).then((db: SQLiteObject) => {
      db.sqlBatch([
        ['INSERT INTO perfil VALUES(?,?,?,?,?)',[1,'',' ','','']]
     ])
      .then(res => console.log('Criado perfil'))
      .catch(e => console.log(e));      
    }).catch(e => console.log(e));
  }

  getPanturrilha() {
    this.sqlite.create({
      name: 'idoso.db',
      location: 'default'
    }).then((db: SQLiteObject) => {
      db.executeSql('CREATE TABLE IF NOT EXISTS teste_panturrilha(id_panturrilha INTEGER PRIMARY KEY, id_perfil INTEGER, circunferencia REAL, recomendacao TEXT, data_exame DATE)', [])
      .then(res => console.log('Criado tabela teste_panturrilha'))
      .catch(e => console.log(e));     
    }).catch(e => console.log(e));
  }

  getImc() {
    this.sqlite.create({
      name: 'idoso.db',
      location: 'default'
    }).then((db: SQLiteObject) => {
      db.executeSql('CREATE TABLE IF NOT EXISTS teste_imc (id_imc INTEGER PRIMARY KEY, id_perfil INTEGER, peso REAL, altura REAL, recomendacao TEXT, data_exame DATE)', [])
      .then(res => console.log('Criado tabela teste_imc'))
      .catch(e => console.log(e));
      }).catch(e => console.log(e));
  }

  getVes13() {
    this.sqlite.create({
      name: 'idoso.db',
      location: 'default'
    }).then((db: SQLiteObject) => {
      db.executeSql('CREATE TABLE IF NOT EXISTS teste_ves13 ( id_ves13 INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, id_perfil INTEGER, idade INTEGER,  percepcao_saude INTEGER, limitacao1 INTEGER, limitacao2 TEXT, limitacao3 INTEGER, limitacao4 INTEGER, limitacao5 INTEGER, limitacao6 INTEGER,  incapacidade1 INTEGER, incapacidade2 INTEGER, incapacidade3 INTEGER, incapacidade4 INTEGER, incapacidade5 INTEGER, data_exame DATE)', [])
      .then(res => console.log('Criado tabela teste_ves13'))
      .catch(e => console.log(e));  
    }).catch(e => console.log(e));
  }

}
