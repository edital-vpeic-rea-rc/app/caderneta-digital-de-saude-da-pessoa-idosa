import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AddImcPage } from './add-imc';

@NgModule({
  declarations: [
    AddImcPage,
  ],
  imports: [
    IonicPageModule.forChild(AddImcPage),
  ],
})
export class AddImcPageModule {}
