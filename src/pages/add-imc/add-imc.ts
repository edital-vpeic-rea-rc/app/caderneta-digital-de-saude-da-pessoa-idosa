import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';
import { Toast } from '@ionic-native/toast';
import { ResultadoImcPage } from '../resultado-imc/resultado-imc';
import { Validators, FormBuilder, FormGroup, AbstractControl } from '@angular/forms';
import { EditDataPage } from '../edit-data/edit-data';
import { AlertController } from 'ionic-angular';
/**
 * Generated class for the AddImcPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-add-imc',
  templateUrl: 'add-imc.html',
})
export class AddImcPage {
  data = { rowid:1, nascimento:"", sexo:"", nome:"", imagem:""};
  photo: string = "assets/imgs/user.png";
  imc = { data_exame:"", recomendacao:"", id_perfil:"", altura:"", peso:"", id_perfil2:"" };
  perfils: any = [];
  alt: AbstractControl;
  pes: AbstractControl;
  formgroup:FormGroup;

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    private sqlite: SQLite,
    private toast: Toast,
    public alertCtrl: AlertController,
    public formbuilder:FormBuilder
  ) {
    this.formgroup = formbuilder.group({
      //nome: ['',Validators.required,Validators.minLength(5)]
     pes: ['', Validators.compose([Validators.maxLength(7), Validators.minLength(2),Validators.max(200),Validators.min(30),Validators.pattern('[^,-]+'), Validators.required])], 
     alt: ['', Validators.compose([Validators.maxLength(5), Validators.minLength(1),Validators.max(2.6),Validators.pattern('[^,-]+'), Validators.required])]   
    });

    this.pes = this.formgroup.controls['pes'];
    this.alt = this.formgroup.controls['alt'];
  }


    ionViewDidLoad() {
      this.getPerfil();
      this.getUser(1);
    }

    infoImc() {
      const alert = this.alertCtrl.create({
        title: 'Índice de massa corporal',
        message:  '<div class="message">Nos procedimentos de diagnóstico e acompanhamento do estado nutricional de pessoas idosas, o critério prioritário a ser utilizado deve ser a classificação do Índice de Massa Corporal (IMC), recomendado pela Organização Mundial de Saúde (OMS), considerando os pontos de corte diferenciados para a população com 60 anos ou mais de idade. Assim, é importante conhecer algumas alterações fisiológicas características dessa fase do curso da vida.</div>'
        + '<br><br><div class="referencia"><strong>Fonte:</strong><br>Brasil. Ministério da Saúde. Secretaria de Atenção à Saúde. Departamento de Ações Programáticas Estratégicas.<br><br>Manual para utilização da Caderneta de Saúde da Pessoa Idosa / Ministério da Saúde, Secretaria de Atenção à Saúde, Departamento de Ações Programáticas Estratégicas. – Brasília : Ministério da Saúde, 2018.<br>96 p. : il. <br><br>(ISBN 978-85-334-2613-9)</div>',
        cssClass: 'referencias',
        buttons: ['OK']
      });
      alert.present();
    }
    
    saveImc() {
          this.sqlite.create({
        name: 'idoso.db',
        location: 'default'
      }).then((db: SQLiteObject) => {
        db.executeSql('INSERT INTO teste_imc VALUES(NULL,?,?,?,?,?)',[1,this.imc.peso,this.imc.altura,this.imc.recomendacao,Date.now()])
          .then(res => { this.navCtrl.push(ResultadoImcPage);
            console.log(res);
            this.toast.show('Avaliação salva', '5000', 'center').subscribe(
              toast => {
                //this.navCtrl.popToRoot();
               
              }
            );
          })
          .catch(e => {
            console.log(e);
            this.toast.show(e, '5000', 'center').subscribe(
              toast => {
                console.log(toast);
              }
            );
          });
      }).catch(e => {
        console.log(e);
        this.toast.show(e, '5000', 'center').subscribe(
          toast => {
            console.log(toast);
          }
        );
      });
    }

    getPerfil() {
      this.sqlite.create({
        name: 'idoso.db',
        location: 'default'
      }).then((db: SQLiteObject) => {

        db.executeSql('SELECT rowid, nome FROM perfil ORDER BY nome DESC', [])
        .then(res => {
          this.perfils = [];
          for(var i=0; i<res.rows.length; i++) {
            this.perfils.push({rowid:res.rows.item(i).rowid,nascimento:res.rows.item(i).nascimento,nome:res.rows.item(i).nome,sexo:res.rows.item(i).sexo})
          }
        })
        .catch(e => console.log(e));
      }).catch(e => console.log(e));
    }

    addPerfil2() {
      this.navCtrl.push(EditDataPage, {
        rowid:1
      });
    }

    getUser(rowid) {
      this.sqlite.create({
        name: 'idoso.db',
        location: 'default'
      }).then((db: SQLiteObject) => {
        db.executeSql('SELECT * FROM perfil WHERE rowid=?', [rowid])
          .then(res => {
            if(res.rows.length > 0) {
              this.data.rowid = res.rows.item(0).rowid;
              this.data.nome = res.rows.item(0).nome;
              this.data.sexo = res.rows.item(0).sexo;
              this.data.nascimento = res.rows.item(0).nascimento;
              this.data.imagem = res.rows.item(0).imagem;
              this.photo=res.rows.item(0).imagem;
            }
            if (this.photo=="") {this.photo="assets/imgs/user.png"}
          })
          
        })
      }
}
