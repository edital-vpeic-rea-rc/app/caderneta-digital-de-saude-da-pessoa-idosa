import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ListPanturrilhaPage } from './list-panturrilha';

@NgModule({
  declarations: [
    ListPanturrilhaPage,
  ],
  imports: [
    IonicPageModule.forChild(ListPanturrilhaPage),
  ],
})
export class ListPanturrilhaPageModule {}
