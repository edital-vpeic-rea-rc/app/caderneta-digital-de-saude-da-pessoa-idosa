import { Component } from '@angular/core';
import { IonicPage, NavController, ActionSheetController, AlertController  } from 'ionic-angular';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';
//import { AddPanturrilhaPage } from '../add-panturrilha/add-panturrilha';
//import { EditPanturrilhaPage } from '../edit-panturrilha/edit-panturrilha';
import { ResultadoPanturrilhaPage } from '../resultado-panturrilha/resultado-panturrilha';
import { DatePipe } from '@angular/common';
import { EditDataPage } from '../edit-data/edit-data';


@IonicPage()
@Component({
  selector: 'page-list-panturrilha',
  templateUrl: 'list-panturrilha.html',
})

export class ListPanturrilhaPage  {
  data = { rowid:1, nascimento:"", sexo:"", nome:"", imagem:""};
  photo: string = "assets/imgs/user.png";
  //panturrilhas: any = [];
  public panturrilhas: Array<any>;
  panturrilhas2: any = [];
  nome_teste:string ="";
  nome_teste2:string ="";
  num: number =0;
  num2: number =0;
  data_final: any;
  linha: number=0;
  



  constructor(public navCtrl: NavController,
    private sqlite: SQLite,
    public alertCtrl: AlertController,
    public actionSheetCtrl: ActionSheetController,
    public datepipe: DatePipe) {  }
 
    ionViewDidLoad() {
     this.getData();
     this.getData2();
     this.getUser(1);
 }
/*
 getData() : void
 {
     this.panturrilhas = [
       {id_panturrilha:'1', circunferencia:'33', data_exame:'2018-11-22'},
       {id_panturrilha:'2', circunferencia:'34', data_exame:'2018-11-22'},
       {id_panturrilha:'3', circunferencia:'35', data_exame:'2018-11-22'},
       {id_panturrilha:'4', circunferencia:'36', data_exame:'2018-11-22'},
       {id_panturrilha:'5', circunferencia:'37', data_exame:'2018-11-22'},
       {id_panturrilha:'6', circunferencia:'38', data_exame:'2018-11-23'},
       {id_panturrilha:'7', circunferencia:'39', data_exame:'2018-11-23'},
       {id_panturrilha:'8', circunferencia:'40', data_exame:'2018-11-23'},
       
];
  }
  */

getData() {
     this.sqlite.create({

       name: 'idoso.db',
       location: 'default'
     }).then((db: SQLiteObject) => {
      let linha=2;

        db.executeSql('SELECT * FROM (select * from teste_panturrilha ORDER BY  data_exame DESC limit 6) order by data_exame ASC', [])
       .then(res => {
         this.panturrilhas = [];
         this.num = res.rows.length;
           for(var i=0; i<res.rows.length; i++) {        

          var data_ex=new Date(res.rows.item(i).data_exame);
          this.data_final =this.datepipe.transform(data_ex, 'dd/MM/yyyy');

           var nome_teste = res.rows.item(i).nome;


          if (nome_teste !== nome_teste2) {
            var nome_teste2 = res.rows.item(i).nome;
             linha=linha+1;
 
           }
      
           var calculo_panturrilha=parseFloat(res.rows.item(i).circunferencia.toFixed(1));
           var calculo_string = calculo_panturrilha.toString().replace(/[.]/gi,",");

           var myDateString = new Date(res.rows.item(i).nascimento);
           var milisecondsDiff = Date.now() - (myDateString.getTime());
           let idade=Math.floor(( milisecondsDiff )/(1000 * 3600 * 24)/365);
         
          this.panturrilhas.push({id_panturrilha:res.rows.item(i).id_panturrilha,nome:res.rows.item(i).nome,data_exame:res.rows.item(i).data_exame,circunferencia:res.rows.item(i).circunferencia,circunferencia2:calculo_string,idade:idade})

         }



       })
       .catch(e => console.log(e));

     }

    ).catch(e => console.log(e));

   }



   getData2() {
    this.sqlite.create({

      name: 'idoso.db',
      location: 'default'
    }).then((db: SQLiteObject) => {
     this.linha=2;

       db.executeSql('SELECT * FROM (select * from teste_panturrilha ORDER BY  data_exame ASC) order by data_exame DESC', [])
      .then(res => {
        this.panturrilhas2 = [];
        this.num2 = res.rows.length;
          for(var i=0; i<res.rows.length; i++) {
         //passa parametros para gerar o gráfico
        

         var data_ex=new Date(res.rows.item(i).data_exame);
         this.data_final =this.datepipe.transform(data_ex, 'dd/MM/yyyy');

          this.nome_teste = res.rows.item(i).nome;


         

          var myDateString = new Date(res.rows.item(i).nascimento);
          var milisecondsDiff = Date.now() - (myDateString.getTime());
          let idade=Math.floor(( milisecondsDiff )/(1000 * 3600 * 24)/365);
    
         this.panturrilhas2.push({id_panturrilha:res.rows.item(i).id_panturrilha,nome:res.rows.item(i).nome,data_exame:res.rows.item(i).data_exame,circunferencia:res.rows.item(i).circunferencia,idade:idade})

        }



      })
      .catch(e => console.log(e));

    }

   ).catch(e => console.log(e));

  }



   editPanturrilha(id_panturrilha) {
     this.navCtrl.push(ResultadoPanturrilhaPage, {
      id_panturrilha:id_panturrilha
     });
   }

   deletePanturrilha2(id_panturrilha) {
    this.sqlite.create({
      name: 'idoso.db',
      location: 'default'
    }).then((db: SQLiteObject) => {
      db.executeSql('DELETE FROM teste_panturrilha WHERE id_panturrilha=?', [id_panturrilha])
      .then(res => { this.navCtrl.push(ListPanturrilhaPage)
        console.log(res);        
        
      })
      .catch(e => console.log(e));
    }).catch(e => console.log(e));
  }

  deletePanturrilha(id_panturrilha) {
    let alert = this.alertCtrl.create({
      title: 'Tem certeza que quer excluir o resultado?',
      message: '',
      buttons: [
        {
          text: 'Não',
          role: 'cancel',         
          handler: () => {
            //console.log('Cancel clicked');
          }
        },
        {
          text: 'Sim',
          handler: () => {
           // console.log('Buy clicked');
           this.deletePanturrilha2(id_panturrilha)
          }
        }
      ]
    });
    alert.present();
  }

  addPerfil2() {
    this.navCtrl.push(EditDataPage, {
      rowid:1
    });
  }
  
  getUser(rowid) {
    this.sqlite.create({
      name: 'idoso.db',
      location: 'default'
    }).then((db: SQLiteObject) => {
      db.executeSql('SELECT * FROM perfil WHERE rowid=?', [rowid])
        .then(res => {
          if(res.rows.length > 0) {
            this.data.rowid = res.rows.item(0).rowid;
            this.data.nome = res.rows.item(0).nome;
            this.data.sexo = res.rows.item(0).sexo;
            this.data.nascimento = res.rows.item(0).nascimento;
            this.data.imagem = res.rows.item(0).imagem;
            this.photo=res.rows.item(0).imagem;
          }
          if (this.photo=="") {this.photo="assets/imgs/user.png"}
        })
        
      })
    }
 }
