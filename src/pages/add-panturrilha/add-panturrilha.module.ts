import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AddPanturrilhaPage } from './add-panturrilha';

@NgModule({
  declarations: [
    AddPanturrilhaPage,
  ],
  imports: [
    IonicPageModule.forChild(AddPanturrilhaPage),
  ],
})
export class AddPanturrilhaPageModule {}
