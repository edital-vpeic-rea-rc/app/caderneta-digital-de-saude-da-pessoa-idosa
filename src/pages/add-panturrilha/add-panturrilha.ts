import { Component } from '@angular/core';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Toast } from '@ionic-native/toast';
//import { ListPanturrilhaPage } from '../list-panturrilha/list-panturrilha';
import { ResultadoPanturrilhaPage } from '../resultado-panturrilha/resultado-panturrilha';
import { EditDataPage } from '../edit-data/edit-data';
import { Validators, FormBuilder, FormGroup, AbstractControl } from '@angular/forms';
//import {Validators, FormBuilder, FormGroup } from '@angular/forms';
//import { PerfilProvider} from '../provider/perfil/perfil'
import { AlertController } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-add-panturrilha',
  templateUrl: 'add-panturrilha.html',
})
export class AddPanturrilhaPage {
  data = { rowid:1, nascimento:"", sexo:"", nome:"", imagem:""};
  photo: string = "assets/imgs/user.png";
  panturrilha = { data_exame:"", recomendacao:"", id_perfil:"", circunferencia:"",id_perfil2:"" };
  perfils: any = [];
  pan: AbstractControl;
  formgroup:FormGroup;


  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    private sqlite: SQLite,
    private toast: Toast,
    public alertCtrl: AlertController,
    public formbuilder:FormBuilder
  ) {
    this.formgroup = formbuilder.group({
      //nome: ['',Validators.required,Validators.minLength(5)]
     pan: ['', Validators.compose([Validators.maxLength(7), Validators.minLength(2),Validators.min(15),Validators.max(99),Validators.pattern('[^-]+'), Validators.required])]  
    });

    this.pan = this.formgroup.controls['pan'];
   
    
  }

    ionViewDidLoad() {
      this.getPerfil();
      this.getUser(1);
    }

    infoPanturrilha() {
      const alert = this.alertCtrl.create({
        title: 'Perímetro da panturrilha',
        message: '<div class="message">Essa é uma medida de triagem indicada para avaliação da massa muscular, cuja redução correlaciona-se com risco de desenvolver a síndrome chamada sarcopenia e desfechos desfavoráveis. sarcopenia é uma síndrome clínica caracterizada pela perda progressiva e generalizada da massa, força e desempenho muscular, com risco de desfechos adversos como incapacidade física, baixa qualidade de vida e óbito. É diagnosticada através da densidade corporal total ou inferida através de medidas indiretas.</div>'
         + '<br><br><div class="referencia"><strong>Fonte:</strong><br>Brasil. Ministério da Saúde. Secretaria de Atenção à Saúde. Departamento de Ações Programáticas Estratégicas.<br><br>Manual para utilização da Caderneta de Saúde da Pessoa Idosa / Ministério da Saúde, Secretaria de Atenção à Saúde, Departamento de Ações Programáticas Estratégicas. – Brasília : Ministério da Saúde, 2018.<br>96 p. : il. <br><br>(ISBN 978-85-334-2613-9)</div>',
        cssClass: 'referencias',
        buttons: ['OK']
      });
      alert.present();
    }

  savePanturrilha() {
        this.sqlite.create({
      name: 'idoso.db',
      location: 'default'
    }).then((db: SQLiteObject) => {
      db.executeSql('INSERT INTO teste_panturrilha VALUES(NULL,?,?,?,?)',[1,this.panturrilha.circunferencia,this.panturrilha.recomendacao,Date.now()])
        .then(res => { this.navCtrl.push(ResultadoPanturrilhaPage);
          console.log(res);
          this.toast.show('Avaliação salva', '5000', 'center').subscribe(
            toast => {
              //this.navCtrl.popToRoot();
          
            }
          );
        })
        .catch(e => {
          console.log(e);
          this.toast.show(e, '0', 'center').subscribe(
            toast => {
              console.log(toast);
            }
          );
        });
    }).catch(e => {
      console.log(e);
      this.toast.show(e, '0', 'center').subscribe(
        toast => {
          console.log(toast);
        }
      );
    });
  }

  getPerfil() {
    this.sqlite.create({
      name: 'idoso.db',
      location: 'default'
    }).then((db: SQLiteObject) => {

      db.executeSql('SELECT rowid, nome FROM perfil ORDER BY nome DESC', [])
      .then(res => {
        this.perfils = [];
        for(var i=0; i<res.rows.length; i++) {
          this.perfils.push({rowid:res.rows.item(i).rowid,nascimento:res.rows.item(i).nascimento,nome:res.rows.item(i).nome,sexo:res.rows.item(i).sexo})
        }
      })
      .catch(e => console.log(e));
    }).catch(e => console.log(e));
  }


  addPerfil2() {
    this.navCtrl.push(EditDataPage, {
      rowid:1
    });
  }

  getUser(rowid) {
    this.sqlite.create({
      name: 'idoso.db',
      location: 'default'
    }).then((db: SQLiteObject) => {
      db.executeSql('SELECT * FROM perfil WHERE rowid=?', [rowid])
        .then(res => {
          if(res.rows.length > 0) {
            this.data.rowid = res.rows.item(0).rowid;
            this.data.nome = res.rows.item(0).nome;
            this.data.sexo = res.rows.item(0).sexo;
            this.data.nascimento = res.rows.item(0).nascimento;
            this.data.imagem = res.rows.item(0).imagem;
            this.photo=res.rows.item(0).imagem;
          }
          if (this.photo=="") {this.photo="assets/imgs/user.png"}
        })
        
      })
    }
}
