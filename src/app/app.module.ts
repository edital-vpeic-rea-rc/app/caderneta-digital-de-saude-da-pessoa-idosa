import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
import { ChartsModule } from 'ng2-charts';
import { DatePipe } from '@angular/common';
import { Camera } from '@ionic-native/camera';
import { CallNumber } from '@ionic-native/call-number';


import { ContactPage } from '../pages/contact/contact';
import { HomePage } from '../pages/home/home';
import { InicialPage } from '../pages/inicial/inicial';
import { SobrePage } from '../pages/sobre/sobre';
import { TabsPage } from '../pages/tabs/tabs';
import { AddDataPage } from '../pages/add-data/add-data';
import { EditDataPage } from '../pages/edit-data/edit-data';
import { AvaliacaoPage } from '../pages/avaliacao/avaliacao';
import { AddAvaliacaoPage } from '../pages/add-avaliacao/add-avaliacao';

import { DireitosPage } from '../pages/direitos/direitos';
import { DireitosSecundariaPage } from '../pages/direitos-secundaria/direitos-secundaria';
import { DireitosInternoPage } from '../pages/direitos-interno/direitos-interno';

import { Orientacoes2SecundariaPage } from '../pages/orientacoes2-secundaria/orientacoes2-secundaria';
import { Orientacoes2Page } from '../pages/orientacoes2/orientacoes2';

import { TelefonesPage } from '../pages/telefones/telefones';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { SQLite } from '@ionic-native/sqlite';
import { Toast } from '@ionic-native/toast';

//import { HttpClientModule } from '@angular/common/http';
import { HttpModule } from '@angular/http';
import { DireitosProvider } from '../providers/direitos/direitos';
import { AddPanturrilhaPage } from '../pages/add-panturrilha/add-panturrilha';
import { ListPanturrilhaPage } from '../pages/list-panturrilha/list-panturrilha';
import { ResultadoPanturrilhaPage } from '../pages/resultado-panturrilha/resultado-panturrilha';
import { AddImcPage } from '../pages/add-imc/add-imc';
import { ListImcPage } from '../pages/list-imc/list-imc';
import { ResultadoImcPage } from '../pages/resultado-imc/resultado-imc';
import { AddVes13Page } from '../pages/add-ves13/add-ves13';
import { ListVes13Page } from '../pages/list-ves13/list-ves13';
import { ResultadoVes13Page } from '../pages/resultado-ves13/resultado-ves13';


@NgModule({
  declarations: [
    MyApp,

   
    ContactPage,
    HomePage,
    SobrePage,
    InicialPage,
    TabsPage,
    AddDataPage,
    EditDataPage,
    AvaliacaoPage,
    AddAvaliacaoPage,
    
    DireitosPage,
    DireitosInternoPage,
    DireitosSecundariaPage, 
    Orientacoes2Page,
    Orientacoes2SecundariaPage,
    TelefonesPage,
    AddPanturrilhaPage,
    AddImcPage,
    AddVes13Page,
    ListPanturrilhaPage,
    ListImcPage,
    ListVes13Page,
    ResultadoImcPage,
    ResultadoVes13Page,
    ResultadoPanturrilhaPage,
    SobrePage
  
  ],

  
  imports: [
    BrowserModule,
    HttpModule,
    ChartsModule,
  //  HttpClientModule,
    
    IonicModule.forRoot(MyApp, {
      navExitApp: false,
    }),
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,

 
    ContactPage,
    HomePage,
    TabsPage,
    AddDataPage,
    EditDataPage,
    AvaliacaoPage,
    AddAvaliacaoPage,
  
    DireitosPage,
    DireitosInternoPage,
    DireitosSecundariaPage,
    Orientacoes2Page,
    Orientacoes2SecundariaPage,
    TelefonesPage,
    AddPanturrilhaPage,
    AddImcPage,
    AddVes13Page,
    ListPanturrilhaPage,
    ListImcPage,
    ListVes13Page,
    ResultadoImcPage,
    ResultadoVes13Page,
    ResultadoPanturrilhaPage,
    SobrePage,
    InicialPage

  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    SQLite,
    Toast,
  
   DireitosProvider,
   DatePipe,
   Camera,
   CallNumber

    
  ]
})
export class AppModule {}
