# Atenção ao Idoso
> O aplicativo “Atenção ao Idoso” foi baseado em seções Caderneta de Saúde da Pessoa Idosa elaborada pelo Ministério da Saúde em parceria com o LIS/Icict/Fiocruz. A caderneta original é um instrumento de registro e acompanhamento de informações sobre saúde para identificação de vulnerabilidades e para fornecimento de orientações para o autocuidado da pessoa idosa. Este aplicativo é uma adaptação baseada nesse conteúdo e tem o objetivo de expandir o uso desses instrumentos, além de manter a continuidade dos registros e facilitar a portabilidade dos dados.


>O "Atenção ao Idoso" foi elaborado por meio de uma parceria entre o Laboratório de Informação em Saúde (LIS) e o Centro de Tecnologia da Informação e Comunicação em Saúde (CTIC), ambos do Instituto de Comunicação e Informação Científica e Tecnológica em Saúde da Fundação Oswaldo Cruz (Icict/Fiocruz).


## Framework 

Ionic 3


## Instalação do ionic (caso não esteja instalado)

OS X & Linux:

```sh
sudo npm install -g ionic cordova
```

Windows:

```sh
sudo npm install -g ionic cordova
```

## Baixando os Pacotes

Na primeira vez após abaixar os arquivos, precisará instalar os pacotes do projeto, com o comando:

```sh
npm install
```

_Para mais exemplos, consulte a [Wiki][wiki]._ 

## Para gerar o aplicativo android

Rode os seguintes comandos para gerar o aplicativo para android

```sh
ionic cordova platform add android
ionic build android
```

## Para gerar o aplicativo IOS

Rode os seguintes comandos para gerar o aplicativo para o IOS

```sh
ionic cordova platform add ios
ionic cordova build ios
```

## Histórico de lançamentos

* 1.0.0
    * Versão Final para lançamento


## Meta

Marcelo Rabaco – [@marcelo.rabaco] – marcelo.rabaco@icict.fiocruz.br


[https://gitlab.com/marcelo.rabaco](https://gitlab.com/marcelo.rabaco)

